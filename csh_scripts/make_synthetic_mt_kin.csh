######################################
# Set things up
######################################

set reference_volume = $1
set reference_voxel_size = $2
set reference_bin_factor = $3
set reference_radius = $4
set decorate = $5

if(-e $reference_volume) then
    exit(0)
endif

set reference_pdb = $reference_volume:r.pdb

######################################
# Determine spider script parameters
######################################

set mt_diameter = `echo $filament_outer_radius | awk '{print $1*2}'`  # MT diameter in Angstroms, generous (~175 is good for kin*MT)

# We now oversample when doing projections, so I set bin_factor to 1 in the following:
# set vol_dim = `echo $reference_voxel_size $helical_repeat_distance $reference_bin_factor $mt_diameter | awk -f $chuff_dir/awk_scripts/get_good_reference_dimension.awk`

set vol_dim = `echo $reference_voxel_size $helical_repeat_distance 1 $mt_diameter | awk -f $chuff_dir/awk_scripts/get_good_reference_dimension.awk`

# The y-dimension returned is not useful here, just use the same number for x and y
set vol_dim = $vol_dim[1]

if($vol_dim < 0) then
    echo 'ERROR: programming incomplete.  Failed to find good volume dimensions...'
    exit(2)
endif

cd $orig_dir

/bin/cp $chuff_dir/data/tub_only.pdb $reference_pdb
if($decorate != 0) then
    if($decorate == 1) then
        cat $chuff_dir/data/kinesin_docked.pdb >>! $reference_pdb
    else
        if(-e $decorate) then
            cat $decorate >>! $reference_pdb
        else
            echo 'ERROR: pdb file "'$decorate'" not found...'
            exit(2)
        endif
    endif
endif

if($status != 0) then
    echo 'ERROR: could create the model pdb file "'$reference_pdb'"...'
    exit(2)
endif

/bin/ls $reference_pdb:r'_pf'??.pdb >& /dev/null
if($status == 0) then
    /bin/rm $reference_pdb:r'_pf'??.pdb
endif

awk -f $chuff_dir/awk_scripts/pdb2mt_general.awk -v pdb2dxtal=1 -v num_pfs=13 -v helix_start_num=3 -v d_repeat=$helical_repeat_distance -v r13pf=$reference_radius $reference_pdb |& grep -i err
cat  $reference_pdb:r'_pf'??.pdb | awk '$1 != "END" {print}' > $reference_pdb  # WTF?  awk fails with cryptic error with the "cat" pipe,
                                                                               #       on oxford only.....
echo 'END' >> $reference_pdb

/bin/ls $reference_pdb:r'_pf'??.pdb >& /dev/null
if($status == 0) then
    /bin/rm $reference_pdb:r'_pf'??.pdb
endif

cd $chumps_dir/spider

/bin/cp $chuff_dir/spider_scripts/make_synthetic_mt_kin.spi{,.spif} .

$spif_prog make_synthetic_mt_kin.spi \
          ../../$reference_pdb \
          ../../$reference_pdb:r \
          $reference_voxel_size \
          $helical_repeat_distance \
          $vol_dim
if($status != 0) then
    echo 'ERROR: failed to extend the pdb coordinates...'
    exit(2)
endif

if($status != 0) then
    exit(2)
endif

cd $orig_dir
