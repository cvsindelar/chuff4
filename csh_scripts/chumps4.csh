foreach mt($*)
    foreach pf(13 14 12 11)
#    foreach pf(13 14)
	chumps_ref_align $mt num_pfs=$pf
	chumps_select_seam $mt
	chumps_ref_align $mt num_pfs=$pf final=1
	chumps_select_seam $mt
	chumps_ref_align $mt num_pfs=$pf final=2
	chumps_select_seam $mt
    end

    if(-e chumps_round1/$mt:r:t/selected_mt_type.txt) then
	set num_pfs = `cat chumps_round1/$mt:r:t/selected_mt_type.txt`
	if($#num_pfs > 1) then
	    set num_pfs = $num_pfs[1]
	    chumps_final_diagnostic $mt num_pfs=$num_pfs
	endif
    endif

    set micrograph_name = `echo $mt:r | awk '{sub("_MT[0-9]*_[0-9]*$", ""); print};'`
    set micrograph_name = ${micrograph_name}.spi

    /bin/rm scans/$micrograph_name
end

