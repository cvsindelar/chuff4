#! /bin/csh -f

#############################################
# Control script to run SPARX's IHRSR function
#
# Necessary preparations:
#
#  1. Prepare stack with vertically oriented filament segments
#
#  2. Convert stack to hdf format with EMAN's proc2d
#
#  3. Add essential header info to stack using "fix_sparx_header.py":
#     /home/cvs2/chuff3/python_scripts/fix_sparx_header.py stack_bin4.hdf 5.48
#
#  4. Convert initial volume to hdf format with EMAN's proc3d
#
#  5. Then, modify this script as necessary and run using qsub, i.e.:
#       qsub -q sindelar -d $cwd -V -lnodes=4:ppn=16 sxihrsr.csh
#############################################

set EMAN2DIR = /home/cvs2/programs/EMAN2

set n_procs = 64

#############################################
# Specify input and output data:
#############################################

set job_name = sxihrsr1

set stack = sxihrsr_stack_bin3.hdf
# set ini_vol = volf0006.hdf
# set ini_vol = actomyosin_synthetic_050A.spi
set ini_vol = actomyosin150.spi

set pixel_size = 5.11                    # Pixel size (Angstroms)

set twist_per_subunit = 166.715          # Initial guess for helical symmetry (degrees)
set helical_repeat_distance = 27.6       # Initial guess for helical symmetry (Angstroms))

set filament_outer_radius = 220          # Outer radius of filament (Angstroms; for masking)

#############################################
# The following parameters can be modified to control the refinement
#############################################

set n_sub_iterations = 3                 # No. of iterations for every value of phi_step, etc.

set start_symm_refine = 5                # Which iteration to start refining symmetry parameters

set max_in_plane = 12                    # Max. in-plane rotation (degrees)

set max_tilt =     15                    # Max. out of plane tilt (degrees)
set delta_theta =  2.5                   # Angular step for out of plane search (degrees)

set pad_factor = 2                       # Pad factor for FFT's, etc.

#####################
# The following refinement parameters are given in list form, and all lists
#   MUST have the same length!
#
# SPARX will perform n_sub_interations of refinement for each list value, before
#   moving on to the next one.
#####################

set phi_step =    '10  4  1.5'           # Angular step for axial angle search

set xrange =      '40  4  2'             # Range of x-shifts to search (in pixels)
set x_step =      '2   1  0.5'            # Step size of x-shift search (in pixels)
set y_step =      '2   1  0.5'            # Step size of y-shift search (in pixels)

#############################################################
# No need to change below this point
#############################################################

#####################
# Convert above values to SPARX conventions in Angstroms, weird ynumber value, etc
#####################

set xr = (`echo $xrange | awk -v psize=$pixel_size '{for(i=1;i<=NF;++i) print $i*psize}'`)
set txs = (`echo $x_step | awk -v psize=$pixel_size '{for(i=1;i<=NF;++i) print $i*psize}'`)

set initial_theta = `echo $max_tilt | awk '{print 90 - $1}'`

####################
# To convert y_step to ynumber: From SPARX wiki:
#
# "By selecting a proper even ynumber, we can make tys approach txs
# tys = dp( Angstroms) /Pixel_size/ynumber"
# 
#  -> ynumber = dp/Pixel_size/tys; ynumber = 2*ceil(ynumber/2)
####################

set ynumber = (`echo $y_step | \
  awk -v dp=$helical_repeat_distance -v psize=$pixel_size \
      '{for(i=1;i<=NF;++i) {\
          ynumber=dp/psize/$i; \
          print 2*int(ynumber/2+0.999999) \
        }}'`)

#############################################################
# Now run SPARX
#############################################################

source $EMAN2DIR/eman2.cshrc
set sxihrsr_script = $EMAN2DIR/bin/sxihrsr.py

echo $EMAN2DIR/bin/mpirun -np "${n_procs}" "${sxihrsr_script}" \
  "${stack}" "${ini_vol}" "${job_name}" \
  --apix="${pixel_size}" \
  --dp="${helical_repeat_distance}" --dphi="${twist_per_subunit}" \
  --xr="${xr}" \
  --txs="${txs}" \
  --ynumber="${ynumber}" \
  --delta="${phi_step}" \
  --initial_theta="${initial_theta}" --delta_theta="${delta_theta}" \
  --maxit="${n_sub_iterations}" --nise="${start_symm_refine}" --psi_max="${max_in_plane}" \
  --rmax="${filament_outer_radius}" --npad="${pad_factor}" --datasym=symdoc.txt --MPI \
  --function="[.,nofunc,apply_mask]"

$EMAN2DIR/bin/mpirun -np "${n_procs}" "${sxihrsr_script}" \
  "${stack}" "${ini_vol}" "${job_name}" \
  --apix="${pixel_size}" \
  --dp="${helical_repeat_distance}" --dphi="${twist_per_subunit}" \
  --xr="${xr}" \
  --txs="${txs}" \
  --ynumber="${ynumber}" \
  --delta="${phi_step}" \
  --initial_theta="${initial_theta}" --delta_theta="${delta_theta}" \
  --maxit="${n_sub_iterations}" --nise="${start_symm_refine}" --psi_max="${max_in_plane}" \
  --rmax="${filament_outer_radius}" --npad="${pad_factor}" --datasym=symdoc.txt --MPI \
  --function="[.,nofunc,apply_mask]"
