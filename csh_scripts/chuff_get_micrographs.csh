######################################
# Obtains a list of micrographs from the scans directory,
#  or from the user input, and returns this in the shell
#  variable $micrographs
######################################

if($?chuff_debug) then
    echo 'Entering accessory script: chuff_get_micrographs.csh ...'
endif

######################################
# Find the data directories: "$job_dirs"
######################################

if($#script_args < 1) then
    if(! -d scans) then
        echo 'ERROR: there is no scans/ directory...'
        exit(2);
    endif

    set micrographs = `/bin/ls -1 scans/*mrc | grep '....[.]mrc$' | grep -v align`

    if($#micrographs == 0) then
	set micrographs = `/bin/ls -1 scans/*mrc | grep '*[.]mrc$' | grep -v align`
    endif

    if($#micrographs == 0) then
	set micrographs = `/bin/ls -1 scans/*mrc | grep '*[.]mrc$'`
    endif

    if($#micrographs == 0) then
	echo 'ERROR: no micrographs found: "scans/*.mrc"'
	exit(2)
    endif
#    set micrographs = scans/*.mrc
else
    set micrographs = ()
    set temp_list = ($script_args)

    foreach micrograph($temp_list)
        /bin/ls $micrograph >& /dev/null
        if($status == 0) then
	    set micrographs = ($micrographs $micrograph)
	else
	    /bin/ls scans/$micrograph:t >& /dev/null
	    if($status != 0) then
		set error_terminate
		break
	    endif
	    set micrographs = ($micrographs scans/$micrograph:t)
	endif
    end
endif

if($?error_terminate) then
    echo 'ERROR: file "'$micrograph'" not found...'
    echo '   (is this an option, mispelled?)'
    exit(2)
endif

if($#micrographs < 1) then
    echo 'ERROR: no micrographs found...'
    exit(2)
endif

if($?chuff_debug) then
    echo 'Successfully completed accessory script: chuff_get_micrographs.csh .'
endif

