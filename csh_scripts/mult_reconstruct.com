#!/bin/csh -f
#
#   Control script to submit multiple jobs on a cluster using
#   the Sun Grid Engine, or on a multi-core machine.
#
set working_directory	= `pwd`
set SCRATCH		= `grep scratch_dir mparameters | awk '{print $2}'`
if ( $status || $SCRATCH == "" ) then
  set SCRATCH		= ${working_directory}/scratch
endif
if ( ! -d $SCRATCH ) then
  mkdir $SCRATCH
endif

cp mparameters $SCRATCH/mparameters_run

set first		= ${1}
set last		= ${2}
set start		= ${3}
set data_input		= `grep data_input $SCRATCH/mparameters_run | awk '{print $2}'`
set nclass		= `grep nclasses $SCRATCH/mparameters_run | awk '{print $2}'`
set npar = `ls ${data_input}_${start}_r*.par | wc -l`
if ( $nclass > $npar ) set nclass = $npar
set bin_dir		= `grep frealign_bin_dir $SCRATCH/mparameters_run | awk '{print $2}'`
if ( $status || $bin_dir == "" ) then
  set bin_dir		= `which frealign_v9.exe`
  set bin_dir		= ${bin_dir:h}
endif
set cluster_type	= `grep cluster_type $SCRATCH/mparameters_run | awk '{print $2}'`
set night_queue		= `grep night_queue $SCRATCH/mparameters_run | awk '{print $2}'`
set stn			= `grep qsub_string_rec mparameters | awk -F\" '{print $2}'`
set no_delete           = `grep delete_scratch $SCRATCH/mparameters_run | awk '{print $2}'`
if ( $no_delete == "F" ) then
  set no_delete = 1
else
  set no_delete = 0
endif

set raw_images		= `grep raw_images_low $SCRATCH/mparameters_run | awk '{print $2}'`
if ( $raw_images == "" ) set raw_images = `grep raw_images $SCRATCH/mparameters_run | awk '{print $2}'`
set raw_images = `echo ${raw_images:r}`
set extension = `ls $raw_images.* | head -1`
if ( $extension == "" ) then
  set raw_images = ${working_directory}/${raw_images}
  set extension = `ls $raw_images.* | head -1`
endif
set extension = `echo ${extension:e}`

set nx = `${bin_dir}/fheader.exe ${raw_images}.${extension} | grep --binary-files=text NX | awk '{print $4}'`
set mem_small = `echo $nx | awk '{print int(10 * $1^3 * 4 * 3 /1024^3 + 1)/10}'`  
set mem_small = `echo $mem_small | awk '{if ($1 < 1) {print 1} else {print $1} }'`

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "LSF") then
 set mem_small = `echo $mem_small | awk '{print int($1 * 1024)}'`
endif

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SGE") then
  qstat >& /dev/null
  if ($status) then
#    echo "No Sun Grid Engine available. Switching to non-SGE mode." >> frealign.log
    set cluster_type = "none"
  endif
endif

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "LSF") then
  bjobs >& /dev/null
  if ($status) then
#    echo "No LSF system available. Switching to non-LSF mode." >> frealign.log
    set cluster_type = "none"
  endif
endif

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then
  squeue -u $USER >& /dev/null
  if ($status) then
#    echo "No SLURM system available. Switching to non-SLURM mode." >> frealign.log
    set cluster_type = "none"
  endif
endif

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "PBS") then
  qstat >& /dev/null
  if ($status) then
#    echo "No PBS system available. Switching to non-PBS mode." >> frealign.log
    set cluster_type = "none"
  endif
endif

mainloop:

cp mparameters $SCRATCH/mparameters_run
set nproc		= `grep nprocessor_rec $SCRATCH/mparameters_run | awk '{print $2}'`
set sym			= `grep Symmetry $SCRATCH/mparameters_run | awk '{print $2}'`

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then
  set nproc = `echo ${nproc} | awk '{print int($1/16+0.5)*16}'`
endif
@ incr = $last + 1 - $first
set incr = `echo ${incr} ${nclass} ${nproc} | awk '{print int($1*$2/$3+1)}'`
# set incr = `expr ${incr} \* ${nclass} / ${nproc}`
# @ incr++

reconstruct:
# echo "Calculating 3D structure...." >> frealign.log

set h = `date +%k`
set nq = ""
if ($h > 18) set nq = "-l night=true"
if ($h < 6) set nq = "-l night=true"
if ($night_queue != "T" ) set nq = ""

\rm $SCRATCH/pid_temp.log >& /dev/null
if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then
  echo \#\!\/bin\/csh -f > $SCRATCH/slurm.com
endif
set npart = 0
set firstn = $first
@ lastn = $first + $incr - 1
if ( $lastn >= $last ) set lastn = $last

while ( $lastn <= $last )
  echo "Cycle "$start": reconstructing particles $firstn to $lastn on "`date` >> frealign.log

  grep kill $SCRATCH/pid.log >& /dev/null
    if ( ! $status ) exit
  endif

  @ npart++

  set nc = 1
  while ( $nc <= $nclass )
    \rm $SCRATCH/${data_input}_mult_reconstruct_r${nc}_n${firstn}.log >& /dev/null
    \rm $SCRATCH/${data_input}_${start}_r${nc}.shft_* >& /dev/null
    \rm $SCRATCH/${data_input}_mult_refine_n_r${nc}.log_* >& /dev/null
    if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SGE") then
      if ( $sym == "H" || $sym == "HP" ) then
        set pid = `qsub $stn -l mem_free=${mem_small}G $nq -cwd -r y -V -terse -e $SCRATCH/stderr -o $SCRATCH/stderr -N r${nc}_${firstn} ${bin_dir}/mult_hreconstruct_n.com $firstn $lastn $start $nc`
      else
        set pid = `qsub $stn -l mem_free=${mem_small}G $nq -cwd -r y -V -terse -e $SCRATCH/stderr -o $SCRATCH/stderr -N r${nc}_${firstn} ${bin_dir}/mult_reconstruct_n.com $firstn $lastn $start $nc`
      endif
    else if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "LSF") then 
      if ( $sym == "H" || $sym == "HP" ) then
        set pid = `bsub -W 12:00 -q short -R "select[model!=Opteron2216]" -R "rusage[mem=${mem_small}]" -cwd  "$working_directory" -r -e $SCRATCH/stderr -o $SCRATCH/stderr ${bin_dir}/mult_hreconstruct_n.com $firstn $lastn $start $nc | awk -F\< '{print $2}' | awk -F\> '{print $1}'`
      else
        set pid = `bsub -W 12:00 -q short -R "select[model!=Opteron2216]" -R "rusage[mem=${mem_small}]" -cwd  "$working_directory" -r -e $SCRATCH/stderr -o $SCRATCH/stderr ${bin_dir}/mult_reconstruct_n.com $firstn $lastn $start $nc | awk -F\< '{print $2}' | awk -F\> '{print $1}'`
      endif
    else if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then
      if ( $sym == "H" || $sym == "HP" ) then
        echo ${bin_dir}/mult_hreconstruct_n.com $firstn $lastn $start $nc \& >> $SCRATCH/slurm.com
      else
        echo ${bin_dir}/mult_reconstruct_n.com $firstn $lastn $start $nc \& >> $SCRATCH/slurm.com
      endif
    else if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "PBS") then
      echo \#\!\/bin\/csh -f > $SCRATCH/pbs_${firstn}_${nc}.com
      echo cd $working_directory > $SCRATCH/pbs_${firstn}_${nc}.com
      if ( $sym == "H" || $sym == "HP" ) then
        echo ${bin_dir}/mult_hreconstruct_n.com $firstn $lastn $start $nc >> $SCRATCH/pbs_${firstn}_${nc}.com
      else
        echo ${bin_dir}/mult_reconstruct_n.com $firstn $lastn $start $nc >> $SCRATCH/pbs_${firstn}_${nc}.com
      endif
      set pid = `qsub -V $stn -l walltime=100:00:00,nodes=1:ppn=1 -o $SCRATCH -e $SCRATCH $SCRATCH/pbs_${firstn}_${nc}.com`
      set pid = `echo $pid | awk -F. '{print $1}'`
    else
      if ( $sym == "H" || $sym == "HP" ) then
        ${bin_dir}/mult_hreconstruct_n.com $firstn $lastn $start $nc &
      else
        ${bin_dir}/mult_reconstruct_n.com $firstn $lastn $start $nc &
      endif
      set pid = $!
    endif

    if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` != "SLURM") then
      echo $pid ${data_input}_mult_reconstruct_r${nc}_n${firstn}.log >> $SCRATCH/pid_temp.log
    endif
    @ nc++
  end

#  sleep 2

  if ( $lastn == $last ) then
    goto submission_done
  endif
  @ firstn = $firstn + $incr
  @ lastn = $lastn + $incr
  if ( $firstn >= $last ) set firstn = $last
  if ( $lastn >= $last ) set lastn = $last
end

submission_done:

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then          
  chmod +x $SCRATCH/slurm.com
  set incs = `grep com $SCRATCH/slurm.com | wc -l`
  set nnode = `echo $incs | awk '{print int(($1+15)/16)}'`
  set pid = `sbatch  -D $working_directory -J recon -N $nnode -n $incs -p normal -o $SCRATCH/frealign.o%j -t 12:00:00 ${bin_dir}/launcher.slurm`
  set pid = `echo $pid | awk '{ print $NF }'`
  echo $pid ${data_input}_mult_reconstruct_${first}_${last}.log >> $SCRATCH/pid_temp.log
endif

sleep 10

grep kill $SCRATCH/pid.log >& /dev/null
  if ( ! $status ) exit
endif

cat $SCRATCH/pid_temp.log >> $SCRATCH/pid.log

set nc = 1
while ( $nc <= $nclass )

  grep kill $SCRATCH/pid.log >& /dev/null
    if ( ! $status ) exit
  endif

  set firstn = $first
  @ lastn = $first + $incr - 1
  if ( $lastn >= $last ) set lastn = $last

checkdone_rec:

  sleep 5
  while ( $firstn <= $last )

    grep --binary-files=text finished $SCRATCH/${data_input}_mult_reconstruct_r${nc}_n${firstn}.log >& /dev/null

    if ($status) goto checkdone_rec

    echo "Cycle "$start": reconstruction for particles $firstn to $lastn, ref $nc, finished "`date` >> frealign.log
    if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` != "SLURM") then
      set pid = `grep "${data_input}_mult_reconstruct_r${nc}_n${firstn}.log" $SCRATCH/pid.log | awk '{print $1}'`
      cp $SCRATCH/pid.log $SCRATCH/pid_temp.log
      grep -v $pid $SCRATCH/pid_temp.log > $SCRATCH/pid.log
    endif

    @ firstn = $firstn + $incr
    @ lastn = $lastn + $incr
    if ( $lastn >= $last ) set lastn = $last
  end

  @ nc++

end

grep kill $SCRATCH/pid.log >& /dev/null
  if ( ! $status ) exit
endif

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then
  set pid = `grep "${data_input}_mult_reconstruct_${first}_${last}" $SCRATCH/pid.log | awk '{print $1}'`
  cp $SCRATCH/pid.log $SCRATCH/pid_temp.log
  grep -v $pid $SCRATCH/pid_temp.log > $SCRATCH/pid.log
endif

set h = `date +%k`
set nq = ""
if ($h > 18) set nq = "-l night=true"
if ($h < 6) set nq = "-l night=true"
if ($night_queue != "T" ) set nq = ""

\rm $SCRATCH/pid_temp.log >& /dev/null
set nc = 1
while ( $nc <= $nclass )

  grep kill $SCRATCH/pid.log >& /dev/null
    if ( ! $status ) exit
  endif

  echo "Cycle "$start": merging 3D dump files for ref $nc "`date` >> frealign.log
  echo $npart > $SCRATCH/merge_3d_r${nc}.in
  set firstn = $first
  @ lastn = $first + $incr - 1
  while ( $firstn <= $last )

    echo $SCRATCH/${data_input}_${start}_r${nc}_n${firstn}.${extension} >> $SCRATCH/merge_3d_r${nc}.in

    @ firstn = $firstn + $incr
    @ lastn = $lastn + $incr
    if ( $lastn >= $last ) set lastn = $last

  end

  echo $SCRATCH/${data_input}_${start}_r${nc}.res >> $SCRATCH/merge_3d_r${nc}.in
  echo $SCRATCH/${data_input}_${start}_r${nc}.${extension} >> $SCRATCH/merge_3d_r${nc}.in
  echo $SCRATCH/${data_input}_${start}_r${nc}_weights >> $SCRATCH/merge_3d_r${nc}.in
  echo $SCRATCH/${data_input}_${start}_r${nc}_map1.${extension} >> $SCRATCH/merge_3d_r${nc}.in
  echo $SCRATCH/${data_input}_${start}_r${nc}_map2.${extension} >> $SCRATCH/merge_3d_r${nc}.in
  echo $SCRATCH/${data_input}_${start}_r${nc}_phasediffs >> $SCRATCH/merge_3d_r${nc}.in
  echo $SCRATCH/${data_input}_${start}_r${nc}_pointspread >> $SCRATCH/merge_3d_r${nc}.in

  cp $SCRATCH/${data_input}_${start}_r${nc}_n${first}.res $SCRATCH/${data_input}_${start}_r${nc}.res

  if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SGE") then
    set pid = `qsub $stn -l mem_free=${mem_small}G $nq -cwd -r y -V -terse -e $SCRATCH/stderr -o $SCRATCH/stderr -N m${nc} ${bin_dir}/merge_3d.com ${nc}`
  else if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "LSF") then 
    set pid = `bsub -W 12:00 -q short -R "select[model!=Opteron2216]" -R "rusage[mem=${mem_small}]" -cwd  "$working_directory" -r -e $SCRATCH/stderr -o $SCRATCH/stderr ${bin_dir}/merge_3d.com ${nc} | awk -F\< '{print $2}' | awk -F\> '{print $1}'`
  else if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then 
    echo \#\!\/bin\/csh -f > $SCRATCH/slurm.com
    echo ${bin_dir}/merge_3d.com ${nc} \& >> $SCRATCH/slurm.com
  else if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "PBS") then
    echo \#\!\/bin\/csh -f > $SCRATCH/pbs_${nc}.com
    echo cd $working_directory > $SCRATCH/pbs_${nc}.com
    echo ${bin_dir}/merge_3d.com ${nc} >> $SCRATCH/pbs_${nc}.com
    set pid = `qsub -V $stn -l walltime=100:00:00,nodes=1:ppn=1 -o $SCRATCH -e $SCRATCH $SCRATCH/pbs_${nc}.com`
    set pid = `echo $pid | awk -F. '{print $1}'`
  else
    ${bin_dir}/merge_3d.com ${nc} &
    set pid = $!
  endif

  if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` != "SLURM") then
    echo $pid merge_3d_r${nc}.log >> $SCRATCH/pid_temp.log
  endif
  @ nc++

end

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then
  chmod +x $SCRATCH/slurm.com
  set incs = `grep com $SCRATCH/slurm.com | wc -l`
  set nnode = `echo $incs | awk '{print int(($1+15)/16)}'`
  set pid = `sbatch  -D $working_directory -J merge -N $nnode -n $incs -p normal -o $SCRATCH/frealign.o%j -t 12:00:00 ${bin_dir}/launcher.slurm`
  set pid = `echo $pid | awk '{ print $NF }'`
  echo $pid merge_3d.log >> $SCRATCH/pid_temp.log
endif

sleep 10

grep kill $SCRATCH/pid.log >& /dev/null
  if ( ! $status ) exit
endif

cat $SCRATCH/pid_temp.log >> $SCRATCH/pid.log

set nc = 1
while ( $nc <= $nclass )
  
checkdonem:

  grep kill $SCRATCH/pid.log >& /dev/null
    if ( ! $status ) exit
  endif

  sleep 2

  grep --binary-files=text "merge_3d.com finished" $SCRATCH/merge_3d_r${nc}.log >& /dev/null
  if ($status) goto checkdonem

  ls $SCRATCH/${data_input}_${start}_r${nc}.${extension} >& /dev/null
  if ($status) goto checkdonem

  ls $SCRATCH/${data_input}_${start}_r${nc}.res >& /dev/null
  if ($status) goto checkdonem

  if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` != "SLURM") then
    set pid = `grep "merge_3d_r${nc}.log" $SCRATCH/pid.log | awk '{print $1}'`
    cp $SCRATCH/pid.log $SCRATCH/pid_temp.log
    grep -v $pid $SCRATCH/pid_temp.log > $SCRATCH/pid.log
  endif

  sleep 1

  echo "Cycle "$start": merging for ref $nc finished "`date` >> frealign.log
  cat $SCRATCH/${data_input}_${start}_r${nc}.res >> ${working_directory}/${data_input}_${start}_r${nc}.par
  mv $SCRATCH/${data_input}_${start}_r${nc}.${extension} ${working_directory}/${data_input}_${start}_r${nc}.${extension}

##############
# CVS 10/2014: For the Imagic format, there are volume files that need to be saved:
##############
  set form = `${bin_dir}/fheader.exe ${raw_images}.${extension} | grep --binary-files=text Opening | awk '{print $2}'`

  if ( $form == "IMAGIC" ) mv $SCRATCH/${data_input}_${start}_r${nc}.img ${working_directory}/${data_input}_${start}_r${nc}.img
##############
# End of modification (CVS)
##############

  if ( ! $no_delete ) then
    \rm $SCRATCH/${data_input}_${start}_r${nc}.res
    \rm $SCRATCH/${data_input}_${start}_r${nc}.par
  endif

  @ nc++

end

grep kill $SCRATCH/pid.log >& /dev/null
  if ( ! $status ) exit
endif

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then
  set pid = `grep "merge_3d" $SCRATCH/pid.log | awk '{print $1}'`
  cp $SCRATCH/pid.log $SCRATCH/pid_temp.log
  grep -v $pid $SCRATCH/pid_temp.log > $SCRATCH/pid.log
endif

# Clean up

@ prev = $start - 1

if ( ! $no_delete ) then
  set nc = 1
  while ( $nc <= $nclass )

    set firstn = $first
    @ lastn = $first + $incr - 1
    while ( $firstn <= $last )

      \rm $SCRATCH/${data_input}_${start}_r${nc}_n${firstn}.${extension}
      \rm $SCRATCH/${data_input}_${start}_r${nc}_n${firstn}.res

      @ firstn = $firstn + $incr
      @ lastn = $lastn + $incr
      if ( $lastn >= $last ) set lastn = $last

    end

    \rm $SCRATCH/${data_input}_${prev}_r${nc}_map1.${extension} >& /dev/null
    \rm $SCRATCH/${data_input}_${prev}_r${nc}_map2.${extension} >& /dev/null
    \rm $SCRATCH/${data_input}_${prev}_r${nc}_weights >& /dev/null
    @ nc++

  end
endif

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "SLURM") then
  \rm $SCRATCH/slurm.com
endif

if (`echo ${cluster_type} | tr '[a-z]' '[A-Z]'` == "PBS") then
  \rm $SCRATCH/pbs_*.com*
endif

echo "Cycle "$start": reconstruction done." >> frealign.log

date
