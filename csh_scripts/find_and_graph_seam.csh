# Plot microtubule data as a function of box position...

if($?find_seam) then
    set min_good_boxes=3         # minimum number of boxes whose seams align; "1" means accept all mts
    set cutoff_ratio=1         # ratio of best peak to second best peak; "1" means no cutoff
    set cutoff_frac=0.2        # minimum ratio of "good" to "bad" boxes
else
    set min_good_boxes=1         # minimum number of boxes whose seams align; "1" means accept all mts
    set cutoff_ratio=1         # ratio of best peak to second best peak; "1" means no cutoff
    set cutoff_frac=0.000001   # minimum ratio of "good" to "bad" boxes
endif

set mt_plot_file = $job_dir:t/$job_dir:t'_mt_plot.txt'

##################
# Extract FORTRAN-style info from the reference alignment file
#  AND subtract 90 from the 8th data column (theta, the out of plane tilt + 90)
##################

cd $orig_dir
cd $chumps_dir

awk '$2 >= 8 {line_info[$1] = ""; line = $0; split(line, args); box_num = args[1]; for(i = 0; 9+13*i < length(line); ++i) {val = substr(line,9+13*i,13) + 0;  if(i == 5) {val = val - 90}; line_info[$1] = line_info[$1] " "val;} if($1 > num_boxes) num_boxes = $1} END {for(i = 1; i <= num_boxes; ++i) {n = split(line_info[i], vals); printf "%3d", i; for(j = 1; j <= n; ++j) printf("%8.3f ", vals[j]); printf "\n";}}' $job_dir:t/ref_align_doc.spi > $mt_plot_file

##################
# Select the good boxes
#   (first remove the old mtplot_good.txt file)
#   NB: if the MT is rejected, the awk script does NOT generate a mtplot_good.txt file
##################
awk -v min_good_boxes=$min_good_boxes -v cutoff_ratio=$cutoff_ratio -v cutoff_frac=$cutoff_frac -f $chuff_dir/awk_scripts/select_good_mts_pf13.awk $mt_plot_file

##################
# Do the plotting
##################
if(-X gnuplot) then
    if(-e $mt_plot_file:r'_good.txt') then
#	echo 'set y2tics; set terminal png; set size 1.1,1.3; set y2tics; set output "'$mt_plot_file:r'.png"; plot "'$mt_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp, "'$mt_plot_file:r'_good.txt" using 1:8 title "_" with points pt 4 ps 5,"'$mt_plot_file:r'_good.txt" using 1:8 title "good_boxes" with points pt 3 ps 5;' | gnuplot
	echo 'set y2tics; set terminal png; set y2tics; set output "'$mt_plot_file:r'.png"; plot "'$mt_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp, "'$mt_plot_file:r'_good.txt" using 1:8 title "_" with points pt 4 ps 5,"'$mt_plot_file:r'_good.txt" using 1:8 title "good_boxes" with points pt 3 ps 5;' | gnuplot
    else
	if(-e $mt_plot_file) then
#	    echo 'set y2tics; set terminal png; set size 1.1,1.3; set y2tics; set output "'$mt_plot_file:r'.png"; plot "'$mt_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp;' | gnuplot
	    echo 'set y2tics; set terminal png; set y2tics; set output "'$mt_plot_file:r'.png"; plot "'$mt_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp;' | gnuplot
	endif
    endif

    if(-x `which convert` && -e $mt_plot_file:r'.png') then
	convert $mt_plot_file:r{'.png','.jpg'}
    endif
else
    echo '    Warning: program "gnuplot" not found... skipping the plotting step...'
endif

##################
# Guess the seam position
##################

if(-e $mt_plot_file:r'_good.txt') then
    set seam_orient=`awk 'NR == 1 {print $6, $7, $8}' $mt_plot_file:r'_good.txt'` 

    awk -v seam_psi=$seam_orient[1] -v seam_theta=$seam_orient[2] -v seam_phi=$seam_orient[3] -f $chuff_dir/awk_scripts/guess_seam_pf13.awk $job_dir:t/ref_align_doc.spi > $job_dir:t/temp.spi

    awk -f $chuff_dir/awk_scripts/smooth_seam_pf13.awk $job_dir:t/temp.spi > $job_dir:t/guess_seam_doc.spi
    /bin/rm $job_dir:t/temp.spi

    if($status == 2) then
        exit(2)
    endif
endif
