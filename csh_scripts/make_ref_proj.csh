######################################
# Set things up
######################################

set ref_base = $chumps_dir/ref
if(! -d $ref_base) then
    mkdir $ref_base
endif
cd $ref_base

set reference_volume = $1
set reference_voxel_size = $2
set reference_bin_factor = $3

if(! -e ../../$reference_volume) then
  echo 'ERROR: reference volume '$reference_volume' does not exist...'
  exit(2)
endif

if($#argv > 3) then
    set d_angle = $4
else
    set d_angle = 1
endif

unset reference_complete

if(-e ref_angle.spi) then
    set last_image_no = `/usr/bin/tail -1 ref_angle.spi | awk '{printf "%06d\n", $1}'`
    if(-e ref_${last_image_no}.spi) then
        set reference_complete
    endif
endif

if($?reference_complete) then
     exit(0)
endif 

ln -s ../../$reference_volume reference_volume.spi

######################################
# Determine if input volume needs rescaling
######################################
# set reference_bin_factor = `echo nothing | awk -v helical_repeat_distance=$helical_repeat_distance -v reference_voxel_size=$reference_voxel_size -v pixels_per_repeat=$pixels_per_repeat -v bin_factor=$bin_factor '{reference_bin_factor = bin_factor / (pixels_per_repeat/(helical_repeat_distance/reference_voxel_size)); if(abs(reference_bin_factor - int(reference_bin_factor)) < 0.001) print int(reference_bin_factor); else print -1; } function abs(x) {if(x < 0) return(-x); else return(x);}'`
# if($reference_bin_factor < 0) then
######################################
# Rescale volume
######################################
#     echo 'Need to rescale volume, but not implemented yet!'
#     echo Voxel size of volume: $reference_voxel_size Angstroms
#     echo Required voxel size:  `echo $helical_repeat_distance $pixels_per_repeat | awk '{print $1/$2}'` Angstroms
#     exit(2)
# endif

######################################
# Determine spider script parameters
######################################

set mt_diameter = `echo $filament_outer_radius | awk '{print $1*2}'`  # MT diameter in Angstroms, generous (~175 is good for kin*MT)
set min_phi = 0
set max_phi = 359.99
set min_theta = 75
set max_theta = 105
set d_angle = $d_angle

set voxel_size = `echo $chumps_pixels_per_repeat $helical_repeat_distance | awk '{print $2/$1}'`

set proj_dim = `echo $reference_voxel_size $helical_repeat_distance $reference_bin_factor $mt_diameter | awk -f $chuff_dir/awk_scripts/get_good_reference_dimension.awk`

if($proj_dim[1] < 0) then
    echo 'ERROR: programming incomplete.  Need to fix the repeat distance.'
    exit(2)
endif

echo Projecting images...
echo '  Check '$chumps_dir/ref/ref'_xxxxxx.spi for progress'

$spif_prog ../spider/make_ref_proj_pf13.spi \
    ref \
    reference_volume.spi \
    $reference_voxel_size $helical_repeat_distance $reference_bin_factor \
    $mt_diameter \
    $proj_dim[1] $proj_dim[2] \
    $min_phi $max_phi $min_theta $max_theta $d_angle \
    |& awk '$1 == "Projecting" || $1 ~ "Check" {print; fflush();} tolower($0) ~ "error" || tolower($0) ~ "warn" {print; exit(2)}'

if($status != 0) then
    exit(2)
endif

cd $orig_dir
