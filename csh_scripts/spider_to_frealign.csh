
set magnification = 60000
set spider_file = 'test_doc.spi'
set frealign_file = 'test.par'

echo 'nothing' | awk -v spider_file=$spider_file -v magnification=$magnification \
                     -f spider_to_frealign.awk >! $frealign_file

