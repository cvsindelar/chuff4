# Plot microtubule data as a function of box position...

set mt_final_plot_file = $job_dir:t/$job_dir:t'_finalplot.txt'

##################
# Extract FORTRAN-style info from the reference alignment file
#  AND subtract 90 from the 8th data column (theta, the axial tilt + 90)
##################

cd $orig_dir
cd $chumps_dir

if(! -e $job_dir:t/final_align_doc.spi) then
    exit(0)
endif

awk '$2 >= 8 {line_info[$1] = ""; line = $0; split(line, args); box_num = args[1]; for(i = 0; 9+13*i < length(line); ++i) {val = substr(line,9+13*i,13) + 0;  if(i == 5) {val = val - 90}; line_info[$1] = line_info[$1] " "val;} if($1 > num_boxes) num_boxes = $1} END {for(i = 1; i <= num_boxes; ++i) {n = split(line_info[i], vals); printf "%3d", i; for(j = 1; j <= n; ++j) printf("%8.3f ", vals[j]); printf "\n";}}' $job_dir:t/final_align_doc.spi > $mt_final_plot_file

##################
# Select the good boxes
#   NB: if the MT is rejected, the awk script does NOT generate a finalplot_good.txt file
##################

awk -f $chuff_dir/awk_scripts/select_final_mts_pf13.awk $mt_final_plot_file

##################
# Do the plotting
##################
if(-X gnuplot) then
    if(! -e $mt_final_plot_file:r'_good.txt') then
#	echo 'set y2tics; set terminal png; set size 1.1,1.3; set y2tics; set output "'$mt_final_plot_file:r'.png"; plot "'$mt_final_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_final_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_final_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp;' | gnuplot
	echo 'set y2tics; set terminal png; set y2tics; set output "'$mt_final_plot_file:r'.png"; plot "'$mt_final_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_final_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_final_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp;' | gnuplot
    else
#	echo 'set y2tics; set terminal png; set size 1.1,1.3; set y2tics; set output "'$mt_final_plot_file:r'.png"; plot "'$mt_final_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_final_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_final_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp, "'$mt_final_plot_file:r'_good.txt" using 1:8 title "_" with points pt 4 ps 5,"'$mt_final_plot_file:r'_good.txt" using 1:8 title "good_boxes" with points pt 3 ps 5;' | gnuplot
	echo 'set y2tics; set terminal png; set y2tics; set output "'$mt_final_plot_file:r'.png"; plot "'$mt_final_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_final_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_final_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp, "'$mt_final_plot_file:r'_good.txt" using 1:8 title "_" with points pt 4 ps 5,"'$mt_final_plot_file:r'_good.txt" using 1:8 title "good_boxes" with points pt 3 ps 5;' | gnuplot
    endif

    if(-X convert && -e $mt_final_plot_file:r'.png') then
	convert $mt_final_plot_file:r{'.png','.jpg'}
    endif
else
    echo '    Warning: program "gnuplot" not found... skipping the plotting step...'
endif
