#! /bin/csh

#########################
# end of 'Master script' header
#########################

if($#argv != 5 && $#argv != 3) then
    echo 'Usage: histogram.csh <#bins> <col. #> <data file> [<min. val.> <max. val.>]'
    echo '   NOTES: '
    echo '     -> output data file is in "histograph/<data_file [no extension]>_graph.txt"'
    echo '     -> gnuplot commands are in "histograph/<data_file [no extension]>_graph.gnu"'
    echo '     -> lines in the data file beginning with "#" are ignored'
    echo '     -> awk may choke if #bins > 1000'
    exit(2)
endif

set num_bins = $1
set col_num = $2
set data_file = $3

if($data_file:t == $data_file) then
#    set output_dir = histograph
    set output_dir = .
else
#    set output_dir = $data_file:h/histograph
    set output_dir = $data_file:h
endif

# if(! -d $output_dir) mkdir $output_dir

if($#argv == 3) then
    set bounds = (`awk -v col=$col_num '$col + 0 == $col && substr($1,1,1) != "#" {if(min == "") min = $col; if(max == "") max = $col; if($col < min) min = $col; if($col > max) max = $col} END {print min-0.00001, max}' $data_file`)
    if($#bounds != 2) then
        echo 'ERROR: could not determine bounds of the data...'
        exit(2)
    endif

    set min_val = $bounds[1]
    set max_val = $bounds[2]
else
    set min_val = $4
    set max_val = $5
endif

echo $min_val $max_val

set output_file = $output_dir/$data_file:t:r'_histograph.txt'

set gnu_file = $output_file:r'.gnu'
if(-e $gnu_file) /bin/rm $gnu_file

set bin_width = `echo $min_val $max_val $num_bins | awk '{print ($2-$1)/$3}'`

awk -f $chuff_dir/awk_scripts/histograph.awk '\'$min_val '\'$max_val $num_bins $col_num $output_file $data_file

echo 'set term post' > $gnu_file
echo 'set out '"'"$output_file:r.ps"'" >> $gnu_file
echo 'set boxwidth '$bin_width >> $gnu_file
echo plot "'"$output_file"'" with boxes notitle >> $gnu_file
# echo pause -1  >> $gnu_file
echo exit  >> $gnu_file

gnuplot $gnu_file
convert -rotate 90 $output_file:r.ps $output_file:r.jpg
exit(0)

###########################################################################################
# "histograph.awk"
###########################################################################################

Unwrap_Awk:

# Note: in creating the file below, the shell converts all instances of 
#  "\$" to "$", and all instances of "\\" to "\";
#  thus "\\\\\\\\" becomes "\\\\", for example

cat >! $script_dir/histograph.awk <<EOF-awk
# Bin data for a histogram
# Modifying Tara Murphy's code
# usage: for data values between 0 and 10, with 5 bins
# gawk -f histbin.awk 0 10 5 data.out data.in

BEGIN { 
  m = ARGV[1]      # minimum value of x axis
  gsub("[\\\\\\\\]", "", m);
  ARGV[1] = ""
  M = ARGV[2]      # maximum value of x axis
  gsub("[\\\\\\\\]", "", M);
  ARGV[2] = ""
  b = ARGV[3]      # number of bins
  ARGV[3] = ""
  col = ARGV[4]    # column with data
  ARGV[4] = ""
  file = ARGV[5]   # output file for plotting
  ARGV[5] = ""

  w = (M-m)/b      # width of each bin
  
  print "number of bins = "b
  print "width of bins  = "w 
  print

  # set up arrays
  for (i = 0 ; i <= b; ++i) {
    n[i] = m+(i*w)        # upper bound of bin
    c[i] = n[i] - (w/2)   # centre of bin
    f[i] = 0              # frequency count
  }

  n[b] = M;                # Fix rounding errors

  c[0] = "# -infinity";    # These bins count everything less than the min bin value
  n[b+1] = "# +infinity";    #  so there is no effective bin center
  c[b+1] = "# +infinity";
  f[b+1] = 0;
}

substr(\$1, 1, 1) != "#" { 
  # bins the data
  for (i = 0; i <= b; ++i) {
    if (\$col <= n[i]) {
         ++f[i]
         break        
    }
  }
  if(\$col > n[b])
    ++f[b+1];
}

END {
  # print results to screen
  # and to a file for plotting
#  print "# bin(centre), freq"
  print "# bin(centre), freq" > file

  for (i = 0; i <= b+1; ++i) {
    if (f[i] > 0) {
#      print ""c[i]"", f[i]
      print c[i], f[i] >> file
    }
    else {
#      print ""c[i]"", 0
      print c[i], 0 >> file
    }
  }
}
EOF-awk

goto ReturnUnwrap_Awk

