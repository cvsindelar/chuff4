# Plot microtubule data as a function of box position...

set mt_individual_plot_file = $job_dir:t/$job_dir:t'_individualplot.txt'
set mt_final_plot_file = $job_dir:t/$job_dir:t'_finalplot.txt'

set repeats_per_box = 7

##################
# Extract FORTRAN-style info from the document file;
#  AND subtract 90 from the 8th data column (theta, the axial tilt + 90)
##################

cd $orig_dir
cd $chumps_dir

# Require that this MT was processed
if(! -e $job_dir:t/final_individual_doc.spi) then
    exit(0)
endif

# Require that this MT was previously identified as "good"
if(! -e $mt_final_plot_file:r'_good.txt' && ! $?force_select) then
    if(! -e ../chumps_round1/$mt_final_plot_file:r'_good.txt') then
	exit(0)
    else
	set good_box_nums = `awk '{print $1}' ../chumps_round1/$mt_final_plot_file:r'_good.txt'`
    endif
else
    set good_box_nums = `awk '{print $1}' $mt_final_plot_file:r'_good.txt'`
endif


awk -v repeats_per_box=$repeats_per_box -v good_box_nums="$good_box_nums" \
    '$2 == 8 { line_info[$1] = ""; \
	       line = $0; split(line, args); box_num = args[1]; \
	       num_good = split(good_box_nums, good_boxes); \
	       printout = 0; \
	       for(i = 1; i <= num_good; ++i) { \
		 if($1/repeats_per_box > good_boxes[i] - 1 && $1/repeats_per_box < good_boxes[i] +1)  { \
		   printout = 1; \
		 } \
	       } \
	       if(printout)  { \
	         good[$1] = 1; \
	         for(i = 0; 9+13*i < length(line); ++i) { \
		   val = substr(line,9+13*i,13) + 0; \
		   if(i == 5) {val = val - 90}; \
                   line_info[$1] = line_info[$1] " "val; \
	         }  \
	       } \
	       num_boxes = $1} \
      END {for(i = 1; i <= num_boxes; ++i) if(good[i]) {print i" "line_info[i]}}' \
   $job_dir:t/final_individual_doc.spi > $mt_individual_plot_file:r'_good.txt'

awk -v repeats_per_box=$repeats_per_box -v good_box_nums="$good_box_nums" \
    '$2 == 8 { line_info[$1] = ""; \
	       line = $0; split(line, args); box_num = args[1]; \
	       num_good = split(good_box_nums, good_boxes); \
	       printout = 1; \
	       if(printout)  { \
	         good[$1] = 1; \
	         for(i = 0; 9+13*i < length(line); ++i) { \
		   val = substr(line,9+13*i,13) + 0; \
		   if(i == 5) {val = val - 90}; \
                   line_info[$1] = line_info[$1] " "val; \
	         }  \
	       } \
	       num_boxes = $1} \
      END {for(i = 1; i <= num_boxes; ++i) if(good[i]) {print i" "line_info[i]}}' \
   $job_dir:t/final_individual_doc.spi > $mt_individual_plot_file

##################
# Do the plotting
##################
if(-X gnuplot) then
    if(! -e $mt_individual_plot_file:r'_good.txt') then
#	echo 'set y2tics; set terminal png; set size 1.1,1.3; set y2tics; set output "'$mt_individual_plot_file:r'.png"; plot "'$mt_individual_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_individual_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_individual_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp;' | gnuplot
	echo 'set y2tics; set terminal png; set y2tics; set output "'$mt_individual_plot_file:r'.png"; plot "'$mt_individual_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_individual_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_individual_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp;' | gnuplot
    else
#	echo 'set y2tics; set terminal png; set size 1.1,1.3; set y2tics; set output "'$mt_individual_plot_file:r'.png"; plot "'$mt_individual_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_individual_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_individual_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp, "'$mt_individual_plot_file:r'_good.txt" using 1:8 title "_" with points pt 4 ps 5,"'$mt_individual_plot_file:r'_good.txt" using 1:8 title "good_boxes" with points pt 3 ps 5;' | gnuplot
	echo 'set y2tics; set terminal png; set y2tics; set output "'$mt_individual_plot_file:r'.png"; plot "'$mt_individual_plot_file'" using 1:8 title "axial_twist" with lines, "'$mt_individual_plot_file'" using 1:2 axes x1y2 title "radon_orient" with linesp, "'$mt_individual_plot_file'" using 1:7 axes x1y2 title "out_plane_tilt" with linesp, "'$mt_individual_plot_file:r'_good.txt" using 1:8 title "_" with points pt 4 ps 5,"'$mt_individual_plot_file:r'_good.txt" using 1:8 title "good_boxes" with points pt 3 ps 5;' | gnuplot
    endif

    if(-X convert && -e $mt_individual_plot_file:r'.png') then
	convert $mt_individual_plot_file:r{'.png','.jpg'}
    endif
else
    echo '    Warning: program "gnuplot" not found... skipping the plotting step...'
endif

