################
# Chuff version 3 - scripts for MT processing (Chuck Sindelar, Yale, spring 2013)
# Please cite the following reference when using these scripts:
# Shang Z., Zhou K., Xu C., Csencsits R., Cochran J. C., and Sindelar C. V (2014). 
#   High-resolution structures of the kinesin-microtubule complex reveal 
#   the basis of nucleotide-gated force generation. eLife 3:e04686.
################

################
# Specific instructions
#   Note: These scripts should work well with most 64-bit linux workstations.
#         OSX and 32-bit linux are also supported, but the user must provide three
#         of the binary executables (batchboxer, proc2d, bimg) - use the corresponding
#         chuff_parameters.txt_OSX or chuff_parameters.txt_linux32
################

1. In your ".cshrc" (or similar) add the line:
   setenv chuff_dir <path-to-chuff3>

   where <path-to-chuff3> is your chuff3 installation, i.e. /home/cvs2/chuff3

2. Within the chuff3 installation, make sure that "chuff_machine_parameters.m"
   specifies the correct operating system type, i.e. "linux", "linux32", or "OSX"

   Note, you should also make sure that your system has the following working commands:
    From the EMAN package:  proc2d, batchboxer
    From the bsoft package: bimg, bint

2. Make a "box" file that follows one or more microtubules in the 
  micrograph.  Normally you would use "boxer" from EMAN, as follows:
  a. click "Helix" option in main program pane
  b. choose a suitable box size (about twice as wide as the microtubule, 
       preferably a "nice" number like 256)
  c. choose an "Olap" distance of about 1/3 the box size (i.e. 80 for box size 256)
  d. click endpoints of microtubules (you can do several of these)
  e. choose "Save Box DB" from the "Boxes" menu, and give the same name as the micrograph
     but with ".box"

3. Make a working directory (preferably NOT in your "chuff3" directory!)

4. Copy "chuff_parameters.txt" from the chuff3 directory into your working directory

5. MODIFY all appropriate entries in "chuff_parameters.txt" to match your system!

6. Add your micrographs to the analysis: 
   add_micrograph_links <mic1.mrc> <mic2.mrc> ...
 
   Note, you get an error here if you try to add too many micrographs at one time.
    If so, break the micrograph list into chunks and run the command several times.

7. Add your box files (one per micrograph) to the analysis:
   add_micrograph_links <mic1.box> <mic2.box> ...

   Note, these box files must be named the same as the micrographs, ***AND***
    be in the same directory!

8. Copy "process_microtubules" from the chuff3 directory into your working directory, 
  and modify to taste

9. Execute the commands in process_microtubules.  I recommend doing these one at a time 
   and checking the results until you are sure all the scripts actually run, 
   and that they are reliably doing the right thing.

#################
Results will be in the "chumps_round1" directory. If all goes well, you will have a series
 of reconstructions in "chumps_round1/frealign_bin2" :
   xxxxxxxx_1.spi, xxxxxxxx_2.spi, xxxxxxxx_3.spi, xxxxxxxx_4.spi

FSC information is in:
   xxxxxxxx_1_fsc.txt, xxxxxxxx_2_fsc.txt, ...

#################
The following files are essential
  diagnostics and should ALWAYS be looked at for each microtubule/micrograph:

#################
   A. ctf_files/xxxx_pw.jpg - results of CTFFIND defocus finding for each micrograph.  
     If these aren't good, you will need to try "ctf" on the problem micrographs,
     using additional arguments.

     Typically you might try changing the resolution range and defocus search ranges, 
       (using "nuke=1" to redo the calculation) as follows:

       "ctf 0001.mrc min_def=10000 max_def=20000 min_res=15 max_res=50 nuke=1"

      For other options, type "ctf help=1".

#################
   B. Within chumps_round1/xxxx_MTx_1/ :

      xxxx_MTx_1_stacked_final.tif  : 
          compressed image of microtubule - should look pretty nice!

      xxxx_MTx_1_straightened_pw.tif
          power spectrum of microtubule - good ones show 10A layer lines

      xxxx_MTx_1_mtplot.txt
      xxxx_MTx_1_finalplot.txt
      xxxx_MTx_1_individualplot.txt
          Alignment parameters at stages 1,2,3 of SPIDER refinement.
          Column 2 is in-plane rotation
          Column 3 is secondary magnification scale factor
          Column 4-5 are x,y-shift
          Columns 6-8 are Euler angles psi, theta, phi

      xxxx_MTx_1_mtplot.{png,jpg}
      xxxx_MTx_1_finalplot.{png,jpg}
      xxxx_MTx_1_individualplot.{png,jpg}
          Graphical versions of the above alignment files (if you have working gnuplot)

          Most importantly, red boxes in these graphs are the image segments selected 
           for further processing.


