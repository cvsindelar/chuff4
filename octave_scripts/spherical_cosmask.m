function m = spherical_cosmask(n,mask_radius, edge_width,origin)
% function m = spherical_cosmask(n,mask_radius, edge_width,origin)

sz = [1 1 1];
sz(1:prod(size(n))) = n;
if(nargin < 4)
  origin=floor(sz/2)+1;  % default center is appropriate for ffts.
end

origin3d = [1 1 1];
origin3d(1:prod(size(origin))) = origin;

[x,y,z]=ndgrid(1-origin3d(1):sz(1)-origin3d(1),1-origin3d(2):sz(2)-origin3d(2), 1-origin3d(3):sz(3)-origin3d(3));
r = sqrt(x.^2 + y.^2 + z.^2);

edgezone = find(r >= mask_radius & r <= mask_radius + edge_width);

m = zeros(sz);
m(edgezone) = 0.5 + 0.5*cos( 2*pi*(r(edgezone) - mask_radius) / (2*edge_width));
m(find(r <= mask_radius)) = 1;

