function micrograph = paste_chumps_box(micrograph,box, box_num,coords,...
                               bin_factor,filter)
  stdout = 1;

  box_dim_bin = size(box);
  mic_dim_full = size(micrograph);

  if(nargin < 4)
    bin_factor = 1;
  end

  bin_shift = bin_factor/2 - 0.5

%%%%%%%%%%%%%%%%%
% NOTE: there is an extra factor of 2 in the line below, which I don't understand.
%  May be incorrect, but required for alignment in tests. Probably related
%  to the fact that the reference images were already centered when they were binned.
%%%%%%%%%%%%%%%%%

  coords = (coords + 2*bin_shift)/bin_factor;

  int_coords = round(coords(box_num,:));
  frac_coords = coords(box_num,:) - int_coords;

  box_origin = int_coords-floor(box_dim_bin/2);

  wi_dx = box_dim_bin(1) - 1;
  wi_dy = box_dim_bin(2) - 1;
  proj_ox = 1;
  proj_oy = 1;
%%%%%%%%%
% Handle clipping if image falls partially or fully outside micrograph
%%%%%%%%%

  border_avg = [];

  leftclip = 0; rightclip = 0; topclip = 0; botclip = 0;

  if(box_origin(1) < 1)
    wi_dx = (box_origin(1) + box_dim_bin(1) - 1) - 1;
    box_origin(1) = 1;
    proj_ox = 1 + box_dim_bin(1) - (wi_dx + 1);
    leftclip = 1;
  end

  if(box_origin(1)+wi_dx > mic_dim_full(1))
    wi_dx = mic_dim_full(1) - box_origin(1);
    rightclip = 1;
  end

  if(box_origin(2) < 1)
    wi_dy = (box_origin(2) + box_dim_bin(2) - 1) - 1;
    box_origin(2) = 1;
    proj_oy = 1 + box_dim_bin(2) - (wi_dy + 1);
    topclip = 1;
  end

  if(box_origin(2)+wi_dy > mic_dim_full(2))
    wi_dy = mic_dim_full(2) - box_origin(2);
    botclip = 1;
  end

  box_shift = real(ifftn(double(fftn(box) .* FourierShift(box_dim_bin(1), frac_coords))));
  box_shift = box_shift(proj_ox:proj_ox+wi_dx,proj_oy:proj_oy+wi_dy);

  if(leftclip == 0)
    border_avg = [border_avg box_shift(1,:)];
  end

  if(rightclip == 0)
    border_avg = [border_avg box_shift(wi_dx + 1,:)];
  end

  if(topclip == 0)
    border_avg = [border_avg box_shift(:,1)'];
  end

  if(botclip == 0)
    border_avg = [border_avg box_shift(:,wi_dy + 1)'];
  end

%%%%%%%%%%%%%%%%%%%%%
% Here, border_avg is used to make sure the edges of the box have an average value of 
%  approximately zero.  This is important when the boxes are modulated by a CTF, which
%  shifts the average value from zero by an amount that depends on the size of the box [!]
%%%%%%%%%%%%%%%%%%%%%
  border_avg = sum(border_avg) / prod(size(border_avg));

  box_shift = box_shift - border_avg;

  micrograph(box_origin(1):box_origin(1)+wi_dx, ...
             box_origin(2):box_origin(2)+wi_dy) = ...
    micrograph(box_origin(1):box_origin(1)+wi_dx, ...
               box_origin(2):box_origin(2)+wi_dy) + ...
             box_shift;
