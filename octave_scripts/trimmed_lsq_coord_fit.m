function [u_params, v_params, outliers] = trimmed_lsq_coord_fit(x, coords, fit_order, trim_spec)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User can specify which points to trim out
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin > 3)
  tempx = x;
  tempx(trim_spec) = [];
  tempu = coords(:,1);
  tempu(trim_spec) = [];
  tempv = coords(:,2);
  tempv(trim_spec) = [];

  if(fit_order == 3)
    a =  [((tempx(:)).^2)'; tempx(:)'; ones([1 prod(size(tempx))])]';
    u_params = (transpose(a)*a) \ (transpose(a)*tempu(:));
    v_params = (transpose(a)*a) \ (transpose(a)*tempv(:));
  else
    a =  [tempx(:)'; ones([1 prod(size(tempx))])]';
    u_params = [0; (transpose(a)*a) \ (transpose(a)*tempu(:))];
    v_params = [0; (transpose(a)*a) \ (transpose(a)*tempv(:))];
  end
  return
end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ...otherwise, omit two points and combinatorially search all pairs of such omitted points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

best_resid_error = -1;
for trim1 = 1:prod(size(x))
for trim2 = trim1+1:prod(size(x))
%for trim3 = trim1+1:prod(size(x))

  tempx = x;
  tempx([trim1 trim2]) = [];
% tempx([trim1 trim2 trim3]) = [];

  tempu = coords(:,1);
  tempu([trim1 trim2]) = [];
%  tempu([trim1 trim2 trim3]) = [];
  tempv = coords(:,2);
  tempv([trim1 trim2]) = [];
%  tempv([trim1 trim2 trim3]) = [];
  
  if(fit_order == 3)
    a =  [((tempx(:)).^2)'; tempx(:)'; ones([1 prod(size(tempx))])]';
    u_params = (transpose(a)*a) \ (transpose(a)*tempu(:));
    v_params = (transpose(a)*a) \ (transpose(a)*tempv(:));
  else
    a =  [tempx(:)'; ones([1 prod(size(tempx))])]';
    u_params = [0; (transpose(a)*a) \ (transpose(a)*tempu(:))];
    v_params = [0; (transpose(a)*a) \ (transpose(a)*tempv(:))];
  end

%  resid_error = sum( (params(1)*tempx(:).^2+params(2)*tempx(:)+params(3) - tempy(:)).^2);
  resid_error = ...
    sum(sum( [u_params(1)*tempx.^2 + u_params(2)*tempx + u_params(3) - tempu';...
                   v_params(1)*tempx.^2 + v_params(2)*tempx + v_params(3) - ...
                      tempv'].^2, 1),2);
  if(best_resid_error == -1 || resid_error < best_resid_error)
    best_resid_error = resid_error;
    best_u_params = u_params;
    best_v_params = v_params;
    best_trim1 = trim1;
    best_trim2 = trim2;
%    best_trim3 = trim3;
  end
% end % trim3
end % trim2
end % trim1

u_params = best_u_params;
v_params = best_v_params;

outliers = zeros([1 prod(size(x))]);
outliers(best_trim1) = 1;
outliers(best_trim2) = 1;
% outliers(best_trim3) = 1;
