function [tot_decorate_map,sorted_map,ref_tubout,ref_kinout] = ...
           process_found_kinesin(job_dir)
                        

%%%%%%%%%%%%%%%%%%
% Get input arguments
%%%%%%%%%%%%%%%%%%

mkdir(sprintf('chumps_round1/%s/find_kinesin/',job_dir));
overlap_file = sprintf('chumps_round1/%s/find_kinesin/overlap_map.spi',job_dir);

tub_ref_file = 'chumps_round1/ref_tub_subunit/ref_tot.spi';
kin_ref_file = 'chumps_round1/ref_kin_subunit/ref_tot.spi';
ref_params_name = 'chumps_round1/ref_tub_subunit/ref_params.spi';
com_doc_name = 'chumps_round1/ref_tub_subunit/tub_cgr_doc.spi';
ctf_doc_name = 'ctf_files/ctf_docfile_for_spider.spi';
mt_info_name = sprintf('chumps_round1/%s/selected_mt_type.txt',job_dir));

mt_info = dlmread(mt_info_name);
num_pfs = mt_info(1);
num_starts = mt_info(2);
num_starts = num_tub_starts/2; % All functions require the "true" number of helical starts
                               %  (can be non-integral for tubulin)
%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m'

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

n_kins_to_find = 2*num_pfs; % 6*num_pfs;
max_overlap = 2*num_pfs;

num_repeats = ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
kin_lib_size = 2*num_repeats*num_pfs;

pixel_size = 10000*scanner_pixel_size/target_magnification;

num_starts = num_tub_starts/2; % All functions require the "true" number of helical starts
                               %  (can be non-integral for tubulin)

elastic_params = [0 0 0 0];
radius_scale_factor = 1;

select_pf = 0; # [num_pfs 1];

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

if(job_dir(prod(size(job_dir))) == '/')
  job_dir=job_dir(1:prod(size(job_dir))-1);
end
[start] = regexp(job_dir,'[^/]*$','start');
job_dir = job_dir(start:prod(size(job_dir)));

[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
micrograph_name = sprintf('scans/%s.mrc',job_dir(1:start-1));

micrograph_number = job_dir(1:start-1);
[start] = regexp(micrograph_number,'[1-9][0-9]*$','start');
micrograph_number = str2num(micrograph_number(start:prod(size(micrograph_number))));

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

ctf_doc = dlmread(ctf_doc_name);

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
box_dim = 64*ceil(box_dim/64);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

%%%%%%%%%%%%%%%%%%%%%%%
% Compute CTF's
%%%%%%%%%%%%%%%%%%%%%%%

index = find(ctf_doc(:,13) == micrograph_number);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));
defocus = ctf_doc(index,3);
astig_mag = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);
b_factor = 200;

electron_lambda = EWavelength(accelerating_voltage);



if(nargin < 4)
  tubrefs = readSPIDERfile(tub_ref_file);
end
if(nargin < 5)
  kinrefs = readSPIDERfile(kin_ref_file);
end

%%%%%%%%%%%%%%%%%%%%%%%
% Finally- the kinesin-finding loop
%%%%%%%%%%%%%%%%%%%%%%%

tot_decorate_map = zeros([num_pfs 2*n_good_boxes]);
sorted_map = zeros([num_pfs 2*n_good_boxes]);

tot_overlap_map = [];

if(fopen(overlap_file) > 0)
  [header, overlap_dim] = readSPIDERheader(overlap_file);
overlap_dim(3)
n_good_boxes
  if(overlap_dim(3) == n_good_boxes)
    tot_overlap_map = readSPIDERfile(overlap_file);
    fprintf(stdout,'Finished reading pre-existing overlap map...\n');
  else
    delete(overlap_file);
  end
end

for box_num=1:n_good_boxes

  fprintf(stdout,'Box %4d of %4d\n', box_num, n_good_boxes);

%%%%%%%%%%%%%%%%%%
% Read in boxed image, after re-centering the coordinates using the found
%  coords
%%%%%%%%%%%%%%%%%%

  int_coords = round(coords(box_num,:));
  frac_coords = coords(box_num,:) - int_coords;

  box_origin = int_coords-floor(box_dim/2);
  [box,mrcinfo,msg] = ReadMRCwin(micrograph_name, box_dim, box_origin);
  if(box == -1)
    box_origin
    continue;
  end

  box = invert_density * real(ifftn(fftn(box).*FourierShift(box_dim(1), -frac_coords)));
  box = BinImageCentered(box, bin_factor);
  std_dev = std(box(:));
  avg = mean(box(:));
  box = (box - avg)/std_dev;

  writeSPIDERfile(sprintf('chumps_round1/%s/find_kinesin/find_%04d_1.spi',job_dir,box_num),box);
sprintf('chumps_round1/%s/find_kinesin/find_%04d_1.spi',job_dir,box_num)

  ref_dim = floor(box_dim/bin_factor);
  pad_ref_dim = 2*ref_dim;

  if(defocus > 0)
    ctf_term = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                       spherical_aberration_constant, ...
                       b_factor, amplitude_contrast_ratio, 0);
  else
    ctf_term = ones(pad_ref_dim(1),pad_ref_dim(1));
  end

%%%%%%%%%%%%%%%%%%
% Generate the reference images, padded 2x to ensure that 
%  generate_micrograph() has room to place all the subunits
%%%%%%%%%%%%%%%%%%
 if(nargin < 7)

  monomer_offset = 0;

  [x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
   x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist mt_repeat_index mt_pf_index] =...
     microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                            est_repeat_distance,radius_scale_factor,monomer_offset,...
                            [phi_est(box_num) d_phi_d_repeat(box_num)],...
                            [theta_est(box_num) d_theta_d_repeat(box_num)],...
                            [psi_est(box_num) d_psi_d_repeat(box_num)],elastic_params);
  ref_tub = generate_micrograph(pad_ref_dim, tubrefs, ref_pixel, min_theta, d_angle, select_pf,...
                            x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                            [0 0]);
  ref_kin = ...
        generate_micrograph(pad_ref_dim, kinrefs, ref_pixel, min_theta, d_angle, select_pf,...
                            x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                            [0 0],1,1);

%%%%%%%%%%%%%%%%%%%%%
% Generate images corresponding to the alternate kinesin binding sites
%  by effectively swapping alpha tubulin for beta tubulin- 
%  we use a monomer-sized axial shift.
%%%%%%%%%%%%%%%%%%%%%

  monomer_offset = 1;
  [x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
   x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist] =...
     microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                            est_repeat_distance,radius_scale_factor,monomer_offset,...
                            [phi_est(box_num) d_phi_d_repeat(box_num)],...
                            [theta_est(box_num) d_theta_d_repeat(box_num)],...
                            [psi_est(box_num) d_psi_d_repeat(box_num)],elastic_params);
  ref_kin2 = ...
        generate_micrograph(pad_ref_dim, kinrefs, ref_pixel, min_theta, d_angle, select_pf,...
                            x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                            [0 0],1,1);

  ref_tub = fftshift(fftn(ref_tub)) .* ctf_term;
  ref_tub = ifftn(ifftshift(ref_tub));
  avg = mean(ref_tub(:));
  ref_tub = (ref_tub - avg);

%%%%%%%%%%%%%%%%%%
% Window out the final-sized reference images, after applying CTF
%%%%%%%%%%%%%%%%%%

  in_x = 1+floor(pad_ref_dim(1)/2)-floor(ref_dim(1)/2);
  in_y = 1+floor(pad_ref_dim(2)/2)-floor(ref_dim(2)/2);

  ref_tub = ref_tub(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);

  ref_kin_final = zeros([ref_dim(1) ref_dim(2) kin_lib_size]);

%%%%%%%%%%%%%%%%%%%%%
% Make the final library with alternating repeats from the normal
%  and offset images, to keep them roughly in order by axial position
%%%%%%%%%%%%%%%%%%%%%

  for i=1:kin_lib_size/2
    k = 2*i-1;
    temp = real(ifftn(ifftshift(fftshift(fftn(ref_kin(:,:,i))) .* ctf_term)));
    ref_kin_final(:,:,k) = temp(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
    kin_repeat_index(k) = 2*mt_repeat_index(i)-1;
    kin_pf_index(k) = mt_pf_index(i);
    k = 2*i;
    temp = real(ifftn(ifftshift(fftshift(fftn(ref_kin2(:,:,i))) .* ctf_term)));
    ref_kin_final(:,:,k) = temp(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
    kin_repeat_index(k) = 2*mt_repeat_index(i);
    kin_pf_index(k) = mt_pf_index(i);
  end

%%%%%%%%%%%%%%%%%%
% Finished generating images
%%%%%%%%%%%%%%%%%%

  ref_kin = ref_kin_final;
  clear ref_kin_final ref_kin2

  ref_tubout = ref_tub;
  ref_kinout = ref_kin;
 end

%%%%%%%%%%%%%%%%%%
% Now do the kinesin finding 
%%%%%%%%%%%%%%%%%%

  writeSPIDERfile(sprintf('chumps_round1/%s/find_kinesin/find_%04d_2.spi',job_dir,box_num),ref_tub);

  [decorate] = combinatorial_cc(box,ref_tub,ref_kin, n_kins_to_find, ...
                                max_overlap,tot_overlap_map, overlap_file,box_num);
  dec_origin = floor(kin_lib_size/2) - floor(n_kins_to_find/2);

  mini_decorate = decorate(dec_origin+1 : dec_origin+2*num_pfs);
  tot_decorate_map( 1+(box_num-1)*2*num_pfs : box_num*2*num_pfs) = ...
     mini_decorate;

  mini_repeat_index = kin_repeat_index(dec_origin+1 : dec_origin+2*num_pfs);
  mini_pf_index = kin_pf_index(dec_origin+1 : dec_origin+2*num_pfs);

  dec_repeat_origin = kin_repeat_index(dec_origin + 1);
  mini_dec_index = find(mini_decorate);

  mini_pf_index = mini_pf_index(mini_dec_index);

  mini_repeat_index = mini_repeat_index(mini_dec_index);
  mini_repeat_index = mini_repeat_index - dec_repeat_origin;
  mini_repeat_index = mini_repeat_index + (box_num-1)*2;

  sorted_repeat_index = mini_repeat_index(find(mini_repeat_index >= 1));
  sorted_pf_index = mini_pf_index(find(mini_repeat_index >= 1));

  sorted_index = num_pfs*(sorted_repeat_index-1)+sorted_pf_index;
  sorted_map(sorted_index) = 1;

%%%%%%%%%%%%%%%%%%
% Re-create the best match
%%%%%%%%%%%%%%%%%%

  final_map = ref_tub;
  final_map2 = ref_tub;
  for index=1:n_kins_to_find
    i = index + dec_origin;
    final_map = final_map + decorate(i)*ref_kin(:,:,i);
    if(i >= dec_origin && i <= dec_origin + n_kins_to_find-1)
      final_map2 = final_map2 + ref_kin(:,:,i);
    end
  end

  writeSPIDERfile(sprintf('chumps_round1/%s/find_kinesin/find_%04d_3.spi',job_dir,box_num),final_map);
  writeSPIDERfile(sprintf('chumps_round1/%s/find_kinesin/find_%04d_4.spi',job_dir,box_num),final_map2);

  writeSPIDERfile(sprintf('chumps_round1/%s/find_kinesin/tot_decorate_map.spi',job_dir),...
                  tot_decorate_map);

  sum(sum(decorate))
  sum(sum(decorate))/n_kins_to_find
  ccc(box,final_map)
  ccc(box,ref_tub)
  ccc(box,final_map2)
end

return

