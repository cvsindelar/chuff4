function m = soft_wedge_mask2d(v_size,num_pfs,subunit_num,offset_angle)

%%%%%%%%%%%%%%%%%%
% Generates a wedge-shaped mask surrounding the i'th subunit of a helical lattice.
% The origin of the helical lattice is defined by ref_com; 
%  subunit_num = 1 gives a wedge centered around ref_com.
%%%%%%%%%%%%%%%%%%

if(nargin < 3)
  offset_angle = 0;
end

integral_twist = 360/num_pfs;
integral_twist_rad = integral_twist*pi/180;

wedge_angle = offset_angle + (subunit_num-1)*integral_twist;
wedge_angle_rad = wedge_angle*pi/180;

[x,y] = ndgrid(-floor(v_size(1))/2:floor(v_size(1))/2-1, ...
                 -floor(v_size(2))/2:floor(v_size(2))/2-1);

theta = atan2(y,x);

theta1 = mod(pi + wedge_angle_rad + theta, 2*pi) - pi;

m = zeros(v_size);

m( find( abs(theta1) <= integral_twist_rad/2)) = 2;
