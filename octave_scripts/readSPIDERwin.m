function data=readSPIDERwin(filename, window_dim, window_origin, pad_out)
% readSPIDERfile : read a file in SPIDER format. It returns: 
%   a 2D matrix if input is a SPIDER image,
%   a 3D matrix if input is a SPIDER volume or a stack of images.
% 
% 'Windowed' version of readSPIDERfile (CVS 1/15)
% 'pad_out' can be used to control what happens if the window is partially or
%   fully outside the file.
%  pad_out = 3 means to fill out the window with a mirror image of
%   the box near the boundary, possibly augmented 'noise' from  a nearby tile within the file)
%  pad_out = 2 means to fill out the window with 'noise' from a nearby tile within the file)
%  pad_out = 1 means to fill out the window with a constant background value, estimated
%   by the median of the truncated edge(s).
%  pad_out = 0 means return whatever clipped window is within the file
%  pad_out = -1 means return -1 if any clipping is detected.
%
% Returned matrix is type double. Does not handle complex formats.
%
% requires readSPIDERheader.m
%
% version 1.0 (Feb 2009) B. Baxter
% Copyright (C) 2009 Health Research Inc.
% Tested in Matlab 7.7.0 (R2008b, R2008a)

if nargin<4
  pad_out = -1;
end;

data = -1;
pix_bytes = 4;

h = readSPIDERheader(filename);
if h == -1
  disp(sprintf('%s does not seem to be a valid SPIDER file\n', filename));
  return
end

nslice    = h(1);
nrow      = h(2);
nsam      = h(12);
nhdrbytes = h(22);  % aka labbyt
isstack   = h(24);
nstackitems = h(26);
if h(29) == 1       %  h(29):  1 = big-endian, 0 = little-endian
    endian = 'ieee-be';
else
    endian = 'ieee-le';
end

displayvals = 0;
if displayvals == 1
    disp(sprintf('nslice: %d', nslice));
    disp(sprintf('nrow: %d', nrow));
    disp(sprintf('nsam: %d', nsam));
    disp(sprintf('nhdrbytes: %d', nhdrbytes));
    disp(sprintf('isstack: %d', isstack));
end

[fp, errmsg] = fopen(filename, 'r', endian);

if fp == -1
  disp(errmsg)
  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% Set default window size : whole image, or square/cube if some dim's
%  not specified.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

if(nargin < 2)
  wi_x = nsam;
  wi_y = nrow;
  wi_z = nslice;
else
  wi_x = window_dim(1);
  if(prod(size(window_dim)) < 2)
    wi_y = wi_x;
  else
    wi_y = window_dim(2);
  end
  if(prod(size(window_dim)) < 3)
    if(nslice == 1)
      wi_z = 1;
    else
      wi_z = wi_x;
    end
  else
    wi_z = window_dim(3);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% Set default window origin (1,1,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

if(nargin < 3)
  wi_ox = 1;
  wi_oy = 1;
  wi_oz = 1;
else
  wi_ox = window_origin(1);
  if(prod(size(window_origin)) < 2)
    wi_oy = 1;
  else
    wi_oy = window_origin(2);
  end
  if(prod(size(window_origin)) < 3)
    wi_oz = 1;
  else
    wi_oz = window_origin(3);
  end
end

[wi_xyz_clip, wi_oxyz_clip, background_oxyz] = ...
  get_clipping([wi_x wi_y wi_z], [wi_ox wi_oy wi_oz], [nsam nrow nslice]);

wi_tx = wi_x; wi_ty = wi_y; wi_tz = wi_z; 
wi_tox = wi_ox; wi_toy = wi_oy; wi_toz = wi_oz; 
wi_x = wi_xyz_clip(1); wi_y = wi_xyz_clip(2); wi_z = wi_xyz_clip(3);
wi_ox = wi_oxyz_clip(1); wi_oy = wi_oxyz_clip(2); wi_oz = wi_oxyz_clip(3);

if(wi_ox+wi_x-1 > nsam || wi_oy+wi_y-1 > nrow || wi_oz+wi_z-1 > nslice || ...
   wi_ox < 1 || wi_oy < 1 || wi_oz < 1)
  map = -1;
  return;
end

% read the header
nwords = nhdrbytes/4;
[hdr, count] = fread(fp, nwords, 'float32');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% SPIDER image file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

if (nslice == 1) && (isstack == 0) 
  if(nargin < 2)
    data = fread(fp, [wi_x,wi_y], 'float32');
  else
    data = zeros([wi_x wi_y], 'single');
    fseek(fp, pix_bytes*(nsam*(wi_oy-1)), 'cof');

    for i=1:wi_y
      fseek(fp, pix_bytes*(wi_ox-1), 'cof');
      data(:,i) = fread(fp, wi_x, 'float32');
      fseek(fp, pix_bytes*(nsam - (wi_ox+wi_x-1)), 'cof');
    end
  end
  fclose(fp);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% SPIDER volume    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

elseif (nslice > 1) && (isstack == 0)

  if(nargin < 2)
    data = fread(fp, wi_x*wi_y*wi_z, 'float32');
    data= reshape(data, [wi_x wi_y wi_z]);
  else
    fseek(fp, pix_bytes*((wi_oz-1)*nsam*nrow), 'cof');
    data = zeros([wi_x wi_y wi_z], 'single');
    for j=1:wi_z
      fseek(fp, pix_bytes*(wi_oy-1)*nsam, 'cof');
      for i=1:wi_y
        fseek(fp, pix_bytes*(wi_ox-1), 'cof');
        data(:,i,j) = fread(fp, wi_x, 'float32');
        fseek(fp, pix_bytes*(nsam - (wi_ox+wi_x-1)), 'cof');
      end
      fseek(fp, pix_bytes*(nrow - (wi_oy+wi_y-1))*nsam, 'cof');
    end
  end
  fclose(fp);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% stack of images    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

elseif (nslice == 1) && (isstack > 0)
    % switch nsam and nrow since we're going to transpose the matrix
%     data = zeros([nrow nsam nstackitems], 'single');
%    data = zeros([nsam nrow nstackitems], 'single');
    data = zeros([wi_x wi_y nstackitems], 'single');
    for v = 1:nstackitems
       hdr = fread(fp, nwords, 'float32');
       if(nargin < 2)
         data(:,:,v) = fread(fp, [nsam,nrow], 'float32');
       else
         fseek(fp, pix_bytes*nsam*(wi_oy-1), 'cof');
         for i=1:wi_y
           fseek(fp, pix_bytes*(wi_ox-1), 'cof');
           data(:,i,v) = fread(fp, wi_x, 'float32');
           fseek(fp, pix_bytes*(nsam - (wi_ox+wi_x-1)), 'cof');
         end
       end
    end
    fclose(fp);
end

%%%%%%%%%%%
% Fill in any part of the box that falls outside the micrograph, with
%  a mirror image of the matching edge segment.
%%%%%%%%%%%

if(!isequal([wi_tx wi_ty wi_tz], wi_xyz_clip(1:3)))
  if(pad_out == 3)
    mirrorpad = 1;
  else
    mirrorpad = 0;
  end
  if(pad_out >= 2 && background_oxyz(1) != -1)
    noise_box = readSPIDERwin(filename, [wi_tx wi_ty wi_tz], background_oxyz, -1);

    data = pad_gently(data, [wi_tx wi_ty wi_tz], ...
                          wi_oxyz_clip, [wi_tox wi_toy wi_toz],...
                          mirrorpad, noise_box);
  elseif(pad_out > 0)  % 
      data = pad_gently(data, [wi_tx wi_ty wi_tz], ...
                            wi_oxyz_clip, [wi_tox wi_toy wi_toz],mirrorpad);
  elseif(pad_out == -1)
    fprintf('WARNING: readSPIDERwin.m: window is not completely within the image file!\n');
    map = -1;
  end
end
