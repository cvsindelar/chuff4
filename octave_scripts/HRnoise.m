function v_HRnoise=HRnoise(v, pixel_size, noise_cut_on)

% First, construct the radius values for defining the shells.
sz = [1 1 1];
sz(1:prod(size(size(v)))) = size(v);
n = max(sz(:));

[x y z] = ndgrid(-floor(sz(1)/2):-floor(sz(1)/2)+sz(1)-1,...
                 -floor(sz(2)/2):-floor(sz(2)/2)+sz(2)-1,...
                 -floor(sz(3)/2):-floor(sz(3)/2)+sz(3)-1);
R = sqrt(x.^2 + y.^2 + z.^2)/(pixel_size*n);

% Fourier-transform the map
f1=fftshift(fftn(v));

zone = find(R >= 1/noise_cut_on);
prod(size(zone));
f1(zone) = abs(f1(zone)) .* exp(i*2*pi*rand([prod(size(zone)) 1]));

v_HRnoise = abs(ifftn(ifftshift(f1)));
