function m = soft_wedge_mask_filament(v_size,subunit_num,num_pfs,num_starts,rise_pix,twist, ...
                              edge_width_pix, outer_radius_pix, ref_com, shifts, eulers, ref_vol)

%%%%%%%%%%%%%%%%%%
% Generates a wedge-shaped mask surrounding the i'th subunit of a helical lattice.
% The origin of the helical lattice is defined by ref_com; 
%  subunit_num = 1 gives a wedge centered around ref_com.
%%%%%%%%%%%%%%%%%%
'calling soft_wedge_mask_filament'
v_size,subunit_num,num_pfs,num_starts,rise_pix,twist, ...
                                  edge_width_pix, outer_radius_pix, ref_com
fudge = 0.000001;
if(nargin < 9)
  ref_com = [0 v_size(2)/4 0];
end

repeat_num = floor((subunit_num-1));

subunit_radius = sqrt(sum(ref_com.^2));

[X,Y,Z] = ndgrid(-floor(v_size(1))/2:floor(v_size(1))/2-1, ...
                 -floor(v_size(2))/2:floor(v_size(2))/2-1, ...
                 -floor(v_size(3))/2:floor(v_size(3))/2-1);

if(nargin >= 10)
  X = X - shifts(1);
  Y = Y - shifts(2);
  Z = Z - shifts(3);
end

if(nargin >= 11)
  rot = EulerMatrix(-[eulers(3) eulers(2) eulers(1)]);

  x=rot(1,1)*X+rot(1,2)*Y+rot(1,3)*Z;
  y=rot(2,1)*X+rot(2,2)*Y+rot(2,3)*Z;
  z=rot(3,1)*X+rot(3,2)*Y+rot(3,3)*Z;
else
  x = X; y = Y; z = Z;
end

X = []; Y = []; Z = [];

if(num_pfs == 1)
  supertwist = twist;
else
  supertwist = twist*num_pfs/num_starts;
  supertwist = supertwist - round(supertwist/360)*360;
end

% num_pfs = round((360 + supertwist)/twist)

subunit_height = num_pfs * rise_pix/num_starts;
%integral_twist = 360/num_pfs;
%subunit_height = rise_pix * num_pfs;
integral_twist = abs(twist);

integral_twist_rad = integral_twist*pi/180;
supertwist_rad = supertwist*pi/180;
%offset_angle_rad = -atan2(ref_com(2), ref_com(1)) + (pf_num-1)*integral_twist*pi/180;
offset_angle_rad = -atan2(ref_com(2), ref_com(1));

edge_width_rad = edge_width_pix/subunit_radius;

theta = atan2(y,x);

if(edge_width_rad > integral_twist_rad)
   edge_width_rad = integral_twist_rad
   edge_width_rad*subunit_radius
end

edge_offset_rad = (integral_twist_rad/2 - edge_width_rad/2);

theta1 = mod(pi + offset_angle_rad + theta, 2*pi) - pi;
theta1 = theta1 + (z - ref_com(3)) / subunit_height * supertwist_rad;
theta1 = theta1 + fudge;              % This keeps away a weird 'fencepost' problem
                                      %  where single rows of pixels escape every wedge.
z_offset = z - ref_com(3) + rise_pix * theta1/integral_twist_rad;
m = zeros(v_size);

if(edge_width_pix ~= 0)
  theta2 = mod(pi + offset_angle_rad + edge_offset_rad + theta, 2*pi) - pi;
  theta2 = theta2 + (z - ref_com(3)) / subunit_height * supertwist_rad;
  theta3 = mod(pi + offset_angle_rad - edge_offset_rad + theta, 2*pi) - pi;
  theta3 = theta3 + (z - ref_com(3)) / subunit_height * supertwist_rad;

  edge = cos( 2*pi*theta2 / (4*edge_width_rad)).^2;
  edge( find(theta2 >= 0 | theta2 <= -edge_width_rad)) = 0;
  m = m + edge;

  edge = 0.5 + 0.5*cos( 2*pi*theta3 / (2*edge_width_rad)).^2;
  edge( find(theta3 <= 0 | theta3 >= edge_width_rad)) = 0;
  m = m + edge;
end
m( find( abs(theta1) <= integral_twist_rad/2 - edge_width_rad/2)) = 1;
%m( find( theta1 >= -(integral_twist_rad/2 - edge_width_rad/2) & ...
%         theta1 <  integral_twist_rad/2 - edge_width_rad/2)) = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG 
%if(sum(ref_vol(find( abs(theta1) <= integral_twist_rad/2 - edge_width_rad/2))(:)) > 0)
%  'yay1'
%temp = m.*ref_vol;
%temp(find(temp)) = theta1(find(temp));
%abs(temp(find(temp != 0))) - (integral_twist_rad/2 - edge_width_rad/2)
%(integral_twist_rad/2 - edge_width_rad/2)*180/pi
%integral_twist_rad/2*180/pi
%edge_width_rad/2*180/pi
%
%writeSPIDERfile('ref_vol2.spi', ref_vol);
%end
% DEBUG 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

z_mask = zeros(v_size);
%z_begin = subunit_height*repeat_num + (pf_num-1)*rise_pix;
z_begin = subunit_height*repeat_num;

if(edge_width_pix ~= 0)
  edge = 0.5 + 0.5*cos( 2*pi*(z_offset - z_begin - ...
                    subunit_height/2 + edge_width_pix/2) / (2*edge_width_pix)).^2;

  edge( find(z_offset - z_begin > subunit_height/2 + edge_width_pix/2 | ...
             z_offset - z_begin < subunit_height/2 - edge_width_pix/2)) = 0;
  z_mask = z_mask + edge;
  
  edge = 0.5 + 0.5*cos( 2*pi*(z_offset - z_begin + ...
                    subunit_height/2 - edge_width_pix/2) / (2*edge_width_pix)).^2;

  edge( find(z_offset - z_begin < -(subunit_height/2 + edge_width_pix/2) | ...
             z_offset - z_begin > -(subunit_height/2 - edge_width_pix/2))) = 0;
  z_mask = z_mask + edge;
end

z_mask(find( abs(z_offset - z_begin) <= subunit_height/2 - edge_width_pix/2)) = 1;
m = m.*z_mask;
r = sqrt(x.^2 + y.^2);
m(find(r > outer_radius_pix)) = 0;
if(edge_width_pix != 0)
  edgezone = find(r >= outer_radius_pix & r <= (outer_radius_pix + edge_width_pix));
  m(edgezone) = m(edgezone).*(0.5 + 0.5*cos( 2*pi*(r(edgezone) - outer_radius_pix) / (2*edge_width_pix)));
end
