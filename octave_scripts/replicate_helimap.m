function [synth_boxes, boxes, ...
          norm_params_vol, norm_params_image,micrograph1] = ...
  replicate_helimap(igold, goldmap, run_dir, job_dir,helimap_round,...
                    num_pfs,num_starts,coords,phi,theta,psi,...
                    d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                    est_repeat_distance_13pf,ref_dim, pad_ref_dim,ref_pixel,ref_com,...
                    use_ctf,focal_mate,chumps_round,...
                    bin_factor, min_resolution, filament_outer_radius, ...
                    tot_helimap, normalize, write_files, outer)
stdout = 1;

global subunit_array debug;

if(nargin < 26)
  tot_helimap = [];
end

if(nargin < 27)
  normalize = 0;
end

if(nargin < 28)
  write_files = 0;
end

if(nargin < 29)
  outer = 1;
end

n_good_boxes = prod(size(phi));

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m';
else
  run 'chuff_parameters';
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification

if(!get_helimap_ref_vols(run_dir, helimap_round))
  printf('ERROR: cannot read reference volumes!\n');
  exit(2)
end

n_states = size(subunit_array);
n_states = n_states(4);

%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG
%%%%%%%%%%%%%%%%%%%%%%%%

if(1 == 1)

%%%%%%%%%%%%%%%
% Get CTF info
%%%%%%%%%%%%%%%
% Note: padding the references improves the behavior of the CTF at image boundaries

  [micrograph_name,defocus,astig_mag,astig_angle] = ...
    chumps_micrograph_info(job_dir,focal_mate,chumps_round);

  b_factor = 0;

  if(use_ctf ~= 0)
    electron_lambda = EWavelength(accelerating_voltage);
    ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  else
    [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
    micrograph_name = sprintf('%s.mrc',job_dir(1:start-1));
    if(micrograph_name(1:6) ~= 'scans/')
      micrograph_name = sprintf('scans/%s',micrograph_name);
    end

    ctf = ones(pad_ref_dim);
  end

  mic_dim = ReadMRCheader(micrograph_name);
  micrograph = ReadMRC(micrograph_name);

  if(bin_factor != 1)
    micrograph = BinImageCentered(micrograph, bin_factor);
  end

  if(isempty(tot_helimap))
    tot_helimap = zeros([num_pfs n_good_boxes n_states]);
    tot_helimap(:,:,1) = 1;
  end

%%%%%%%%%%%%%%%%%%
% Read in decoration map
%%%%%%%%%%%%%%%%%%

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;

  fprintf(stdout,'\n%s\n', job_dir);

  micrograph1 = zeros(floor(mic_dim(1)/bin_factor),floor(mic_dim(2)/bin_factor));
  micrograph2 = zeros(floor(mic_dim(1)/bin_factor),floor(mic_dim(2)/bin_factor));
  micrograph3 = zeros(floor(mic_dim(1)/bin_factor),floor(mic_dim(2)/bin_factor));

  for box_num=1:n_good_boxes
    fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
    if(isoctave)
      fflush(stdout);
    end

    if(box_num > 1 && box_num < n_good_boxes)
      box_num_repeats = 1; % We request the image of a single tubulin repeat, 
                           %  representing a full repeat of subunits beginning at the origin
                           %  and extending in the direction of travel.
                           % Note that requesting a single repeat from get_findkin_refs
                           %  only gets the subunits at one side of the origin,
                           %  with the first subunit located precisely at the origin.
                           % Requesting two repeats would obtain the above repeat plus the
                           %  repeat located on the other side of the origin.
      box_repeat_origin = 1;
    else
%%%%%%%%%%%%%%%%%%%
% For the bounding boxes, extend the microtubule out towards the edges
%%%%%%%%%%%%%%%%%%%
      box_num_repeats = ceil(ref_dim(1)/3*ref_pixel/est_repeat_distance_13pf);
      if(box_num == 1)
        box_repeat_origin = box_num_repeats;
      else
        box_repeat_origin = 1;
      endif
    end

%%%%%%%%%%%%%%%%%%
% NOTE: need to fix code so we can use est_repeat_distance instead of helical_repeat_distance
% -> DONE
%%%%%%%%%%%%%%%%%%
  [box_bounds, tot_bounds, sorted_indices,...
     x_tub, y_tub, z_tub, phi_tub, theta_tub, psi_tub, phi_mt, theta_mt, psi_mt, refs,...
     bkg, refs_nosigma2] = ...
      get_helimap_boxrefs(igold, goldmap, [box_num box_num_repeats box_repeat_origin],...
                   ref_dim,...
                   num_pfs,num_starts,...
                   coords, phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance_13pf,radius_scale_factor,ref_com,...
                   ctf,0.5*ones(size(ctf)),...
                   ref_pixel,bin_factor, tot_helimap);

% writeSPIDERfile(sprintf('wedgerefs_%d.spi',box_num),refs(:,:,:,1));

    lib_size = size(refs);
    lib_size = lib_size(3);
    n_classes = size(refs);
    n_classes = n_classes(4);
    sites_per_repeat = num_pfs;

    [box_bounds, tot_bounds] = ...
      box_to_tot_findmap(zeros(lib_size,1), [1 box_num_repeats], box_repeat_origin, ...
                     tot_helimap(:,:,1), box_num, sites_per_repeat);

    final_map1 = zeros(ref_dim);
    final_map2 = zeros(ref_dim);
    final_map3 = zeros(ref_dim);

    for j=1:n_classes
      if(j == 1)
        class_prob = ones(lib_size,1);
      else
        class_prob = zeros(lib_size,1);
      end

      temp = tot_helimap(:,:,j);

      class_prob(box_bounds(1):box_bounds(2)) = temp(tot_bounds(1):tot_bounds(2));

      for i=1:lib_size
        final_map1 = final_map1 + class_prob(i) * refs(:,:,i,j);
      end
    end

    for i=1:lib_size
      final_map2 = final_map2 + refs(:,:,i,1);
      final_map3 = final_map3 + refs(:,:,i,2);
    end

'pasting...'
fflush(stdout);
    micrograph1 = paste_chumps_box(micrograph1,final_map1, box_num,coords,...
                        bin_factor,ones(size(final_map1)));
    micrograph2 = paste_chumps_box(micrograph2,final_map2, box_num,coords,...
                        bin_factor,ones(size(final_map1)));
    micrograph3 = paste_chumps_box(micrograph3,final_map3, box_num,coords,...
                        bin_factor,ones(size(final_map1)));
'...done'
fflush(stdout);
end % for box_num...

wi_oxy = 1 + floor(pad_ref_dim/2) - floor(ref_dim/2);

boxes = zeros([ref_dim n_good_boxes]);
synth_boxes = zeros([ref_dim n_good_boxes]);

% save -binary debug2.binmat

real_n = 0;
for box_num = 1:n_good_boxes
  if(!box_in_micrograph(coords(box_num,:), bin_factor*pad_ref_dim, mic_dim))
    printf('  Note: box %d not completely within micrograph. \n',box_num);
%    continue
  end
  real_n = real_n+1;

%  temp = rotate2d(copy_chumps_box(micrograph,box_num,coords,rot_box_dim,bin_factor),...
%                  -psi(box_num));
  temp = copy_chumps_box(micrograph,box_num,coords,pad_ref_dim,bin_factor);
  temp = invert_density*temp;
  boxes(:,:,real_n) = temp(wi_oxy(1):wi_oxy(1)+ref_dim(1)-1,...
                                  wi_oxy(2):wi_oxy(2)+ref_dim(2)-1);

%  temp = rotate2d(copy_chumps_box(micrograph1,box_num,coords,rot_box_dim,bin_factor),...
%                  -psi(box_num));
  temp = copy_chumps_box(micrograph1,box_num,coords,pad_ref_dim,bin_factor);
  synth_boxes(:,:,real_n) = temp(wi_oxy:wi_oxy+ref_dim(1)-1,...
                                  wi_oxy:wi_oxy+ref_dim(1)-1);
end

norm_params_vol = zeros(2,n_good_boxes);
norm_params_image = zeros(2,n_good_boxes);

for box_num=1:n_good_boxes
  if(outer)
    helimask = ...
        1-helix_cos_mask_2d(ref_dim, -psi(box_num), filament_outer_radius, ...
                            min_resolution, ref_pixel);
  else
    helimask = ...
      get_helimap_mask_simple(ref_dim, ref_pixel, ...
                              box_num, helical_repeat_distance, coords, psi, min_resolution);
  end

  helimask(find(helimask)) = 1;
  f_mask = sum(helimask(:).^2)/prod(size(helimask));

  sigma_est = std(temp(find(helimask)));

%%%%%%%%%%%%%%%%%%%%%%%
% Note below: synth_boxes would be zero in the solvent region (helimask, for "outer = 1")
%  EXCEPT for the presence of a CTF, which adds non-zero fringes which we subtract out,
%  to get the "best possible" normalization (?). In this formulation, 
%  when we subtract the synthetic image from the experimental one, the mean of the resulting 
%  "noise" is centered at zero in the region defined by helimask.
%
% According to Hemant, it should be fine (in fact, better) to use the solvent region to obtain
%  the sigma2 estimate; otherwise, one obtains exaggerated sigma2's in early cycles of the
%  ML refinement that can converge to false minima... 
% 
% However, it is worth noting here that solvent noise is convolved by the CTF
%  and could have a different mean than the shot noise.
%%%%%%%%%%%%%%%%%%%%%%%

  temp = boxes(:,:,box_num) - synth_boxes(:,:,box_num);

if(debug)
  norm_params_image(:,box_num) = [1 0];
else
  norm_params_image(:,box_num) = ...
                      [1/sigma_est ...
                       -sum(temp(find(helimask))) / prod(size(find(helimask(:)))) ];
end

%  temp1 = synth_boxes(:,:,box_num);
%  temp2 = boxes(:,:,box_num);
%  norm_params_vol(:,box_num) = linear_fit(temp1(find(helimask)),temp2(find(helimask)));
%  norm_params_image(:,box_num) = [1/norm_params_vol(1,box_num) ...
%                                  -norm_params_vol(2,box_num)/norm_params_vol(1,box_num)];
end

if(normalize)
  micrograph1 = micrograph1 * norm_params_vol(1) + norm_params_vol(2);

  for box_num=1:n_good_boxes
    boxes(:,:,box_num) = norm_params_image(1,box_num)*boxes(box_num) + ...
                         norm_params_image(2,box_num);
  end
end

% save -binary 'replicate_helimap_debug.binmat'

%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG
%%%%%%%%%%%%%%%%%%%%%%%%
else
'DEBUG: preloaded replicate_helimap data!'
  load 'replicate_helimap_debug.binmat'
end

% save -binary 'replicate_helimap.binmat' synth_boxes boxes micrograph1 micrograph2 micrograph3

return
