function return_box = apply_sigma2_kludge(box, sigma2, final_dim)

pad_ref_dim = size(box);
wi_x = 1+floor(pad_ref_dim(1)/2)-floor(final_dim(1)/2);
wi_y = 1+floor(pad_ref_dim(2)/2)-floor(final_dim(2)/2);

softmask = spherical_cosmask([pad_ref_dim(1) 1], (pad_ref_dim(1)-final_dim(1)/2)/2, final_dim(1)/5);
softmask = repmat(softmask', [pad_ref_dim(1) 1]) .*...
           repmat(softmask, [1 pad_ref_dim(2)]);

writeSPIDERfile('softmask.spi',softmask);

return_box = box - sum(box(:))/prod(size(box));
return_box = fftn(softmask.*return_box);
return_box(2:prod(size(box))) = return_box(2:prod(size(box)))./sqrt(sigma2(2:prod(size(box))));
return_box = abs(ifftn(return_box));
return_box = return_box(wi_x:wi_x+final_dim(1)-1,wi_y:wi_y+final_dim(2)-1);
return_box = return_box - sum(return_box(:))/prod(size(return_box));

return
