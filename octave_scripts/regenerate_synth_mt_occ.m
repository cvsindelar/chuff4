function regenerate_synth_mt_occ(synth_header,output_file,occ_file)

param_file = sprintf('%s_param.txt',synth_header);
if(nargin < 4)
  occ_file = sprintf('%s_occ.txt',synth_header);
end

load(param_file)
load(occ_file)

if(isoctave)
  run 'chuff_parameters.m';
else
  run 'chuff_parameters';
end

%%%%%%%%%%%%%%%%%%%%%%%
% Generate tubulin coordinates
%%%%%%%%%%%%%%%%%%%%%%%

[x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
 x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist, repeat_index, pf_index] =...
   microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                          helical_repeat_distance,radius_scale_factor,monomer_offset,...
                          phi_params,theta_params,psi_params,elastic_params);

%%%%%%%%%%%%%%%%%%%%%%%
% If we are decorating with kinesin dimers, we now fill in the
%  second head of the kinesin:
%%%%%%%%%%%%%%%%%%%%%%%

tot_decorate_map = occupancy;

if(kin_dimer_pattern ~= 0)
  for i=1:num_repeats
    for j=1:num_pfs
      if(occupancy( (i-1)*num_pfs + j))
        if(i < num_repeats)
          tot_decorate_map(i*num_pfs + j) = 1;
        end
      end
    end
  end
end

sites_per_repeat = num_pfs;
[box_bounds, tot_bounds, sorted_indices] = ...
  map_to_helimap(tot_decorate_map, tot_decorate_map, 1, [1 num_repeats], 1, ...
                 num_pfs, sites_per_repeat, repeat_index, pf_index);

pad_sort_map = 3;
sorted_map = zeros([num_pfs (2*pad_sort_map+num_repeats)]);

sorted_map(sorted_indices) = tot_decorate_map(box_bounds(1):box_bounds(2));
full_sorted_map = zeros([num_pfs 2*(2*pad_sort_map+num_repeats)]);
full_sorted_map(:, 1:2:2*(2*pad_sort_map+num_repeats)) = sorted_map;

writeSPIDERfile(output_file, full_sorted_map);

full_decorate_map = zeros([2*num_pfs num_repeats]);
full_decorate_map(1:2:2*num_pfs*num_repeats) = tot_decorate_map;
writeSPIDERfile('test_tot_decorate_map.spi', full_decorate_map);
return

