%%%%%%%%%%%%%%%%%%%%%%
% Generate overlap map, denotes which kinesins overlap
%%%%%%%%%%%%%%%%%%%%%%

size_ref_kin = size(ref_kin);

overlap_map = zeros(n_kins_to_find,max_overlap);
mini_overlap_map = zeros(n_kins_to_find,max_overlap);

max_overlap_count = -1;
oj = 1 + floor(max_overlap/2);
for i=1:n_kins_to_find
  for j=1:max_overlap
     if(i+j-oj >= 1 && i+j-oj <= size_ref_kin(3))
       overlap_map(i, j) = ccc(ref_kin(:,:,i), ref_kin(:,:,i+j-oj)) > 0.01;
     end
  end
  mini_overlap_map(i,1:prod(size(find(overlap_map(i,:))))) = ...
    find(overlap_map(i,:)) + i - oj;
end

%%%%%%%%%%%%%%%%%%%%%%
% Search for the binding pattern
%%%%%%%%%%%%%%%%%%%%%%
debugio = fopen('debug_find.txt', 'w');

decorate = zeros(n_kins_to_find,1);

for i=1:n_kins_to_find
  fprintf(debugio, '%d\n', i);
  best_ccc = -1;

  max_overlap_count = max(find(mini_overlap_map(i,:) ~= 0));

  mask = ref_kin(:,:,i);
  mask(find(mask ~= 0)) = 1;
%  writeSPIDERfile(sprintf('test2b_%d.spi',i),mask);

  for j=0:2^max_overlap_count-1
    test_map = ref_tub;
    for k=1:max_overlap_count
      if(bitand(j,2^(k-1)))
        fprintf(debugio, '1');
        test_map = test_map + ref_kin(:,:,mini_overlap_map(i,k));
      else
        fprintf(debugio, '0');
      end

      if(i == mini_overlap_map(i,k))
        if(bitand(j,2^(k-1)))
          i_occupied = 1;
        else
          i_occupied = 0;
        end
      end
%if(i == 26)
%      writeSPIDERfile(sprintf('test2_%d.spi',j),test_map);
%end
    end

    cur_ccc = ccc(test_map(find(mask)), image(find(mask)));

%writeSPIDERfile('test2.spi',test_map);
    fprintf(debugio, ' %f\n', cur_ccc);
    if(cur_ccc > best_ccc)
      best_ccc = cur_ccc;
      best_j = j;
      decorate(i) = i_occupied;
    end
  end
%if(i == 26)
%  mini_overlap_map(i,:)
%  fclose(debugio);
%  return;
%end
%  fprintf('%3d %1d %4d %f\n', i, decorate(i), best_j, best_ccc);
%  output_name = sprintf('%s%03d.spi', 'test', i);
%  writeSPIDERfile(output_name, ctf_mt_image + refs(:,:,i));
end
