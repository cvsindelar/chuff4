function rotate_spider_stack(input,output, job_dir, align_info_file, frealign_info_file, ...
                             box_file, apix, hp_resolution, ...
                             input_bin_factor, frealign_bin_factor, output_bin_factor, final_dim, ...
                             stack_format, pre_rotate, repair_psi)

%%%%%%%%%%%%%%%%%%%%%
% Here, 'angle' is the angle that would rotate the filament segments so that 
%  they are approximately horizontal, running left to right 
%  (corresponding to a projection with Euler angles phi, theta ~= 90, psi ~= 0).
%
% Note that we add 90 to angle, to make the segments vertical, when exporting
%  to sxihrsr, but this gets undone by the euler angles found by sxihrsr
%  (psi ~= 90 or 270) if we are exporting to FREALIGN.
%%%%%%%%%%%%%%%%%%%%%

box_coords = dlmread(box_file);
size_box_coords = size(box_coords);
angle = box_coords(2:size_box_coords(1),6);

v = double(readSPIDERfile(input));
size_v = size(v);

write_stack = 1;        % for debugging

%%%%%%%%%%%%%%%%%%%%%
% Note: sxihrsr only accepts stacks of spider images, while frealign only accepts 
%  spider stacks in the form of a 3D volume.
% Thus, we allow the possibility to change the output format of the spider file.
% However, currently we postprocess the spider stack for frealign, by using proc2d 
%  to convert the output spider stack to IMAGIC format, which frealign accepts.
%
%%%%%%%%%%%%%%%%%%%%%

if(strcmp(stack_format,'null') == 1)
  write_stack = 0;
end

spider_type = 'stack';

%if(strcmp(stack_format,'sparx') == 1)
%  spider_type = 'stack';
%else
%  spider_type = '';
%end

%%%%%%%%%%%%%
% Read in sxihrsr alignment
%%%%%%%%%%%%%

if(strcmp(align_info_file,'null') != 1)
  align_info = dlmread(align_info_file);
  size_align = size(align_info);

  filament_number = align_info(1,6);
  phi = align_info(:,1);
  theta = align_info(:,2);
  psi = align_info(:,3);
  dx = align_info(:,4) * input_bin_factor/output_bin_factor;
  dy = align_info(:,5) * input_bin_factor/output_bin_factor;

  if(repair_psi != 0)
    n_90 = prod(size(find(psi < 180)));
    if(repair_psi == 90 || repair_psi == 270)
      psi_polarity = repair_psi
    else
      if(n_90 > size_v(3)/2)
        psi_polarity = 90
      else
        psi_polarity = 270
      end
    end

    n_flips = 0;
    for i=1:size_v(3)
      if(abs(psi(i)-psi_polarity) > 90)
        n_flips = n_flips + 1;
        if(psi_polarity == 90)
          psi(i) = psi(i) - 180;
        else
          psi(i) = psi(i) + 180;
        end
      end
    end
    psi;
    n_flips
  end
else

  filament_number = 1;
  dx = zeros([1 size_v(3)]);
  dy = zeros([1 size_v(3)]);
  psi = zeros([1 size_v(3)]);
  theta = 90*ones([1 size_v(3)]);
  phi = zeros([1 size_v(3)]);
endif

if(strcmp(frealign_info_file,'null') != 1 && strcmp(align_info_file,'null') != 1)
  frealign_info = dlmread(frealign_info_file);

  size_frealign = size(frealign_info);
  if(size_frealign(1) != size_align(1))
    fprintf(stdout,'ERROR: #particles in frealign file differs from that in sxihrsr alignment... \n');
    return
  end

%  filament_number = align_info(1,6);
%%%%%%%%%%%%%%%%%
% Frealign phi, theta values replace the sxihrsr ones while the frealign psi gets added on.
%  Note that the frealign shifts are defined with the opposite sign convention as other shifts
%%%%%%%%%%%%%%%%%
  phi = frealign_info(:,1);
  theta = frealign_info(:,2);
  frealign_psi = frealign_info(:,3);
  frealign_shifts = -frealign_info(:,4:5) * frealign_bin_factor/output_bin_factor;
else
  frealign_psi = zeros([size_v(3) 1]);
  frealign_shifts = zeros([size_v(3) 2]);
end

freq = apix/hp_resolution;
band_freq = freq/10;

for i=1:size_v(3)
  if(write_stack == 1)
    printf('%d ', i);
    v(find(v == 0)) = sum(v(find(v != 0)))/prod(size(find(v != 0)));
%%%%%%%%%%%%%%%%
% High-pass filter to remove any slow gradients from the image
%  (should be done *before* shifting and rotation!)
%%%%%%%%%%%%%%%%

    v(:,:,i) = SharpHP(v(:,:,i),freq,band_freq);

%%%%%%%%%%%%%%%%
% Apply original filament rotation (angle+90), as well as additional rotation and shifts 
%  from sxihrsr (if read)
%  Good thing we are doing "lossless" rotation and shifts!
%
% Using sxihrsr, we have discovered shifts that center the particles 
%  following a rotation by angle+90 *clockwise* when viewing the x,y axes
%  in "math" view (i.e. with the origin in the lower left). 
%
% To determine shifts to apply in the absence of this rotation, we therefore
%  map the transformed coordinate system of the applied shifts (after rotation)
%  back to the unrotated coordinate system. The mapping gives x,y axes that 
%  are rotated *counterclockwise* by angle+90 (in math view). 
% Thus, the shifts are modified by a rotation matrix corresponding to this counterclockwise
%  rotation, which is [cos(angle+90) -sin(angle+90); sin(angle+90) cos(angle+90)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%

    r = (angle(i)+90)*pi/180;

    shift_x = cos(r)*dx(i)-sin(r)*dy(i);
    shift_y = sin(r)*dx(i)+cos(r)*dy(i);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Similar logic applies for frealign parameters, except that the image was rotated further 
%  (by -psi(i)) before frealign analyzed it.
%%%%%%%%%%%%%%%%%%%%%%%%%%%
    r = (angle(i)+90-psi(i))*pi/180;

    shift_x = shift_x + cos(r)*frealign_shifts(i,1)-sin(r)*frealign_shifts(i,2);
    shift_y = shift_y + sin(r)*frealign_shifts(i,1)+cos(r)*frealign_shifts(i,2);

    v(:,:,i) = ifftn(fftn(v(:,:,i)).*FourierShift(size_v(1),[shift_x shift_y]));
%%%%%%%%%%%%%%%%
% "Pre-rotate" the particle to be mostly vertical
%%%%%%%%%%%%%%%%
    if(pre_rotate != 0)
% 'pre-rotating'
% angle(i)+90-psi(i)-frealign_psi(i)
      v(:,:,i) = rotate2d(v(:,:,i),angle(i)+90-psi(i)-frealign_psi(i));
    endif

    img = v(:,:,i);
    avg = sum(sum(img))/prod(size(img));
    std_img = std(img(:));
    v(:,:,i) = (img - avg)/std_img;

%%%%%%%%%%%%%%%%
% Slow (but no more accurate) way to do the above
%%%%%%%%%%%%%%%%
%      v(:,:,i) = rotate2d(v(:,:,i),angle(i)+90);
%      v(:,:,i) = ifftn(fftn(v(:,:,i)).*FourierShift(size_v(1),[dx(i) dy(i)]));
%      v(:,:,i) = rotate2d(v(:,:,i),-psi(i));
  endif
end

if(write_stack == 1)
  origin = 1 + floor(size_v(1)/2) - floor(final_dim/2);
  writeSPIDERfile(output, ...
                  real(v(origin:origin+final_dim-1,origin:origin+final_dim-1,:)), ...
                  spider_type);
end
