function [coords, phi, theta, psi, directional_psi] = ...
  read_chuff_alignment(align_info_file, frealign_info_file, box_file, ...
                       input_bin_factor, frealign_bin_factor, repair_psi, output_bin_factor)

%%%%%%%%%%%%%%%%%%%%%
% Here, 'angle' is the angle that would rotate the filament segments so that 
%  they are approximately horizontal, running left to right 
%  (corresponding to a projection with Euler angles phi, theta ~= 90, psi ~= 0).
%
% Note that we add 90 to angle, to make the segments vertical, when exporting
%  to sxihrsr, but this gets undone by the euler angles found by sxihrsr
%  (psi ~= 90 or 270) if we are exporting to FREALIGN.
%%%%%%%%%%%%%%%%%%%%%

if(nargin < 7)
  output_bin_factor = 1;
end

box_coords = dlmread(box_file);
size_box_coords = size(box_coords);
coords = box_coords(2:size_box_coords(1),3:4) + floor(box_coords(2,5)/2);
angle = box_coords(2:size_box_coords(1),6)';

n_boxes = prod(size(angle));

%%%%%%%%%%%%%
% Read in sxihrsr alignment
%%%%%%%%%%%%%

if(strcmp(align_info_file,'null') != 1)
  align_info = dlmread(align_info_file);
  size_align = size(align_info);

  filament_number = align_info(1,6);
  phi = align_info(:,1)';
  theta = align_info(:,2)';
  psi = align_info(:,3)';
  dx = align_info(:,4)' * input_bin_factor/output_bin_factor;
  dy = align_info(:,5)' * input_bin_factor/output_bin_factor;

  n_90 = prod(size(find(psi < 180)));
  if(n_90 > n_boxes/2)
    directional_psi = 90
  else
    directional_psi = 270
  end

  if(repair_psi != 0)
    if(repair_psi == 90 || repair_psi == 270)
      psi_polarity = repair_psi
    else
      psi_polarity = directional_psi
    end

    n_flips = 0;
    for i=1:n_boxes
      if(abs(psi(i)-psi_polarity) > 90)
        n_flips = n_flips + 1;
        if(psi_polarity == 90)
          psi(i) = psi(i) - 180;
        else
          psi(i) = psi(i) + 180;
        end
      end
    end
    psi;
    n_flips
  end
else

  filament_number = 1;
  dx = zeros([1 n_boxes]);
  dy = zeros([1 n_boxes]);
  psi = zeros([1 n_boxes]);
  theta = 90*ones([1 n_boxes]);
  phi = zeros([1 n_boxes]);
endif

if(strcmp(frealign_info_file,'null') != 1 && strcmp(align_info_file,'null') != 1)
  frealign_info = dlmread(frealign_info_file);

  size_frealign = size(frealign_info);
  if(size_frealign(1) != size_align(1))
    fprintf(stdout,'ERROR: #particles in frealign file differs from that in sxihrsr alignment... \n');
    return
  end

%  filament_number = align_info(1,6);
%%%%%%%%%%%%%%%%%
% Frealign phi, theta values replace the sxihrsr ones while the frealign psi gets added on.
%  Note that the frealign shifts are defined with the opposite sign convention as other shifts
%%%%%%%%%%%%%%%%%
  phi = frealign_info(:,1)';
  theta = frealign_info(:,2)';
  frealign_psi = frealign_info(:,3)';
  frealign_shifts = -frealign_info(:,4:5) * frealign_bin_factor/output_bin_factor;
else
  frealign_psi = zeros([n_boxes 1]);
  frealign_shifts = zeros([n_boxes 2]);
end

%%%%%%%%%%%%%%%%
% Apply original filament rotation (angle+90), as well as additional rotation and shifts 
%  from sxihrsr (if read)
%
% Using sxihrsr, we have discovered shifts that center the particles 
%  following a rotation by angle+90 *clockwise* when viewing the x,y axes
%  in "math" view (i.e. with the origin in the lower left). 
%
% To determine shifts to apply in the absence of this rotation, we therefore
%  map the transformed coordinate system of the applied shifts (after rotation)
%  back to the unrotated coordinate system. The mapping gives x,y axes that 
%  are rotated *counterclockwise* by angle+90 (in math view). 
% Thus, the shifts are modified by a rotation matrix corresponding to this counterclockwise
%  rotation, which is [cos(angle+90) -sin(angle+90); sin(angle+90) cos(angle+90)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%

r = (angle'+90)*pi/180;

shift_x = cos(r).*dx'-sin(r).*dy';
shift_y = sin(r).*dx'+cos(r).*dy';

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Similar logic applies for frealign parameters, except that the image was rotated further 
%  (by -psi(i)) before frealign analyzed it.
%%%%%%%%%%%%%%%%%%%%%%%%%%%
r = (angle'+90-psi')*pi/180;

shift_x = shift_x + cos(r).*frealign_shifts(:,1)-sin(r).*frealign_shifts(:,2);
shift_y = shift_y + sin(r).*frealign_shifts(:,1)+cos(r).*frealign_shifts(:,2);

coords = coords - [shift_x shift_y];    

psi = mod(psi - (90 + angle),360) + frealign_psi(:)';
