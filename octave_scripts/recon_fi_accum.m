function [numerator, denominator, numerator1, numerator2, denominator1, denominator2] = ...
 recon_fi_accum(stack, psis, thetas, phis, shift_x, shift_y, sigma2, ctf, pad_factor, class_prob)
% [numerator, denominator, numerator1, numerator2, denominator1, denominator2] = ...
%  recon_fi_accum(image_parameters, image_stack,sigma2, ctf, pad_factor, class_prob)
% 3D Reconstruction by Fourier inversion
%
% REQUIRED ARGUMENTS: 
% - image_parameters can be a FREALIGN-style parameter file or
%    else an array of size N x 3 (or optionally N x 5 or N x 6)
%   For the array:
%       Col. 1-3 are psi, theta, phi;
%       Col. 4,5 are x and y shifts (in pixels)
%   For the file:
%       Col. 2-4 are psi, theta, phi;
%       Col. 5,6 are x and y shifts (in pixels)
% - image_stack is a 3D array containing the input images
%
% - pad_factor defaults to 2 if not specified, should be an integer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parse input and initialize parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < 5)
  pad_factor = 2;
end

stdout = 1;
precision = 0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make accessory grids and masks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

r = size(stack);
last_image = r(3);
r = [r(1) r(1) r(1)];
s = r * pad_factor;
r_origin = floor(r/2) + 1;
s_origin = floor(s/2) + 1;

[h k] = ndgrid((1:s(1)) - s_origin(1), (1:s(2)) - s_origin(2));
l = zeros(s(1), s(2));

radius = sqrt(h.*h + k.*k);
mask_fac = 1;

ftmask2d = ones(s(1),s(2));
% ftmask2d(find(radius > 0.5 + mask_fac*(s(1)-s_origin(1)))) = 0;
ftmask2d = fuzzymask(s(1),2,mask_fac*(s(1)-s_origin(1))-4,6);

[h k l] = ndgrid((1:r(1)) - r_origin(1), (1:r(2)) - r_origin(2), (1:r(3)) - r_origin(3));
radius = sqrt(h.*h + k.*k + l.*l);

[h k] = ndgrid((1:s(1)) - s_origin(1), (1:s(2)) - s_origin(2));
l = zeros(s(1), s(2));

time1=cputime();

numerator1 = zeros(s);
denominator1 = zeros(s);
numerator2 = zeros(s);
denominator2 = zeros(s);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Projection insertion loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

image = zeros([s(1) s(1)]);

progressbar = 0.1;
for z=1:last_image

  if(r(1) <= s(1))
    image(1 + floor(s(1)/2)-floor(r(1)/2):1 + floor(s(1)/2)-floor(r(1)/2)+r(1)-1, ...
          1 + floor(s(2)/2)-floor(r(2)/2):1 + floor(s(2)/2)-floor(r(2)/2)+r(2)-1, 1) = stack(:,:,z);

  else
    image = stack(1 + floor(r(1)/2)-floor(s(1)/2):1 + floor(r(1)/2)-floor(s(1)/2)+s(1)-1, ...
          1 + floor(r(2)/2)-floor(s(2)/2):1 + floor(r(2)/2)-floor(s(2)/2)+s(2)-1, z);
  end

  phi=phis(z)*pi/180;
  theta=thetas(z)*pi/180;
  psi=psis(z)*pi/180;

%########
% Get rotation matrix
%########
  cphi=cos(phi);
  sphi=sin(phi);
  ctheta=cos(theta);
  stheta=sin(theta);
  cpsi=cos(psi);
  spsi=sin(psi);

  rot_matrix(1,1)=cphi*ctheta*cpsi-sphi*spsi;
  rot_matrix(2,1)=-cphi*ctheta*spsi-sphi*cpsi;
  rot_matrix(3,1)=cphi*stheta;
  rot_matrix(1,2)=sphi*ctheta*cpsi+cphi*spsi;
  rot_matrix(2,2)=-sphi*ctheta*spsi+cphi*cpsi;
  rot_matrix(3,2)=sphi*stheta;
  rot_matrix(1,3)=-stheta*cpsi;
  rot_matrix(2,3)=stheta*spsi;
  rot_matrix(3,3)=ctheta;

  new_h = rot_matrix(1,1)*h + rot_matrix(2,1)*k + rot_matrix(3,1)*l;
  new_k = rot_matrix(1,2)*h + rot_matrix(2,2)*k + rot_matrix(3,2)*l;
  new_l = rot_matrix(1,3)*h + rot_matrix(2,3)*k + rot_matrix(3,3)*l;

  i = mod(round(new_h+s_origin(1)-1), s(1))+1;
  i = i + mod(round(new_k+s_origin(1)-1), s(1))*s(1);
  i = i + mod(round(new_l+s_origin(1)-1), s(1))*s(1)^2;

  proj_insert = ftmask2d.*fftshift( ...
                   FourierShift(s(1), [shift_x(z); shift_y(z)]).* ...
                                   fftn(ifftshift(image)));

  if(precision == 0)
    if(mod(z,2) == 1)
      numerator1(i) = numerator1(i) + class_prob(z) * ctf./fftshift(sigma2).*proj_insert;
      denominator1(i) = denominator1(i) + class_prob(z) * ctf.^2./fftshift(sigma2);
    else
      numerator2(i) = numerator2(i) + class_prob(z) * ctf./fftshift(sigma2).*proj_insert;
       denominator2(i) = denominator2(i) + class_prob(z) * ctf.^2./fftshift(sigma2);
    end
  else
    for j=1:length(i(:))
      if(mod(z,2) == 1)
         numerator1(i(j)) = numerator1(i(j)) + class_prob(z) * ctf(j)*sigma2(j)*proj_insert(j);
         denominator1(i(j)) = denominator1(i(j)) + class_prob(z) * ctf(j)^2*sigma2(k)*ftmask2d(j);
      else
         numerator2(i(j)) = numerator2(i(j)) + class_prob(z) * ctf(j)*sigma2(j)*proj_insert(j);
         denominator2(i(j)) = denominator2(i(j)) + class_prob(z) * ctf(j)^2*sigma2(j)*ftmask2d(j);
      end
    end
  end

  if(log10(z) == floor(log10(z)))
    fprintf(stdout,'Net time after projection %d : %f\n', z, cputime() - time1);
    if(isoctave)
      fflush(1);
    end
  end

  if(z/last_image > progressbar)
    fprintf(stdout,'%3.0f%% of insertions complete.\n', 100*progressbar);
    if(isoctave)
      fflush(1);
    end
    progressbar = progressbar + 0.1;
  end


end

numerator = numerator1 + numerator2;
denominator = denominator1 + denominator2;

fprintf(stdout,'Total time in seconds: %d\n', cputime()-time1);
if(isoctave)
  fflush(1);
end
