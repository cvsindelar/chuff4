if(nargin < 3)
  fprintf('PROGRAMMING ERROR: wrong number of arguments to octave script! (%d)\n', nargin);
%  exit(2)
end

use_ctf = 0;
focal_mate = 0;
find_monomer = 0;
recon_num_pfs = 0;
recon_num_starts = 0;


output_directory = argv(){1}
output_header = argv(){2};
frealign_bin_factor = str2num(argv(){3});
dimer_map_option = argv(){4};
target_model = str2num(argv(){5});
target_num_pfs = str2num(argv(){6});
target_num_starts = str2num(argv(){7});
find_bin_factor = str2num(argv(){8});
chumps_round = str2num(argv(){9});
num_args = 9;

tubrefs = [];
kinrefs = [];
ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,find_bin_factor);
kin_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,find_bin_factor);
tub_ref_file = sprintf('%s/ref_tot.spi',ref_dir);

%tubrefs = readSPIDERfile(tub_ref_file);
%kinrefs = readSPIDERfile(kin_ref_file);

mkdir(output_directory);

num_mts = nargin - num_args;

output_stack = sprintf('%s/frealign_image_stack.spi',output_directory);
frealign_parameter_file = sprintf('%s/%s_0.par',output_directory,output_header);

if(prod(size(stat(output_stack))) != 0)
  fprintf(stdout, 'ERROR: stack file exists already: "%s"\n', output_stack);
  return
end

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m';

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification
final_pixel_size = scanner_pixel_size*10000/target_magnification*frealign_bin_factor;

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);

%%%%%%%%%%%%                                                                                    
% Make the dimension closer to a power of 2                                               
%%%%%%%%%%%%                                                                                  
granularity = 2**(floor(log(box_dim)/log(2))-2);
if(granularity < 8)
  granularity = 8;
end
box_dim = granularity*ceil(box_dim/granularity);

box_dim = box_dim*frealign_bin_factor;
box_dim = [box_dim box_dim];
ref_dim = floor(box_dim/frealign_bin_factor);

box_filter = ifftn(ones(box_dim));
lp_freq = 0.5/frealign_bin_factor;
delta_lp_freq = lp_freq/10;
box_filter = SharpFilt(box_filter,lp_freq,delta_lp_freq);
box_filter = fftn(box_filter);

% TEMPORARY!!
pad_ref_dim = 4*ref_dim;

tot_n_particles = 1;
scale_avg = 0;
n_scale_avg = 0;
tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

f = fopen(frealign_parameter_file, 'w');
fclose(f);
% ctf_doc = dlmread('ctf_files/ctf_docfile_for_frealign.spi');

tot_n_particles = 1;

for mt_num = 1: num_mts
%%%%%%%%%%%%%%%
% Read in filenames, etc.
%%%%%%%%%%%%%%%

  job_dir = argv(){mt_num+num_args};

  if(job_dir(prod(size(job_dir))) == '/')
    job_dir=job_dir(1:prod(size(job_dir))-1);
  end
  [start] = regexp(job_dir,'[^/]*$','start');
  job_dir = job_dir(start:prod(size(job_dir)));

  [micrograph,defocus,astig_mag,astig_angle,b_factor] = ...
    chumps_micrograph_info(job_dir,focal_mate);

  box_file_name = sprintf('scans/%s.txt',job_dir);

  mt_type_info = dlmread(sprintf('chumps_round1/%s/selected_mt_type.txt',job_dir));
  num_pfs = mt_type_info(1);
  num_starts = mt_type_info(2);

  if(num_pfs != target_num_pfs && target_num_pfs != 0)
    continue
  end
  if(num_starts != target_num_starts && target_num_starts != 0)
num_starts
target_num_starts
    continue
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read in box info and extract particles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

  if(focal_mate == 0)
    find_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
    micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
  else
    find_dir = sprintf('chumps_round1/%s/fmate_find',job_dir);
    micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
  end

  micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
  ctf_doc = dlmread(sprintf('scans/%s_ctf_doc.spi',job_dir(1:start-1)));

  micrograph_number = job_dir(1:start-1);
  [start2] = regexp(micrograph_number,'[1-9][0-9]*$','start');
  micrograph_number = str2num(micrograph_number(start2:prod(size(micrograph_number))));

  mic_dim = ReadMRCheader(micrograph);

  index = find(ctf_doc(:,1) == 1);
  if(prod(size(index)) == 0)
    fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
    return
  end
  index = index(prod(size(index)));
  defocus1 = ctf_doc(index,3);
  defocus2 = ctf_doc(index,4);
  astig_angle = ctf_doc(index,5);

  presa = 50.0;
  dpres = 50.0;

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

  com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
  com_info = dlmread(com_doc_name);
  index = find(com_info(:,1) == 1);
  ref_com = com_info(index(1),3:5);

  ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
  ref_pixel_info = dlmread(ref_params_name);
  index = find(ref_pixel_info(:,1) == 2);
  ref_pixel = ref_pixel_info(index(1),5);
  index = find(ref_pixel_info(:,1) == 1);
  min_theta = ref_pixel_info(index(1),5);
  index = find(ref_pixel_info(:,1) == 3);
  d_angle = ref_pixel_info(index(1),3);

  [num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
    est_repeat_distance] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,1,micrograph_pixel_size);

  est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

  [coords,phi,theta,psi,...
   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance] = ...
    fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,
                         est_pix_per_repeat);

  n_good_boxes = prod(size(phi));

  radon_scale = dlmread(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));
  index = find(radon_scale(:,1) == 1);
  radon_scale = radon_scale(index(prod(size(index))),3);
  scale_avg = scale_avg + radon_scale;
  n_scale_avg = n_scale_avg + 1;

%%%%%%%%%%%%%%%%%%
% Count how many "good" boxes are fully inside the micrograph
%%%%%%%%%%%%%%%%%%

  desorted_map = zeros([num_pfs 2*n_good_boxes]);

  real_n_good_boxes = 0;
  for box_num=1:n_good_boxes
    int_coords = round(coords(box_num,:));
    box_origin = int_coords-floor(box_dim/2);
    if(box_origin(1) < 1 || box_origin(2) < 1 || ...
       box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
      continue
    end
    real_n_good_boxes = real_n_good_boxes + 1;
  end

%%%%%%%%%%%%%%%%%%
% Read in decoration map
%%%%%%%%%%%%%%%%%%
  max_overlap = 0*num_pfs;
  n_kins_to_find = 2*num_pfs; % 6*num_pfs;
% TEMPORARY!!
%  num_repeats = ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
  num_repeats = 2*ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
  kin_lib_size = 2*num_repeats*num_pfs;

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;

  map_file = sprintf('%s/tot_decorate_map.spi',find_dir);
  tot_decorate_map = readSPIDERfile(map_file);
  sorted_map_file = sprintf('%s/sorted_map_full_dimer.spi',find_dir);

  if(strcmp(dimer_map_option,'hidf') == 1)
    sorted_map_file = sprintf('%s/sorted_map_full_hidf_dimer.spi',find_dir)
  end
  if(strcmp(dimer_map_option,'lowdf') == 1)
    sorted_map_file = sprintf('%s/sorted_map_full_dimer.spi',find_dir)
  end
  if(strcmp(dimer_map_option,'hidf_monomer') == 1)
    temp_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
    sorted_map_file = sprintf('%s/sorted_map_full_hidf.spi',temp_dir);
    find_monomer = 1;
  end
  if(strcmp(dimer_map_option,'lowdf_monomer') == 1)
    temp_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
    sorted_map_file = sprintf('%s/sorted_map_full.spi',temp_dir);
    find_monomer = 1;
  end
  if(strcmp(dimer_map_option,'composite') == 1)
    sorted_map_file = sprintf('%s/sorted_map_full_composite_dimer.spi',find_dir);
  end
  if(strcmp(dimer_map_option,'hidf_lateral') == 1)
    sorted_map_file = sprintf('%s/sorted_map_full_hidf_lat_dimer.spi',find_dir);
  end

  sorted_map = readSPIDERfile(sorted_map_file);

%%%%%%%%%%%%%%%%
% Find the seam(s)
%%%%%%%%%%%%%%%%
  temp_file = sprintf('%s/sorted_map_full.spi',find_dir)
  temp_map = readSPIDERfile(temp_file);
  temp_file = sprintf('%s/sorted_map_full_hidf.spi',find_dir)
  temp_map_hidf = readSPIDERfile(temp_file);
  seam_mask = find_seams(temp_map);

%%%%%%%%%%%%%%%%%
% Ensure that the wrong tubulin sites are not used
%%%%%%%%%%%%%%%%%

  if(find_monomer == 1)
    sorted_map(find(1-seam_mask)) = -1;
  end

%%%%%%%%%%%%%%%%%
% Figure out padding factors
%%%%%%%%%%%%%%%%%
  s = size(sorted_map);
  sorted_map_pad = s(2);
  s = size(tot_decorate_map);
  sorted_map_pad = (sorted_map_pad - s(2))/2

  fprintf(stdout,'\n%s\n', job_dir);

  real_box_num = 0;
  for box_num=1:n_good_boxes
    f = fopen(frealign_parameter_file, 'a');

    fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
    fflush(stdout);

    int_coords = round(coords(box_num,:));
    box_origin = int_coords-floor(box_dim/2);
    if(box_origin(1) < 1 || box_origin(2) < 1 || ...
       box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
      printf('  Box not completely within micrograph: (%d, %d)\n',...
              box_origin(1),box_origin(2));
      continue
    end

if(1 == 1)

    ctf = -ones(pad_ref_dim);  % not used
    ctf_flip = sign(ctf);      % not used

    filter = ones(pad_ref_dim);
    ref_pixel = micrograph_pixel_size * frealign_bin_factor;  % not used

    [kin_repeat_index, kin_pf_index,...
     x_tub, y_tub, phi_tub, theta_tub, psi_tub] = ...
      get_findkin_refs(box_num, ref_dim,num_repeats,num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com);

    dec_origin = floor(kin_lib_size/2) - floor(n_kins_to_find/2);
    dec_repeat_origin = kin_repeat_index(dec_origin + 1);

    mini_repeat_index = kin_repeat_index(dec_origin+1 : dec_origin+n_kins_to_find);
    mini_pf_index = kin_pf_index(dec_origin+1 : dec_origin+n_kins_to_find);

    mini_repeat_index = mini_repeat_index - dec_repeat_origin;
    mini_repeat_index = mini_repeat_index + (box_num-1)*2;

    sorted_index = num_pfs*(mini_repeat_index-1)+mini_pf_index;
    repeat_map = sorted_map(sorted_index + sorted_map_pad*num_pfs);

%    desorted_map( 1+(box_num-1)*n_kins_to_find: ...
%                               box_num*n_kins_to_find) = repeat_map;
%    writeSPIDERfile('desorted_dimer_map.spi',desorted_map);

debug = 0;
if(debug == 1)

    final_map = ref_tub;
    final_map2 = ref_tub;

    for i=1:n_kins_to_find
      if(find_monomer == 0)
        if(directional_psi == 90)
          target_model = 1;
        else
          target_model = 2;
        end
      end

      if(repeat_map(i) == target_model)
        phi_particle = phi_tub(dec_origin+i);
        theta_particle = theta_tub(dec_origin+i);
        psi_particle = psi_tub(dec_origin+i);

%%%%%%%%%%%%%%%%%%
% The following two lines compute the shift that puts the tubulin of interest into the
%  3D reconstruction at the 3D coordinates of ref_com.
%  
%%%%%%%%%%%%%%%%%%
        euler_mt_inv = EulerMatrix([-psi_particle ...
                                    -theta_particle ...
                                    -phi_particle]);
        origin_adjust = -ref_com * euler_mt_inv/micrograph_pixel_size;

%%%%%%%%%%%%%%%%%%
% Read in boxed image, after re-centering the coordinates using the found
%  coords
%%%%%%%%%%%%%%%%%%

        particle_coords = coords(box_num,:) + ...
          [x_tub(dec_origin+i) y_tub(dec_origin+i)]/micrograph_pixel_size + ...
          [origin_adjust(1) origin_adjust(2)];

        int_coords = round(particle_coords);
        frac_coords = particle_coords - int_coords;

        box_origin = int_coords-floor(box_dim/2);

        [orig_box,mrcinfo,msg] = ReadMRCwin(micrograph, box_dim, box_origin);
        if(orig_box == -1)
          printf('  Kinesin box not completely within micrograph: (%d, %d)\n',...
                  box_origin(1),box_origin(2));
          continue;
        end

%        box = real(ifftn(fftn(orig_box) .* FourierShift(box_dim(1), -frac_coords)));
        box = invert_density * real(ifftn(box_filter.*fftn(orig_box).*FourierShift(box_dim(1), -frac_coords)));

        box = BinImageCentered(box, frealign_bin_factor);

%        writeSPIDERfile(sprintf('test_%04d.spi',tot_n_particles),box);
        fprintf(f, ...
               '%7d%8.2f%8.2f%8.2f%8.2f%8.2f%8.0f%6d%9.1f%9.1f%8.2f%7.2f%6.2f\n', ...
               tot_n_particles, ...
               psi_particle*180/pi, theta_particle*180/pi, phi_particle*180/pi, 0, 0, ...
               target_magnification/frealign_bin_factor, micrograph_number, ...
               defocus1, defocus2, astig_angle, presa, dpres);

        tilt_scale_avg = tilt_scale_avg + 1/sin(theta_particle);
        n_tilt_scale_avg = n_tilt_scale_avg + 1;

%        test_map = generate_micrograph(box_dim/frealign_bin_factor, kinrefs, ...
%                                   ref_pixel, min_theta, d_angle, 0,...
%                                   x_tub(dec_origin+i),y_tub(dec_origin+i),...
%                                   0,phi_particle,theta_particle,psi_particle,...
%                                   [0 0],1,0);
%        test_map2 = generate_micrograph(box_dim/frealign_bin_factor, kinrefs, ...
%                                   ref_pixel, min_theta, d_angle, 0,...
%		                   0,0,...
%                                   0,phi_particle,theta_particle,psi_particle,...
%                                   [0 0],1,0);
%
%        test_map = ifftn(fftn(test_map).*FourierShift(ref_dim(1),...
%                                       -[x_tub(dec_origin+i),y_tub(dec_origin+i)]/ref_pixel)));
%        writeSPIDERfile(sprintf('testd_%04d.spi',tot_n_particles),test_map);
%        writeSPIDERfile(sprintf('teste_%04d.spi',tot_n_particles),test_map2);

        box = (box - mean(box(:)))/std(box(:),1);

        if(tot_n_particles == 1)
          writeSPIDERfile(output_stack,box);
        else
          appendSPIDERvol(output_stack,box);
        end

        tot_n_particles = tot_n_particles + 1;
      end

%      final_map = final_map + repeat_map(i)*ref_kin(:,:,i+dec_origin);
%      final_map2 = final_map2 + ref_kin(:,:,i+dec_origin);
%      final_map = final_map + control_repeat_map(i)*ref_kin(:,:,i+dec_origin);
    end
%    writeSPIDERfile(sprintf('testb_%04d.spi',tot_n_particles),final_map);

end % debug

end % if(1 == 1)

fclose('all');

end % for box_num...

end % for mt_num ...

scale_avg = scale_avg/n_scale_avg
tilt_scale_avg = tilt_scale_avg/n_tilt_scale_avg

# micrograph_pixels_per_repeat = ...
#   helical_repeat_distance/(scale_avg*tilt_scale_avg)/micrograph_pixel_size

%%%%%%%%%
% Now, guestimate symmetry parameters for refinement and reconstruction - 
%  ideally we would do this from the data...
% Default is to use geometry of 13-pf MT (regardless of whether the data is actually
%  a mixture of different types, which does not affect the subunit of interest)
%%%%%%%%%

if(recon_num_pfs == 0)
  recon_num_pfs = 13
end
if(recon_num_starts == 0)
  recon_num_starts = 3
end
[twist_per_subunit, rise_per_subunit, mt_radius, axial_repeat_dist, twist_per_repeat] = mt_lattice_params(recon_num_pfs, recon_num_starts/2, 80, 112);

info_file = sprintf('%s/info.txt',output_directory);
f = fopen(info_file,'w');
fprintf(f,'output_header %s\n', output_header);
fprintf(f,'bin_factor %d\n',frealign_bin_factor);
fprintf(f,'micrograph_pixels_per_repeat %8.3f\n',est_repeat_distance/micrograph_pixel_size);
fprintf(f,'voxel_size %8.3f\n',final_pixel_size);
fprintf(f,'estimated_repeat_distance %8.3f\n',est_repeat_distance);
fprintf(f,'helical_twist %8.3f\n',twist_per_subunit*180/pi);
fprintf(f,'num_pfs %d\n',recon_num_pfs);
fprintf(f,'num_starts %d\n',recon_num_starts);
fclose(f);
