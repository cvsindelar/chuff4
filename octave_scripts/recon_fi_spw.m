function [final_vol, final_vol_fsc1, final_vol_fsc2, final_vol_spw, coremask] = recon_fi_spw(image_parameters, image_stack, pad_factor, use_ctf, pixel_size, kV, Cs, amplitude_contrast)
% [final_vol final_vol_fsc1 final_vol_fsc2, coremask] = 
%   recon_fi_spw(image_parameters, image_stack, use_ctf, pixel_size, 
%              kV, Cs, amplitude_contrast, pad_factor)
% 3D Reconstruction by Fourier inversion
% Note: only the first two arguments are required
%
% REQUIRED ARGUMENTS: 
% - image_parameters can be a FREALIGN-style parameter file or
%    else an array of size N x 3 (or optionally N x 5 or N x 6)
%   For the array:
%       Col. 1-3 are psi, theta, phi;
%       Col. 4,5 are x and y shifts (in pixels)
%       Col.  6 is the defocus (in Angstroms)
%   For the file:
%       Col. 2-4 are psi, theta, phi;
%       Col. 5,6 are x and y shifts (in pixels)
%       Col. 9 is the defocus (in Angstroms)
% - image_stack is a 3D array containing the input images
%
% OPTIONAL ARGUMENTS:
% - use_ctf = 0 for no CTF correction, 1 otherwise
% - pixel_size is in Angstroms
% - kV is accelerating voltage in kV
% - Cs is spherical aberration constant
% - amplitude_contrast (value of 0.07 ~ 0.1 is appropriate)
% - pad_factor defaults to 2 if not specified, should be an integer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parse input and initialize parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < 3)
  pad_factor = 2;
end

if(nargin < 4)
  use_ctf = 0;
end
if(nargin < 5)
  pixel_size = 0;
  kV = 0;
  Cs = 0;
  amplitude_contrast = 0;
end

stdout = 1;
precision = 0;
frealign_fudge = 0.1;
f_coremask = 0.01;              % This should be significantly smaller
                                 %  than the actual volume fraction 
                                 %  taken up by the particle; 0.01 = "safe"
if(isa(image_stack, 'char'))
  stack = readSPIDERfile(image_stack);
else
  stack = image_stack;
  clear image_stack;
end

stack_dim = size(stack);
dim = stack_dim(1); % temp_dim(1);
last_image = stack_dim(3);

r = [dim dim dim];
s = r * pad_factor;
r_origin = floor(r/2) + 1;
s_origin = floor(s/2) + 1;

if(isa(image_parameters, 'char'))
  eulers = dlmread(image_parameters);
  psis = eulers(1:last_image,2)*pi/180;
  thetas = eulers(1:last_image,3)*pi/180;
  phis = eulers(1:last_image,4)*pi/180;

  par_dim = size(eulers);

  if(par_dim(2) >= 5)
    shift_x = -eulers(1:end,5);
    shift_y = -eulers(1:end,6);
  else
    shift_x = zeros(par_dim(1), 1);
    shift_y = zeros(par_dim(1), 1);
  end

  if(par_dim(2) >= 9)
    defocuses = eulers(1:last_image,9)/10000;   % convert from angstroms to microns
  else
    defocuses = zeros(1:last_image, 1);
  end
else
  par_dim = size(image_parameters);
  psis = image_parameters(1:last_image,1)*pi/180;
  thetas = image_parameters(1:last_image,2)*pi/180;
  phis = image_parameters(1:last_image,3)*pi/180;

  if(par_dim(2) >= 4)
    shift_x = -image_parameters(1:end,4);
    shift_y = -image_parameters(1:end,5);
  else
    shift_x = zeros(par_dim(1), 1);
    shift_y = zeros(par_dim(1), 1);
  end

  if(par_dim(2) >= 6)
    defocuses = image_parameters(1:last_image,6)/10000;   % convert from angstroms to microns
  else
    defocuses = zeros(last_image, 1);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do particle insertions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pixel_size
use_ctf
kV
Cs
amplitude_contrast
[numerator1, numerator2, denominator1, denominator2] =recon_fi_insert(stack, psis, thetas, phis, shift_x, shift_y, defocuses, pad_factor, use_ctf, pixel_size, kV, Cs, amplitude_contrast);

clear stack

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get 'correct3d' point spread function to restore density at volume edges
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

correct3d = get_correct3d(s, s);

mask_fac = 1;
mask3d = fuzzymask(s(1),3,mask_fac*(r(1)-r_origin(1))-4,6);
f_mask3d = sum(sum(sum(mask3d.*mask3d)))/prod(s)
% writeSPIDERfile('mask3d.spi', mask3d);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make half-dataset reconstructions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

final_vol_fsc1 = fftshift(real(ifftn(ifftshift(numerator1./(denominator1+frealign_fudge)))));
final_vol_fsc1 = final_vol_fsc1./correct3d;

final_vol_fsc2 = fftshift(real(ifftn(ifftshift(numerator2./(denominator2+frealign_fudge)))));
final_vol_fsc2 = final_vol_fsc2./correct3d;

if(nargout > 3)

%%%%%%%%%%%%%%%%%%
% Calculate "single-particle Wiener-filtered" volume
%%%%%%%%%%%%%%%%%%

  masked_fsc(1) = 0.99999;
  masked_fsc(2:floor(s(1)/2)+1) = double(FSCorr(final_vol_fsc1.*mask3d,final_vol_fsc2.*mask3d));
  ssnr = f_mask3d * masked_fsc./(1-masked_fsc);
  fsc = ssnr./(1+ssnr);

  est_ssnr_term = estimate_ssnr(masked_fsc, denominator1 + denominator2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use conventional Wiener filtered volume to make core mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  final_vol = real(fftshift(ifftn(ifftshift((numerator1+numerator2)./((denominator1+denominator2) + 4/3*pi*0.5^3 ./ (est_ssnr_term))))));

  final_vol = final_vol(1 + floor(s(1)/2)-floor(r(1)/2):1 + floor(s(1)/2)-floor(r(1)/2)+r(1)-1, ...
     1 + floor(s(2)/2)-floor(r(2)/2):1 + floor(s(2)/2)-floor(r(2)/2)+r(2)-1, ...
     1 + floor(s(3)/2)-floor(r(3)/2):1 + floor(s(3)/2)-floor(r(3)/2)+r(3)-1);

  coremask = zeros(s);
  coremask(1 + floor(s(1)/2)-floor(r(1)/2):1 + floor(s(1)/2)-floor(r(1)/2)+r(1)-1, ...
         1 + floor(s(2)/2)-floor(r(2)/2):1 + floor(s(2)/2)-floor(r(2)/2)+r(2)-1, ...
         1 + floor(s(3)/2)-floor(r(3)/2):1 + floor(s(3)/2)-floor(r(3)/2)+r(3)-1) = thresh_mask(final_vol, f_coremask);;

% writeSPIDERfile('core_mask.spi', coremask);

  best_ccc = -1;
  for est_f_particle_3d=[0.05:0.05:1.25]

    final_vol_spw_fsc1 = fftshift(real(ifftn(ifftshift(numerator1./(denominator1 + est_f_particle_3d ./ (est_ssnr_term))))))./correct3d;
    ccc_spw_masked_est = ccc(final_vol_spw_fsc1 .* coremask, final_vol_fsc2.* coremask);
    clear final_vol_spw_fsc1;

    if(ccc_spw_masked_est > best_ccc)
      best_f_particle_3d = est_f_particle_3d;
      best_ccc = ccc_spw_masked_est;
    end

    fprintf(stdout,'%6f %10.7e\n', est_f_particle_3d, ccc_spw_masked_est);
  end

  best_f_particle_3d

  final_vol_spw = real(fftshift(ifftn(ifftshift((numerator1+numerator2)./((denominator1+denominator2) + best_f_particle_3d ./ (est_ssnr_term))))));
  final_vol_spw = final_vol_spw(1 + floor(s(1)/2)-floor(r(1)/2):1 + floor(s(1)/2)-floor(r(1)/2)+r(1)-1, ...
       1 + floor(s(2)/2)-floor(r(2)/2):1 + floor(s(2)/2)-floor(r(2)/2)+r(2)-1, ...
       1 + floor(s(3)/2)-floor(r(3)/2):1 + floor(s(3)/2)-floor(r(3)/2)+r(3)-1);
end
final_vol = real(fftshift(ifftn(ifftshift((numerator1+numerator2)./((denominator1+denominator2) + frealign_fudge)))));
final_vol = final_vol(1 + floor(s(1)/2)-floor(r(1)/2):1 + floor(s(1)/2)-floor(r(1)/2)+r(1)-1, ...
       1 + floor(s(2)/2)-floor(r(2)/2):1 + floor(s(2)/2)-floor(r(2)/2)+r(2)-1, ...
       1 + floor(s(3)/2)-floor(r(3)/2):1 + floor(s(3)/2)-floor(r(3)/2)+r(3)-1);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: recon_fi_insert()
% Do the main insertion loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [numerator1, numerator2, denominator1, denominator2] = recon_fi_insert(stack, psis, thetas, phis, shift_x, shift_y, defocuses, pad_factor, use_ctf, pixel_size, kV, Cs, amplitude_contrast)

precision = 0
stdout = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make accessory grids and masks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

r = size(stack);
last_image = r(3);
r = [r(1) r(1) r(1)];
s = r * pad_factor;
r_origin = floor(r/2) + 1;
s_origin = floor(s/2) + 1;

[h k] = ndgrid((1:s(1)) - s_origin(1), (1:s(2)) - s_origin(2));
l = zeros(s(1), s(2));

radius = sqrt(h.*h + k.*k);
mask_fac = 1;

ftmask2d = ones(s(1),s(2));
% ftmask2d(find(radius > 0.5 + mask_fac*(s(1)-s_origin(1)))) = 0;
ftmask2d = fuzzymask(s(1),2,mask_fac*(s(1)-s_origin(1))-4,6);

[h k l] = ndgrid((1:r(1)) - r_origin(1), (1:r(2)) - r_origin(2), (1:r(3)) - r_origin(3));
radius = sqrt(h.*h + k.*k + l.*l);

[h k] = ndgrid((1:s(1)) - s_origin(1), (1:s(2)) - s_origin(2));
l = zeros(s(1), s(2));

time1=cputime();

numerator1 = zeros(s);
denominator1 = zeros(s);
numerator2 = zeros(s);
denominator2 = zeros(s);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Projection insertion loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

image = zeros([s(1) s(1)]);

progressbar = 0.1;
for z=1:last_image

  if(r(1) <= s(1))
    image(1 + floor(s(1)/2)-floor(r(1)/2):1 + floor(s(1)/2)-floor(r(1)/2)+r(1)-1, ...
          1 + floor(s(2)/2)-floor(r(2)/2):1 + floor(s(2)/2)-floor(r(2)/2)+r(2)-1, 1) = stack(:,:,z);

  else
    image = stack(1 + floor(r(1)/2)-floor(s(1)/2):1 + floor(r(1)/2)-floor(s(1)/2)+s(1)-1, ...
          1 + floor(r(2)/2)-floor(s(2)/2):1 + floor(r(2)/2)-floor(s(2)/2)+s(2)-1, z);
  end

  phi=phis(z);
  theta=thetas(z);
  psi=psis(z);

%########
% Get rotation matrix
%########
  cphi=cos(phi);
  sphi=sin(phi);
  ctheta=cos(theta);
  stheta=sin(theta);
  cpsi=cos(psi);
  spsi=sin(psi);

  rot_matrix(1,1)=cphi*ctheta*cpsi-sphi*spsi;
  rot_matrix(2,1)=-cphi*ctheta*spsi-sphi*cpsi;
  rot_matrix(3,1)=cphi*stheta;
  rot_matrix(1,2)=sphi*ctheta*cpsi+cphi*spsi;
  rot_matrix(2,2)=-sphi*ctheta*spsi+cphi*cpsi;
  rot_matrix(3,2)=sphi*stheta;
  rot_matrix(1,3)=-stheta*cpsi;
  rot_matrix(2,3)=stheta*spsi;
  rot_matrix(3,3)=ctheta;

  new_h = rot_matrix(1,1)*h + rot_matrix(2,1)*k + rot_matrix(3,1)*l;
  new_k = rot_matrix(1,2)*h + rot_matrix(2,2)*k + rot_matrix(3,2)*l;
  new_l = rot_matrix(1,3)*h + rot_matrix(2,3)*k + rot_matrix(3,3)*l;

  i = mod(round(new_h+s_origin(1)-1), s(1))+1;
  i = i + mod(round(new_k+s_origin(1)-1), s(1))*s(1);
  i = i + mod(round(new_l+s_origin(1)-1), s(1))*s(1)^2;

  proj_insert = ftmask2d.*fftshift( ...
                   FourierShift(s(1), [shift_x(z); shift_y(z)]).* ...
                                   fftn(ifftshift(image)));
% proj_insert = ftmask2d.*fftshift(fftn(ifftshift(image)));
% proj_insert = fftshift(fftn(ifftshift(image)));

  if(use_ctf == 1)
    defocus = defocuses(z);
    electron_lambda = EWavelength(kV);
    ctf_term = CTF_ACR(s(1), pixel_size, electron_lambda, defocus, Cs, 0, amplitude_contrast, 0);
  else
    ctf_term = ones(s(1),s(2));
  end

  if(precision == 0)
    if(mod(z,2) == 1)
      numerator1(i) = numerator1(i) + ctf_term.*proj_insert;
%     denominator1(i) = denominator1(i) + ctf_term.^2.*ftmask2d;
       denominator1(i) = denominator1(i) + ctf_term.^2;
    else
      numerator2(i) = numerator2(i) + ctf_term.*proj_insert;
%     denominator2(i) = denominator2(i) + ctf_term.^2.*ftmask2d;
       denominator2(i) = denominator2(i) + ctf_term.^2;
    end
  else
    for j=1:length(i(:))
      if(mod(z,2) == 1)
         numerator1(i(j)) = numerator1(i(j)) + ctf_term(j)^2*proj_insert(j);
         denominator1(i(j)) = denominator1(i(j)) + ctf_term(j)^2*ftmask2d(j);
      else
         numerator2(i(j)) = numerator2(i(j)) + ctf_term(j)^2*proj_insert(j);
         denominator2(i(j)) = denominator2(i(j)) + ctf_term(j)^2*ftmask2d(j);
      end
    end
  end

  if(log10(z) == floor(log10(z)))
    fprintf(stdout,'Net time after projection %d : %f\n', z, cputime() - time1);
    if(isoctave)
      fflush(1);
    end
  end

  if(z/last_image > progressbar)
    fprintf(stdout,'%3.0f%% of insertions complete.\n', 100*progressbar);
    if(isoctave)
      fflush(1);
    end
    progressbar = progressbar + 0.1;
  end


end

fprintf(stdout,'Total time in seconds: %d\n', cputime()-time1);
if(isoctave)
  fflush(1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: get_correct3d()
% Make 'correct3d' point spread function to restore density at volume edges:
%  sinc(x) = inverse FT of rectangle function rect(x)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function correct3d = get_correct3d(r, s)

r_origin = floor(r/2) + 1;
s_origin = floor(s/2) + 1;

[h k l] = ndgrid((1:r(1)) - r_origin(1), (1:r(2)) - r_origin(2), (1:r(3)) - r_origin(3));
radius = sqrt(h.*h + k.*k + l.*l);

temp = sin(pi*h/s(1))./(pi*h/s(1));
temp(r_origin(1),:,:) = 1;
correct3d = temp;
temp = sin(pi*k/s(2))./(pi*k/s(2));
temp(:,r_origin(2),:) = 1;
correct3d = correct3d.*temp;
temp = sin(pi*l/s(3))./(pi*l/s(3));
temp(:,:,r_origin(3)) = 1;
correct3d = correct3d.*temp;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: estimate_ssnr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function est_ssnr_term = estimate_ssnr(fsc, denominator_term)

stdout = 1;
r = size(denominator_term);

cutoff_index = floor(r(1)/2) % It would be better to choose the
                             % cutoff based on a reasonable FSC threshold
                             % like 0.5 or 0.143

r_origin = floor(r/2) + 1;

[h k l] = ndgrid((1:r(1)) - r_origin(1), (1:r(2)) - r_origin(2), (1:r(3)) - r_origin(3));
radius = sqrt(h.*h + k.*k + l.*l);

est_ssnr_term = zeros(r);
est_ssnr_term = est_ssnr_term + 1e-10;

fprintf(stdout,'# SSNR info: estimated ssnr, effective # measurements\n');

for i=0:floor(r(1)/2)
  j = find(radius > i - 0.5 & radius < i + 0.5);
  if(length(j) ~= 0 && i <= cutoff_index)
    avg_measure = sum(denominator_term(j))/length(j);  
    est_image_SSNR = 1/avg_measure * 2*fsc(i+1)/(1-fsc(i+1));
    est_ssnr_term(j) = est_image_SSNR;
  end

  fprintf(stdout,'#     %4d %8.6f %12.3f %9.7f\n', i, est_ssnr_term(j(1)), avg_measure, fsc(i+1));
  if(isoctave)
    fflush(1);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: threshold mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [v_thresh, thresh] = thresh_mask(v, frac, max_iter)
% function [v_thresh, thresh] = thresh_mask(v, frac, max_iter)

if(nargin < 3)
  max_iter = 10;
end

min_val = min(min(min(v)));
max_val = max(max(max(v)));
best_coarse_thresh = min_val + (max_val-min_val)/2;

max_i=10;
iter = 1;

while(iter <= max_iter)

  scale_factor = (2/max_i *(max_i+2)/max_i)^(iter-1);
  search_range = (max_val-min_val) * scale_factor;

  for i=1:max_i+1
    thresh = best_coarse_thresh + search_range * (i-floor(max_i/2+1))/max_i;
    cur_frac = numel(find(v > thresh))/prod(size(v));
    if(cur_frac > frac)
       best_thresh = thresh;
    end
  end

%  fprintf('%5d %5d %8f %8f %8f\n', iter, i, cur_frac, best_thresh, search_range);
  best_coarse_thresh = best_thresh;
  iter = iter + 1;
end

thresh = best_thresh;
v_thresh = zeros(size(v));
v_thresh(find(v > thresh)) = 1;

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: CCC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ccc=ccc(accc, bccc)
% function ccc=ccc(a, b)

mean_accc = mean(accc(:));
mean_bccc = mean(bccc(:));

ccc = sum( (accc(:) - mean_accc) .* (bccc(:) - mean_bccc)) /...
  sqrt(sum((accc(:) - mean_accc).^2) * sum((bccc(:) - mean_bccc).^2));

return;
