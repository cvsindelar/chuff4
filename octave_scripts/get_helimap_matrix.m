function [score_matrix, mini_score_list,overlap_map] = ...
   get_helimap_matrix(data_box,synth_background, refs, ...
                      tm_num, begin_box, end_box)

error_tolerance = 0.0001;
max_overlaps = 10;

size_ref = size(refs);
lib_size = size_ref(3);
n_classes = size_ref(4);

%%%%%%%%%%%%%%%%%%%%%%%
% The 'reference background' ref_background has the current best guess of the filament, 
%  but with the current 2 TM repeats of interest replaced by the "ground state" where 
%  all subunits are set to the first class.
%
% Here "TM repeat" refers to the repeating unit of a filament described by one transfer matrix, i.e.
%  where the first dimension of the matrix corresponds to one TM repeat, and the second dimension
%  corresponds to the next sequential TM repeat. Note that a TM repeat may be larger than the true
%  asymmetric unit of a filament.
%%%%%%%%%%%%%%%%%%%%%%%

[overlap_map] = get_overlap_map(data_box,synth_background, refs, error_tolerance, max_overlaps);

mini_score_list = get_mini_score_list(overlap_map, data_box, synth_background, refs);

'...mini_score_list done'
fflush(stdout);

score_matrix = get_full_score_matrix(mini_score_list, overlap_map, n_classes,...
                                     tm_num, begin_box, end_box, 1);
