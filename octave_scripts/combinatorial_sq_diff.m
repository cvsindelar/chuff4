function [decorate] = ...
   combinatorial_sq_diff(inbox,background, particles, n_kins_to_find, ...
                    max_overlap, tot_overlap_map, overlap_file,real_box_num)
stdout = 1;

box = inbox;
std_dev = std(box(:));
avg = mean(box(:));
% box = (box - avg)/std_dev;

%%%%%%%%%%%%%%%%%%%%%%
%  Do an exhaustive correlation search between the experimental "box" image and all combinations
%   of the "particles" superposed on a "background" image.
%%%%%%%%%%%%%%%%%%%%%%

overlap_thresh = 0.5;

%%%%%%%%%%%%%%%%%%%%%%
% Generate overlap map, denotes which particles overlap with each other
%%%%%%%%%%%%%%%%%%%%%%

size_ref = size(particles);
n = size_ref(3);

if(prod(size(tot_overlap_map)) ~= 0)
  overlap_map = squeeze(tot_overlap_map(:,:,real_box_num));
else
  temp_map = zeros(n,n);
  temp_map1 = zeros(n,n);
  temp_map2 = zeros(n,n);
  overlap_map = zeros(n,n);

  fprintf(stdout,'Computing overlap map:\n');
  if(isoctave)
    fflush(stdout);
  end

  progressbar = 0;
  oj = 1 + floor(2*max_overlap/2);
  for i=1:n
    for j=1:2*max_overlap
       if(i+j-oj >= 1 && i+j-oj <= n)
%         temp_map(i, j) = ccc(abs(particles(:,:,i)), abs(particles(:,:,i+j-oj))) > overlap_thresh;
         map1 = background;
	 map2 = background + particles(:,:,i);
	 map3 = background + particles(:,:,i+j-oj);
	 map4 = background + particles(:,:,i+j-oj) + particles(:,:,i);
         temp_map1(i, j) = sum(sum((map2 - map1).^2));
         temp_map2(i, j) = sum(sum((map4 - map3).^2));
	 if( (temp_map1(i, j) / temp_map2(i, j) > max_percent_error)
           temp_map(i, j) = 1;
         end
       end
    end
    overlap_map(i,1:prod(size(find(temp_map(i,:))))) = ...
      find(temp_map(i,:)) + i - oj;
    if(i/n > progressbar)
      fprintf(stdout, '%3.0f%% ', 100*progressbar);
      if(isoctave)
        fflush(stdout);
      end
      progressbar = progressbar + 0.1;
    end
  end
  appendSPIDERvol(overlap_file,overlap_map);
end

