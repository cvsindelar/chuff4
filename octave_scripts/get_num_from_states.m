function n = get_num_from_states(states, n_states)
% This function converts a base-'n_states' number to the unique index 'n'
index = 0 ;
n = 1;
len = prod(size(states));
for index = 1:len
  n = n + n_states.^(index-1) * (states(index)-1);
end
