if(nargin < 2)
  fprintf('PROGRAMMING ERROR: wrong number of arguments to octave script! (%d)\n', nargin);
%  exit(2)
end

output_directory = argv(){1}
output_header = argv(){2};
bin_factor = str2num(argv(){3});
num_pfs = str2num(argv(){4});
num_starts = str2num(argv(){5});
chumps_round = str2num(argv(){6});
box_dim = str2num(argv(){7});
num_args = 7;

num_mts = nargin - num_args;

output_stack = sprintf('%s/frealign_image_stack.spi',output_directory);
frealign_parameter_file = sprintf('%s/%s_0.par',output_directory,output_header);

if(prod(size(stat(output_stack))) != 0)
  fprintf(stdout, 'ERROR: stack file exists already: "%s"\n', output_stack);
  return
end

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m';

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;
final_pixel_size = scanner_pixel_size*10000/target_magnification*bin_factor;

if(box_dim <= 0)
  box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);

%%%%%%%%%%%%                                                                                    
% Make the dimension closer to a power of 2                                               
%%%%%%%%%%%%                                                                                  
  granularity = 2**(floor(log(box_dim)/log(2))-2);
  if(granularity < 8)
    granularity = 8;
  end

  box_dim = granularity*ceil(box_dim/granularity);
end

box_dim = box_dim*bin_factor;
box_dim

filter = ifftn(ones(box_dim));
lp_freq = 0.5/bin_factor;
delta_lp_freq = lp_freq/10;
filter = SharpFilt(filter,lp_freq,delta_lp_freq);
filter = fftn(filter);

tot_n_particles = 1;
scale_avg = 0;
n_scale_avg = 0;
tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

f = fopen(frealign_parameter_file, 'w');
fclose(f);
% ctf_doc = dlmread('ctf_files/ctf_docfile_for_frealign.spi');

for i = 1: num_mts

  f = fopen(frealign_parameter_file, 'a');

%%%%%%%%%%%%%%%
% Read in filenames, etc.
%%%%%%%%%%%%%%%

  job_dir = argv(){i+num_args};

  if(job_dir(prod(size(job_dir))) == '/')
    job_dir=job_dir(1:prod(size(job_dir))-1);
  end
  [start] = regexp(job_dir,'[^/]*$','start');
  job_dir = job_dir(start:prod(size(job_dir)));

  box_file_name = sprintf('scans/%s.txt',job_dir);

  mt_type_info = dlmread(sprintf('chumps_round%d/%s/selected_mt_type.txt',chumps_round,job_dir));
  cur_num_pfs = mt_type_info(1);
  cur_num_starts = mt_type_info(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process only MT's of the selected type (for now)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(num_pfs == cur_num_pfs && num_starts == cur_num_starts)

  radon_scale = dlmread(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));
  index = find(radon_scale(:,1) == 1);
  radon_scale = radon_scale(index(prod(size(index))),3);
  scale_avg = scale_avg + radon_scale;
  n_scale_avg = n_scale_avg + 1;

  [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
  micrograph_name = sprintf('scans/%s.mrc',job_dir(1:start-1));
  ctf_doc = dlmread(sprintf('scans/%s_ctf_doc.spi',job_dir(1:start-1)));

  micrograph_number = job_dir(1:start-1);
  [start] = regexp(micrograph_number,'[1-9][0-9]*$','start');
  micrograph_number = str2num(micrograph_number(start:prod(size(micrograph_number))));

  align_doc_name = ...
    sprintf('chumps_round%d/%s/individual_align_doc_pf%02d_start%d.spi',...
            chumps_round,job_dir,cur_num_pfs,cur_num_starts);

  good_align_doc_name = ...
    sprintf('chumps_round%d/%s/guess_individual_align_doc_pf%02d_start%d_good.spi',...
            chumps_round,job_dir,cur_num_pfs,cur_num_starts)

  need_to_skip = 0;
  fileinfo = dir(good_align_doc_name);
good_align_doc_name

  if(isempty(fileinfo))
    need_to_skip = 1;
  else
    if(fileinfo.bytes == 0)
      need_to_skip = 1;
    end
  end

  if(need_to_skip == 1)
    continue
  end

  align_doc = readSPIDERdoc_dlmlike(align_doc_name);
  good_align_doc = readSPIDERdoc_dlmlike(good_align_doc_name);

  n_particles = size(good_align_doc);
  n_particles = n_particles(1);
  fprintf(stdout,'%s %4d %7d', box_file_name, n_particles, tot_n_particles);

%%%%%%%%%%%%%%%
% Read in particles
%%%%%%%%%%%%%%%

  for j=1:n_particles
    eulers = good_align_doc(j,3:5);
    box_num = good_align_doc(j,1);

    index = find(align_doc(:,1) == box_num);
    index = index(prod(size(index)));
    extra_inplane_rot = align_doc(index,3);
    coords = align_doc(index,12:13);
    shifts = align_doc(index,5:6);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Using SPIDER, we have discovered shifts that center the particles 
%  following a rotation by extra_inplane_rot *clockwise* when viewing the x,y axes
%  in "math" view (i.e. with the origin in the lower left). 
% To determine shifts to apply in the absence of the extra_inplane_rot, we therefore
%  map the transformed coordinate system of the applied shifts (after rotation)
%  back to the unrotated coordinate system. The mapping gives x,y axes that 
%  are rotated *counterclockwise* by extra_inplane_rot (in math view). 
% Thus, the shifts are modified by a rotation matrix corresponding to this counterclockwise
%  rotation, which is [cos(r) -sin(r); sin(r) cos(r)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%

    r = extra_inplane_rot*pi/180;

    shifts = [cos(r)*shifts(1)-sin(r)*shifts(2) ...
              sin(r)*shifts(1)+cos(r)*shifts(2)];

    psi = eulers(1) - extra_inplane_rot;
    theta = eulers(2);
    phi = eulers(3);

    tilt_scale_avg = tilt_scale_avg + 1/sin(theta*pi/180);
    n_tilt_scale_avg = n_tilt_scale_avg + 1;

%%%%%%%%%%%%%%%%%%
% Read in boxed image, after re-centering the coordinates using the found
%  coords
%%%%%%%%%%%%%%%%%%

    coords = coords - shifts;
    int_coords = round(coords);
    frac_coords = coords - int_coords;

    box_origin = int_coords-floor(box_dim/2);
    [box,mrcinfo,msg] = ReadMRCwin(micrograph_name, box_dim, box_origin, 3);

    if(box != -1)
      box = invert_density * real(ifftn(filter.*fftn(box).*FourierShift(box_dim(1), -frac_coords)));
      box = BinImageCentered(box, bin_factor);
      std_dev = std(box(:));
      avg = mean(box(:));
      box = (box - avg)/std_dev;

      if(tot_n_particles == 1)
        writeSPIDERfile(output_stack,box);
      else
        appendSPIDERvol(output_stack,box);
      end

      index = find(ctf_doc(:,1) == 1);
      if(prod(size(index)) == 0)
        fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
        return
      end
      index = index(prod(size(index)));
      defocus1 = ctf_doc(index,3);
      defocus2 = ctf_doc(index,4);
      astig_angle = ctf_doc(index,5);

      presa = 50.0;
      dpres = 50.0;
      fprintf(f, ...
             '%7d%8.2f%8.2f%8.2f%8.2f%8.2f%8.0f%6d%9.1f%9.1f%8.2f%7.2f%6.2f\n', ...
             tot_n_particles, psi, theta, phi, 0, 0, ...
             target_magnification/bin_factor, micrograph_number, ...
             defocus1, defocus2, astig_angle, presa, dpres);
      tot_n_particles = tot_n_particles + 1;
    end
  end
  fprintf(stdout,' %7d\n', tot_n_particles-1);
else
  fprintf(stdout,'%s [skipped; %4d protofilaments, %4d starts]\n',...
          box_file_name, cur_num_pfs, cur_num_starts);
end

fclose('all');

end % for i = 1: num_mts

scale_avg = scale_avg/n_scale_avg
tilt_scale_avg = tilt_scale_avg/n_tilt_scale_avg

micrograph_pixels_per_repeat = ...
  helical_repeat_distance/(scale_avg*tilt_scale_avg)/micrograph_pixel_size

%%%%%%%%%
% Now, guestimate symmetry parameters for refinement and reconstruction - 
%  ideally we would do this from the data...
%%%%%%%%%
[twist_per_subunit, rise_per_subunit, mt_radius, axial_repeat_dist, twist_per_repeat] = mt_lattice_params(num_pfs, num_starts/2, 80, 112);

info_file = sprintf('%s/info.txt',output_directory);
f = fopen(info_file,'w');
fprintf(f,'output_header %s\n', output_header);
fprintf(f,'bin_factor %d\n',bin_factor);
fprintf(f,'micrograph_pixels_per_repeat %8.3f\n',micrograph_pixels_per_repeat);
fprintf(f,'voxel_size %8.3f\n',final_pixel_size);
fprintf(f,'estimated_repeat_distance %8.3f\n',micrograph_pixels_per_repeat*micrograph_pixel_size);
fprintf(f,'helical_twist %8.3f\n',twist_per_subunit*180/pi);
fprintf(f,'num_pfs %d\n',num_pfs);
fprintf(f,'num_starts %d\n',num_starts);
fclose(f);
