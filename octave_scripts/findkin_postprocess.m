chumps_round = str2num(argv(){1});
job_dir = argv(){2}
job_dir = trim_dir(job_dir);

input_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full.spi',chumps_round,job_dir);
sorted_map = readSPIDERfile(input_file);

[seam_mask, pf_offset] = find_seams(sorted_map);

if(exist(sprintf('chumps_round%d/%s/pf_offset.txt', chumps_round, job_dir)) != 2)
  fp = fopen(sprintf('chumps_round%d/%s/pf_offset.txt', chumps_round, job_dir), 'w');
  fprintf(fp, '%d\n', pf_offset);
else
  fprintf(stdout, 'NOTE: pf_offset has already been determined for this microtubule');
end

output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_clean.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,sorted_map.*seam_mask);

s = size(sorted_map);

tmap = sorted_map;
tmap(:,2:2:s(2)) = 0;
output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_even.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,tmap);

tmap = sorted_map;
tmap(:,1:2:s(2)) = 0;
output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_odd.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,tmap);

dimer_map = find_dimers(sorted_map.*seam_mask);
lat_dimer_map = find_lateral_dimers(sorted_map.*seam_mask);

output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_dimer.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,dimer_map);
output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_lat_dimer.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,lat_dimer_map);

outmat = zeros(s(1),7);
outmat(:,1) = 1:s(1);
outmat(:,2) = sum(sorted_map(:,1:2:s(2)),2)/(s(2)/2);
outmat(:,3) = sum(sorted_map(:,2:2:s(2)),2)/(s(2)/2);

input_file = sprintf('chumps_round%d/%s/fmate_find/sorted_map_full.spi',chumps_round,job_dir);
if(fopen(input_file) > 0)

  sorted_map_hidf = readSPIDERfile(input_file);
  sorted_map_composite = sorted_map.*sorted_map_hidf;

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_map_composite);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_clean.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_map_hidf.*seam_mask);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_clean.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_map_composite.*seam_mask);

  dimer_map = find_dimers(sorted_map_hidf.*seam_mask);
  lat_dimer_map = find_lateral_dimers(sorted_map_hidf.*seam_mask);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,dimer_map);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_lat_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,lat_dimer_map);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_map_hidf);

  tmap = sorted_map_composite;
  tmap(:,2:2:s(2)) = 0;
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_even.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tmap);

  tmap = sorted_map_composite;
  tmap(:,1:2:s(2)) = 0;
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_odd.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tmap);

  dimer_map = find_dimers(sorted_map_composite.*seam_mask);
  lat_dimer_map = find_lateral_dimers(sorted_map_composite.*seam_mask);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,dimer_map);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_lat_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,lat_dimer_map);

  outmat(:,4) = sum(sorted_map_hidf(:,1:2:s(2)),2)/(s(2)/2);
  outmat(:,5) = sum(sorted_map_hidf(:,2:2:s(2)),2)/(s(2)/2);
  outmat(:,6) = sum(sorted_map_composite(:,1:2:s(2)),2)/(s(2)/2);
  outmat(:,7) = sum(sorted_map_composite(:,2:2:s(2)),2)/(s(2)/2);
end

output_file = sprintf('chumps_round%d/%s/find_kinesin/decoration_plot.txt',chumps_round,job_dir);
dlmwrite(output_file,outmat,' ');

output_file = sprintf('chumps_round%d/%s/find_kinesin/decoration_plot.txt',chumps_round,job_dir);
dlmwrite(output_file,outmat,' ');
