function random_dimers(s_final,thresh,false_pos_rate,false_neg_rate)

s = s_final;
m = zeros(s);
r = rand(s);

m(find(r < thresh)) = 1;

m_final = zeros(s);

for row = 1:2:s(2)-2
for pf = 1:s(1)
  if(m(pf,row))
    if(!m(pf,row+2))
      m_final(pf,row) = 1;
      m_final(pf,row+2) = 1;
    end
  end
end
end
sum(sum(m_final))/prod(size(m_final))
writeSPIDERfile('sorted_map_random_dimer.spi',m_final);

r = rand(s);
m_final(find(r < false_pos_rate)) = 1;
r = rand(s);
m_final(find(r < false_neg_rate)) = 0;

writeSPIDERfile('sorted_map_random_dimer_error.spi',m_final);
