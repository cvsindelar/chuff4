function [decorate] = ...
   iter_combinatorial_cc(box,background, particles, n_kins_to_find, ...
                    max_overlap, tot_overlap_map, overlap_file,real_box_num)
%%%%%%%%%%%%%%%%%%%%%%
%  Do an exhaustive correlation search between the experimental "box" image and all combinations
%   of the "particles" superposed on a "background" image.
%%%%%%%%%%%%%%%%%%%%%%

overlap_thresh = 0.3;

%%%%%%%%%%%%%%%%%%%%%%
% Generate overlap map, denotes which particles overlap with each other
%%%%%%%%%%%%%%%%%%%%%%

size_ref = size(particles);
n = size_ref(3);

if(prod(size(tot_overlap_map)) ~= 0)
  overlap_map = squeeze(tot_overlap_map(:,:,real_box_num));
else
  temp_map = zeros(n,n);
  overlap_map = zeros(n,n);

  fprintf(stdout,'Computing overlap map:\n');
  fflush(stdout);
  
  progressbar = 0;
  oj = 1 + floor(2*max_overlap/2);
  for i=1:n
    for j=1:2*max_overlap
       if(i+j-oj >= 1 && i+j-oj <= n)
         temp_map(i, j) = ccc(abs(particles(:,:,i)), abs(particles(:,:,i+j-oj))) > overlap_thresh;
       end
    end
    overlap_map(i,1:prod(size(find(temp_map(i,:))))) = ...
      find(temp_map(i,:)) + i - oj;
    if(i/n > progressbar)
      fprintf( '%3.0f%% ', 100*progressbar);
      fflush(stdout);
      progressbar = progressbar + 0.1;
    end
  end
  appendSPIDERvol(overlap_file,overlap_map);
end

%%%%%%%%%%%%%%%%%%%%%%
% Search for the binding pattern
%%%%%%%%%%%%%%%%%%%%%%

fprintf(stdout,'\nFinding kinesin:\n');
fflush(stdout);

%debugio = fopen('debug_find.txt', 'w');

decorate = zeros(n,1);

progressbar = 0;

oi = floor(n/2) - floor(n_kins_to_find/2);
for index=1:n_kins_to_find
  i = index + oi;
  fflush(stdout);
%  fprintf(debugio, '%d\n', i);
%  fprintf(stdout, '%d\n', i);
  best_ccc = -1;

  max_overlap_count = max(find(overlap_map(i,:) ~= 0));

  mask = particles(:,:,i);
  mask(find(mask ~= 0)) = 1;

  last_j = 0;
  for j=0:2^max_overlap_count-1
    test_map = background;
    bitwise_changes = bitxor(j,last_j);
    for k=1:max_overlap_count
%%%%%%%%%%%%%%%%%
% The following bit of code computes the difference between
%  the current test map and the previous one, and adds or subtracts
%  the appropriate references.
%%%%%%%%%%%%%%%%%
      if(bitand(bitwise_changes,2^(k-1)))
        if(bitand(j,2^(k-1)))
          test_map = test_map + particles(:,:,overlap_map(i,k));
        else
          test_map = test_map - particles(:,:,overlap_map(i,k));
        end
      end
      if(bitand(j,2^(k-1)))
%        fprintf(debugio, '1');
%        fprintf(stdout, '1');
      else
%        fprintf(debugio, '0');
%        fprintf(stdout, '0');
      end

      if(overlap_map(i,k) == i)
        if(bitand(j,2^(k-1)))
          i_occupied = 1;
        else
          i_occupied = 0;
        end
      end
    end

    cur_ccc = ccc(test_map(find(mask)), box(find(mask)));

%    fprintf(debugio, ' %f\n', cur_ccc);
%    fprintf(stdout, ' %f', cur_ccc);
    if(cur_ccc > best_ccc)
%      fprintf(stdout, '*\n');
      best_ccc = cur_ccc;
      best_j = j;
      decorate(i) = i_occupied;
    else
%      fprintf(stdout, '\n');
    end
  end
  if(decorate(i) == 1)
%      fprintf(stdout, '*\n');
    else
%      fprintf(stdout, '\n');

    last_j = j;
  end
%  writeSPIDERfile(sprintf('findref%03d.spi',i),particles(:,:,i));

  fflush(stdout);
  if(index/n_kins_to_find > progressbar)
    fprintf( '%3.0f%% ', 100*progressbar);
    fflush(stdout);
    progressbar = progressbar + 0.1;
  end
end
fprintf(stdout,'\n');
%fclose(debugio);
