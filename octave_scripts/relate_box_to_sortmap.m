function [box_bounds, sorted_indices] = ...
  relate_box_to_sortmap(box_map, tot_decorate_map, ...
                     box_num, input_box_repeat_bounds, box_repeat_origin, ...
                     num_pfs, sites_per_tot_repeat,...
                     repeat_index, pf_index, pad_sort_map)

if(nargin < 10)
  pad_sort_map = 3;
end
                     
kin_lib_size = prod(size(box_map));

filament_polarity = 1;

%%%%%%%%%%%%%%%%%%%%
% Note below: directional_psi is now ensured to be 270, i.e. there are no backwards filaments!
%if(directional_psi == 270)
%  filament_polarity = 1;
%else
%  filament_polarity = -1;
%end

% The following would center place the central subunit of a repeat at the origin, as opposed to
%     lib_origin_offset = 0, which places the first subunit at the origin:
% lib_origin_offset = -floor(sites_per_tot_repeat/2);

lib_origin_offset = 0;

[box_bounds, tot_bounds] = ...
   box_to_tot_findmap(box_map, input_box_repeat_bounds, ...
              box_repeat_origin, ...
              tot_decorate_map, box_num, sites_per_tot_repeat)

sorted_repeat_index = repeat_index(box_bounds(1):box_bounds(2))
sorted_pf_index = pf_index(box_bounds(1):box_bounds(2));

%%%%%%%%%%%%%%%%%%%%%%%%
% 'sorted_repeat_index' labels which repeat a given member of the unsorted input map
%  belongs to, noting the following:
%  - the first repeat is numbered 1, not 0
%  - sites_per_repeat may be 2*num_pfs, rather than num_pfs, if alpha and beta tubulin
%     are being treated separately
%  - the sorted map is padded at the beginning (and end) by 'pad_sort_map' tubulin dimer
%    repeats
%%%%%%%%%%%%%%%%%%%%%%%%

sorted_repeat_index = sorted_repeat_index + ...
    (pad_sort_map + box_num - box_repeat_origin) * sites_per_tot_repeat/num_pfs

sorted_indices = num_pfs * (sorted_repeat_index - 1) + sorted_pf_index;
