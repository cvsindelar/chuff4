% 
% DCTSHIFT Shift a signal. 
%   OUT = DCTSHIFT(INP, P) generates P-shifted sinc-interpolated
%   signal. The sinc-interpolation is done in DCT domain.   
%
%   If INP is a matrix DCTSHIFT shifts columns of INP by P.
%
%   The length of the columns of INP must be a power of two. P is a
%   arbitrary scalar. 
%
%   See also DCTROTATE

% Copyright (c) 2002 by Antti Happonen, happonea@cs.tut.fi

function out = dctshift(inp, p)
  
  inp = double(inp);
  % Sinc interpolation (shifting) in DCT domain for the columns: 
  out = fastinterp(inp, 1, 1, p, 3, 0, 0, 0);
  