function image_pad = dct_pad(image,strip_dim)

s = size(image);

if(nargin < 2)
  strip_dim = floor(s/2);
end

pad_dim = s + 2*strip_dim;
image_pad = zeros(pad_dim);

image_pad(strip_dim(1)+1:strip_dim(1)+s(1),strip_dim(2)+1:strip_dim(2)+s(2)) = image;

image_pad(1:strip_dim(1),strip_dim(2)+1:strip_dim(2)+s(2)) = ...
  image(strip_dim(1):-1:1,1:s(2));
image_pad(strip_dim(1)+s(1)+1:s(1)+2*strip_dim(1),strip_dim(2)+1:strip_dim(2)+s(2)) = ...
  image(s(1):-1:s(1)-strip_dim(1)+1,1:s(2));

image_pad(strip_dim(1)+1:strip_dim(1)+s(1),1:strip_dim(2)) = ...
  image(1:s(1),strip_dim(2):-1:1);
image_pad(strip_dim(1)+1:strip_dim(1)+s(1),strip_dim(2)+s(2)+1:s(2)+2*strip_dim(2)) = ...
  image(1:s(1),s(2):-1:s(2)-strip_dim(2)+1);

image_pad(1:strip_dim(1),1:strip_dim(2)) = ...
  image(strip_dim(1):-1:1,strip_dim(2):-1:1);

image_pad(1:strip_dim(1),strip_dim(2)+s(2)+1:s(2)+2*strip_dim(2)) = ...
  image(strip_dim(1):-1:1,s(2):-1:s(2)-strip_dim(2)+1);

image_pad(strip_dim(1)+s(1)+1:s(1)+2*strip_dim(1),1:strip_dim(2)) = ...
  image(s(1):-1:s(1)-strip_dim(1)+1,strip_dim(2):-1:1);

image_pad(strip_dim(1)+s(1)+1:s(1)+2*strip_dim(1),strip_dim(2)+s(2)+1:s(2)+2*strip_dim(2)) = ...
  image(s(1):-1:s(1)-strip_dim(1)+1,s(2):-1:s(2)-strip_dim(2)+1);
