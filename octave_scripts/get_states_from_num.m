function states = get_states_from_num(n, n_states, len)
% This function converts 'n' to a base-'n_states' number, returned as an array of digits
index = 0 ;
num = n-1;
states = zeros([1 len]);
for index = 1:len
  remainder = mod(num,n_states) ;  % assume n_states > 1
  num       = floor(num / n_states) ;  % integer division
  states(index) = remainder + 1;
end
