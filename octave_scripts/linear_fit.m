function params = linear_fit(x,y)
% params = linear_fit(x,y)
% params = [CalcSlope CalcIntercept];
% Least-squares computation without polyfit function.

NumPoints = prod(size(x));
Sxx=sum((x-mean(x)).^2);
Syy=sum((y-mean(y)).^2);
Sxy=sum((x-mean(x)).*(y-mean(y)));
CalcSlope=Sxy./Sxx;
CalcIntercept=mean(y)-CalcSlope*mean(x);
Sy=sqrt((Syy-CalcSlope^2*Sxx)/(NumPoints-2));
SDSlope=Sy/sqrt(Sxx);
SDIntercept=Sy*sqrt(1./(NumPoints-(sum(x).^2)./sum(x.^2)));

params = [CalcSlope CalcIntercept];
