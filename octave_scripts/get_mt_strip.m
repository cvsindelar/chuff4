function [strip_ref_kin,strip_psi,strip_x,strip_y] = ...
           get_mt_strip(strip_min,strip_max,psi_tub,x_tub,y_tub,ref_kin);

kin_lib_size = prod(size(psi_tub));

psi_tub_monomer1 = psi_tub(1:2:kin_lib_size);
x_tub_monomer1 = x_tub(1:2:kin_lib_size);
y_tub_monomer1 = y_tub(1:2:kin_lib_size);

mt_norm_vec = zeros([2 kin_lib_size/2]);

mt_norm_vec(1,:) = cos(-psi_tub_monomer1+pi/2);
mt_norm_vec(2,:) = sin(-psi_tub_monomer1+pi/2);
ind = find(mt_norm_vec(1,:).*x_tub_monomer1 + mt_norm_vec(2,:).*y_tub_monomer1 >= strip_min & ...
           mt_norm_vec(1,:).*x_tub_monomer1 + mt_norm_vec(2,:).*y_tub_monomer1 <= strip_max);

complete_ind = 2*ind - 1;

srk = size(ref_kin);
strip_ref_kin = zeros([srk(1) srk(2) 2*prod(size(complete_ind))]);
strip_psi = zeros([1 2*prod(size(complete_ind))]);
strip_x = zeros([1 2*prod(size(complete_ind))]);
strip_y = zeros([1 2*prod(size(complete_ind))]);

for j=1:prod(size(complete_ind))
%%%%%%%%%%
% Get the matching tubulin monomer that is translated by 40A
%%%%%%%%%%
  strip_ref_kin(:,:,2*j-1) = ref_kin(:,:,complete_ind(j));
  strip_psi(2*j-1) = psi_tub(complete_ind(j));
  strip_x(2*j-1) = x_tub(complete_ind(j));
  strip_y(2*j-1) = y_tub(complete_ind(j));

  strip_ref_kin(:,:,2*j) = ref_kin(:,:,complete_ind(j)+1);
  strip_psi(2*j) = psi_tub(complete_ind(j)+1);
  strip_x(2*j) = x_tub(complete_ind(j)+1);
  strip_y(2*j) = y_tub(complete_ind(j)+1);
end

return

