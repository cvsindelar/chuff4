function helimap_mt_reconstruct(run_dir,job_dir,helimap_round, nuke, user_debug)
stdout = 1;

if(nargin < 5)
  user_debug = 0;
end

global debug test_mode;

debug = user_debug;

global vol_array numerator_array denominator_array subunit_array

if(nargin < 3)
  nuke = 0;
end

notebook_dir = '15_06_13_tests'
%%%%%%%%%%%%%%%%%%%%%
% Testmode: 'perfect','perfect_nosigma2', 'wedgetest_sinc','wedgetest_interp', 'perfect_sinc', 'perfect_interp', 'sinc','interp','sinc_sigma2','interp_sigma2'
% In test runs, 'nosigma2' has been used for all except 'perfect' and 'interp_sigma2' and 'sinc_sigma2'

testmode = 'sinc'
interpmode = 'sinc'
sigma2mode = 'sigma2'

job_dir = trim_dir(job_dir);

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end

invert_density = invert_density;

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read the filament list
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filament_list = textread(sprintf('%s/filament_list.txt', run_dir), '%s');
n_filaments = prod(size(filament_list));

filament_index = [];
for ind = 1:n_filaments
  if(isequal(filament_list{ind}, job_dir))
    filament_index = ind;
    break;
  end
end

if(isempty(filament_index))
  printf('ERROR: cannot ID the filament: %s\n', job_dir);
  exit(2)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load parameters defined by the first refinement step
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[prev_helimap_dir, prev_reference_dir, prev_filament_dir] = ...
  save_helimap_alignment(run_dir, helimap_round-1, job_dir);

load(sprintf('%s/info.mat', run_dir));
load(sprintf('%s/fil_info.mat', prev_filament_dir));

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% helimap_exclusions = get_helimap_exclusions(num_pfs, num_starts, est_repeat_distance,...
%                                             coords, ref_com, ...
%                                             phi, theta, psi, ...
%                                             d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat);
% 
% dlmwrite('helimap_exclusions.txt', helimap_exclusions', ' ');
% writeSPIDERfile('helimap_exclusions.spi', helimap_exclusions);
% 
% min_exclusion_dist = 10;  % in Angstroms
% 
% helimap_exclusions = helimap_exclusions < min_exclusion_dist;
% writeSPIDERfile('helimap_exclusions_binary.spi', helimap_exclusions);
% %%%%%%%%%%%%%%%%%%%%%%%%%

[helimap_dir, reference_dir, filament_dir] = ...
  save_helimap_alignment(run_dir, helimap_round, job_dir, filament_index);

%%%%%%%%%%%%%%%%%%%%%%%
% 'Perfect' case using whole MT volumes generated synthetically:
%%%%%%%%%%%%%%%%%%%%%%%
if(helimap_round == 1 && ...
    (isequal(test_mode,'perfect') || isequal(test_mode,'perfect_nosigma2')))
  printf('Using perfect references... skipping the reconstruction step.\n');
  exit(0);
end

for igold=1:2
for ind=1:n_classes
  vol_name = sprintf('%s/ref_helimap_class%d_gold%d.spi', reference_dir, ind,igold);
  if(exist(vol_name) && !nuke)
    printf('Reference volume "%s" exists already... exiting.', vol_name);
    exit(0)
  end      
end
end

n_good_boxes = prod(size(phi));

%%%%%%%%%%%%%%%%%%
% Get micrograph info and compute the CTF's
%%%%%%%%%%%%%%%%%%

[micrograph_name,defocus,astig_mag,astig_angle,b_factor, find_dir] = ...
  chumps_micrograph_info(job_dir,focal_mate,chumps_round);

%%%%%%%%%%%%%%%%%%%%%%%
% Read in sigma2
%%%%%%%%%%%%%%%%%%%%%%%

if(helimap_round == 1)
  sigma2_recon = ones(recon_pad_factor*ref_dim);
  sigma2_recon = repmat(sigma2_recon, [1 1 2]);
else
  sigma2_recon = readSPIDERfile(sprintf('%s/sigma2_recon_gold1.spi',prev_filament_dir));
  sigma2_recon = repmat(sigma2_recon, [1 1 2]);
  sigma2_recon(:,:,2) = readSPIDERfile(sprintf('%s/sigma2_recon_gold2.spi',prev_filament_dir));
end

%%%%%%%%%%%%%%%%%%%%%%%
% Make reference volumes
%%%%%%%%%%%%%%%%%%%%%%%

if(small_subunit_dim)
  [wedge, adjusted_ref_com, adjusted_eulers, wedge_xform] = ...
    normalized_mt_wedge_mask(subunit_dim(1), num_pfs,num_starts,ref_com,...
                             est_repeat_distance_13pf, ref_pixel,...
                             min_resolution,filament_outer_radius);
end

[wedge, adjusted_ref_com, adjusted_eulers] = ...
  normalized_mt_wedge_mask(ref_dim(1), num_pfs,num_starts,ref_com,...
                           est_repeat_distance_13pf, ref_pixel,...
                           min_resolution,filament_outer_radius);

if(helimap_round == 1 && ...
   (isequal(test_mode,'perfect2') || isequal(test_mode,'perfect2_nosigma2')))
%%%%%%%%%%%%%%%%%%%%%%%
% 'Perfect' case using whole MT volumes generated synthetically
%%%%%%%%%%%%%%%%%%%%%%%

  if(filament_index != 1)
    return
  end

  vol_array = zeros([ref_dim(1) ref_dim(1) ref_dim(1) n_classes 2]);
  fsc_array = ones([ref_dim(1)/2 n_classes]);
  sigma2_array = zeros([ref_dim(1)/2 n_classes 2]);
  inv_tau2_array = zeros([ref_dim(1)/2 n_classes 2]);

  for iclass=1:n_classes
    for igold=1:2
      vol_name = sprintf('%s/vol_class%d_gold%d_perfect.spi', ...
                         reference_dir, iclass, igold);
      vol_array(:,:,:,iclass,igold) = readSPIDERfile(vol_name);
    end
  end

else

%%%%%%%%%%%%%%%%%%%%%%%
% Ordinary case: reconstruct MT volumes
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%
% The following variable is saved globally to save on data transfer times for large volumes
%  vol_array = ...
%%%%%%%
  reconstruct_mt_accum(reference_dir, filament_index, n_filaments, micrograph_name, ...
                             num_pfs, num_starts, est_repeat_distance_13pf,...
                             coords, ref_com, phi, theta, psi, ...
                             d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                             bin_factor, tot_helimap, goldmap, repmat([1; 0],[1 n_good_boxes]), ...
                             recon_pad_factor, sigma2_recon, use_ctf, defocus, astig_mag, astig_angle);

  if(filament_index != 1)
    return
  end

  fprintf(stdout, 'Symmetrizing the reconstructed volumes:\n');
  fflush(stdout);

  symmetrize_helimap_vols(num_pfs,num_starts,ref_com,est_repeat_distance_13pf, ref_pixel, ...
                          filament_outer_radius);

  for class_num=1:n_classes
    for igold=1:2
      vol_name = sprintf('%s/vol_class%d_gold%d_symm.spi',reference_dir, class_num, igold);
      writeSPIDERfile(vol_name, vol_array(:,:,:,class_num, igold));
    end
  end

  printf('Wienerizing volumes...\n');
  fflush(stdout);
  [fsc_array sigma2_array inv_tau2_array] = wienerize_helimap_vols(recon_pad_factor);

  printf('Sorting volumes...\n');
  fflush(stdout);
  sort_helimap_vols(wedge);
end
%%%%%%%%%%%%%%%%%%%%%%%
% End of MT reconstruction step
%%%%%%%%%%%%%%%%%%%%%%%

inv_adjusted_eulers = -[adjusted_eulers(3) adjusted_eulers(2) adjusted_eulers(1)]

subunit_array = zeros([subunit_dim n_classes 2]);

for igold = 1:2
for class_num = 1:n_classes

  if(small_subunit_dim)
   if(isequal(interpmode,'sinc'))
     subunit_array(:,:,:,class_num,igold) = ...
         sqrt(abs(wedge_xform)) .* ...
         rotate3d(window_vol(vol_array(:,:,:,class_num,igold) .* sqrt(abs(wedge)),...
                             [subunit_dim], ...
                             1+adjusted_ref_com/ref_pixel + ...
                               floor([ref_dim(1:2) ref_dim(1)]/2) - floor(subunit_dim/2),...
                             0,1.25,'sinc'),...
                  180/pi*inv_adjusted_eulers);
   elseif(isequal(interpmode,'interp'))
     subunit_array(:,:,:,class_num,igold) = ...
         sqrt(abs(wedge_xform)) .* ...
         ERotate3(window_vol(vol_array(:,:,:,class_num,igold).*sqrt(abs(wedge)),...
                             [subunit_dim], ...
                             1+adjusted_ref_com/ref_pixel + ...
                               floor([ref_dim(1:2) ref_dim(1)]/2) - floor(subunit_dim/2),...
                             'interp'),...
                  -[inv_adjusted_eulers(3) inv_adjusted_eulers(2) inv_adjusted_eulers(1)]);
    end
  else
    subunit_array(:,:,:,class_num,igold) = ...
      vol_array(:,:,:,class_num,igold).*wedge;
  endif 

end % n_classes
end % igold

save_helimap_ref_vols(run_dir, helimap_round, fsc_array, sigma2_array, inv_tau2_array, nuke);

return
