function [twist_per_subunit, rise_per_subunit, mt_radius,axial_repeat_dist,twist_per_repeat] = mt_lattice_params(num_pfs,  helix_start_num, dimer_repeat_dist, radius13pf)
% function [twist_per_subunit, rise_per_subunit, mt_radius,axial_repeat_dist,twist_per_repeat] = mt_lattice_params(num_pfs,  helix_start_num, dimer_repeat_dist, radius13pf)
%%%%%%%%%%
% Note that helix_start_num is the number of starts in terms of tubulin monomers,
%  so it can assume values as multiples of 0.5
%%%%%%%%%% 

%%%%%%%%%%
% total_pitch is the angle formed, after "unrolling" the microtubule onto a plane, between
%  an equatorial line and the path of lateral tubulin contacts.
%%%%%%%%%%
total_pitch13 = atan2(3/2*dimer_repeat_dist, 2*pi*radius13pf);

%%%%%%%%%%
% w_pf is the width of the tubulin along the path of lateral contacts
%%%%%%%%%%
w_pf = 2*pi*radius13pf/(13*cos(total_pitch13));

%%%%%%%%%%
% The next two formulas come from Chretien and Wade, Biol. Cell. 71:161-174 (1991)
% delta_n is the lattice mismatch that needs to be corrected by adjusting total_pitch13
%%%%%%%%%%
delta_n = 3/2 * dimer_repeat_dist * num_pfs / 13 - helix_start_num * dimer_repeat_dist;
total_pitch = total_pitch13 - atan2(delta_n, num_pfs * w_pf * cos(total_pitch13));

mt_radius = num_pfs * w_pf * cos(total_pitch13) / (2*pi);
axial_repeat_dist = cos(total_pitch-total_pitch13)*dimer_repeat_dist;

% theta_pf = pi/2 - total_pitch13;
% d = helix_start_num * dimer_repeat_dist * sin(theta_pf)/cos(total_pitch);

twist_per_subunit = w_pf * cos(total_pitch) / mt_radius;
rise_per_subunit = w_pf * sin(total_pitch);

% twist_per_repeat = dimer_repeat_dist*sin(total_pitch - total_pitch13) / mt_radius;
twist_per_repeat = (twist_per_subunit * num_pfs - 2*pi) / helix_start_num;

% printf('twist %9f rise %9f prototwist %9f \n',twist_per_subunit*180/pi,rise_per_subunit,protofilament_twist*180/pi);

return
