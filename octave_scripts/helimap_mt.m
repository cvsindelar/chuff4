function [tot_helimap] = helimap_mt(job_dir,bin_factor, n_classes, use_ctf, helimap_round, ...
                                    chumps_round, focal_mate)
debug = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(debug == 0) % debug1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

stdout = 1;

%%%%%%%%%%%%%%%%%%%%%
% Testmode: 'perfect','perfect_nosigma2', 'wedgetest_sinc','wedgetest_interp', 'perfect_sinc', 'perfect_interp', 'sinc','interp','sinc_sigma2','interp_sigma2'
% In test runs, 'nosigma2' has been used for all except 'perfect' and 'interp_sigma2' and 'sinc_sigma2'

run_dir = 'helimap_v0'

notebook_dir = '15_06_15_tests'
testmode = 'sinc'
testmode = 'perfect'
interpmode = 'sinc'
sigma2mode = 'sigma2'
if(!mkdir(sprintf('%s',notebook_dir)))
  printf('ERROR: cannot create directory "%s"\n',notebook_dir);
  exit(2)
end
if(!mkdir(sprintf('%s/%s',notebook_dir,testmode)))
  printf('ERROR: cannot create directory "%s/%s"\n',notebook_dir, testmode);
  exit(2)
end

if(nargin < 6)
  use_ctf = 1;
end
use_ctf

if(n_classes <= 0)
  n_classes = 2;
end

recon_pad_factor = 1;
min_resolution = 16;

[header, subunit_dim] = ...
  readSPIDERheader(sprintf('chumps_round%d/ref_kin_subunit_bin%d/kin_centered_vol.spi', ...
                           chumps_round, bin_factor));

% highpass_resolution = 200;
highpass_resolution = 1e9

job_dir = trim_dir(job_dir);

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
tub_ref_file = sprintf('%s/ref_tot.spi',ref_dir);

ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,bin_factor);
kin_ref_file = sprintf('%s/ref_tot.spi',ref_dir);

elastic_params = [0 0 0 0];
radius_scale_factor = 1;
monomer_offset = 0;

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end

invert_density = invert_density;

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

[micrograph_name,defocus,astig_mag,astig_angle,b_factor, find_dir] = ...
  chumps_micrograph_info(job_dir,focal_mate,chumps_round);

[num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
  est_repeat_distance] = ...
  read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);

sites_per_repeat = num_pfs;

ref_pixel = dlmread(sprintf('%s/info.txt',ref_dir));
ref_pixel = ref_pixel(2);

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
com_info = readSPIDERdoc_dlmlike(com_doc_name);

index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

[coords,phi,theta,psi,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance] = ...
  fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_pix_per_repeat, num_pfs, num_starts, ref_com);

est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

%%%%%%%%%%%%%%%%%%
% Initialize file-related parameters
%%%%%%%%%%%%%%%%%%

n_good_boxes = prod(size(phi));

fprintf(stdout,'%s %4d\n', job_dir, n_good_boxes);

bin_factor = round(ref_pixel/micrograph_pixel_size);
final_pixel_size = micrograph_pixel_size*bin_factor;

%%%%%%%%%%%%%%%%%%%%%%%
% load the reference images
%%%%%%%%%%%%%%%%%%%%%%%

global subunit_array

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
ref_pixel = dlmread(sprintf('%s/info.txt',ref_dir));
ref_pixel = ref_pixel(2);

com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
com_info = readSPIDERdoc_dlmlike(com_doc_name);
index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

box_dim = ceil(1.5*2*filament_outer_radius/ref_pixel);
box_dim = 16*ceil(box_dim/16);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);

subunit_dim = 2.3 * (filament_outer_radius - abs(sqrt(sum(ref_com.^2,2)))) / ref_pixel;
subunit_dim = 16*ceil(subunit_dim/16);

subunit_dim = [subunit_dim(1) subunit_dim(1) subunit_dim(1)]

%[subunit_array, box_dim, ref_dim, ref_pixel, ref_com] = ...
%  get_helimap_ref_vols(chumps_round, micrograph_pixel_size, filament_outer_radius, bin_factor,...
%                       [],subunit_dim, n_classes);

pad_ref_dim = 4*ref_dim;

%%%%%%%%%%%%%%%%%%%%%%%
% Compute CTF's
%%%%%%%%%%%%%%%%%%%%%%%

electron_lambda = EWavelength(accelerating_voltage);

if(defocus > 0 && use_ctf ~= 0)
  ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                       spherical_aberration_constant, ...
                       b_factor, amplitude_contrast_ratio, 0);
  ctf = ifftshift(ctf);

  box_ctf = CTF_ACR(2*box_dim(1), micrograph_pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  box_ctf = ifftshift(box_ctf);
else
  ctf = ones(pad_ref_dim);
  box_ctf = ones(2*box_dim);
end

ctf_flip = sign(ctf);
box_ctf_flip = sign(box_ctf);

%%%%%%%%%%%%%%%
% Pre-compute filtering terms
%%%%%%%%%%%%%%%

box_filter = ifftn(ones(2*box_dim));
% lp_freq = 0.5*(2*micrograph_pixel_size) / filter_resolution;
% delta_lp_freq = lp_freq/10;
% box_filter = SharpFilt(box_filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*micrograph_pixel_size) / highpass_resolution;
delta_hp_freq = hp_freq/10;
box_filter = SharpHP(box_filter,hp_freq,delta_hp_freq);

box_filter = fftn(box_filter);

%%%%%%%%%%%%%%%%%%%%%%%
% Make reference volumes and micrograph
%%%%%%%%%%%%%%%%%%%%%%%

tot_helimap = zeros(num_pfs, n_good_boxes, n_classes);
findmap_file = sprintf('%s/tot_decorate_map.spi',find_dir);
tot_decorate_map = readSPIDERfile(findmap_file);
s = size(tot_decorate_map);
tot_helimap(:,:,1) = 1-tot_decorate_map(1:2:s(1),1:s(2));
tot_helimap(:,:,2) = tot_decorate_map(1:2:s(1),1:s(2));

% if(fopen(sprintf('%s/%s/repmat2_%s_%s.binmat',notebook_dir,testmode,job_dir,testmode)) > 0)
%if(fopen(sprintf('vol_array_%s.binmat',job_dir)) > 0)
%   'loading data'
%   load (sprintf('%s/%s/repmat2_%s_%s.binmat',notebook_dir,testmode,job_dir,testmode));
%  load(sprintf('vol_array_%s.binmat',job_dir));
% else

if(isequal(testmode,'wedgetest_interp') || isequal(testmode,'wedgetest_sinc'))
'#######DEBUG'
  vol_array = ones([ref_dim(1) ref_dim(1) ref_dim(1) 2]);
elseif(isequal(testmode,'perfect') || isequal(testmode,'perfect_nosigma2'))
'#######DEBUG'
  vol_array = zeros([ref_dim(1) ref_dim(1) ref_dim(1) 2]);
  vol_array(:,:,:,1) = readSPIDERfile('vol_array1_perfect.spi');
  vol_array(:,:,:,2) = readSPIDERfile('vol_array2_perfect.spi');
else
%%%%%%%%%%%%%%%%%%%
% NOTE: need to fix code so we can use est_repeat_distance instead of helical_repeat_distance
%%%%%%%%%%%%%%%%%%%

'debug'
  vol_array = reconstruct_mt(micrograph_name, num_pfs, num_starts, helical_repeat_distance,...
                             coords, ref_com, phi, theta, psi, ...
                             d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                             bin_factor, tot_helimap, repmat([1; 0],[1 n_good_boxes]), ...
                             recon_pad_factor, use_ctf, defocus, astig_mag, astig_angle);
'debug'

end

%%%%%%%%%%%%%%%%%%%
% Level the volume background so it is zero
%  DOESN'T WORK at the moment because reconstructed volume is off-kilter:
%  It puts the subunit of interest in the middle!
%%%%%%%%%%%%%%%%%%%

[x y z] = ndgrid(-floor(ref_dim(1)/2):-floor(ref_dim(1)/2)+ref_dim(1)-1,...
                 -floor(ref_dim(2)/2):-floor(ref_dim(2)/2)+ref_dim(2)-1,...
                 -floor(ref_dim(1)/2):-floor(ref_dim(1)/2)+ref_dim(1)-1);
% zero at element n/2+1.

R = repmat(sqrt(x.^2 + y.^2),[1 1 1 n_classes]);

background_pix = find(R <   0.5 + filament_outer_radius/ref_pixel &...
                      R >= -0.5 + filament_outer_radius/ref_pixel);
%avg_background = sum(vol_array(background_pix))/prod(size(background_pix))
%vol_array = vol_array - avg_background;

save -binary 'debug.binmat'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
  load debug.binmat
end % debug1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
% NOTE: need to fix code so we can use est_repeat_distance instead of helical_repeat_distance
%%%%%%%%%%%%%%%%%%%
'debug'
  [wedge, adjusted_ref_com, adjusted_eulers, wedge_no_xform] = ...
     normalized_mt_wedge_mask(ref_dim(1), num_pfs,num_starts,ref_com,...
                              helical_repeat_distance, final_pixel_size,...
                              min_resolution,filament_outer_radius);

%  small_wedge = ...
%     normalized_mt_wedge_mask(subunit_dim(1), num_pfs,num_starts,ref_com,...
%                              helical_repeat_distance, final_pixel_size,...
%                              min_resolution,filament_outer_radius);

  save('-binary', sprintf('%s/%s/vol_array_%s_%s.binmat',notebook_dir,testmode,job_dir,testmode), 'vol_array', 'wedge', 'adjusted_ref_com', 'adjusted_eulers');

inv_adjusted_eulers = -[adjusted_eulers(3) adjusted_eulers(2) adjusted_eulers(1)]

vol_array(:,:,:,1) = SharpFilt(vol_array(:,:,:,1),0.1,0.01);
vol_array(:,:,:,2) = SharpFilt(vol_array(:,:,:,2),0.1,0.01);

% wedge = ones(size(wedge));
subunit_dim
  for i = 1:n_classes
%    subunit_array(:,:,:,i) = ...
%       realfilter .* ...
%       rotate3d(window_vol(wedge .* ...
%                           window_vol(rotate3d(vol_array(:,:,:,i), adjusted_eulers),...
%                                      size(wedge), ...
%                                     1-adjusted_ref_com/final_pixel_size,0,1),
%                           [subunit_dim],...
%                           1+adjusted_ref_com/final_pixel_size + ...
%                             floor(size(wedge)/2) - floor(subunit_dim/2),0,1), ...
%                180/pi*inv_adjusted_eulers);

   if(isequal(interpmode,'sinc'))
      subunit_array(:,:,:,i) = ...
         rotate3d(window_vol(vol_array(:,:,:,i).*wedge_no_xform,...
                             [subunit_dim], ...
                             1+adjusted_ref_com/final_pixel_size + ...
                               floor([ref_dim(1:2) ref_dim(1)]/2) - floor(subunit_dim/2),...
                             0,1.25,'sinc'),...
                  180/pi*inv_adjusted_eulers);
    elseif(isequal(interpmode,'interp'))
      subunit_array(:,:,:,i) = ...
         ERotate3(window_vol(vol_array(:,:,:,i).*wedge_no_xform,...
                             [subunit_dim], ...
                             1+adjusted_ref_com/final_pixel_size + ...
                               floor([ref_dim(1:2) ref_dim(1)]/2) - floor(subunit_dim/2),...
                             'interp'),...
                  -[inv_adjusted_eulers(3) inv_adjusted_eulers(2) inv_adjusted_eulers(1)]);
      end
  end

if(isequal(testmode,'perfect') || isequal(testmode,'perfect_nosigma2'))
  subunit_array = [];
  exit;
end

%[subunit_array, box_dim, ref_dim, ref_pixel, ref_com] = ...
%  get_helimap_ref_vols(chumps_round, micrograph_pixel_size, filament_outer_ra%dius, bin_factor,...
%                       [],subunit_dim,n_classes);

writeSPIDERfile(sprintf('%s/%s/subunit1_%s.spi',notebook_dir,testmode,testmode), squeeze(subunit_array(:,:,:,1)));
writeSPIDERfile(sprintf('%s/%s/subunit2_%s.spi',notebook_dir,testmode,testmode), squeeze(subunit_array(:,:,:,2)));

save_helimap_ref_vols(run_dir, helimap_round);

% subunit_array(:,:,:,1) = readSPIDERfile(sprintf('%s/%s/subunit1_%s.spi',notebook_dir,testmode,testmode));;
% subunit_array(:,:,:,2) = readSPIDERfile(sprintf('%s/%s/subunit2_%s.spi',notebook_dir,testmode,testmode));

%  [synth_boxes, boxes, norm_params_vol, norm_params_image,synth_micrograph] = ...
%    replicate_helimap(num_pfs, num_starts, coords(42:44,:),phi(42:44),theta(42:44),psi(42:44),...
%                      d_phi_d_repeat(42:44),d_theta_d_repeat(42:44),d_psi_d_repeat(42:44),est_repeat_distance,...
%                      use_ctf,job_dir,focal_mate,chumps_round,...
%                      bin_factor,min_resolution, ...
%                      tot_helimap,0);

  [synth_boxes, boxes, norm_params_vol, norm_params_image,synth_micrograph] = ...
    replicate_helimap(run_dir,job_dir,helimap_round,...
                      num_pfs, num_starts, coords,phi,theta,psi,...
                      d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                      est_repeat_distance,...
                      use_ctf,focal_mate,chumps_round,...
                      bin_factor,min_resolution, ...
                      tot_helimap,0);

save('-binary', sprintf('%s/%s/debug_sigma2_%s.binmat',notebook_dir,testmode,testmode), 'boxes', 'synth_boxes', 'pad_ref_dim', 'norm_params_image', 'coords', 'phi', 'theta', 'psi', ...
                    'd_phi_d_repeat',  'd_theta_d_repeat',  'd_psi_d_repeat',  'elastic_params', ...
                    'ref_pixel', 'helical_repeat_distance', 'num_pfs', 'num_starts', ...
                    'ref_com', 'min_resolution');

if(!isequal(sigma2mode,'nosigma2'))
  sigma2 = get_sigma2(boxes,synth_boxes,pad_ref_dim,norm_params_image,coords,phi,theta,psi,...
                      d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat, elastic_params,...
                      ref_pixel,helical_repeat_distance,num_pfs,num_starts,...
                      ref_com,min_resolution);
else
  sigma2 = ones(pad_ref_dim);
end

  save('-binary', sprintf('%s/%s/repmat2_%s_%s.binmat',notebook_dir,testmode,job_dir,testmode), 'synth_micrograph', 'sigma2', 'synth_boxes', 'boxes', 'norm_params_vol', 'norm_params_image','vol_array', 'wedge', 'adjusted_ref_com', 'final_pixel_size', 'ref_dim', 'tot_helimap', 'subunit_array', 'subunit_dim','sigma2');

%for tm_num=begin_box:end_box+1
for tm_num=38:48

  fprintf(stdout,'\nTransfer matrix # %4d/%4d : \n', tm_num, end_box+1);
  if(isoctave())
    fflush(stdout);
  end

  if(tm_num <= end_box)
    box_num = tm_num;
  else
    box_num = end_box;
  end

%  box = boxes(:,:,box_num);

  wi_x = 1+floor(pad_ref_dim(1)/2)-floor(ref_dim(1)/2);
  wi_y = 1+floor(pad_ref_dim(2)/2)-floor(ref_dim(2)/2);

  box = read_chumps_box(micrograph_name,box_num,coords,pad_ref_dim*bin_factor,bin_factor);
  box = invert_density * box;
  box = norm_params_image(1,box_num)*box + norm_params_image(2,box_num);
%writeSPIDERfile(sprintf('%s/%s/bo%d_%s.spi',notebook_dir,testmode,box_num,testmode),...
%                box(wi_x:wi_x+ref_dim(1)-1,wi_y:wi_y+ref_dim(2)-1));

box_no_sigma2 = apply_sigma2(box, ones(pad_ref_dim), ref_dim);
writeSPIDERfile(sprintf('%s/%s/bo%d_%s.spi',notebook_dir,testmode,box_num,testmode),...
                box_no_sigma2);
%box_no_sigma2 = box;

  box = apply_sigma2(box, sigma2, ref_dim);

writeSPIDERfile(sprintf('%s/%s/bo%d_sigma2_%s.spi',notebook_dir,testmode,box_num,testmode),box);

  synth_box = copy_chumps_box(synth_micrograph,box_num,coords,pad_ref_dim,1,1);
%writeSPIDERfile(sprintf('%s/%s/synth_bo%d_%s.spi',notebook_dir,testmode,box_num,testmode),...
%                synth_box(wi_x:wi_x+ref_dim(1)-1,wi_y:wi_y+ref_dim(2)-1));

synth_box_no_sigma2 = apply_sigma2(synth_box, ones(pad_ref_dim), ref_dim);
writeSPIDERfile(sprintf('%s/%s/synth_bo%d_%s.spi',notebook_dir,testmode,box_num,testmode),...
                synth_box_no_sigma2);

%synth_box_no_sigma2 = synth_box;

  synth_box = apply_sigma2(synth_box, sigma2, ref_dim);

writeSPIDERfile(sprintf('%s/%s/synth_bo%d_sigma2_%s.spi',notebook_dir,testmode,box_num,testmode),synth_box);

%%%%%%%%%%%%%%%%%%
% Generate the reference images
% NOTE: need to fix code so we can use est_repeat_distance instead of helical_repeat_distance
%%%%%%%%%%%%%%%%%%
'debug'
  [box_bounds, tot_bounds, sorted_indices,...
          x_final, y_final, z_final, phi_final, theta_final, psi_final,...
          phi_mt, theta_mt, psi_mt, refs, synth_box_bkg, refs_no_sigma2, bkg_no_sigma2] = ...  
   get_helimap_boxrefs(tm_num, ref_dim,num_pfs,num_starts,...
                    coords, phi, theta, psi, elastic_params,...
                    d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                    helical_repeat_distance,radius_scale_factor,ref_com,...
                    ctf,sigma2,...
                    ref_pixel,bin_factor,tot_helimap);
'debug'

% save -binary 'debug_bkg.binmat' synth_box_bkg synth_box refs

synth_box_bkg = synth_box - synth_box_bkg;
bkg_no_sigma2 = synth_box_no_sigma2 - bkg_no_sigma2;

if(isequal(testmode,'wedgetest_sinc') || isequal(testmode,'wedgetest_interp'))
  return
end

 [m, mini_score_list,omap] = ...
     get_helimap_matrix(box,synth_box_bkg, sigma2, refs, ...
                        tm_num, begin_box, end_box);

  if(isempty(whos('tm_raw')))
    global tm_raw = zeros([size(m) n_good_boxes]);
  end

  tm_raw(:,:,tm_num) = m;

  [i,j] = find(m == min(m(:)));
  get_states_from_num( i(1),n_classes,num_pfs)
  get_states_from_num( j(1),n_classes,num_pfs)

  fflush(stdout);
end

if(1 == 1)

save('-binary', sprintf('%s/%s/helimapfilt_%s_%s.binmat',notebook_dir,testmode,job_dir,testmode), 'new_helimap', 'tm_raw', 'num_pfs', 'n_classes', 'm', 'mini_score_list', 'omap', 'box', 'synth_box', 'synth_box_bkg', 'refs', 'tm_num', 'sigma2');;

%save('-binary', sprintf('%s/%s/helimap_%s_%s.binmat',notebook_dir,testmode,job_dir,testmode), 'new_helimap', 'tm_raw', 'num_pfs', 'n_classes', 'm', 'mini_score_list', 'omap', 'box', 'synth_box','synth_box_bkg', 'refs', 'tm_num', 'sigma2');;

new_helimap = get_helimap_class_scores(tm_raw,num_pfs,n_classes);

save('-binary', sprintf('%s/%s/helimapfilt_%s_%s.binmat',notebook_dir,testmode,job_dir,testmode), 'new_helimap', 'tm_raw', 'num_pfs', 'n_classes', 'm', 'mini_score_list', 'omap', 'box', 'synth_box','synth_box_bkg', 'refs', 'tm_num', 'sigma2');;

%save('-binary', sprintf('%s/%s/helimap_%s_%s.binmat',notebook_dir,testmode,job_dir,testmode), 'new_helimap', 'tm_raw', 'num_pfs', 'n_classes', 'm', 'mini_score_list', 'omap', 'box', 'synth_box','synth_box_bkg', 'refs', 'tm_num', 'sigma2');;

end

return
