function [score_matrix] = ...
   get_helimap_matrix_exact(data_box,synth_background, refs, tm_num, begin_box, end_box)

size_ref = size(refs);
lib_size = size_ref(3);
n_classes = size_ref(4);

if(tm_num == begin_box || tm_num == end_box + 1)
  n_subunits = lib_size;
else
  n_subunits = lib_size/2;
end

score_matrix = zeros([n_classes^n_subunits n_classes^n_subunits]);

%%%%%%%%%%%%%%%%%%%%%%%
% The 'reference background' ref_background has the current best guess of the filament, 
%  but with the current 2 TM repeats of interest replaced by the "ground state" where 
%  all subunits are set to the first class.
%
% Here "TM repeat" refers to the repeating unit of a filament described by one transfer matrix, i.e.
%  where the first dimension of the matrix corresponds to one TM repeat, and the second dimension
%  corresponds to the next sequential TM repeat. Note that a TM repeat may be larger than the true
%  asymmetric unit of a filament.
%%%%%%%%%%%%%%%%%%%%%%%

progressbar = 0;

for ind=1 : n_classes^lib_size
  state_vec = get_states_from_num(ind,n_classes,lib_size);

  ref_box = synth_background;
  for k=1:lib_size
   ref_box = ref_box + refs(:,:,k,state_vec(k));
  end

  if(tm_num == begin_box)
    ;
  elseif(tm_num == end_box + 1)
    ;
  else
    i = get_num_from_states(state_vec(1:lib_size/2), n_classes);
    j = get_num_from_states(state_vec(lib_size/2+1:lib_size), n_classes);

    score_matrix(i,j) = sum(sum((ref_box - data_box).^2)) * prod(size(data_box));
  end

  if(ind/(n_classes^lib_size) > progressbar)
    fprintf(stdout, '%3.0f%% (%d)', 100*progressbar, ind);
    if(isoctave)
      fflush(stdout);
    end
    progressbar = progressbar + 0.01;
  end
end
