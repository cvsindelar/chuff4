function [phi_deform, r_deform, coords, rots] = map_elastic(phis,elastic_params)
% function [phi_deform, r_deform] = map_elastic(phis,mode2_amp,mode2_dphi,mode3_amp, mode3_dphi)

mode2_amp = elastic_params(1);
mode2_dphi = elastic_params(2);
mode3_amp = elastic_params(3);
mode3_dphi = elastic_params(4);

n_points = 10000;

t = 0:2*pi/n_points:2*pi;
ux = cos(t);
uy = sin(t);
dux = -sin(t);
duy = cos(t);

order = 2;
u2 = mode2_amp * cos(t*order-mode2_dphi*pi/180);
v2 = mode2_amp/order * sin(t*order-mode2_dphi*pi/180);
order = 3;
u3 = mode3_amp * cos(t*order-mode3_dphi*pi/180);
v3 = mode3_amp/order * sin(t*order-mode3_dphi*pi/180);

x = ux + (u2+u3).*ux + (v2+v3).*dux;
y = uy + (u2+u3).*uy + (v2+v3).*duy;

dL = sqrt(diff(x).^2 + diff(y).^2);
% Circularize the dL's
dL(n_points) = sqrt((x(1)-x(n_points))^2 + (y(1)-y(n_points))^2);
% Use cumtrapz numerical integration routine to compute the length of the deformed curve
L = cumtrapz(dL);

% Normalize the deformed coordinates so the curve length is 2*pi
r_adjust = 2*pi/L(n_points);
L = r_adjust*L;
x = r_adjust*x;
y = r_adjust*y;

for i=1:size(phis(:))
  current_L = mod(phis(i), 2*pi);
  j2 = find(L >= current_L, 1);
  phi = mod(atan2(y(j2),x(j2)),2*pi);
  coords(i,1) = x(j2);
  coords(i,2) = y(j2);
  rots(i) = phi;  
  r = sqrt(x(j2)^2 + y(j2)^2);
  phi_deform(i) = phi;
  r_deform(i) = r;

end

%%%%%%%%%%%%%%%%
% Idea for increased accuracy; currently doesn't work because of numerical error from trapz
%  if(j2 == 1)
%    j1 = n_points;
%  else
%    j1 = j2-1;
%  end
%  delta = 0 ; % (L(j2)-current_L)/dL(j2);
%  phi1 = mod(atan2(y(j1),x(j1)),2*pi);
%  r1 = sqrt(x(j1)^2 + y(j1)^2);
%  phi2 = mod(atan2(y(j2),x(j2)),2*pi);
%  r2 = sqrt(x(j2)^2 + y(j2)^2);
%  phi_deform(i) = delta*phi1 + (1-delta)*phi2;
%  r_deform(i) = delta*r1 + (1-delta)*r2;
%%%%%%%%%%%%%%%%

