function vol = make_synthetic_filament_volume(invol, vol_size, ref_com_in_APIX, helical_twist, helical_rise_in_pixel, n_repeat, angles)
% function vol = make_synthetic_filament_volume(invol, vol_size, ref_com_in_APIX, helical_twist, helical_rise_in_pixel, n_repeats, angles)
%
% make synthetic filament volume 
%
% invol: the centered volume
% vol_size: the output vol_size
% ref_com_in_APIX: the center of the volume 
% helical_twist, helical_rise_in_pixel: the helical parameter
% n_repeat: the repeat of subunts we want
% angles: the euler angles from conventional z direction filament
%
[x_sub y_sub z_sub phi_sub theta_sub psi_sub...
    x y z phi theta psi origin_repeat] = filament_parametric(n_repeat, ref_com_in_APIX, helical_twist, helical_rise_in_pixel, [angles(1) 0], [angles(2) 0], [angles(3) 0]);

% size of original volume
s1 = size(invol);
s1_2 = s1 / 2;
c_invol = s1 / 2 + 1;
% new size
s2 = [vol_size vol_size vol_size];
vol = zeros(s2);
c_vol = s2 / 2 + 1;
if n_repeat * helical_rise_in_pixel > vol_size
    n_repeat = floor(vol_size / helical_rise_in_pixel) - 2
end
ang_sub_tmp = ang_sub / pi * 180
% loop throught repeats
for i = 1 : n_repeat
    % rotate the invol and translate by fraction shift
    coords_sub = [x_sub(i) y_sub(i) z_sub(i)];
    shft = coords_sub - floor(coords_sub));
    ang = [phi_sub(i) theta_sub(i) psi_sub(i)];
    new_vol = TransformVol(invol, ang, shft);
    shft = floor(coords_sub);
    new_coords = shft + c_vol;
    % get the window in output volume
    rl = new_coords - s1_2;
    ru = new_coords + s1_2 - 1;
    vol(rl(1):ru(1),rl(2):ru(2),rl(3):ru(3)) += new_vol;
end
