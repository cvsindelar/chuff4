function helimap_mt_refine(run_dir, job_dir,helimap_round, nuke, user_debug, test_mode)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following variables are global for the listed reasons:
%
%  'tm_raw' can be very large, so we want to avoid passing copies of it to functions via
%  matlab's data-passing convention (can't use C-type aliases)
% 
% 'debug' is just a flag we want any function to know about, without having to pass it all the
%  time.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global tm_raw debug test_mode;

debug = user_debug;

stdout = 1;

job_dir = trim_dir(job_dir);

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%%%
% The following lines create results directories, and check if any data 
%  was saved previously. Nuke=1 will override previous results, else error exit.
%%%%%%%%%%%%%%%%%%%%
[helimap_dir, reference_dir, filament_dir] = ...
  save_helimap_alignment(run_dir, helimap_round, job_dir);

fil_info_file = sprintf('%s/fil_info.mat',filament_dir);
if(exist(fil_info_file) && !nuke)
  printf('ERROR: output "%s" exists already...', fil_info_file);
  exit(2)
end

[previous_helimap_dir, reference_dir, previous_filament_dir] =  ...
   save_helimap_alignment(run_dir, helimap_round-1, job_dir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Restore parameters from values used in the previous round.
%  These are:
% 'num_pfs','num_starts',...
% 'coords','phi','theta','psi', ...
% 'd_phi_d_repeat','d_theta_d_repeat','d_psi_d_repeat', ...
% 'est_repeat_distance_13pf', ...
% 'elastic_params', 'radius_scale_factor', ...
% 'tot_helimap', 'goldmap'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load(sprintf('%s/info.mat', run_dir));
load(sprintf('%s/fil_info.mat', previous_filament_dir));

n_good_boxes = prod(size(phi));

fprintf(stdout,'%s ; Number of boxes:%4d\n', job_dir, n_good_boxes);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now estimate the likelihoods of particle states
%  for each "gold" standard subset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for igold=1:2

%%%%%%%%%%%%%%%%%%
% We skip the subsequent code if there are no matches to igold- it wouldn't even work!
%%%%%%%%%%%%%%%%%%
  if(prod(size(find(goldmap == igold)) == 0))
    continue
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get micrograph info and compute the CTF's
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  [micrograph_name,defocus,astig_mag,astig_angle,b_factor, find_dir] = ...
    chumps_micrograph_info(job_dir,focal_mate,chumps_round);

  pad_ref_dim = ref_pad_factor*ref_dim;

  electron_lambda = EWavelength(accelerating_voltage);

  if(defocus > 0 && use_ctf ~= 0)
    ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                  spherical_aberration_constant, ...
                   b_factor, amplitude_contrast_ratio, 0);
  else
    ctf = ones(pad_ref_dim);
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimate sigma2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if(~exist( sprintf('%s/norm_params_gold%d.mat', filament_dir, igold)) ||
     ~exist(sprintf('%s/%s_synth_gold%d.spi', ...
                    filament_dir, job_dir, igold)))
    [synth_boxes, boxes, norm_params_vol, norm_params_image,synth_micrograph] = ...
      replicate_helimap(igold, goldmap, run_dir, job_dir,helimap_round,...
                        num_pfs, num_starts, coords,phi,theta,psi,...
                        d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                        est_repeat_distance_13pf,pad_ref_dim,pad_ref_dim,ref_pixel,ref_com,...
                        use_ctf,focal_mate,chumps_round,...
                        bin_factor,min_resolution, filament_outer_radius, ...
                        tot_helimap(:,:,:,igold),0, 1);
    writeSPIDERfile(sprintf('%s/%s_synth_gold%d.spi', ...
                    filament_dir, job_dir, igold), synth_micrograph);
    save(sprintf('%s/norm_params_gold%d.mat', filament_dir, igold),'norm_params_image');
    save('-binary',sprintf('%s/synth_boxes_gold%d.binmat', filament_dir, igold),'synth_boxes', 'boxes');
  else
    synth_micrograph = readSPIDERfile(sprintf('%s/%s_synth_gold%d.spi', ...
                                      filament_dir, job_dir, igold));

    load(sprintf('%s/norm_params_gold%d.mat', filament_dir, igold));
    load(sprintf('%s/synth_boxes_gold%d.binmat', filament_dir, igold));
  end

  if(~exist(sprintf('%s/sigma2_recon_gold%d.spi',filament_dir, igold)))
    sigma2_recon = get_sigma2(igold, goldmap, boxes,synth_boxes,recon_pad_factor*ref_dim,...
                        norm_params_image,...
                        coords,phi,theta,psi,...
                        d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat, elastic_params,...
                        ref_pixel,est_repeat_distance,min_resolution, filament_outer_radius);
    writeSPIDERfile(sprintf('%s/sigma2_recon_gold%d.spi',filament_dir, igold), sigma2_recon);
  else
    sigma2_recon = readSPIDERfile(sprintf('%s/sigma2_recon_gold%d.spi',filament_dir, igold));
  end

  if(~exist(sprintf('%s/sigma2_gold%d.spi',filament_dir, igold)))
    sigma2 = get_sigma2(igold, goldmap, boxes,synth_boxes,pad_ref_dim,norm_params_image,...
                        coords,phi,theta,psi,...
                        d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat, elastic_params,...
                        ref_pixel,est_repeat_distance,min_resolution, filament_outer_radius);
    writeSPIDERfile(sprintf('%s/sigma2_gold%d.spi',filament_dir, igold), sigma2);
  else
    sigma2 = readSPIDERfile(sprintf('%s/sigma2_gold%d.spi',filament_dir, igold));
  end
% '%%%%%%%%%%%%%%%%%%%%%%%%%'
% '%%%%%%%%%%%%%%%%%%%%%%%%%'
% 'DEBUG'
% sigma2 = 0.01 * ones(pad_ref_dim);
% '%%%%%%%%%%%%%%%%%%%%%%%%%'
% '%%%%%%%%%%%%%%%%%%%%%%%%%'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the transfer matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  get_helimap_ref_vols(run_dir, helimap_round);

  final_boxes = zeros([ref_dim n_good_boxes+1]);
  final_synth_boxes = zeros([ref_dim n_good_boxes+1]);
  final_synth_bkg = zeros([ref_dim n_good_boxes+1]);
  final_boxes_mask = zeros([ref_dim n_good_boxes+1]);
  final_synth_boxes_mask = zeros([ref_dim n_good_boxes+1]);
  final_synth_bkg_mask = zeros([ref_dim n_good_boxes+1]);

  begin_box = 1;
  end_box = n_good_boxes;

%  if(debug)
%    tm_begin = 43;
%    tm_end = 43;
%
%    temp = apply_sigma2(boxes(:,:,43),ones(size((ctf))) ./ sqrt(2*sigma2));
%    synth_mic2 = ones(size(synth_micrograph)) * ...
%      get_helical_background(temp, ...
%                             psi(43),filament_outer_radius/ref_pixel);
%
%    synth_mic3 = synth_mic2;
%  else
    tm_begin = begin_box;
    tm_end = end_box+1;
%  endif

  for tm_num=tm_begin:tm_end

    fprintf(stdout,'\nTransfer matrix # %4d/%4d : \n', tm_num, end_box+1);
    if(isoctave())
      fflush(stdout);
    end

    if(tm_num <= end_box)
      box_num = tm_num;
    else
      box_num = end_box;
    end

    fprintf(stdout,'  Reading boxes...\n');
    if(isoctave())
      fflush(stdout);
    end

%%%%%%%%%%%%%%%%%%
% Mask to truncate the image/references into their corresponding helical fragments
%%%%%%%%%%%%%%%%%%

    helimask = ...
      get_helimap_mask_simple(ref_dim, ref_pixel, ...
                       tm_num, helical_repeat_distance, coords, ...
                       psi, min_resolution);

    wi_ox = 1+floor(pad_ref_dim(1)/2)-floor(ref_dim(1)/2);
    wi_oy = 1+floor(pad_ref_dim(2)/2)-floor(ref_dim(2)/2);

%%%%%%%%%%%%%%%%%%
%    box = read_chumps_box(micrograph_name,box_num,coords,pad_ref_dim*bin_factor,bin_factor);
%    box = invert_density * box;
%    box = norm_params_image(1,box_num)*box + norm_params_image(2,box_num);
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
% Now: (1) Soften padded box, apply sigma2; 
%      (2) window and apply helical fragment mask
%      (3) normalize so avg value of comparison region is zero 
%         (required for likelihood scoring... ? At the
%            very least, required that the different classes have same normalization value)
%%%%%%%%%%%%%%%%%%%
    box = boxes(:,:,box_num);
    box_no_sigma2 = box(wi_ox:wi_ox+ref_dim(1)-1,wi_oy:wi_oy+ref_dim(2)-1);

    box = soften_box_boundary(box, get_helical_background(box, psi(box_num), ...
                                                          filament_outer_radius/ref_pixel));

    if(!isequal(test_mode, 'perfect_nosigma2') && !isequal(test_mode, 'nosigma2'))
      box = apply_sigma2(box, ones(size(ctf)) ./ sqrt(2*sigma2));
    end
    box = box(wi_ox:wi_ox+ref_dim(1)-1,wi_oy:wi_oy+ref_dim(2)-1);

%%%%%%%%%%%%%%%%%%
%    synth_box = copy_chumps_box(synth_micrograph,box_num,coords,pad_ref_dim,1,1);
%%%%%%%%%%%%%%%%%%
    synth_box = synth_boxes(:,:,box_num);
    synth_box_no_sigma2 = synth_box(wi_ox:wi_ox+ref_dim(1)-1,wi_oy:wi_oy+ref_dim(2)-1);

    synth_box = ...
      soften_box_boundary(synth_box, get_helical_background(box, psi(box_num), ...
                                                            filament_outer_radius/ref_pixel));

    if(!isequal(test_mode, 'perfect_nosigma2') && !isequal(test_mode, 'nosigma2'))
      synth_box = apply_sigma2(synth_box, ifftshift(ctf) ./ sqrt(2*sigma2));
    end
    synth_box = synth_box(wi_ox:wi_ox+ref_dim(1)-1,wi_oy:wi_oy+ref_dim(2)-1);

%%%%%%%%%%%%%%%%%%
% Generate the reference images
% NOTE: need to fix code so we can use est_repeat_distance instead of helical_repeat_distance
% -> DONE.
%%%%%%%%%%%%%%%%%%

    [box_bounds, tot_bounds, sorted_indices,...
            x_final, y_final, z_final, phi_final, theta_final, psi_final,...
            phi_mt, theta_mt, psi_mt, refs, synth_box_bkg, refs_no_sigma2, bkg_no_sigma2] = ...  
     get_helimap_boxrefs(igold, goldmap, tm_num, ref_dim,num_pfs,num_starts,...
                      coords, phi, theta, psi, elastic_params,...
                      d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                      est_repeat_distance_13pf,radius_scale_factor,ref_com,...
                      ctf,sigma2,...
                      ref_pixel,bin_factor,tot_helimap(:,:,:,igold));

    synth_box_bkg = synth_box - synth_box_bkg;

    if(isequal(test_mode,'wedgetest_sinc') || isequal(test_mode,'wedgetest_interp'))
      return
    end

%'%%%%%%%%%%%%%%%%%%%%%%%%%'
%'%%%%%%%%%%%%%%%%%%%%%%%%%'
%'DEBUG2'
%box_mask = box;
%synth_box_mask = synth_box;
%synth_box_bkg_mask = synth_box_bkg;
%'%%%%%%%%%%%%%%%%%%%%%%%%%'
%'%%%%%%%%%%%%%%%%%%%%%%%%%'

    box_mask = SharpFilt(helimask.*box, 0.48, 0.02);
    synth_box_mask = SharpFilt(helimask.*synth_box, 0.48, 0.02);
    synth_box_bkg_mask = SharpFilt(synth_box_bkg.*helimask, 0.48, 0.02);
%    box_mask = box_mask - sum(box_mask(:))/sum(helimask(:).^2);
%    synth_box_mask = synth_box_mask - sum(synth_box_mask(:))/sum(helimask(:).^2);
%    synth_box_bkg_mask = synth_box_bkg_mask - ...
%                           sum(synth_box_mask(:))/sum(helimask(:).^2);
'%%%%%%%%%%%%%%%%%%%%%%%%%'
'%%%%%%%%%%%%%%%%%%%%%%%%%'

    refs_mask = zeros(size(refs));
    n_refs = size(refs);
    n_refs = n_refs(3);
    for class_num=1:n_classes
    for ref_num=1:n_refs
%'%%%%%%%%%%%%%%%%%%%%%%%%%'
%'%%%%%%%%%%%%%%%%%%%%%%%%%'
%'DEBUG3'
%      refs_mask(:,:,ref_num,class_num) = refs(:,:,ref_num,class_num);
%'%%%%%%%%%%%%%%%%%%%%%%%%%'
%'%%%%%%%%%%%%%%%%%%%%%%%%%'

      refs_mask(:,:,ref_num,class_num) = ...
        SharpFilt(refs(:,:,ref_num,class_num).*helimask, 0.48, 0.02);
%%      ref_mask = refs_mask(:,:,ref_num,class_num);
%%      refs_mask(:,:,ref_num,class_num) = refs_mask(:,:,ref_num,class_num) - ...
%%                                         sum(ref_mask(:))/sum(helimask(:).^2);
    end
    end

%    if(debug)
%      synth_mic2 = paste_chumps_box(synth_mic2, box_mask, box_num, coords, ...
%                                    bin_factor, ones(size(box)));
%      synth_mic3 = paste_chumps_box(synth_mic3, synth_box_mask, box_num, coords, ...
%                                    bin_factor, ones(size(box)));
%
%      synth_mic2 = paste_chumps_box(synth_mic2, box_no_sigma2, box_num, coords, ...
%                                    bin_factor, ones(size(box)));
%      synth_mic3 = paste_chumps_box(synth_mic3, synth_box_no_sigma2, box_num, coords, ...
%                                    bin_factor, ones(size(box)));
%    end

%%%%%%%%%%%%%%%%%%
% Generate the transfer matrix
%%%%%%%%%%%%%%%%%%
   [m, mini_score_list,omap] = ...
       get_helimap_matrix(box_mask,synth_box_bkg_mask, refs_mask, ...
                          tm_num, begin_box, end_box);

    if(isempty(tm_raw))
      tm_raw = zeros([size(m) n_good_boxes+1]);
    end

    tm_raw(:,:,tm_num) = m;

    [i,j] = find(m == min(m(:)));
    get_states_from_num( i(1),n_classes,num_pfs)
    get_states_from_num( j(1),n_classes,num_pfs)

    fflush(stdout);

    final_boxes(:,:,tm_num) = box;
    final_synth_boxes(:,:,tm_num) = synth_box;
    final_synth_box_bkg(:,:,tm_num) = synth_box_bkg;

    final_boxes_mask(:,:,tm_num) = box_mask;
    final_synth_boxes_mask(:,:,tm_num) = synth_box_mask;
    final_synth_box_bkg_mask(:,:,tm_num) = synth_box_bkg_mask;

save('-binary', sprintf('%s/%s/helimap1_%s_%s_gold%d.binmat',notebook_dir,test_mode,job_dir,test_mode, igold), 'new_helimap', 'tm_raw', 'num_pfs', 'n_classes', 'm', 'mini_score_list', 'omap', 'box', 'synth_box','synth_box_bkg', 'refs', 'tm_num', 'sigma2', 'box_no_sigma2', 'synth_box_no_sigma2', 'bkg_no_sigma2', 'refs_no_sigma2', 'helimask');

  end % tm_num loop

% if(debug)
%  writeSPIDERfile('synth_mic2.spi',synth_mic2);
%  writeSPIDERfile('synth_mic3.spi',synth_mic3);
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the transfer matrix products to obtain likelihoods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save('-binary', sprintf('%s/%s/helimap2_%s_%s.binmat',notebook_dir,test_mode,job_dir,test_mode), 'new_helimap', 'tm_raw', 'num_pfs', 'n_classes', 'm', 'mini_score_list', 'omap', 'box', 'synth_box', 'synth_box_bkg', 'refs', 'box_mask', 'refs_mask', 'synth_box_bkg_mask', 'tm_num', 'sigma2', 'boxes', 'synth_boxes');;

  new_helimap = get_helimap_class_scores(tm_raw,num_pfs,n_classes);

save('-binary', sprintf('%s/%s/helimap3_%s_%s.binmat',notebook_dir,test_mode,job_dir,test_mode), 'new_helimap', 'tm_raw', 'num_pfs', 'n_classes', 'm', 'mini_score_list', 'omap', 'box', 'synth_box','synth_box_bkg', 'refs', 'tm_num', 'sigma2', 'box_no_sigma2', 'synth_box_no_sigma2', 'bkg_no_sigma2', 'refs_no_sigma2', 'boxes', 'synth_boxes');

  tot_helimap(:,:,:,igold) = new_helimap;

end %igold loop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save the results; we are done!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save_helimap_alignment(run_dir, helimap_round,job_dir, ...
                       nuke, notebook_dir, test_mode,...
                       n_classes, bin_factor, ...
                       focal_mate, use_ctf, chumps_round,...
                       min_resolution, recon_pad_factor, ref_pad_factor,...
                       box_dim, ref_dim, subunit_dim,...
                       ref_pixel, ref_com, filament_inner_radius,...
                       est_pix_per_repeat,num_pfs,num_starts,...
                       coords,phi,theta,psi,...
                       d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat, ...
                       est_repeat_distance_13pf, est_repeat_distance, ...
                       elastic_params, radius_scale_factor, ...
                       tot_helimap, goldmap);

return
