function [ref_tub, ref_kin, kin_repeat_index,kin_pf_index] = ...
   find_kinesin_ml(job_dir,filter_resolution,focal_mate,tubrefs,kinrefs);

%%%%%%%%%%%
%  Each dimension of the transfer matrices will be 2^(max_t_matrix_subunits)
%   or 3^(max_t_matrix_subunits)
%%%%%%%%%%%

max_t_matrix_subunits = 6;
min_subunit_overlap = 2;

stdout = 1;
n_normal_args = 3;

iteration = 1;

use_ctf = 1;
highpass_resolution = 200;

job_dir = trim_dir(job_dir);

mkdir(sprintf('chumps_round1/%s/find_kinesin/',job_dir));
mkdir(sprintf('chumps_round1/%s/fmate_find/',job_dir));

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

[micrograph,defocus,astig_mag,astig_angle,b_factor] = ...
  chumps_micrograph_info(job_dir,focal_mate);

[num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
  est_repeat_distance] = ...
  read_chumps_alignment(job_dir,helical_repeat_distance,1,micrograph_pixel_size);

est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

[coords,phi,theta,psi,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
  fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_pix_per_repeat);

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

tub_ref_file = 'chumps_round1/ref_tub_subunit/ref_tot.spi';
kin_ref_file = 'chumps_round1/ref_kin_subunit/ref_tot.spi';
ref_params_name = 'chumps_round1/ref_tub_subunit/ref_params.spi';
com_doc_name = 'chumps_round1/ref_tub_subunit/tub_cgr_doc.spi';
ctf_doc_name = 'ctf_files/ctf_docfile_for_spider.spi';

n_kins_to_find = 2*num_pfs; % 6*num_pfs;
max_overlap = 2*num_pfs;

% num_repeats = ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
num_repeats = 2;
kin_lib_size = 2*num_repeats*num_pfs;

elastic_params = [0 0 0 0];
radius_scale_factor = 1;

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

if(focal_mate == 0)
  output_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
  micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
else
  output_dir = sprintf('chumps_round1/%s/fmate_find',job_dir);
  micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
end

overlap_file = sprintf('%s/overlap_map.spi',output_dir);

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

com_info = readSPIDERdoc_dlmlike(com_doc_name);

%%%%%%%%%%%%%%%%%%
% Initialize file-related parameters
%%%%%%%%%%%%%%%%%%

n_good_boxes = prod(size(phi));

fprintf(stdout,'%s %4d\n', job_dir, n_good_boxes);

ref_pixel_info = readSPIDERdoc_dlmlike(ref_params_name);

index = find(ref_pixel_info(:,1) == 2);
ref_pixel = ref_pixel_info(index(1),5);

index = find(ref_pixel_info(:,1) == 1);
min_theta = ref_pixel_info(index(1),5);

index = find(ref_pixel_info(:,1) == 3);
d_angle = ref_pixel_info(index(1),3);

index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

bin_factor = round(ref_pixel/micrograph_pixel_size);
final_pixel_size = micrograph_pixel_size*bin_factor;

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
box_dim = 64*ceil(box_dim/64);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);
pad_ref_dim = ref_dim;

%%%%%%%%%%%%%%%%%%%%%%%
% Compute CTF's
%%%%%%%%%%%%%%%%%%%%%%%

electron_lambda = EWavelength(accelerating_voltage);

if(defocus > 0 && use_ctf ~= 0)
  ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                       spherical_aberration_constant, ...
                       b_factor, amplitude_contrast_ratio, 0);
  ctf = ifftshift(ctf);

% ZS FIX
  box_ctf = CTF_ACR(2*box_dim(1), micrograph_pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  box_ctf = ifftshift(box_ctf);
else
  ctf = -ones(pad_ref_dim);
  box_ctf = ones(box_dim);
end

ctf_flip = sign(ctf);
box_ctf_flip = sign(box_ctf);

%%%%%%%%%%%%%%%
% Pre-compute filtering terms
%%%%%%%%%%%%%%%

filter = ifftn(ones(pad_ref_dim));
lp_freq = 0.5*(2*ref_pixel) / filter_resolution;
delta_lp_freq = lp_freq/10;
filter = SharpFilt(filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*ref_pixel) / highpass_resolution;
delta_hp_freq = hp_freq/10;
filter = SharpHP(filter,hp_freq,delta_hp_freq);

filter = fftn(filter);

% ZS Fix!
box_filter = ifftn(ones(2*box_dim));
lp_freq = 0.5*(2*micrograph_pixel_size) / filter_resolution;
delta_lp_freq = lp_freq/10;
box_filter = SharpFilt(box_filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*micrograph_pixel_size) / highpass_resolution;
delta_hp_freq = hp_freq/10;
box_filter = SharpHP(box_filter,hp_freq,delta_hp_freq);

box_filter = fftn(box_filter);

%%%%%%%%%%%%%%%%%%%%%%%
% To save time, the user can pre-load the reference images and pass them to this function
%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < n_normal_args+1)
  tubrefs = readSPIDERfile(tub_ref_file);
end
if(nargin < n_normal_args+2)
  kinrefs = readSPIDERfile(kin_ref_file);
end

%%%%%%%%%%%%%%%%%%%%%%%
% Compute and store information about each 8nm repeat (but not reference images,
%  because of possible large memory requirement)
%%%%%%%%%%%%%%%%%%%%%%%

psi_tub_monomer1 = zeros([kin_lib_size/2 n_good_boxes]);
x_tub_monomer1 = zeros([kin_lib_size/2 n_good_boxes]);
y_tub_monomer1 = zeros([kin_lib_size/2 n_good_boxes]);
psi_tub_monomer2 = zeros([kin_lib_size/2 n_good_boxes]);
x_tub_monomer2 = zeros([kin_lib_size/2 n_good_boxes]);
y_tub_monomer2 = zeros([kin_lib_size/2 n_good_boxes]);

for box_num=1:n_good_boxes

  fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
  if(isoctave)
    fflush(stdout);
  end
  box = read_chumps_box(micrograph,box_num,coords,box_dim,...
                        bin_factor,box_ctf_flip.*box_filter);
  if(box == -1)
    continue
  end

%%%%%%%%%%%%%%%%%%
% Get the reference information
%%%%%%%%%%%%%%%%%%
  [ref_tub, ref_kin, kin_repeat_index, kin_pf_index,...
   x_tub, y_tub, phi_tub, theta_tub, psi_tub] = ...
    get_findkin_refs(box_num, ref_dim,num_repeats,num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,...
                   tubrefs,kinrefs,ctf,ctf_flip,filter,...
                   ref_pixel,min_theta,d_angle,ref_com,0);

%%%%%%
% Odd-numbered coordinates are "alpha" tubulin
%%%%%%
  psi_tub_monomer1(:,box_num) = psi_tub(1:2:kin_lib_size);
  x_tub_monomer1(:,box_num) = x_tub(1:2:kin_lib_size);
  y_tub_monomer1(:,box_num) = y_tub(1:2:kin_lib_size);
%%%%%%
% Even-numbered coordinates are "beta" tubulin
%%%%%%
  psi_tub_monomer2(:,box_num) = psi_tub(2:2:kin_lib_size);
  x_tub_monomer2(:,box_num) = x_tub(2:2:kin_lib_size);
  y_tub_monomer2(:,box_num) = y_tub(2:2:kin_lib_size);
  fclose('all');
end

mt_norm_vec = zeros([2 kin_lib_size/2 n_good_boxes]);

mt_norm_vec(1,:,:) = cos(-psi_tub_monomer1+pi/2);
mt_norm_vec(2,:,:) = sin(-psi_tub_monomer1+pi/2);
lateral_displace = squeeze(mt_norm_vec(1,:,:)).*x_tub_monomer1 + ...
                     squeeze(mt_norm_vec(2,:,:)).*y_tub_monomer1;

%%%%%%%%%%%
% Define the lateral positions of a series of long overlapping "strips" 
%  of the microtubule
%%%%%%%%%%%

[strip_min strip_max] = define_mt_strips(lateral_displace,num_pfs,n_good_boxes,...
                                          max_t_matrix_subunits,min_subunit_overlap);

n_strips = prod(size(strip_min));

%%%%%%%%%%%%%%%%%%%%%%%
% Finally- the kinesin-finding loop
%%%%%%%%%%%%%%%%%%%%%%%

mic_dim = ReadMRCheader(micrograph);

for box_num=1:n_good_boxes

  fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
  if(isoctave)
    fflush(stdout);
  end
  box = read_chumps_box(micrograph,box_num,coords,box_dim,...
                        bin_factor,box_ctf_flip.*box_filter);

  if(box == -1)
    continue;
  end

  box = invert_density * box;

%%%%%%%%%%%%%%%%%%
% Get the reference information for two sequential 8 nm repeats of the microtubule
%%%%%%%%%%%%%%%%%%
  [ref_tub, ref_kin, kin_repeat_index, kin_pf_index,...
   x_tub, y_tub, phi_tub, theta_tub, psi_tub] = ...
    get_findkin_refs(box_num, ref_dim,num_repeats,num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,...
                   tubrefs,kinrefs,ctf,ctf_flip,filter,...
                   ref_pixel,min_theta,d_angle,ref_com);

%%%%%%
% Now define a series of overlapping "strips" to define the transfer matrices
%   This loops write out test images to demonstrate.
%%%%%%
  for strip_num=1:n_strips
    [strip_ref_kin,strip_psi,strip_x,strip_y] = ...
       get_mt_strip(strip_min(strip_num),strip_max(strip_num),psi_tub,x_tub,y_tub,ref_kin);
    writeSPIDERfile(sprintf('strip_test%d.spi',strip_num),sum(strip_ref_kin,3));

    srk = size(strip_ref_kin);
    n_repeat_subunits = srk(3)/2;
    writeSPIDERfile(sprintf('strip_test_back%d.spi',strip_num),...
                     sum(strip_ref_kin(:,:,1:n_repeat_subunits),3));
    writeSPIDERfile(sprintf('strip_test_forward%d.spi',strip_num),...
                     sum(strip_ref_kin(:,:,n_repeat_subunits+1:2*n_repeat_subunits),3));
  end

return

%%%%%%%%%%%%%%%%%%%%%
% Zhiguo - your work should be mostly after here -
%  In the comments below I wrote functions the way I imagine they might look...
%%%%%%%%%%%%%%%%%%%%%

% for strip_num=1:n_strips
%  t_matrix = one_mlmatrix(box,ref_tub,strip_ref_kin,strip_psi,strip_x,strip_y,ref_pixel,...
%                          est_repeat_distance, box_num, n_good_boxes);
%
%  tm_size = size(t_matrix);
%  if(box_num == 1)
%    t_matrices = zeros([tm_size(1) tm_size(2) num_boxes]);
%  end
%
%  t_matrices(1:tm_size(1),1:tm_size(2),box_num) = t_matrix;
% end
  fclose('all');
end

% [pos,modmap,fmap] = mlmatrix_product(t_matrices,tmap,rkin);

return
