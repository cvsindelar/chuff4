%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: get_correct3d()
% Make 'correct3d' point spread function to restore density at volume edges:
%  sinc(x) = inverse FT of rectangle function rect(x)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function correct3d = get_correct3d(r, s)

r_origin = floor(r/2) + 1;
s_origin = floor(s/2) + 1;

[h k l] = ndgrid((1:r(1)) - r_origin(1), (1:r(2)) - r_origin(2), (1:r(3)) - r_origin(3));
radius = sqrt(h.*h + k.*k + l.*l);

temp = sin(pi*h/s(1))./(pi*h/s(1));
temp(r_origin(1),:,:) = 1;
correct3d = temp;
temp = sin(pi*k/s(2))./(pi*k/s(2));
temp(:,r_origin(2),:) = 1;
correct3d = correct3d.*temp;
temp = sin(pi*l/s(3))./(pi*l/s(3));
temp(:,:,r_origin(3)) = 1;
correct3d = correct3d.*temp;
