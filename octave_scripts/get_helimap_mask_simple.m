function helimask = get_helimap_mask_simple(ref_dim, ref_pixel, tm_num, ...
                     repeat_distance, coords, psi, min_resolution)

n_good_boxes = prod(size(psi));

if(tm_num == 1)
  target_box_num = 1;

%  x_junction1 = (coords(1,1)+coords(2,1))/2;
%  y_junction1 = (coords(1,2)+coords(2,2))/2;
  x_junction1 = coords(1,1) + 0.5*cos(pi/180*psi(1))*repeat_distance/ref_pixel;
  y_junction1 = coords(1,2) - 0.5*sin(pi/180*psi(1))*repeat_distance/ref_pixel;
  x_junction2 = coords(1,1);
  y_junction2 = coords(1,2);

  shift_x = -0.5*cos(pi/180*psi(1))*repeat_distance/ref_pixel;
  shift_y = 0.5*sin(pi/180*psi(1))*repeat_distance/ref_pixel;

  mask_origin_x = coords(1,1);
  mask_origin_y = coords(1,2);
%  mask_origin_x = coords(1,1)
%  mask_origin_y = coords(1,2)

elseif(tm_num == n_good_boxes + 1)
  mask_origin_x = coords(n_good_boxes,1);
  mask_origin_y = coords(n_good_boxes,2);

  x_junction1 = coords(n_good_boxes,1);
  y_junction1 = coords(n_good_boxes,2);
  x_junction2 = (coords(n_good_boxes-1,1)+coords(n_good_boxes,1))/2;
  y_junction2 = (coords(n_good_boxes-1,2)+coords(n_good_boxes,2))/2;

  shift_x =  -( x_junction2 - coords(n_good_boxes,1));
  shift_y =  -( y_junction2 - coords(n_good_boxes,2));

  x_junction2 = coords(n_good_boxes,1) - ( x_junction2 - coords(n_good_boxes,1));
  y_junction2 = coords(n_good_boxes,2) - ( y_junction2 - coords(n_good_boxes,2));

else
  mask_origin_x = coords(tm_num,1);
  mask_origin_y = coords(tm_num,2);

  x_junction1 = coords(tm_num-1,1);  
  y_junction1 = coords(tm_num-1,2);  
  x_junction2 = coords(tm_num,1);  
  y_junction2 = coords(tm_num,2);  

  shift_x = -(x_junction1 + x_junction2)/2 + x_junction2;
  shift_y = -(y_junction1 + y_junction2)/2 + y_junction2;
end

x_junction1 = x_junction1 - mask_origin_x + shift_x;
x_junction2 = x_junction2 - mask_origin_x + shift_x;
y_junction1 = y_junction1 - mask_origin_y + shift_y;
y_junction2 = y_junction2 - mask_origin_y + shift_y;

vec = [x_junction1 - x_junction2 y_junction1 - y_junction2];
vec = vec/(sqrt(sum(vec.^2)));

[x y] = ndgrid(-floor(ref_dim(1)/2):-floor(ref_dim(1)/2)+ref_dim(1)-1,...
               -floor(ref_dim(2)/2):-floor(ref_dim(2)/2)+ref_dim(2)-1);

helimask = ones(ref_dim);

helimask(find( (x - x_junction1) * vec(1) + (y - y_junction1) * vec(2) > 0)) = 0;
helimask(find( (x - x_junction2) * vec(1) + (y - y_junction2) * vec(2) < 0)) = 0;

cos_edge_dim = min_resolution / (2*ref_pixel);

cos_edge = cos( 2*pi* ...
               (0.25 + ...
                ((x - x_junction1) * vec(1) + (y - y_junction1) * vec(2)) / (2*cos_edge_dim)));
cos_edge = (cos_edge + 1)/2;
cos_edge_area = find( (x - x_junction1) * vec(1) + (y - y_junction1) * vec(2) > ...
                         -cos_edge_dim/2 & ...
                      (x - x_junction1) * vec(1) + (y - y_junction1) * vec(2) < ...
                         cos_edge_dim/2);

helimask(cos_edge_area) = cos_edge(cos_edge_area);

cos_edge = cos( 2*pi* ...
               (0.25 + ...
                (-(x - x_junction2) * vec(1) - (y - y_junction2) * vec(2)) / (2*cos_edge_dim)));
cos_edge = (cos_edge + 1)/2;
cos_edge_area = find( (x - x_junction2) * vec(1) + (y - y_junction2) * vec(2) > ...
                         -cos_edge_dim/2 & ...
                      (x - x_junction2) * vec(1) + (y - y_junction2) * vec(2) < ...
                         cos_edge_dim/2);

helimask(cos_edge_area) = cos_edge(cos_edge_area);
