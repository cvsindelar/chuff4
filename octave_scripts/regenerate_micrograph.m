function mic = regenerate_micrograph(mic_dim, particle_info, refs)

s = size(particle_info);
ref_dim = size(refs);

mic = zeros(mic_dim);

for i=1:s(1)

  wi_ox = particle_info(i,10);
  wi_oy = particle_info(i,11);
  frac_tub_x = particle_info(i,12);
  frac_tub_y = particle_info(i,13);
  tub_psi = particle_info(i,8);
  ref_index = particle_info(i,9);

  proj = mrotshift(refs(:,:,ref_index),-tub_psi,...
                          [frac_tub_x frac_tub_y],...
                          [floor(ref_dim(1)/2)+1 floor(ref_dim(2)/2)+1]);

  mic(wi_ox:wi_ox+ref_dim(1)-1,wi_oy:wi_oy+ref_dim(1)-1) = ...
      mic(wi_ox:wi_ox+ref_dim(2)-1,wi_oy:wi_oy+ref_dim(2)-1) + proj;


end
