function winvol = window_vol(volume, wi_xyz, wi_oxyz, pad_out, pad_factor, interpmode)

if(nargin < 4)
  pad_out = 0;
end

if(nargin < 5)
  pad_factor = [];
end

if(nargin < 6)
  interpmode = 'sinc';
end

stdout = 1;

coords = wi_oxyz;
if(prod(size(coords)) < 2)
  coords = [coords 1];
end
if(prod(size(coords)) < 3)
  coords = [coords 1];
end

wi_dim = wi_xyz;
if(prod(size(wi_dim)) < 2)
  wi_dim = [wi_dim 1];
end
if(prod(size(wi_dim)) < 3)
  wi_dim = [wi_dim 1];
end

full_vol_dim = size(volume);
full_vol_dim = size(volume);
if(prod(size(full_vol_dim)) < 2)
  full_vol_dim = [full_vol_dim 1];
end
if(prod(size(full_vol_dim)) < 3)
  full_vol_dim = [full_vol_dim 1];
end

int_coords = round(coords);
frac_coords = coords - int_coords;

if(isempty(pad_factor))
  if(!isequal(frac_coords(:), [0 0 0]'))
    pad_factor = 2;
  else
    pad_factor = 1;
  end
end

pad_box_dim = pad_factor * wi_dim;
for i = 1:3
  if(wi_dim(i) == 1)
    pad_box_dim(i) = 1;
  end
end

box_origin = int_coords + floor(wi_dim/2) - floor(pad_box_dim/2);

%%%%%%%%%
% Handle clipping if image falls partially or fully outside volume
%%%%%%%%%

[wi_xyz_clip, wi_oxyz_clip, source_oxyz, background_oxyz] = ...
  get_clipping(pad_box_dim, box_origin, full_vol_dim);

winvol = volume(wi_oxyz_clip(1):wi_oxyz_clip(1)+wi_xyz_clip(1)-1, ...
                wi_oxyz_clip(2):wi_oxyz_clip(2)+wi_xyz_clip(2)-1,...
                wi_oxyz_clip(3):wi_oxyz_clip(3)+wi_xyz_clip(3)-1);

%%%%%%%%
% Restore box to proper size by filling in borders with "noise"
%%%%%%%%

if(!isequal(pad_box_dim(1:3), wi_xyz_clip(1:3)))
  if(pad_out == 3)
    mirrorpad = 1
  else
    mirrorpad = 0;
  end

  if(background_oxyz(1) != -1)
    noise_box = volume(background_oxyz(1):background_oxyz(1)+wi_dim(1)-1, ...
                       background_oxyz(2):background_oxyz(2)+wi_dim(2)-1, ...
                       background_oxyz(3):background_oxyz(3)+wi_dim(3)-1);
    winvol = pad_gently(winvol, pad_box_dim, wi_oxyz_clip, box_origin,...
                     mirrorpad, noise_box);
  else
    winvol = pad_gently(winvol, pad_box_dim, wi_oxyz_clip, box_origin,...
                     mirrorpad);
  end
end

if(pad_factor > 1)
  temp1 = spherical_cosmask([pad_box_dim(1) 1], wi_dim(1)/2+1, (pad_box_dim(1)-wi_dim(1))/2-2);
  temp2 = spherical_cosmask([pad_box_dim(2) 1], wi_dim(2)/2+1, (pad_box_dim(2)-wi_dim(2))/2-2);
  temp3 = spherical_cosmask([pad_box_dim(3) 1], wi_dim(3)/2+1, (pad_box_dim(3)-wi_dim(3))/2-2);

  softmask = zeros([1 1 pad_box_dim(3)]);
  softmask(:) = temp3;
  softmask = repmat(repmat(softmask, [1 pad_box_dim(2) 1]),[pad_box_dim(1) 1 1]);
  softmask = softmask .* repmat(repmat(temp1', [pad_box_dim(1) 1 1]), [1 1 pad_box_dim(3)]);
  softmask = softmask .* repmat(repmat(temp2, [1 pad_box_dim(2) 1]), [1 1 pad_box_dim(3)]);
  winvol = winvol .* softmask;
%writeSPIDERfile('softmask.spi',softmask);
%writeSPIDERfile('winvol.spi',winvol);
  softmask = [];
end

if(!isequal(frac_coords(:), [0 0 0]'))
  if(isequal(interpmode,'sinc'))
    winvol = real(ifftn(double(fftn(winvol) .* FourierShiftVol(pad_box_dim, -frac_coords))));
  else
    winvol = TransformVol(winvol, [0 0 0], -frac_coords, 'spline');
  end
end

wi_oxyz = 1 + floor(pad_box_dim/2) - floor(wi_dim/2);
winvol = winvol(wi_oxyz(1):wi_oxyz(1)+wi_dim(1)-1,...
                wi_oxyz(2):wi_oxyz(2)+wi_dim(2)-1,...
                wi_oxyz(3):wi_oxyz(3)+wi_dim(3)-1);

return
