function mini_score_list = ...
  get_mini_score_list(overlap_map_list, data_box, synth_background, refs);

size_ref = size(refs);
n_refs = size_ref(3);
n_classes = size_ref(4);
max_overlaps = size(overlap_map_list);
max_overlaps = max_overlaps(2);
mini_score_list = zeros([n_refs n_classes.^max_overlaps]);

ref_background = synth_background;
for i=1:n_refs
  ref_background = ref_background + squeeze(refs(:,:,i,1));
end

% writeSPIDERfile('ref_bo1.spi',ref_background);

progressbar = 0;

fprintf(stdout,'Computing the compressed score list:\n');

for i=1:n_refs
  hit_list = overlap_map_list(i,find(overlap_map_list(i,:)));
  n_overlaps = prod(size(hit_list));
  for j=1 : n_classes^n_overlaps
    state_vec = ones([1 n_refs]);
    state_vec(hit_list) = get_states_from_num(j,n_classes,n_overlaps);

    ref_box = ref_background;
    for k=1:n_refs
      if(state_vec(k) != 1)
        ref_box = ref_box - refs(:,:,k,1) + ...
                          refs(:,:,k,state_vec(k));
      end
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERY important: the score for this MAP computation comes from the Fourier domain,
%  but here we have computed it in real space; we therefore need to multiply the score
%  by the number of pixels in the box.
%%%%%%%%%%%%%%%%%%%%%%%%%%%
    mini_score_list(i,j) = sum(sum((ref_box - data_box).^2)) * prod(size(data_box));

    if(i/n_refs > progressbar)
      fprintf(stdout, '%3.0f%% ', 100*progressbar);
      if(isoctave)
        fflush(stdout);
      end
      progressbar = progressbar + 0.1;
    end

% if(isequal(state_vec, [2 2 1 2 2 1 1 2]))
%j
% state_vec
%     temp1 = synth_background+refs(:,:,1,state_vec(1))+refs(:,:,2,state_vec(2))+refs(:,:,3,state_vec(3))+refs(:,:,4,state_vec(4))+refs(:,:,5,state_vec(5))+refs(:,:,6,state_vec(6))+refs(:,:,7,state_vec(7))+refs(:,:,8,state_vec(8));
% 
% 'control_score'
%     sum((temp1 - data_box)(:).^2)
% 'mini_score_list'
% sum(sum((ref_box - data_box).^2))
% end

  end
end

fprintf(stdout, ' done.\n');
fflush(stdout);

% mini_score_list = mini_score_list - mini_score_list(1);
