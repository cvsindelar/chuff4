function [score, ref_box, background] = ...
  compute_hmap_score_exact(data_box, refs, state_vec)

size_ref = size(refs);
ref_dim = size_refs(1:2);
n = size_ref(3);
n_states = size_ref(4);

ref_box = zeros(ref_dim);
background = zeros(ref_dim);

for i=1:n
  background = background + refs(:,:,i,1);
  ref_box = ref_box + refs(:,:,i, state_vec(i));
end

score = sum(sum((ref_box - data_box).^2)) - sum(sum((background - data_box).^2));
