function return_box = soften_box_boundary(box, bkg)

ref_dim = size(box);
final_dim = ref_dim;

softmask = spherical_cosmask([ref_dim(1) 1], (ref_dim(1)-final_dim(1)/2)/2, final_dim(1)/5);
softmask = repmat(softmask', [ref_dim(1) 1]) .*...
           repmat(softmask, [1 ref_dim(2)]);

return_box = box.*softmask + bkg*(1-softmask);
