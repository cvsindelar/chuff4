function [coords_est, phi_est, theta_est, psi_est, d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat] = ...
   smooth_alignment(coords, phi, theta, psi, expected_dphi, smoothing_hwidth, smoothing_method, ...
                 angle_error_tolerance, coord_error_tolerance, ...
                 phi_error_tolerance, theta_error_tolerance, psi_error_tolerance)

if(nargin < 8)
  angle_error_tolerance = 2;
end
if(nargin < 12)
  psi_error_tolerance = angle_error_tolerance;
end
if(nargin < 11)
  theta_error_tolerance = angle_error_tolerance;
end
if(nargin < 10)
  phi_error_tolerance = angle_error_tolerance;
end
if(nargin < 9)
  coord_error_tolerance = angle_error_tolerance;
end

n_good_boxes = prod(size(phi));

%%%%%%%%%%%%%%%%%%%%
% Since phi changes rapidly, we will compare it with its expected behavior to 
%  generate a more slowly varying error reporter.
%%%%%%%%%%%%%%%%%%%%

phi_delta(1:n_good_boxes-1) =  ...
                     mod(phi(2:n_good_boxes) - ...
                           phi(1:n_good_boxes-1) + 180, ...
                         360) - 180;
phi_delta(n_good_boxes) = phi_delta(n_good_boxes-1);

net_phi_delta = cumsum(phi_delta) - phi_delta(1);
expected_phi_delta = [0:n_good_boxes-1]*expected_dphi;

phi_delta_diff = net_phi_delta(:) - expected_phi_delta(:);
phi_delta_diff(find(phi_delta_diff < -180) ) = phi_delta_diff(find(phi_delta_diff < -180)) + 360;
phi_delta_diff(find(phi_delta_diff > 180) ) = phi_delta_diff(find(phi_delta_diff > 180)) - 360;

%%%%%%%%%%%%%%%%%%%%
% Fix psi so it is continuous (avoid wraparound at 360)
%%%%%%%%%%%%%%%%%%%%

  psi_mid = floor(n_good_boxes/2)+1;
  psi_low = psi_mid - smoothing_hwidth;
  psi_high = psi_mid + smoothing_hwidth;
  if(psi_low < 1)
    psi_low = 1;
  end
  if(psi_high > n_good_boxes)
    psi_high = n_n_good_boxes;
  end
  psi_mid_val = median(psi(psi_low:psi_high));

  psi(find(psi > psi_mid_val + 180)) = ...
    psi(find(psi > psi_mid_val + 180)) - 360;
  psi(find(psi < psi_mid_val - 180)) = ...
    psi(find(psi < psi_mid_val - 180)) + 360;

%%%%%%%%%%%%%%%%%%%%
% Loop through the boxes
%%%%%%%%%%%%%%%%%%%%

for j=1:n_good_boxes
  time1=cputime();
  cur_phi_est = 0;
  cur_theta_est = 0;
  cur_psi_est = 0;
  count = 0;

  l_bound = j-smoothing_hwidth;
  if(l_bound < 1)
    l_bound = 1;
  end
  u_bound = j+smoothing_hwidth;
  if(u_bound > n_good_boxes)
    u_bound = n_good_boxes;
  end

  coords_temp = coords(l_bound:u_bound,:);
  phi_temp = mod(phi(l_bound:u_bound),360);
  phi_delta_diff_temp = phi_delta_diff(l_bound:u_bound);
  theta_temp = theta(l_bound:u_bound);
  psi_temp = psi(l_bound:u_bound);

%%%%%%%%%%%%%%%
% Discard outliers and do linear fits, if possible
%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%
% Smooth the coordinates
%%%%%%%%%%%%%%%
  j_temp = l_bound:u_bound;
  x_params = linear_fit(j_temp,coords_temp(:,1)');
  y_params = linear_fit(j_temp,coords_temp(:,2)');
  x_params = [0 x_params];
  y_params = [0 y_params];
%  x_params = parabolic_fit(j_temp,coords_temp(:,1)');
%  y_params = parabolic_fit(j_temp,coords_temp(:,2)');
  coords_delta_std = ...
    sqrt(sum(sum( [x_params(1)*j_temp.^2 + x_params(2)*j_temp + x_params(3) - coords_temp(:,1)';...
                   y_params(1)*j_temp.^2 + y_params(2)*j_temp + y_params(3) - ...
                      coords_temp(:,2)'].^2, 1),2) / ...
             (u_bound - l_bound + 1));
%  coords_delta_std = 1;
  coords_delta = sqrt(sum( [x_params(1)*j_temp.^2 + x_params(2)*j_temp + x_params(3) - ...
                               coords_temp(:,1)';...
                            y_params(1)*j_temp.^2 + y_params(2)*j_temp + y_params(3) - ...
                               coords_temp(:,2)'].^2, 1));
  j_temp = j_temp(find(coords_delta < coord_error_tolerance * coords_delta_std));
  coords_temp = coords_temp(find(coords_delta < coord_error_tolerance * coords_delta_std),:);
  if(prod(size(coords_temp)) > 2*2)
    if(!find(j_temp == j) || isequal(smoothing_method, 'smooth_all'))
      x_params = linear_fit(j_temp,coords_temp(:,1)');
      y_params = linear_fit(j_temp,coords_temp(:,2)');
      x_params = [0 x_params];
      y_params = [0 y_params];
%      x_params = parabolic_fit(j_temp,coords_temp(:,1)');
%      y_params = parabolic_fit(j_temp,coords_temp(:,2)');

      coords_est(j,:) = [x_params(1) * j.^2 + x_params(2)*j + x_params(3) ...
                         y_params(1) * j.^2 + y_params(2)*j + y_params(3)];
    end
  end

%%%%%%%%%%%%%%%
% Smooth psi
%%%%%%%%%%%%%%%

  j_temp = l_bound:u_bound;

%  psi_params=parabolic_fit(j_temp,psi_temp);
  psi_params=linear_fit(j_temp,psi_temp);
  psi_params = [0 psi_params];

  psi_std = sqrt(sum( (psi_params(1)*j_temp.^2+psi_params(2)*j_temp+psi_params(3) - ...
                              psi_temp).^2) / (u_bound-l_bound+1) );
%  psi_std = 1;
  psi_delta = abs(psi_params(1)*j_temp.^2+psi_params(2)*j_temp+psi_params(3) - psi_temp);
%  j_temp = find(psi_delta < psi_error_tolerance * psi_std);
%  j_temp = find(psi_delta < psi_error_tolerance);
%  psi_temp = psi_temp(j_temp);

%  if(prod(size(psi_temp )) > 2)
  if(prod(size(psi_temp )) > 3)
    if(!find(j_temp == j) || ...
       isequal(smoothing_method, 'smooth_eulers') || ...
       isequal(smoothing_method, 'smooth_all'))
%      psi_params=polyfit(j_temp,psi_temp,1);

      psi_params=linear_fit(j_temp,psi_temp);
      psi_est(j) = mod(psi_params(1)*j+psi_params(2),360);
%      psi_params=parabolic_fit(j_temp,psi_temp);
      psi_params=linear_fit(j_temp,psi_temp);
      psi_params = [0 psi_params];
      psi_est(j) = mod(psi_params(1) * (j + 0.5)^2 + psi_params(2) * (j + 0.5) + ...
                   psi_params(3),360);
      d_psi_d_repeat(j) = psi_params(2);
    else
      psi_est(j) = psi(j);
      d_psi_d_repeat(j) = 0;
    end
  else
    psi_est(j) = psi(j);
    d_psi_d_repeat(j) = 0;
  endif

%%%%%%%%%%%%%%%
% Smooth theta
%%%%%%%%%%%%%%%
  theta_avg = sum(theta_temp(:))/prod(size(theta_temp));
  theta_std = sqrt(var(theta_temp(:)));
%  theta_std = 1;
  j_temp = l_bound:u_bound;
  j_temp = j_temp(find(abs(theta_temp - theta_avg) < theta_error_tolerance * theta_std));
  theta_temp = theta_temp(find(abs(theta_temp - theta_avg) < theta_error_tolerance * theta_std));

  if(prod(size(theta_temp)) > 2)
    if(!find(j_temp == j) || ...
       isequal(smoothing_method, 'smooth_eulers') || ...
       isequal(smoothing_method, 'smooth_all'))
%    theta_params=polyfit(j_temp,theta_temp,1)
      theta_params=linear_fit(j_temp,theta_temp);
      theta_est(j) = theta_params(1)*j+theta_params(2);
      d_theta_d_repeat(j) = theta_params(1);
    else
      theta_est(j) = theta(j);
      d_theta_d_repeat(j) = 0;
    end
  else
    theta_est(j) = theta(j);
    d_theta_d_repeat(j) = 0;
  end

%%%%%%%%%%%%%%%
% To find outliers in phi is a special case, as (unlike psi, theta) phi can change
%  rapidly along the filament- i.e. the expected change per subunit, helical_twist, 
%  can be large.
% We therefore use the expected helical twist in our quest for outliers. Note
%  that the helical_twist must be negated to account for the left-handed coordinate
%  system used as a convention in SPIDER, FREALIGN, etc.
%%%%%%%%%%%%%%%

  phi_temp = fix_rollover(phi_temp, expected_dphi);

%  phi_avg = sum(phi_temp(:))/prod(size(phi_temp));
%  j_avg = sum(l_bound:u_bound)/(u_bound-l_bound+1);
%  phi_simple_est = phi_avg + ((l_bound:u_bound) - j_avg) * expected_dphi;
%  phi_std = sqrt(var(phi_temp - phi_simple_est));
%  phi_std = 1;

  phi_delta_diff_avg = sum(phi_delta_diff_temp(:))/prod(size(phi_delta_diff_temp));
  phi_delta_diff_std = sqrt(var(phi_delta_diff_temp - phi_delta_diff_avg));
  phi_delta_diff_std = 1;

  j_temp = l_bound:u_bound;

  j_temp = j_temp(find(abs(phi_delta_diff_temp - phi_delta_diff_avg) < ...
                    phi_error_tolerance * phi_delta_diff_std));
  phi_temp = phi_temp(find(abs(phi_delta_diff_temp - phi_delta_diff_avg) < ...
                        phi_error_tolerance * phi_delta_diff_std));

[j phi_delta_diff_avg]
[phi_delta_diff_temp'; abs(phi_delta_diff_temp - phi_delta_diff_avg)'; ...
(abs(phi_delta_diff_temp - phi_delta_diff_avg) > phi_error_tolerance * phi_delta_diff_std)']

  if(prod(size(phi_temp)) > 2)
    if(!find(j_temp == j) || ...
       isequal(smoothing_method, 'smooth_eulers') || ...
       isequal(smoothing_method, 'smooth_all'))

%      phi_params=polyfit(j_temp,phi_temp,1);
      phi_params=linear_fit(j_temp,phi_temp);
      phi_est(j) = mod(phi_params(1)*j+phi_params(2),360);
      d_phi_d_repeat(j) = phi_params(1);
    else
      phi_est(j) = phi(j);
      d_phi_d_repeat(j) = 0;
    end
  else
    phi_est(j) = phi(j);
    d_phi_d_repeat(j) = 0;
  end
end
