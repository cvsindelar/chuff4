function save_helimap_ref_vols(run_dir, helimap_round, ...
                               fsc_array, sigma2_array, inv_tau2_array, nuke);

global subunit_array vol_array debug;

if(nargin < 3)
  nuke = 0;
end

%%%%%%%%%%%%%%%%%%
% Get reference info
%%%%%%%%%%%%%%%%%%

sz = size(subunit_array)
n_states = sz(4);
subunit_dim = sz(1:3);

[helimap_dir, reference_dir] = ...
  save_helimap_alignment(run_dir, helimap_round);

if(!mkdir(reference_dir))
  printf('ERROR: cannot create directory "%s"\n',reference_dir);
  exit(2)
end

vol_name = sprintf('%s/ref_helimap_class%d_gold%d.spi', ...
                   reference_dir, 1, 1);
if(exist(vol_name) && !nuke)
  printf('ERROR: output "%s" exists already...', vol_name);
  exit(2)
end

for class_num=1:n_states
  doc_name = sprintf('%s/ref_helimap_class%d_fsc_doc.spi', reference_dir, class_num);
  writeSPIDERdoc(doc_name, fsc_array(:,class_num));

  for igold=1:2
    doc_name = sprintf('%s/ref_helimap_class%d_sigma2_doc.spi', reference_dir, class_num);
    writeSPIDERdoc(doc_name, sigma2_array(:,class_num, igold));

    doc_name = sprintf('%s/ref_helimap_class%d_inv_tau2_doc.spi', reference_dir, class_num);
    writeSPIDERdoc(doc_name, inv_tau2_array(:,class_num, igold));

    vol_name = sprintf('%s/ref_helimap_class%d_gold%d.spi', reference_dir, class_num,igold)
    writeSPIDERfile(vol_name, subunit_array(:,:,:,class_num,igold));

    vol_name = sprintf('%s/vol_class%d_gold%d.spi',reference_dir, class_num, igold);
    writeSPIDERfile(vol_name, vol_array(:,:,:,class_num, igold));
  end
end
