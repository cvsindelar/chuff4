function box_to_mask(micrograph, maskfile, box_coords, box_coords_file, edge_width_A, pix_size)

if(strcmp(box_coords_file,'null') != 1)
  box_doc = readSPIDERdoc_dlmlike(box_coords_file);
  n_boxes = size(box_doc);
  n_boxes = n_boxes(1);
else
  box_doc = [];
  n_boxes = 0;
end

edge_width = ceil(edge_width_A/pix_size);

additional_n_boxes = floor(prod(size(box_coords))/4);
box_doc = resize(box_doc, [n_boxes+additional_n_boxes 5]);
for i=1:additional_n_boxes
  box_doc(n_boxes+i,1)=box_coords((i-1)*4+1);
  box_doc(n_boxes+i,2)=box_coords((i-1)*4+2);
  box_doc(n_boxes+i,3)=box_coords((i-1)*4+3);
  box_doc(n_boxes+i,4)=box_coords((i-1)*4+4);
end

n_boxes=n_boxes+additional_n_boxes;

dim = ReadMRCheader(micrograph);

mask = zeros([dim(1) dim(2)]);

for i=1:n_boxes
  x1=box_doc(i,1);
  x2=box_doc(i,1)+box_doc(i,3)-1;
  y1=box_doc(i,2);
  y2=box_doc(i,2)+box_doc(i,4)-1;

  if(x1<edge_width+1)
    x1=edge_width+1;
  end
  if(y1<edge_width+1)
    y1=edge_width+1;
  end
  if(x2>dim(1)-edge_width-1)
    x2=dim(1)-edge_width-1;
  end
  if(y2>dim(2)-edge_width-1)
    y2=dim(2)-edge_width-1;
  end

  mask(x1:x2,y1:y2)=1;
end

writeSPIDERfile(maskfile,mask);
