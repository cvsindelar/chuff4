function sum_boxes(job_dir,focal_mate)

use_ctf = 1;
n_normal_args = 2;

job_dir = trim_dir(job_dir);

mkdir(sprintf('chumps_round1/%s/find_kinesin/',job_dir));
mkdir(sprintf('chumps_round1/%s/fmate_find/',job_dir));

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m'

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

[micrograph,defocus,astig_mag,astig_angle,b_factor] = ...
  chumps_micrograph_info(job_dir,focal_mate);

[num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
  est_repeat_distance] = ...
  read_chumps_alignment(job_dir,helical_repeat_distance,1,micrograph_pixel_size);

est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

[coords,phi,theta,psi,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
  fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,
                         est_pix_per_repeat);

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

ref_params_name = 'chumps_round1/ref_tub_subunit/ref_params.spi';
ctf_doc_name = 'ctf_files/ctf_docfile_for_spider.spi';

n_kins_to_find = 2*num_pfs; % 6*num_pfs;
max_overlap = 2*num_pfs;

num_repeats = ceil( (n_kins_to_find+2*max_overlap)/num_pfs );

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

if(focal_mate == 0)
  output_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
  micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
else
  output_dir = sprintf('chumps_round1/%s/fmate_find',job_dir);
  micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
end

%%%%%%%%%%%%%%%%%%
% Initialize file-related parameters
%%%%%%%%%%%%%%%%%%

n_good_boxes = prod(size(phi));

fprintf(stdout,'%s %4d\n', job_dir, n_good_boxes);

ref_pixel_info = dlmread(ref_params_name);

index = find(ref_pixel_info(:,1) == 2);
ref_pixel = ref_pixel_info(index(1),5);

bin_factor = round(ref_pixel/micrograph_pixel_size);
final_pixel_size = micrograph_pixel_size*bin_factor;

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
box_dim = 64*ceil(box_dim/64);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);
pad_ref_dim = 2*ref_dim;

%%%%%%%%%%%%%%%%%%%%%%%
% Compute CTF's
%%%%%%%%%%%%%%%%%%%%%%%

electron_lambda = EWavelength(accelerating_voltage);

if(defocus > 0 && use_ctf != 0)
  ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                       spherical_aberration_constant, ...
                       b_factor, amplitude_contrast_ratio, 0);
  ctf = ifftshift(ctf);

  box_ctf = CTF_ACR(box_dim(1), micrograph_pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  box_ctf = ifftshift(box_ctf);
else
  ctf = -ones(pad_ref_dim);
  box_ctf = ones(box_dim);
end

ctf_flip = sign(ctf);
box_ctf_flip = sign(box_ctf);

mic_dim = ReadMRCheader(micrograph);

%%%%%%%%%%%%%%%%%%
% Count how many "good" boxes are fully inside the micrograph
%%%%%%%%%%%%%%%%%%

spacing = 9;
for i = 1:spacing
j = 0;
sum = zeros(ref_dim);
while(i+j < n_good_boxes)

  box_num = i + j
  int_coords = round(coords(box_num,:));
  box_origin = int_coords-floor(box_dim/2);
  if(box_origin(1) < 1 || box_origin(2) < 1 || ...
     box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
    j = j + spacing;
    continue
  end

  fprintf(stdout,'\nBox %4d/%4d : %6.1f\n', box_num, n_good_boxes, psi(box_num));
  fflush(stdout);
  box = read_chumps_box(micrograph,box_num,coords,box_dim,...
                        bin_factor,ctf_flip);
  box = invert_density * box;

  sum = sum + dctrotate(box,-psi(box_num));

  j = j + spacing;
%%%%%%%%%%%%%%%%%%
% Write files
%%%%%%%%%%%%%%%%%%

end
outfile = sprintf('%s/box_rot_%04d.spi',output_dir,i);
writeSPIDERfile(outfile,sum);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accessory functions to make the code more readable...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out_dir = trim_dir(job_dir)

if(job_dir(prod(size(job_dir))) == '/')
  job_dir=job_dir(1:prod(size(job_dir))-1);
end
[start] = regexp(job_dir,'[^/]*$','start');
out_dir = job_dir(start:prod(size(job_dir)));

return
