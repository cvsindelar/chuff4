function [helimap_dir,  reference_dir, filament_dir] = ...
  save_helimap_alignment(run_dir, helimap_round, filament_id, ...
                         nuke, notebook_dir, test_mode, ...
                         n_classes, bin_factor, ...
                         focal_mate, use_ctf, chumps_round,...
                         min_resolution, recon_pad_factor, ref_pad_factor,...
                         box_dim, ref_dim, small_subunit_dim, subunit_dim,...
                         ref_pixel, ref_com, filament_inner_radius,...
                         est_pix_per_repeat,num_pfs,num_starts,...
                         coords,phi,theta,psi,...
                         d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat, ...
                         est_repeat_distance_13pf, est_repeat_distance, ...
                         elastic_params, radius_scale_factor, ...
                         tot_helimap, goldmap)

  if(nargin < 4)
    nuke = 0;
  end
  
  helimap_dir = sprintf('%s/helimap_round%d',...
                        run_dir, helimap_round);

  reference_dir = sprintf('%s/references%d', run_dir,helimap_round);

#  if(!mkdir(run_dir))
#    printf('ERROR: cannot create directory "%s"\n',run_dir);
#    exit(2)
#  end
#  if(!mkdir(helimap_dir))
#    printf('ERROR: cannot create directory "%s"\n',helimap_dir);
#    exit(2)
#  end

#  if(nargin > 3)
#  if(filament_id == 1)
#    if(!mkdir(reference_dir))
#      printf('ERROR: cannot create directory "%s"\n',reference_dir);
#      exit(2)
#    end
#    if(!mkdir(sprintf('%s/tallies',reference_dir)))
#      printf('ERROR: cannot create directory "%s"\n',sprintf('%s/tallies',reference_dir));
#      exit(2)
#    end
#  end
#  end

  info_file = sprintf('%s/info.mat',run_dir);

  if(nargin > 4)
    if(!exist(info_file) || nuke == 1)
      save(info_file, 'n_classes', 'bin_factor', ...
                      'focal_mate', 'use_ctf', 'chumps_round',...
                      'min_resolution', 'recon_pad_factor', 'ref_pad_factor',...
                      'box_dim', 'ref_dim', 'small_subunit_dim', 'subunit_dim',...
                      'ref_pixel', 'ref_com', ...
                      'filament_inner_radius','est_pix_per_repeat', ...
                      'notebook_dir', 'test_mode');
    end
  end                    

  if(nargin > 2)
    filament_dir = sprintf('%s/%s', helimap_dir, filament_id);
    fil_info_file = sprintf('%s/fil_info.mat',filament_dir);

#   if(!mkdir(filament_dir))
#      printf('ERROR: cannot create directory "%s"\n',filament_dir);
#      exit(2)
#    end
  end

  if(nargin > 19)
    if(exist(fil_info_file) && nuke == 0)
      printf('ERROR: this round has been started, "%s" exists already...\n',fil_info_file);
      exit(2)
    else
      save(fil_info_file, 'num_pfs','num_starts',...
                    'coords','phi','theta','psi', ...
                    'd_phi_d_repeat','d_theta_d_repeat','d_psi_d_repeat', ...
                    'est_repeat_distance_13pf', 'est_repeat_distance',...
                    'elastic_params', 'radius_scale_factor', ...
                    'tot_helimap', 'goldmap');
    end
    if(nargin > 32)
      for igold=1:2
      for ind=1:n_classes
        writeSPIDERfile(sprintf('%s/tot_helimap_class%d_gold%d.spi',filament_dir, ind, igold), squeeze(tot_helimap(:,:,ind,igold)));
      end
      end
    end
  end
end
