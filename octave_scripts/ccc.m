function ccc=ccc(accc, bccc)
% function ccc=ccc(a, b)

mean_accc = mean(accc(:));
mean_bccc = mean(bccc(:));
ccc = sum( (accc(:) - mean_accc) .* (bccc(:) - mean_bccc)) / ...
  sqrt(sum((accc(:) - mean_accc).^2) * sum((bccc(:) - mean_bccc).^2));
ccc=real(ccc);
return;
