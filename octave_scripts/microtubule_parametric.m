function [x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub...
          x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist...
          mt_repeat_index mt_pf_index origin_repeat phi_first_pf] =...
              microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                                     repeat_dist13pf,radius_scale_factor,monomer_offset,...
                                     phi_params,theta_params,psi_params,elastic_params)
% function [x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub...
%           x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist] =...
%               microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
%                                      repeat_dist13pf,radius_scale_factor,monomer_offset,...
%                                      phi_params,theta_params,psi_params,elastic_params)

if(prod(size(elastic_params)) > 4)
  elastic_params(prod(size(elastic_params))+1:prod(size(elastic_params))) = 0;
end
elastic_params = elastic_params(:);

if(prod(size(num_repeats)) < 2)
  origin_repeat = floor(num_repeats/2) + 1;
else
  origin_repeat = num_repeats(2); % In num_repeats(2), the user can optionally specify 
end                               %  which repeat will contain the origin where z = 0

num_repeats = num_repeats(1);

tub_origin = num_pfs*(origin_repeat-1) + 1; % index of the particle whose z-value is zero
if(tub_origin < 1 || tub_origin > num_pfs*num_repeats)
  printf('ERROR: microtubule_parametric: sorry, the repeat origin must be within the range of 1:num_repeats\n');
  exit(2);
end

%%%%%%%%%
% Convert input euler angles into units of radians
%  Second element of the params arguments, if it exists, reflects the change
%   in the parameter value as a function of distance (Angstroms), relative to
%   the expected value for a microtubule of the given symmetry type. 
%  Note that the expected change in theta and psi is 0, for any kind of helix. 
%  However, the phi twist is not; phi_params(2) thus accounts for variations from
%   the expected d_phi_d_x
%%%%%%%%%
phi_params(1) = phi_params(1) * pi/180;
if(prod(size(phi_params)) > 1)
  phi_params(2) = phi_params(2) * pi/180;
end
theta_params(1) = theta_params(1) * pi/180;
if(prod(size(theta_params)) > 1)
  theta_params(2) = theta_params(2) * pi/180;
end
psi_params(1) = psi_params(1) * pi/180;
if(prod(size(psi_params)) > 1)
  psi_params(2) = psi_params(2) * pi/180;
end

mt_radius13pf = sqrt(ref_com(1)^2 + ref_com(2)^2);

%%%%%%%%%%%%%%%%%
% Obtain the 'canonical' parameters for one ring of tubulin
%%%%%%%%%%%%%%%%%

[z_tub_ring, phi_tub_ring, mt_radius, repeat_dist, twist_per_repeat, repeat_index, pf_index] = mt_cylindrical_coords(num_pfs,num_starts,repeat_dist13pf,mt_radius13pf);

%%%%%%%%%%%%%%%%%
% Scale position of tubulin to match the radius of MT
%%%%%%%%%%%%%%%%%

ref_com_adjust(1) = radius_scale_factor * ref_com(1);
ref_com_adjust(2) = radius_scale_factor * ref_com(2);
ref_com_adjust(3) = ref_com(3);

% The following line would move the COM of tubulin as close to the z origin as possible.
%  However, this is not quite precise because repeat_dist may not go exactly along the z axis.
% ref_com_adjust(3) = ref_com(3) - round( ref_com(3)/repeat_dist) * repeat_dist;

z_tub_ring = z_tub_ring - z_tub_ring(1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pre-compute Euler angles and axial distances for the microtubule subunits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
% OOOPs - without initialization, the following variables are transposed by mistake.
%  No biggie...?
%
% axial_dist = zeros([num_repeats 1]);
% phi_tub = zeros([num_repeats 1]);
% mt_repeat_index = zeros([num_repeats 1]);
% mt_pf_index = zeros([num_repeats 1]);
%%%%%%%%%%%%%%

for i=1:num_repeats
  axial_dist(1+(i-1)*num_pfs:i*num_pfs) = z_tub_ring + (i-origin_repeat)*repeat_dist;
  phi_tub(1+(i-1)*num_pfs:i*num_pfs) = phi_tub_ring + (i-origin_repeat)*twist_per_repeat;
  mt_repeat_index(1+(i-1)*num_pfs:i*num_pfs) = i + repeat_index;
  mt_pf_index(1+(i-1)*num_pfs:i*num_pfs) = pf_index;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% "slide" MT along axis by one monomer length (repeat_dist/2) if requested
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

axial_offset = 0;
if(monomer_offset)
  axial_offset = -repeat_dist/2;
  axial_dist = axial_dist + axial_offset;
  phi_tub = phi_tub - twist_per_repeat/2;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute differences in z positions of tubulin repeats
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

delta_z = diff(axial_dist);
delta_z(num_repeats*num_pfs) = 0;       % This value is not important, just needs to exist

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make the MT "bendy" and "twisty"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[phi_mt d_phi_mt_d_dist] = modulate_parameter(axial_dist,phi_params);
theta_mt =                 modulate_parameter(axial_dist,theta_params);
psi_mt =                   modulate_parameter(axial_dist,psi_params);

d_phi_mt_d_dist = d_phi_mt_d_dist + twist_per_repeat/repeat_dist;
phi_tub = phi_tub + phi_mt;

phi_first_pf = zeros([1 prod(size(phi_tub))]);
for ind1=1:num_repeats
for ind2=1:num_pfs
  phi_first_pf((ind1-1)*num_pfs + ind2) = phi_tub((ind1-1)*num_pfs + 1);
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make distortion map (much faster to do this as a parallel operation,
%  on all phis at once)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(!isequal(elastic_params, [0; 0; 0; 0]))
  [phi_deform, r_deform] = map_elastic(phi_tub,elastic_params);
  phi_tub = phi_deform;
  r_tub = mt_radius * r_deform;
else
  r_tub = mt_radius * ones([prod(size(phi_tub)) 1]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate relative coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

coords_mt = [0; 0; 0];

%%%%%%%%%%%%%%%%
% Loop to compute center positions of each tubulin dimer
%%%%%%%%%%%%%%%%

for particle_index=1:num_repeats*num_pfs

%%%%%%%%%%%%%%%%
% Now compute how much the tubulin has rotated due to skew along the protofilament axis
%  (note: the skew will be small for normal MT's)
% One way to think about the skew: unroll the MT surface into a flat sheet.
% The width of the flat sheet is 2 pi mt_radius.  Over the repeat distance 
%  the MT will rotate by repeat_distance * d_phi_mt_d_dist radians (this is
%  the total helical twist per subunit along a protofilament).
% On the sheet, this rotation will be expressed as a sideways displacement
%  of repeat_distance * d_phi_mt_d_dist * mt_radius.  Thus, the skew will
%  be atan2(repeat_distance * d_phi_mt_d_dist * mt_radius, repeat_distance)
%    = atan2(d_phi_mt_d_dist * mt_radius, 1)
% Thus, for a very large radius the skew will be a lot, whereas for a small radius
%  it will be a little. Using a piece of paper to verify, you will notice that 
%  continuously varying the mt radius while keeping the helical twist constant
%  results in a propellor shape...!  Pretty cool, to me at least. This means that
%  twisting something like actin, where the subunits lie close to the helical
%  axis, would result in a propellor-like distortion of the actin subunit...
%%%%%%%%%%%%%%%%

  proto_skew = atan2(mt_radius*(d_phi_mt_d_dist(particle_index)),1);
  skew_xform = EulerMatrix([pi/2 proto_skew -pi/2]);
  euler_xform = EulerMatrix([phi_tub(particle_index) ...
                             theta_mt(particle_index) ...
                             psi_mt(particle_index)]);

  [final_eulers1 final_eulers2] = inveuler_cvs(euler_xform*skew_xform);

%%%%%%%%%%%%%%%%
% Here we select for theta (out of plane tilt) that is less than pi, 
%  because we assume that only theta ~ pi/2 is available in the reference
%  image library; note this only makes sense for MT's that have are oriented
%  in the XY plane, as in an EM image.
%%%%%%%%%%%%%%%%

  if(final_eulers1(2) < pi && final_eulers1(2) > 0)
    eulers_tub = final_eulers1;
  else
    eulers_tub = final_eulers2;
  end
  eulers_tub = mod(eulers_tub,2*pi);

%%%%%%%%%%%%%%%%
% Scale tubulin vector by the relative radius of the current MT symmetry form
%%%%%%%%%%%%%%%%

  tub_vec = zeros([3 1]);
  tub_vec(1) = ref_com_adjust(1) * r_tub(particle_index)/mt_radius13pf;
  tub_vec(2) = ref_com_adjust(2) * r_tub(particle_index)/mt_radius13pf;
  tub_vec(3) = ref_com_adjust(3);  % note: r is scaled in x-y plane only

%%%%%%%%%%%%%%%%
% Update coordinates
%%%%%%%%%%%%%%%%

  tub_vec = euler_xform * tub_vec;
  tub_coords = coords_mt + tub_vec;

  x_mt(particle_index) = coords_mt(1);
  y_mt(particle_index) = coords_mt(2);
  z_mt(particle_index) = coords_mt(3);

  x_tub(particle_index) = tub_coords(1);
  y_tub(particle_index) = tub_coords(2);
  z_tub(particle_index) = tub_coords(3);

  phi_tub(particle_index) = eulers_tub(1);
  theta_tub(particle_index) = eulers_tub(2);
  psi_tub(particle_index) = eulers_tub(3);

  coords_mt = coords_mt + euler_xform * [0; 0; delta_z(particle_index)];

  if(particle_index == tub_origin)
    offset_euler_xform = euler_xform;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now shift the coordinates so that the origin coincides with the MT axis,
%  at the center repeat.
% Note: origin is also shifted to account for the monomer_offset "slide"
%  (if performed above)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

micrograph_origin = offset_euler_xform * ...
                     [0; 0; axial_dist(tub_origin)];
micrograph_origin = micrograph_origin';

x_tub = x_tub + micrograph_origin(1) - x_mt(tub_origin);
y_tub = y_tub + micrograph_origin(2) - y_mt(tub_origin);
z_tub = z_tub + micrograph_origin(3) - z_mt(tub_origin);

x_mt = x_mt + micrograph_origin(1) - x_mt(tub_origin);
y_mt = y_mt + micrograph_origin(2) - y_mt(tub_origin);
z_mt = z_mt + micrograph_origin(3) - z_mt(tub_origin);

return
