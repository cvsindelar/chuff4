function [tubrefs, kinrefs, box_dim, ref_dim, ref_pixel, ref_com, min_theta, d_angle] = ...
    get_subunit_refs(chumps_round, micrograph_pixel_size, filament_outer_radius, bin_factor)

%%%%%%%%%%%%%%%%%%
% Get reference info
%%%%%%%%%%%%%%%%%%

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
tub_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);

ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,bin_factor);
kin_ref_file = sprintf('%s/ref_tot.spi',ref_dir);

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
com_info = readSPIDERdoc_dlmlike(com_doc_name);
%  com_info = dlmread(com_doc_name);
index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
%  ref_pixel_info = dlmread(ref_params_name);
ref_pixel_info = readSPIDERdoc_dlmlike(ref_params_name);
index = find(ref_pixel_info(:,1) == 2);
ref_pixel = ref_pixel_info(index(1),5);
index = find(ref_pixel_info(:,1) == 1);
min_theta = ref_pixel_info(index(1),5);
index = find(ref_pixel_info(:,1) == 3);
d_angle = ref_pixel_info(index(1),3);

box_dim = ceil(1.5*2*filament_outer_radius/ref_pixel);
box_dim = 16*ceil(box_dim/16);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);

tubrefs = readSPIDERfile(tub_ref_file);
kinrefs = readSPIDERfile(kin_ref_file);

