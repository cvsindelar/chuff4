function make_syn_graph(job_dir, syn_header, mic_dim, pixel_size)


job_dir = trim_dir(job_dir);
load(sprintf('%s_param.txt', syn_header));
load(sprintf('%s_coords.txt', syn_header));

graph_file = sprintf('graphs/%s.txt', job_dir);
graph_file_frealign = sprintf('graphs/%s_5.txt', job_dir);
f = fopen(graph_file, 'w');
ff = fopen(graph_file_frealign, 'w');
pi2 = 180 / pi;
for i = 1 : prod(size(phi_mt))
    fprintf(f, '%8.2f %8.2f %8.2f %8.2f %8.2f %d\n', phi_mt(i) * pi2, theta_mt(i) * pi2, 270, 0,0, 1);
    fprintf(ff, '%8.2f %8.2f %8.2f %8.2f %8.2f %d\n', phi_mt(i) * pi2, theta_mt(i) * pi2, 0, 0,0, 1);
end
fclose(f);
fclose(ff);

