function score_matrix = ...
  get_full_score_matrix(mini_score_list, overlap_map_list, n_classes,...
                        tm_num, begin_box, end_box, accelerate_binary);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The idea here: 
%  We will construct a transfer matrix that enumerates the score for all states of 
%   an overlapping pair of helical repeats (let's call them n and n+1). The first coordinate 
%   of the matrix corresponds to states of repeat n, the second corresponds to states of repeat n+1.
%
%  Then, in order to accelerate the computation, we make use of pre-computed scores
%   corresponding to particular sites (sites defined by overlap_map_list, 
%   scores defined by mini_score_list)
%
%  The following describes the accelerated score computation for a given state of the repeat pair.
%  We start with a state vector like:
%    SV = 0 3 2 1 0 0 2 1 3 0
%  The first half of the sites belong to repeat n; the second half belong to repeat n+1.
%
%  We initialize the list of complete sites to be zero:
%    SVC_i = 0 0 0 0 0 0 0 0 0 0
%  We also initialize the cumulative score:
%     S_0 = 0
%  Note that all scores within this algorithm will be computed with respect to the 
%   "ground state" with all sites identically occupying the first state (= 0).
%  We now proceed through the state vector sites in order. For each non-zero site i, 
%   we identify the overlapping sites with the overlap_map_list.
%  For example, say the overlap_map_list has the following for site i = 1:
%    1 3 4 5
%  We then consider these sites (the overlapping set for site i = 1):
%    0 - 2 1 0 - - - - -
%  Then, for the given state of the overlapping set, we look up the pre-calculated score
%   corresponding to position i,  
%   S_{full_i_overlap} 
%   corresponding to position i's being in the presence of this particular state of 
%   the overlapping set. 
%  We then do the same thing with the completed site list to get the pre-calculated
%   score for the overlapping set, site i, in the completed site list
%   S_{completed_i_overlap}
%  The difference between these,
%   S_{full_i_overlap} - S_{completed_i_overlap}
%   represents the net contribution we can now add to the cumulative score:
%  S_i = S_i-1 + S_{full_i_overlap} - S_{completed_i_overlap}
%  We then update the completed site list to reflect the new sites that have just been
%   considered.  In this case, one gets:
%  SVC_i = 0 0 2 1 0 0 0 0 0 0
%
%  The algorithm then just repeats with the next site i+1 and so on to the last site

if(nargin < 7)
  accelerate_binary = 0;
end

lib_size = size(overlap_map_list);
lib_size = lib_size(1);

if(tm_num == begin_box || tm_num == end_box + 1)
  n_subunits = lib_size;
else
  n_subunits = lib_size/2;
end

score_matrix = zeros([n_classes^n_subunits n_classes^n_subunits]);

complete_omap_list = overlap_map_list;
complete_mini_score_list = mini_score_list;

if(tm_num == begin_box)
  max_i = 1;
%%%%%%%%%%%%%%%%%%%%%%
% For the first box, the reference library only covers the 2nd dimension (j)
%  of the transfer matrix, and so the overlap map corresponds to the 2nd
%  half of the state vector. Thus, we need to fill out the overlap map
%  and adjust the map values so they correspond to the entire state vector
%%%%%%%%%%%%%%%%%%%%%%
  s = size(overlap_map_list);
  complete_omap_list = zeros([2*n_subunits s(2)]);
  complete_omap_list(n_subunits+1:2*n_subunits,:) = overlap_map_list + n_subunits;
  s = size(mini_score_list);
  complete_mini_score_list = zeros([2*n_subunits s(2)]);
  complete_mini_score_list(n_subunits+1:2*n_subunits,:) = mini_score_list;
else
  max_i = n_classes^n_subunits;
end

if(tm_num == end_box + 1)
  max_j = 1;
else
  max_j = n_classes^n_subunits;
end

if(accelerate_binary)
  score_matrix = ...
    get_full_score_matrix_accel(complete_mini_score_list, complete_omap_list, n_classes, n_subunits, ...
                                max_i, max_j);
  return
end

for i=1:max_i
  for j=1:max_j
    state_vec = get_states_from_num( i + (j-1)*n_classes^n_subunits, n_classes, 2*n_subunits);
    completed_vec = ones([1 2*n_subunits]);

    score = 0;
    pos = 1;

    while(! isequal(completed_vec, state_vec) && pos <= 2*n_subunits)
      indices = complete_omap_list(pos,:);
      indices = indices(find(indices));

      mini_state_vec = state_vec(indices);
      mini_completed_vec = completed_vec(indices);

      mini_state_index = get_num_from_states(mini_state_vec,n_classes);
      mini_completed_index = get_num_from_states(mini_completed_vec,n_classes);
      if(mini_state_index != mini_completed_index)
        score = score + ...
                complete_mini_score_list(pos, mini_state_index) - ...
                complete_mini_score_list(pos, mini_completed_index);

        completed_vec(indices) = state_vec(indices);
      end

      pos = pos + 1;

    end
    score_matrix(i,j) = score;
  end
end
