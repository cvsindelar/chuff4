function [fsc_array final_sigma2_array inv_tau2_array] = wienerize_helimap_vols(pad_factor)

global vol_array numerator_array denominator_array subunit_array debug

vol_size = size(vol_array);
n_classes = vol_size(4);
vol_size = vol_size(1:3);
pad_vol_size = vol_size * pad_factor;

%%%%%%%%%%%%%%%%%%%
% Compute gold-standard FSC's in order to estimate the SSNR
%%%%%%%%%%%%%%%%%%%

for iclass=1:n_classes
  fsc = FSCorr(vol_array(:,:,:,iclass,1), vol_array(:,:,:,iclass,2));
  if(iclass == 1)
    fsc_array = zeros(prod(size(fsc)),n_classes);
  end
  fsc_array(:,iclass) = fsc;
end  

%%%%%%%%%%%%%%%%%%%
% Apply the single-particle Wiener/MAP filter using SSNR's derived from the FSC
%%%%%%%%%%%%%%%%%%%

sz = vol_size;
n = sz(1);

[x y z] = ndgrid(-floor(sz(1)/2):-floor(sz(1)/2)+sz(1)-1,...
                 -floor(sz(2)/2):-floor(sz(2)/2)+sz(2)-1,...
                 -floor(sz(3)/2):-floor(sz(3)/2)+sz(3)-1);
% zero at element n/2+1.

R = sqrt(x.^2 + y.^2 + z.^2);
eps = 1e-4;

inv_tau2_array = zeros(prod(size(fsc)),n_classes, 2);
final_sigma2_array = zeros(prod(size(fsc)),n_classes, 2);

for iclass=1:n_classes
for igold=1:2
%%%%%%%%%%%
% Default estimate for 1/tau2 (one divided by the structure factor magnitude squared)
%  is very large, corresponding to a very small value of tau2.
%%%%%%%%%%%
  inv_tau2 = 1/eps * ones(vol_size);

  final_sigma2_est = denominator_array(:,:,:,iclass,igold);

  for i=1:n/2
    ring=find(R<0.5+i+eps & R >= i-0.5+eps);

%%%%%%%%%%%
% 1/tau2 = (1-FSC)/ FSC * est_sigma2
% Make sure the 1/tau2 estimate doesn't blow up (if FSC is too close to 0)
%%%%%%%%%%%
    if( fsc_array(i, iclass) > eps)
      inv_tau2_val = (1 - fsc_array(i, iclass)) / fsc_array(i, iclass);
    else
      inv_tau2_val = eps / (1 - eps);
    end

    final_sigma2_val = sum(sum(sum(final_sigma2_est(ring)))) / prod(size(ring));
    inv_tau2_val = inv_tau2_val * final_sigma2_val;

    inv_tau2(ring) = inv_tau2_val;
                         
    final_sigma2_array(i,iclass,igold) = final_sigma2_val;
    inv_tau2_array(i,iclass,igold) = inv_tau2_val;
  end

  vol_array(:,:,:,iclass,igold) = ...
   fftshift(real(ifftn(ifftshift(numerator_array(:,:,:,iclass,igold) ./ ...
                                (denominator_array(:,:,:,iclass,igold) + inv_tau2)))));
  vol_array(:,:,:,iclass,igold) = ...
    vol_array(:,:,:,iclass,igold) ./ get_correct3d(vol_size, pad_vol_size);
end
end;


