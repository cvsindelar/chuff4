function found_kinesins(target_box_num,job_dir,filter_resolution,use_ctf,focal_mate,bin_factor,...
                        chumps_round,tubrefs,kinrefs,ref_tub,ref_kin)

n_normal_args = 7;

job_dir = trim_dir(job_dir);

%mkdir(sprintf('chumps_round1/%s/find_kinesin/',job_dir));
%mkdir(sprintf('chumps_round1/%s/fmate_find/',job_dir));

stdout = 1;

tubrefs = [];
kinrefs = [];
ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
tub_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);

ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,bin_factor);
kin_ref_file = sprintf('%s/ref_tot.spi',ref_dir);

%focal_mate = str2num(argv(){1});
%target_box_num = str2num(argv(){2});

num_args = 2;

% num_mts = nargin - num_args;
num_mts = 1;

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m';
else
  run 'chuff_parameters';
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification

tot_n_particles = 1;
scale_avg = 0;
n_scale_avg = 0;
tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

if(isoctave)
  ctf_doc = dlmread('ctf_files/ctf_docfile_for_frealign.spi');
else
  ctf_doc = readSPIDERdoc_dlmlike('ctf_files/ctf_docfile_for_frealign.spi');
end

tot_n_particles = 1;

for mt_num = 1: num_mts
%%%%%%%%%%%%%%%
% Read in filenames, etc.
%%%%%%%%%%%%%%%

  [micrograph,defocus,astig_mag,astig_angle,b_factor] = ...
    chumps_micrograph_info(job_dir,focal_mate,chumps_round);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read in box info and extract particles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  presa = 50.0;
  dpres = 50.0;

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

  ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
  com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
  com_info = readSPIDERdoc_dlmlike(com_doc_name);
%  com_info = dlmread(com_doc_name);
  index = find(com_info(:,1) == 1);
  ref_com = com_info(index(1),3:5);

  ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
%  ref_pixel_info = dlmread(ref_params_name);
  ref_pixel_info = readSPIDERdoc_dlmlike(ref_params_name);
  index = find(ref_pixel_info(:,1) == 2);
  ref_pixel = ref_pixel_info(index(1),5);
  index = find(ref_pixel_info(:,1) == 1);
  min_theta = ref_pixel_info(index(1),5);
  index = find(ref_pixel_info(:,1) == 3);
  d_angle = ref_pixel_info(index(1),3);

  bin_factor = round(ref_pixel/micrograph_pixel_size);
  final_pixel_size = micrograph_pixel_size*bin_factor;

  box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
  box_dim = 16*ceil(box_dim/16);
  box_dim = box_dim*bin_factor;
  box_dim = [box_dim box_dim];

  ref_dim = floor(box_dim/bin_factor);

% TEMPORARY!!
  pad_ref_dim = 4*ref_dim;

  [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

  if(focal_mate == 0)
    find_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
    micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
  else
    find_dir = sprintf('chumps_round1/%s/fmate_find',job_dir);
    micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
  end

%    micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));

%%%%%%%%%%%%%%%
% Get CTF info
%%%%%%%%%%%%%%%
  if(use_ctf ~= 0)
    [micrograph,defocus,astig_mag,astig_angle,b_factor] = ...
      chumps_micrograph_info(job_dir,focal_mate,chumps_round);

    electron_lambda = EWavelength(accelerating_voltage);
    ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
    ctf = ifftshift(ctf);

   box_ctf = CTF_ACR(2*box_dim(1), micrograph_pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  box_ctf = ifftshift(box_ctf);
  else
    ctf = -ones(pad_ref_dim);
  box_ctf = -ones(2*box_dim);
  end

  ctf_flip = sign(ctf);
box_ctf_flip = sign(box_ctf);

  mic_dim = ReadMRCheader(micrograph);

%%%%%%%%%%%%%%%%%%%%%%%
% To save time, can pre-load the reference images
%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < n_normal_args+1)
  tubrefs = readSPIDERfile(tub_ref_file);
end
if(nargin < n_normal_args+2)
  kinrefs = readSPIDERfile(kin_ref_file);
end

%%%%%%%%%%%%%%%
% Pre-compute filtering terms
%%%%%%%%%%%%%%%
  filter = ifftn(ones(pad_ref_dim));
  if(filter_resolution > 0)
    lp_freq = 0.5*(2*ref_pixel) / filter_resolution;
    delta_lp_freq = lp_freq/10;
    filter = SharpFilt(filter,lp_freq,delta_lp_freq);
  end
  filter = fftn(filter);

% highpass_resolution = 200;
highpass_resolution = 1e9;
filter_resolution = 2*micrograph_pixel_size;

box_filter = ifftn(ones(2*box_dim));
lp_freq = 0.5*(2*micrograph_pixel_size) / filter_resolution;
delta_lp_freq = lp_freq/10;
box_filter = SharpFilt(box_filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*micrograph_pixel_size) / highpass_resolution;
delta_hp_freq = hp_freq/10;
box_filter = SharpHP(box_filter,hp_freq,delta_hp_freq);

box_filter = fftn(box_filter);

  [num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
    est_repeat_distance] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,1,micrograph_pixel_size);

  est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

  [coords,phi,theta,psi,...
   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance] = ...
    fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_pix_per_repeat, num_pfs, num_starts, ref_com);

  n_good_boxes = prod(size(phi));

  if(isoctave)
    radon_scale = dlmread(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));
  else
    radon_scale = readSPIDERdoc_dlmlike(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));
  end

  index = find(radon_scale(:,1) == 1);
  radon_scale = radon_scale(index(prod(size(index))),3);
  scale_avg = scale_avg + radon_scale;
  n_scale_avg = n_scale_avg + 1;

%%%%%%%%%%%%%%%%%%
% Count how many "good" boxes are fully inside the micrograph
%%%%%%%%%%%%%%%%%%

  real_n_good_boxes = 0;
  for box_num=1:n_good_boxes
    int_coords = round(coords(box_num,:));
    box_origin = int_coords-floor(box_dim/2);
    if(box_origin(1) < 1 || box_origin(2) < 1 || ...
       box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
      continue
    end
    real_n_good_boxes = real_n_good_boxes + 1;
  end

%%%%%%%%%%%%%%%%%%
% Read in decoration map
%%%%%%%%%%%%%%%%%%
  max_overlap = 2*num_pfs;
  n_kins_to_find = 2*num_pfs; % 6*num_pfs;
% TEMPORARY!!
%  num_repeats = ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
  num_repeats = 2*ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
  kin_lib_size = 2*num_repeats*num_pfs;

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;

  map_file = sprintf('%s/tot_decorate_map.spi',find_dir);
%  map_file = 'tot_decorate_map.spi';
  tot_decorate_map = readSPIDERfile(map_file);
%  sorted_map_file = sprintf('%s/sorted_map_composite.spi',find_dir);
%  sorted_map_file = sprintf('%s/sorted_map_full.spi',find_dir);
%  sorted_map = readSPIDERfile(sorted_map_file);

%  s1 = size(tot_decorate_map);
%  s2 = size(sorted_map);
%  sort_map_pad = (s2(2) - s1(2))/2

  fprintf(stdout,'\n%s\n', job_dir);

  real_box_num = 0;
  for box_num=1:n_good_boxes

    fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
    if(isoctave)
      fflush(stdout);
    end

    int_coords = round(coords(box_num,:));
    box_origin = int_coords-floor(box_dim/2);
    if(box_origin(1) < 1 || box_origin(2) < 1 || ...
       box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
      printf('  Box not completely within micrograph: (%d, %d)\n',...
              box_origin(1),box_origin(2));
      continue
    end

%  box = read_chumps_box(micrograph,box_num,coords,box_dim,...
%                        bin_factor,box_ctf_flip.*box_filter);
%  outfile = sprintf('%s/find_%04d_1b.spi',find_dir,box_num);
%  writeSPIDERfile(outfile,box);

%if(box_num > 0 && box_num < 1000)
if(box_num == target_box_num || target_box_num == 0)
    ref_pixel = micrograph_pixel_size * bin_factor;  % not used

    [box_repeat_origin, kin_repeat_index, kin_pf_index,...
     x_tub, y_tub, phi_tub, theta_tub, psi_tub, phi_mt, theta_mt, psi_mt, ref_tub, ref_kin] = ...
      get_findkin_refs(box_num, ref_dim,num_repeats,num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com,...
                   tubrefs,kinrefs,ctf,ctf_flip,filter,...
                   ref_pixel,min_theta,d_angle);

%    lib_origin_offset = -floor(n_kins_to_find/2);
    lib_origin_offset = 0;

    dec_origin = floor(kin_lib_size/2) + lib_origin_offset;
    dec_repeat_origin = kin_repeat_index(dec_origin + 1);

    decorate = zeros(kin_lib_size,1);
    for j = dec_origin - kin_lib_size/2:2*num_pfs:dec_origin + kin_lib_size/2-1
%    for j = dec_origin:dec_origin
%    for j = dec_origin+2*num_pfs:dec_origin+2*num_pfs
%      if(directional_psi == 270)
        offset_sign = 1;
%      else
%        offset_sign = -1;
%      end
      offset = offset_sign * (j - dec_origin);
      lowerb = 1+(box_num-1)*n_kins_to_find + offset;
      upperb = box_num*n_kins_to_find + offset;

      dec_lowerb = j+1;
      dec_upperb = j+2*num_pfs;
      if(lowerb < 1)
        dec_lowerb = dec_lowerb + (1-lowerb);
        lowerb = 1;
      end
      if(upperb > prod(size(tot_decorate_map)))
        dec_upperb = dec_upperb - (upperb - prod(size(tot_decorate_map)));
        upperb = prod(size(tot_decorate_map));;
      end
      if(dec_lowerb < 1)
        lowerb = lowerb + (1-dec_lowerb);
        dec_lowerb = 1;
      end
      if(dec_upperb > kin_lib_size)
        upperb = upperb - (dec_upperb - kin_lib_size);
        dec_upperb = kin_lib_size;
      end

      decorate(dec_lowerb:dec_upperb) = tot_decorate_map(lowerb:upperb);
    end

%    control_repeat_map = tot_decorate_map( 1+(box_num-1)*n_kins_to_find: ...
%                                              box_num*n_kins_to_find);
%    mini_repeat_index = kin_repeat_index(dec_origin+1 : dec_origin+n_kins_to_find);
%    mini_pf_index = kin_pf_index(dec_origin+1 : dec_origin+n_kins_to_find);
%
%    mini_repeat_index = mini_repeat_index - dec_repeat_origin;
%    mini_repeat_index = mini_repeat_index + (box_num-1)*2;
%
%    sorted_repeat_index = mini_repeat_index(find(mini_repeat_index >= 1));
%    sorted_pf_index = mini_pf_index(find(mini_repeat_index >= 1));
%
%    sorted_index = num_pfs*(sorted_repeat_index-1)+sorted_pf_index;
%    repeat_map = sorted_map(sorted_index + sort_map_pad*num_pfs);

    final_map = ref_tub;
    for j=1:kin_lib_size
      final_map = final_map + decorate(j)*ref_kin(:,:,j);
    end

    outfile = sprintf('%s/find_%04d_5.spi',find_dir,box_num);
    writeSPIDERfile(outfile,final_map);

end % if(box_num == 40)

end % for box_num...

end % for mt_num ...

return

