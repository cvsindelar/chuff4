function ref_index = get_ref_index(phi,theta,min_theta,d_angle)
% function ref_index = get_ref_index(phi,theta,min_theta,d_angle)
%  NB: get_ref_index takes arguments in ***degrees***, not radians!

phi = mod(phi,360);
theta = mod(theta,360);

if(theta < min_theta || theta > 180-min_theta)
  fprintf('WARNING: references do not include theta = %f\n',theta);
  if(theta < min_theta - d_angle/2)
    theta = min_theta;
  end
  if(theta > d_angle/2 + 180-min_theta)
    theta = 180-min_theta;
  end
end

min_phi = 0;
max_phi = 359.99;

phi_window = floor((max_phi-min_phi)/d_angle) + 1;
  % NB ASSUME min_phi = 0, max_phi = 179.99 below!!

ref_index = floor((phi - min_phi)/d_angle + 0.51) + 1;
  % angle index corresponding to phi

if(ref_index > floor(359.99/d_angle) + 1)
  % handle wraparound if phi >= 360 (or even if phi is 
  %  slightly less than 360, but would round up to 360)
  phi = phi - 360;
  ref_index = ref_index - (floor(359.99/d_angle) + 1);
end

% mirror = 0
% if(ref_index > phi_window) 
% % For axial angles >= 180 we mirror image
%   mirror = 1
%   ref_index = ref_index - phi_window;   
%    % the ref image corresponding to the axial angle - 180
%   theta = 180 - theta 
%    % Converting theta here is necessary to be consistent with 
%    %  this symmetry operation
% end
  
ref_index = ref_index + phi_window*floor( (theta -min_theta)/d_angle + 0.51);
   % add the tilt index

if(ref_index > ((180-min_theta) - min_theta + 1)*phi_window)
  fprintf('WARNING: ref_index out of bounds: phi,theta = %f %f\n',phi,theta);
  ref_index = ((180-min_theta) - min_theta + 1)*phi_window;
end
