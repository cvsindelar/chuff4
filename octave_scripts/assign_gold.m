function assign_gold(n)

rand_gold = rand([n 1]);

[ordered,ord_idx]=sort(rand_gold(:));

goldvals = 2*ones([n 1]);
goldvals(ord_idx(1:floor(n/2))) = 1;

for ind=1:n
  printf("%d ", goldvals(ind));
end

