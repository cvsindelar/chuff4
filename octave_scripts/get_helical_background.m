function avg = get_helical_background(box, psi, helical_radius)

dim = size(box);

box_orig = 1+floor(dim/2);
[x y] = ndgrid(-box_orig(1):-box_orig(1)+dim(1)-1,...
               -box_orig(2):-box_orig(2)+dim(2)-1);

radial_vec = [-sin(-pi/180*psi) cos(-pi/180*psi)];
axial_vec = [cos(-pi/180*psi) sin(-pi/180*psi)];

background_mask = ones(dim);
background_mask(find( abs(x*radial_vec(1) + y*radial_vec(2)) < helical_radius)) = 0;
background_mask(find( abs(x*axial_vec(1) + y*axial_vec(2)) > helical_radius)) = 0;

avg = sum(background_mask(:).*box(:))/prod(size(find(background_mask)));

% avg = background_mask;

