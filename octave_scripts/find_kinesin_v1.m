function [tot_decorate_map,sorted_map,ref_tubout,ref_kinout] = ...
           find_kinesin_v1(job_dir,filter_resolution,use_ctf,focal_mate,...
                        tubrefs,kinrefs,ref_tub,ref_kin)

iterate = 1;
iteration = 1;

n_normal_args = 4;
highpass_resolution = 200;

%%%%%%%%%%%%%%%%%%
% Get input arguments
%%%%%%%%%%%%%%%%%%

mkdir(sprintf('chumps_round1/%s/find_kinesin/',job_dir));
mkdir(sprintf('chumps_round1/%s/fmate_find/',job_dir));

tub_ref_file = 'chumps_round1/ref_tub_subunit/ref_tot.spi';
kin_ref_file = 'chumps_round1/ref_kin_subunit/ref_tot.spi';
ref_params_name = 'chumps_round1/ref_tub_subunit/ref_params.spi';
com_doc_name = 'chumps_round1/ref_tub_subunit/tub_cgr_doc.spi';
ctf_doc_name = 'ctf_files/ctf_docfile_for_spider.spi';
mt_info_name = sprintf('chumps_round1/%s/selected_mt_type.txt',job_dir);

mt_info = dlmread(mt_info_name);
num_pfs = mt_info(1);
num_tub_starts = mt_info(2);
num_starts = num_tub_starts/2; % All functions require the "true" number of helical starts
                               %  (can be non-integral for tubulin)
%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m'

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

n_kins_to_find = 2*num_pfs; % 6*num_pfs;
max_overlap = 2*num_pfs;

num_repeats = ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
kin_lib_size = 2*num_repeats*num_pfs;

smoothing_hwidth = 2; % Halfwidth for smoothing euler angles

elastic_params = [0 0 0 0];
radius_scale_factor = 1;

select_pf = 0; # [num_pfs 1];

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

if(job_dir(prod(size(job_dir))) == '/')
  job_dir=job_dir(1:prod(size(job_dir))-1);
end
[start] = regexp(job_dir,'[^/]*$','start');
job_dir = job_dir(start:prod(size(job_dir)));

[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

if(focal_mate == 0)
  micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
else
  micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
end

micrograph_number = job_dir(1:start-1);
[start] = regexp(micrograph_number,'[1-9][0-9]*$','start');
micrograph_number = str2num(micrograph_number(start:prod(size(micrograph_number))));

if(focal_mate == 0)
  output_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
else
  output_dir = sprintf('chumps_round1/%s/fmate_find',job_dir);
end

overlap_file = sprintf('%s/overlap_map.spi',output_dir);

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

ctf_doc = dlmread(ctf_doc_name);

com_info = dlmread(com_doc_name);
pixel_info = dlmread(ref_params_name);

box_doc_name = sprintf('scans/%s.box',job_dir);
box_doc = dlmread(box_doc_name);

radon_scale = dlmread(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));

align_doc_name = ...
  sprintf('chumps_round1/%s/individual_align_doc_pf%d_start%d.spi',...
          job_dir,num_pfs,num_tub_starts);

align_doc = dlmread(align_doc_name);

good_align_doc_name = ...
  sprintf('chumps_round1/%s/guess_individual_align_doc_pf%d_start%d_good.spi',...
          job_dir,num_pfs,num_tub_starts);

good_align_doc = dlmread(good_align_doc_name);

%%%%%%%%%%%%%%%%%%
% Initialize file-related parameters
%%%%%%%%%%%%%%%%%%

n_good_boxes = size(good_align_doc);
n_good_boxes = n_good_boxes(1);

fprintf(stdout,'%s %4d\n', job_dir, n_good_boxes);

index = find(pixel_info(:,1) == 2);
ref_pixel = pixel_info(index(1),5);

index = find(pixel_info(:,1) == 1);
min_theta = pixel_info(index(1),5);

index = find(pixel_info(:,1) == 3);
d_angle = pixel_info(index(1),3);

index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

index = find(radon_scale(:,1) == 1);
radon_scale = radon_scale(index(prod(size(index))),3);

bin_factor = round(ref_pixel/micrograph_pixel_size);
final_pixel_size = micrograph_pixel_size*bin_factor;

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
box_dim = 64*ceil(box_dim/64);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);
pad_ref_dim = 2*ref_dim;

%%%%%%%%%%%%%%%%%%%%%%%
% Compute CTF's
%%%%%%%%%%%%%%%%%%%%%%%

index = find(ctf_doc(:,13) == micrograph_number);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));

if(focal_mate == 0)
  defocus = ctf_doc(index,3);

  astig_mag = ctf_doc(index,4);
  astig_angle = ctf_doc(index,5);
  b_factor = 200;
else
  defocus = 97902.2750;
  astig_mag = 0
  astig_angle = 0
  b_factor = 400
end

electron_lambda = EWavelength(accelerating_voltage);

if(defocus > 0 && use_ctf != 0)
  ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                       spherical_aberration_constant, ...
                       b_factor, amplitude_contrast_ratio, 0);
  ctf = ifftshift(ctf);

  box_ctf = CTF_ACR(box_dim(1), micrograph_pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  box_ctf = ifftshift(box_ctf);
else
  ctf = -ones(pad_ref_dim);
  box_ctf = ones(box_dim);
end

ctf_flip = sign(ctf);
box_ctf_flip = sign(box_ctf);

%%%%%%%%%%%%%%%
% Pre-compute filtering terms
%%%%%%%%%%%%%%%

filter = ifftn(ones(pad_ref_dim));
lp_freq = 0.5*(2*ref_pixel) / filter_resolution;
delta_lp_freq = lp_freq/10;
filter = SharpFilt(filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*ref_pixel) / highpass_resolution;
delta_hp_freq = hp_freq/10;
filter = SharpHP(filter,hp_freq,delta_hp_freq);

filter = fftn(filter);

box_filter = ifftn(ones(box_dim));
lp_freq = 0.5*(2*micrograph_pixel_size) / filter_resolution;
delta_lp_freq = lp_freq/10;
box_filter = SharpFilt(box_filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*micrograph_pixel_size) / highpass_resolution;
delta_hp_freq = hp_freq/10;
box_filter = SharpHP(box_filter,hp_freq,delta_hp_freq);

box_filter = fftn(box_filter);

%%%%%%%%%%%%%%%
% Read in particle coordinates
%  and estimate the true repeat spacing
%%%%%%%%%%%%%%%

tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

for j=1:n_good_boxes
  eulers = good_align_doc(j,3:5);
  box_num = good_align_doc(j,1);

  index = find(align_doc(:,1) == box_num);
  index = index(prod(size(index)));
  extra_inplane_rot = align_doc(index,3);
  coords(j,:) = align_doc(index,12:13);
%  coords(j,:) = [box_doc(box_num,1) box_doc(box_num,2)] + floor(box_doc(box_num,3)/2);
  shifts = align_doc(index,5:6);

  r = extra_inplane_rot*pi/180;

  shifts = [cos(r)*shifts(1)-sin(r)*shifts(2) ...
            sin(r)*shifts(1)+cos(r)*shifts(2)];

  psi(j) = eulers(1) - extra_inplane_rot;
  theta(j) = eulers(2);
  phi(j) = eulers(3);

  tilt_scale_avg = tilt_scale_avg + 1/sin(theta(j)*pi/180);
  n_tilt_scale_avg = n_tilt_scale_avg + 1;

  coords(j,:) = coords(j,:) - shifts;
  if(j > 1)
    dist_to_last(j) = norm(coords(j,:) - coords(j-1,:));
  end
end

%%%%%%%%%%%%%%%%%%%%%
% Regularize the spacing between repeats
%  and estimate psi from coordinates
%%%%%%%%%%%%%%%%%%%%%

tilt_scale_avg = tilt_scale_avg/n_tilt_scale_avg;
est_repeat_distance = helical_repeat_distance/(radon_scale*tilt_scale_avg);
pix_per_repeat_from_radon = est_repeat_distance/micrograph_pixel_size;

psi_est = zeros(n_good_boxes);

j=2;
while(j <= n_good_boxes)
  vec_to_last = coords(j,:) - coords(j-1,:);
  psi_est(j) = -180/pi*atan2(vec_to_last(2),vec_to_last(1));

  dist_to_last(j) = norm(vec_to_last);
  tub_monomer_spacing = round(2*dist_to_last/pix_per_repeat_from_radon);

  if(tub_monomer_spacing(j) ~= 2)
    if(tub_monomer_spacing(j) != 0)
      coords(j,:) = coords(j-1,:) + (coords(j,:)-coords(j-1,:))*2/tub_monomer_spacing(j);
    else
%%%%%%%%%%%%%%%%%%%%
% If there is no distance change, get rid of the current box and shift all information
%  to the left.
%%%%%%%%%%%%%%%%%%%%

      n_good_boxes = n_good_boxes - 1;
      if(j < n_good_boxes)
        coords(j:n_good_boxes,:) = coords(j+1:n_good_boxes+1,:);
        phi(j:n_good_boxes) = phi(j+1:n_good_boxes+1);
        theta(j:n_good_boxes) = theta(j+1:n_good_boxes+1);;
        psi(j:n_good_boxes) = psi(j+1:n_good_boxes+1);
        psi_est(j:n_good_boxes) = psi_est(j+1:n_good_boxes+1);
      end
      coords = coords(1:n_good_boxes,:);
      phi = phi(1:n_good_boxes);
      theta = theta(1:n_good_boxes);
      psi = psi(1:n_good_boxes);
      psi_est = psi_est(1:n_good_boxes);
      j=j-1;
    end
  end
  dist_to_last(j) = norm(coords(j,:) - coords(j-1,:));
  j = j + 1;
end

pix_per_repeat_from_params = helical_repeat_distance/micrograph_pixel_size
pix_per_repeat_from_coords = sum(dist_to_last(2:prod(size(dist_to_last))))/(prod(size(dist_to_last))-1)
pix_per_repeat_from_radon

%%%%%%%%%%%%%%%%%%%%%
% Smooth the Euler angles
%%%%%%%%%%%%%%%%%%%%%

psi_est(1) = psi_est(2);
psi_est_temp = psi_est;

for j=1:n_good_boxes
  cur_phi_est = 0;
  cur_theta_est = 0;
  cur_psi_est = 0;
  count = 0;

  if(phi(j) < 90 || phi(j) > 270)
    phi_temp_offset = 180;          % to avoid wraparound issues near 0 and 360
  else
    phi_temp_offset = 0;
  end
  if(psi_est_temp(j) < 90 || psi_est_temp(j) > 270)
    psi_temp_offset = 180;          % to avoid wraparound issues near 0 and 360
  else
    psi_temp_offset = 0;
  end
  for k=-smoothing_hwidth:smoothing_hwidth
    if(j+k-1 >= 1 && j+k <= n_good_boxes)
      cur_phi_est = cur_phi_est + mod(phi(j+k) + phi_temp_offset,360);
      cur_theta_est = cur_theta_est + theta(j+k);
      cur_psi_est = cur_psi_est + mod(psi_est_temp(j+k) + psi_temp_offset,360);
      count = count + 1;
    end
  end
  phi_est(j) = mod(cur_phi_est/count - phi_temp_offset,360);
  theta_est(j) = cur_theta_est/count;
  psi_est(j) = mod(cur_psi_est/count - psi_temp_offset,360);
end

%%%%%%%%%%%%%%%%%%%%%
% Estimate the Euler angle derivatives
%%%%%%%%%%%%%%%%%%%%%

d_phi_d_repeat = zeros([1 n_good_boxes]);
d_theta_d_repeat = zeros([1 n_good_boxes]);
d_psi_d_repeat = zeros([1 n_good_boxes]);
for j=1:n_good_boxes
  d_phi_d_repeat(j) = 0;
  d_theta_d_repeat(j) = 0;
  d_psi_d_repeat(j) = 0;
  count = 0;

  if(phi(j) < 90 || phi(j) > 270)
    phi_temp_offset = 180;          % to avoid wraparound issues near 0 and 360
  else
    phi_temp_offset = 0;
  end
  if(psi_est_temp(j) < 90 || psi_est_temp(j) > 270)
    psi_temp_offset = 180;          % to avoid wraparound issues near 0 and 360
  else
    psi_temp_offset = 0;
  end
  for k=-smoothing_hwidth:smoothing_hwidth
    if(j+k-1 >= 1 && j+k <= n_good_boxes)
      d_phi_d_repeat(j) = d_phi_d_repeat(j) + ...
         mod(phi_est(j+k)+phi_temp_offset,360) - mod(phi_est(j+k-1)+phi_temp_offset,360);
      d_theta_d_repeat(j) = d_theta_d_repeat(j)+ theta_est(j+k)-theta_est(j+k-1);
      d_psi_d_repeat(j) = d_psi_d_repeat(j) + ...
         mod(psi_est(j+k)+psi_temp_offset,360) - mod(psi_est(j+k-1)+psi_temp_offset,360);
      count = count + 1;
    end
  end
  d_phi_d_repeat(j) = d_phi_d_repeat(j)/count;
  d_theta_d_repeat(j) = d_theta_d_repeat(j)/count;
  d_psi_d_repeat(j) = d_psi_d_repeat(j)/count;
end

%%%%%%%%%%%%%%%
% Convert derivatives to units of distance
%%%%%%%%%%%%%%%

d_phi_d_repeat = d_phi_d_repeat/helical_repeat_distance;
d_theta_d_repeat = d_theta_d_repeat/helical_repeat_distance;
d_psi_d_repeat = d_psi_d_repeat/helical_repeat_distance;

%%%%%%%%%%%%%%%
% If psi is 90 (rather than 270), the MT segment points backwards relative to the 
%  path of the boxes...
%%%%%%%%%%%%%%%

if(eulers(1) == 90)
  d_phi_d_repeat = -d_phi_d_repeat;
  d_theta_d_repeat = -d_theta_d_repeat;
  d_psi_d_repeat = -d_psi_d_repeat;
else
  psi_est = psi_est + 180;
end

if(nargin < n_normal_args+1)
  tubrefs = readSPIDERfile(tub_ref_file);
end
if(nargin < n_normal_args+2)
  kinrefs = readSPIDERfile(kin_ref_file);
end

%%%%%%%%%%%%%%%%%%%%%%%
% Finally- the kinesin-finding loop
%%%%%%%%%%%%%%%%%%%%%%%

if(iteration > 1)
  tot_decorate_map = readSPIDERfile(sprintf('%s/tot_decorate_map.spi',output_dir));
else
  tot_decorate_map = zeros([num_pfs 2*n_good_boxes]);
end

sorted_map = zeros([num_pfs 2*n_good_boxes]);

tot_overlap_map = [];

mic_dim = ReadMRCheader(micrograph);

real_n_good_boxes = 0
for box_num=1:n_good_boxes

%%%%%%%%%%%%%%%%%%
% Read in boxed image, after re-centering the coordinates using the found
%  coords
%%%%%%%%%%%%%%%%%%

  int_coords = round(coords(box_num,:));
  box_origin = int_coords-floor(box_dim/2);
  if(box_origin(1) < 1 || box_origin(2) < 1 || ...
     box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
    continue
  end
  real_n_good_boxes = real_n_good_boxes + 1;
end

if(fopen(overlap_file) > 0)
  [header, overlap_dim] = readSPIDERheader(overlap_file);
  if(overlap_dim(3) == real_n_good_boxes)
    tot_overlap_map = readSPIDERfile(overlap_file);
    fprintf(stdout,'Finished reading pre-existing overlap map...\n');
    real_n_good_boxes
  else
    fprintf(stdout,'ERROR: incomplete overlap map found: %s\n',overlap_file);
    fprintf(stdout,'  Expected # of entries: %d\n',real_n_good_boxes);
    fprintf(stdout,'  # of entries in file: %d\n',overlap_dim(3));
    fprintf(stdout,'Please delete the map and re-run...\n');

    return;
%    delete(overlap_file);
  end
end

real_box_num = 0;
for box_num=1:n_good_boxes

  fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);

%%%%%%%%%%%%%%%%%%
% Read in boxed image, after re-centering the coordinates using the found
%  coords
%%%%%%%%%%%%%%%%%%

  int_coords = round(coords(box_num,:));
  frac_coords = coords(box_num,:) - int_coords;

  box_origin = int_coords-floor(box_dim/2);

  [orig_box,mrcinfo,msg] = ReadMRCwin(micrograph, box_dim, box_origin);
  if(orig_box == -1)
    printf('  Box not completely within micrograph: (%d, %d)\n',...
            box_origin(1),box_origin(2));
    continue;
  end

  real_box_num = real_box_num + 1;


  box = fftn(orig_box) .* FourierShift(box_dim(1), -frac_coords);
  box = box .* box_ctf_flip .* box_filter;
  box = invert_density * real(ifftn(box));

  box = BinImageCentered(box, bin_factor);

  std_dev = std(box(:));
  avg = mean(box(:));
  box = (box - avg)/std_dev;

%  outfile = sprintf('%s/find_%04d_0.spi',output_dir,box_num);
%  writeSPIDERfile(outfile,orig_box);

  outfile = sprintf('%s/find_%04d_1.spi',output_dir,box_num);
  writeSPIDERfile(outfile,box);

%%%%%%%%%%%%%%%%%%
% Generate the reference images, padded 2x to ensure that 
%  generate_micrograph() has room to place all the subunits
%%%%%%%%%%%%%%%%%%
 if(nargin < 7)

  monomer_offset = 0;

  [x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
   x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist mt_repeat_index mt_pf_index] =...
     microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                            est_repeat_distance,radius_scale_factor,monomer_offset,...
                            [phi_est(box_num) d_phi_d_repeat(box_num)],...
                            [theta_est(box_num) d_theta_d_repeat(box_num)],...
                            [psi_est(box_num) d_psi_d_repeat(box_num)],elastic_params);
  ref_tub = generate_micrograph(pad_ref_dim, tubrefs, ref_pixel, min_theta, d_angle, select_pf,...
                            x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                            [0 0]);
  ref_kin = ...
        generate_micrograph(pad_ref_dim, kinrefs, ref_pixel, min_theta, d_angle, select_pf,...
                            x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                            [0 0],1,1);

%%%%%%%%%%%%%%%%%%%%%
% Generate images corresponding to the alternate kinesin binding sites
%  by effectively swapping alpha tubulin for beta tubulin- 
%  we use a monomer-sized axial shift.
%%%%%%%%%%%%%%%%%%%%%

  monomer_offset = 1;
  [x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
   x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist] =...
     microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                            est_repeat_distance,radius_scale_factor,monomer_offset,...
                            [phi_est(box_num) d_phi_d_repeat(box_num)],...
                            [theta_est(box_num) d_theta_d_repeat(box_num)],...
                            [psi_est(box_num) d_psi_d_repeat(box_num)],elastic_params);
  ref_kin2 = ...
        generate_micrograph(pad_ref_dim, kinrefs, ref_pixel, min_theta, d_angle, select_pf,...
                            x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                            [0 0],1,1);

  ref_tub = ifftn(fftn(ref_tub) .* ctf .* ctf_flip .* filter);
  avg = mean(ref_tub(:));
  ref_tub = (ref_tub - avg);

%%%%%%%%%%%%%%%%%%
% Window out the final-sized reference images, after applying CTF
%%%%%%%%%%%%%%%%%%

  in_x = 1+floor(pad_ref_dim(1)/2)-floor(ref_dim(1)/2);
  in_y = 1+floor(pad_ref_dim(2)/2)-floor(ref_dim(2)/2);

  ref_tub = ref_tub(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
  ref_kin_final = zeros([ref_dim(1) ref_dim(2) kin_lib_size]);

%%%%%%%%%%%%%%%%%%%%%
% Make the final library with alternating repeats from the normal
%  and offset images, to keep them roughly in order by axial position
%%%%%%%%%%%%%%%%%%%%%

  for i=1:kin_lib_size/2
    k = 2*i-1;
    temp = real(ifftn(fftn(ref_kin(:,:,i)) .* ctf .* ctf_flip .* filter));
    ref_kin_final(:,:,k) = temp(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
    kin_repeat_index(k) = 2*mt_repeat_index(i)-1;
    kin_pf_index(k) = mt_pf_index(i);
    k = 2*i;
    temp = real(ifftn(fftn(ref_kin2(:,:,i)) .* ctf .* ctf_flip .* filter));
    ref_kin_final(:,:,k) = temp(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
    kin_repeat_index(k) = 2*mt_repeat_index(i);
    kin_pf_index(k) = mt_pf_index(i);
  end

%%%%%%%%%%%%%%%%%%
% Finished generating images
%%%%%%%%%%%%%%%%%%

  ref_kin = ref_kin_final;
  clear ref_kin_final ref_kin2

  ref_tubout = ref_tub;
  ref_kinout = ref_kin;
 end

%%%%%%%%%%%%%%%%%%
% Now do the kinesin finding 
%%%%%%%%%%%%%%%%%%

  outfile = sprintf('%s/find_%04d_2.spi',output_dir,box_num);
  writeSPIDERfile(outfile,ref_tub);

  [decorate] = combinatorial_cc(box,ref_tub,ref_kin, n_kins_to_find, ...
                                max_overlap,tot_overlap_map, ...
                                overlap_file,real_box_num);
  dec_origin = floor(kin_lib_size/2) - floor(n_kins_to_find/2);

  mini_decorate = decorate(dec_origin+1 : dec_origin+2*num_pfs);
  tot_decorate_map( 1+(box_num-1)*2*num_pfs : box_num*2*num_pfs) = ...
     mini_decorate;

  mini_repeat_index = kin_repeat_index(dec_origin+1 : dec_origin+2*num_pfs);
  mini_pf_index = kin_pf_index(dec_origin+1 : dec_origin+2*num_pfs);

  dec_repeat_origin = kin_repeat_index(dec_origin + 1);
  mini_dec_index = find(mini_decorate);

  mini_pf_index = mini_pf_index(mini_dec_index);

  mini_repeat_index = mini_repeat_index(mini_dec_index);
  mini_repeat_index = mini_repeat_index - dec_repeat_origin;
  mini_repeat_index = mini_repeat_index + (box_num-1)*2;

  sorted_repeat_index = mini_repeat_index(find(mini_repeat_index >= 1));
  sorted_pf_index = mini_pf_index(find(mini_repeat_index >= 1));

  sorted_index = num_pfs*(sorted_repeat_index-1)+sorted_pf_index;
  sorted_map(sorted_index) = 1;

%%%%%%%%%%%%%%%%%%
% Re-create the best match
%%%%%%%%%%%%%%%%%%

  final_map = ref_tub;
  final_map2 = ref_tub;
  for index=1:n_kins_to_find
    i = index + dec_origin;
    final_map = final_map + decorate(i)*ref_kin(:,:,i);
    if(i >= dec_origin && i <= dec_origin + n_kins_to_find-1)
      final_map2 = final_map2 + ref_kin(:,:,i);
    end
  end

  outfile = sprintf('%s/find_%04d_3.spi',output_dir,box_num);
  writeSPIDERfile(outfile,final_map);
  outfile = sprintf('%s/find_%04d_4.spi',output_dir,box_num);
  writeSPIDERfile(outfile,final_map2);

  outfile = sprintf('%s/tot_decorate_map.spi',output_dir);
  writeSPIDERfile(outfile,tot_decorate_map);

  outfile = sprintf('%s/sorted_map.spi',output_dir);
  writeSPIDERfile(outfile, sorted_map);

  fprintf(stdout, ...
    '\n%2d/%2d occupied; CCC = %6.4f; Naked CCC = %6.4f; Fulldec CCC = %6.4f\n', sum(sum(decorate)), num_pfs,...
                 ccc(box,final_map),...
                 ccc(box,ref_tub),...
                 ccc(box,final_map2));
end

return

