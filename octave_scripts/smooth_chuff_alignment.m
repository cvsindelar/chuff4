[coords_all_good, phi_all_good, theta_all_good, psi_all_good, ...
    d_phi_d_repeat_all_good, d_theta_d_repeat_all_good, d_psi_d_repeat_all_good, ...
    est_repeat_distance, psi_ref_all_good, psi_ref_smooth_all_good, discontinuities ] = ...
    function smooth_chuff_alignment(sparx_graph_file, frealign_graph_file, ...
            box_doc_file, output_file, ...
            job_dir, output_stack_dim, prerotate, ...
            sparx_bin_factor, output_bin_factor, ...
            guessed_helical_twist, subunits_per_repeat, ...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length)

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(nargin < 20)
  min_seg_length = 10;
end

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end
helical_repeat_distance
micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

do_repair_psi = 1;

rise_per_subunit = helical_repeat_distance;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Each 'particle', or repeat, contains one or more subunits. By default, we find the
%  number of subunits per repeat that minimizes the twist from one particle to
%  the next.
%%%%%%%%%%%%%%%%%%%%%%%%%%

if(subunits_per_repeat <= 0)
  subunits_per_repeat = ceil( (turns_per_repeat*360 - 180) / guessed_helical_twist);
  best_twist = 361;
  best_subunits_per_repeat = -1;
  while(abs(subunits_per_repeat*guessed_helical_twist) < turns_per_repeat * 360 + 180)
    cur_twist = mod(subunits_per_repeat*guessed_helical_twist,360);
    if(abs(cur_twist) < abs(best_twist))
      best_twist = cur_twist
      best_subunits_per_repeat = subunits_per_repeat;
    end
    if(abs(cur_twist-360) < abs(best_twist))
      best_twist = cur_twist - 360
      best_subunits_per_repeat = subunits_per_repeat;
    end
    subunits_per_repeat = subunits_per_repeat + 1;
  end
else
  best_subunits_per_repeat = subunits_per_repeat;
end

proto_twist = mod(subunits_per_repeat*guessed_helical_twist, 360)
if(proto_twist < -180) 
  proto_twist = proto_twist + 360;
end
if(proto_twist > 180)
  proto_twist = proto_twist - 360;
end

[coords, phi, theta, psi,directional_psi] = ...
  read_chuff_alignment(sparx_graph_file, 'null', box_doc_file,sparx_bin_factor, ...
                       do_repair_psi, 1);


[coords_frealign, phi_frealign, theta_frealign, psi_frealign,directional_psi] =   ...
  read_chuff_alignment(sparx_graph_file, frealign_graph_file, box_doc_file,...
                       sparx_bin_factor, do_repair_psi, 1);

[coords_est,phi_est,theta_est,psi_est,d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
 est_repeat_distance,  psi_ref] = ...
  fixup_chuff_alignment(coords,phi_frealign,theta_frealign,psi_frealign,...
                        directional_psi,  micrograph_pixel_size,rise_per_subunit,...
                        guessed_helical_twist, best_subunits_per_repeat, ...
                        'null');

[coords_est_smooth,phi_est_smooth,theta_est_smooth,psi_est_smooth,...
 d_phi_d_repeat_smooth,d_theta_d_repeat_smooth,d_psi_d_repeat_smooth,...
 est_repeat_distance_smooth, psi_ref, psi_ref_smooth, discontinuities, outliers] = ...
  fixup_chuff_alignment(coords,phi_frealign,theta_frealign,psi_frealign,...
                        directional_psi,micrograph_pixel_size,rise_per_subunit,...
                        guessed_helical_twist, best_subunits_per_repeat, ...
                        'smooth_all', fit_order, data_redundancy, max_outliers_per_window, ...
                        twist_tolerance, coord_error_tolerance,...
                        phi_error_tolerance, theta_error_tolerance, psi_error_tolerance);

smoothed_ali = [coords_est_smooth,phi_est_smooth,theta_est_smooth,psi_est_smooth,...
     d_phi_d_repeat_smooth,d_theta_d_repeat_smooth,d_psi_d_repeat_smooth];

n_particles = prod(size(phi_est));

%%%%%%%%%%%%%%%%%%%%
% ID the good segments of the filament
%%%%%%%%%%%%%%%%%%%%

% If there is gap between filament, helimap might not work?
% We only keep the longest filament now. 
%%%%%%%%%
% Break filament down into good segments
%%%%%%%%%
goodcount = 0;
segids = zeros([n_particles 1]);
segid = 0;
for ind=1:n_particles
  if(!discontinuities(ind))
    if(goodcount <= 0)
      segid = segid + 1;
    end
    goodcount = goodcount + 1;
    segids(ind) = segid;
  else
    segids(ind) = 0;
    goodcount = 0;
  end
end
max_count = -1;
max_ind = -1;
for ind = 1: 10
    if prod(size(find(segids == ind))) > max_count:
        max_count = prod(size(find(segids == ind)));
        max_ind = ind;
    end
end
max_ind, max_count
good_ali = smooth_ali(find(segids == max_ind), :)
% there will be subunits_per_repeat * (max_count - 1) + 1 units
n_all_subunits = (max_count - 1) * best_subunits_per_repeat + 1;

all_good_alis = zeros([n_all_subunits, 8])

all_good_ind = 1
all_good_alis(n_all_subunits, :) = good_ali(max_count, :)
for ind = 1 : max_count - 1
    
    % difference from next align to current align
    ali_diff= good_ali(ind + 1, :) - good_ali(ind, :)

    % phi and psi need extra logic.

    begin_psi = good_ali(ind, 5)
    begin_phi = good_ali(ind, 3)
    end_psi = good_ali(ind + 1, 5)
    end_phi = good_ali(ind + 1, 3)
    if(end_psi - begin_psi > 180)
      end_psi = end_psi - 360;
    elseif(end_psi - begin_psi < -180)
      end_psi = end_psi + 360;
    end

    diff1 = abs(floor(best_subunits_per_repeat*guessed_helical_twist / 360) * 360 + ...
                  begin_phi - end_phi - ...
                best_subunits_per_repeat*guessed_helical_twist);
    diff2 = abs(floor(best_subunits_per_repeat*guessed_helical_twist / 360 + 1) * 360 + ...
                  begin_phi - end_phi - ...
                best_subunits_per_repeat*guessed_helical_twist);
    
    if(diff1 < diff2)
      end_phi = end_phi + ...
                 floor(best_subunits_per_repeat*guessed_helical_twist / 360) * 360;
    else
      end_phi = end_phi + ...
                 floor(best_subunits_per_repeat*guessed_helical_twist / 360 + 1) * 360;
    end

    ali_diff(3) = begin_phi - end_phi
    ali_diff(5) = begin_psi - end_psi
    
    % now we can do linear inpolation for the middle subunits
    for i = 1 : best_subunits_per_repeat
        fract = (i - 1.0) / best_subunits_per_repeat
        all_good_alis(all_good_idx, :) = (good_ali(ind + 1, :)  - good_ali(ind, :)) * fract + good_ali(ind, :)
        all_good_idx += 1
    end 
    
end

% seperate the ali matrix
% coords_all_good, phi_all_good, theta_all_good, psi_all_good, ...
%        dphi_d_repeat_all_good, d_theta_d_repeat_all_good, d_psi_d_repeat_all_good, ...

coords = all_good_alis(:, 1:2);
phi_all_good = all_good_alis(:, 3);
theta_all_good = all_good_alis(:, 4);
psi_all_good = all_good_alis(:, 5);
d_phi_d_repeat_all_good = all_good_alis(:, 6);
d_theta_d_repeat_all_good = all_good_alis(:, 7);
d_psi_d_repeat_all_good = all_good_alis(:, 8);

