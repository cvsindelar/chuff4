function n_good_boxes = box_in_micrograph(coords, box_dim, mic_dim)
%%%%%%%%%%%%%%%%%%
% Count how many "good" boxes are fully inside the micrograph
%%%%%%%%%%%%%%%%%%

s = size(coords);
n_boxes = s(1);

n_good_boxes = 0;
for box_num=1:n_boxes
  int_coords = round(coords(box_num,:));
  box_origin = int_coords-floor(box_dim/2);
  if(box_origin(1) < 1 || box_origin(2) < 1 || ...
     box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
    continue
  end
  n_good_boxes = n_good_boxes + 1;
end
