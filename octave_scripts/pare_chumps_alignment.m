function [coords_est,phi_est,theta_est,psi_est] = ...
  pare_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_pix_per_repeat)

min_good_run = 3;  % need at least this many good boxes in a row at any given point on the filament
angle_diff_threshold = 3; % Tolerance for in-plane angular difference between adjacent boxes

n_good_boxes = prod(size(phi));

mt_vec = coords(n_good_boxes,:) - coords(1,:);
mt_vec = mt_vec/sqrt(mt_vec(1)**2+mt_vec(2)**2);

psi_est = zeros([1 n_good_boxes]);

dist_to_last = zeros([1 n_good_boxes]);
angle_to_last = zeros([1 n_good_boxes]);
bad_angles = zeros([1 n_good_boxes]);

for j = 2:n_good_boxes
  vec_to_last = coords(j,:) - coords(j-1,:);
  dist_to_last(j) = norm(vec_to_last);
  signed_dist_to_last = sum(vec_to_last.*mt_vec);

  psi_est(j) = -180/pi*atan2(vec_to_last(2),vec_to_last(1));
end

for j = 2:n_good_boxes
  if(j < n_good_boxes)
    angle_diff = psi_est(j+1) - psi_est(j);
    if(angle_diff > 180)
      angle_diff -= 360;
    end
    if(angle_diff < -180)
      angle_diff += 360;
    end
    if(abs(angle_diff) > angle_diff_threshold)
      bad_angles(j) = 1;
    end
  end
end

bad_dists = zeros([1 n_good_boxes]);
dists = dist_to_last - est_pix_per_repeat;
dists = abs(dists - est_pix_per_repeat * round(dists / est_pix_per_repeat));
bad_dists(find(dists > est_pix_per_repeat/4)) = 1;

bad_boxes = bad_angles + bad_dists;

%%%%%%%%%%%%%%%%%
% Find 'runs' of good and bad boxes
%%%%%%%%%%%%%%%%%

good_runs = [];
bad_runs = [];
for j=1:n_good_boxes
  new_bad_run = 0;
  new_good_run = 0;
  if(bad_boxes(j))
    if(j > 1)
      if(bad_boxes(j-1) == 0)
        new_bad_run = 1;
      end
    else
      new_bad_run = 1;
    end
  else
    if(j > 1)
      if(bad_boxes(j-1) != 0)
        new_good_run = 1;
      end
    else
      new_good_run = 1;
    end
  end
  if(new_bad_run)
    index = prod(size(bad_runs))/2;
    bad_runs(index+1,1) = j;
    bad_runs(index+1,2) = 0;
  end
  if(new_good_run)
    index = prod(size(good_runs))/2;
    good_runs(index+1,1) = j;
    good_runs(index+1,2) = 0;
  end
  if(bad_boxes(j))
    index = prod(size(bad_runs))/2;
    bad_runs(index,2) = bad_runs(index,2) + 1;
  else
    index = prod(size(good_runs))/2;
    good_runs(index,2) = good_runs(index,2) + 1;
  end
end

for j=1:prod(size(good_runs))/2
  if(good_runs(j,2) < min_good_run)
    for i = 1:good_runs(j,2)
      bad_boxes(good_runs(j,1)+i-1) = 1;
    end
  end
end

%%%%%%%%%%%%%%%%%%
% Delete the 'bad' boxes...
%%%%%%%%%%%%%%%%%%

j = 1;
while(j <= n_good_boxes)
  if(bad_boxes(j))
    if(j < n_good_boxes)
      bad_boxes(j:n_good_boxes-1) = bad_boxes(j+1:n_good_boxes);
      coords(j:n_good_boxes-1,:) = coords(j+1:n_good_boxes,:);
      phi(j:n_good_boxes-1) = phi(j+1:n_good_boxes);
      theta(j:n_good_boxes-1) = theta(j+1:n_good_boxes);;
      psi(j:n_good_boxes-1) = psi(j+1:n_good_boxes);
      psi_est(j:n_good_boxes-1) = psi_est(j+1:n_good_boxes);
    end
    n_good_boxes = n_good_boxes - 1;
    coords = coords(1:n_good_boxes,:);
    phi = phi(1:n_good_boxes);
    theta = theta(1:n_good_boxes);
    psi = psi(1:n_good_boxes);
    psi_est = psi_est(1:n_good_boxes);
    j = j - 1;
  end
  j = j + 1;
end

coords_est = coords;
phi_est = phi;
theta_est = theta;

% DEBUG
% phi'
% theta'
% psi_est'
% n_good_boxes
