function chuff_to_frealign(chuff_info_file, output_parameter_file, ...
                             job_dir, filament_number, output_bin_factor, prerotate)

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end

%%%%%%%%%%%%%
% Read in sxihrsr alignment
%  Note that the shifts must be scaled by the ratio of binning factors.
%%%%%%%%%%%%%

align_info = dlmread(chuff_info_file);

size_align = size(align_info);

phi = align_info(:,2);
theta = align_info(:,3);
psi = align_info(:,4);
shifts = align_info(:,5:6) / output_bin_factor;

%%%%%%%%%%%%%%%%%%%%%
% Find the current particle number from the output parameter file
%%%%%%%%%%%%%%%%%%%%%
if(prod(size(stat(output_parameter_file))) != 0)
  old_params = dlmread(output_parameter_file);
  size_fp = size(old_params);
  tot_n_particles = old_params(size_fp(1),1);

  fid = fopen(output_parameter_file,'a');
else
  fid = fopen(output_parameter_file,'w');
  tot_n_particles = 0;
endif

%%%%%%%%%%%%%%%%%%%%%
% Get micrograph number
%%%%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'[1-9]+','start');
[last] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
micrograph_number = str2double(sprintf('%s',job_dir(start:last-1)));

%%%%%%%%%%%%%%%%%%%%%
% Read CTF info
%%%%%%%%%%%%%%%%%%%%%
[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
ctf_doc = dlmread(sprintf('%s_ctf_doc.spi',job_dir(1:start-1)));
index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end
index = index(prod(size(index)));
defocus1 = ctf_doc(index,3);
defocus2 = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);

for i=1:size_align(1)
  tot_n_particles = tot_n_particles + 1;

%%%%%%%%%%%%
% Astigmatism angle should be rotated in the opposite direction as 
%  whatever we did with rotate2d in chuff_extract_boxes 
%  (see chuff3/info/rotation_sign_conventions.txt)
%%%%%%%%%%%%

  adjusted_astig_angle = astig_angle + psi(i));
  presa = 50.0;
  dpres = 50.0;
  fprintf(fid, ...
       '%7d%8.2f%8.2f%8.2f%8.2f%8.2f%8.0f%6d%9.1f%9.1f%8.2f%7.2f%6.2f\n', ...
       tot_n_particles, 0, theta(i), phi(i), 0, 0, ...
       magnification/output_bin_factor, filament_number, ...
       defocus1, defocus2, adjusted_astig_angle, presa, dpres);
end
