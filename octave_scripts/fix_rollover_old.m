function [new_angles best_rollover] = fix_rollover(angles, delta)

%%%%%%%%%%%%%%%%%%%%%
% This function addresses the problem that an angle that is constantly increasing or
%  decreasing over large distances along a filament will roll over from 360 to zero.
% We deal with this by sequentially testing the hypotheses that this roll over occurs 
%  at each position along the filament, and choosing the best match.
%%%%%%%%%%%%%%%%%%%%%

angle_std_dev = zeros([1 prod(size(angles))]);

for rollover_pos=0:prod(size(angles))-1
  temp_angles = mod(angles,360);
  temp_angles(1:rollover_pos) = temp_angles(1:rollover_pos) - sign(delta)*360;

  angle_avg = sum(temp_angles(:))/prod(size(temp_angles));
  ind_avg = sum(1:prod(size(angles)))/prod(size(angles));

  simple_est = angle_avg + ((1:prod(size(angles))) - angle_avg) * delta;
  angle_std_dev(rollover_pos+1) = sqrt(var(temp_angles(:) - simple_est(:)));
end

best_rollover = find(angle_std_dev == min(angle_std_dev));
best_rollover = best_rollover(1) - 1;

new_angles = mod(angles,360);
new_angles(1:best_rollover) = new_angles(1:best_rollover) - sign(delta)*360;
