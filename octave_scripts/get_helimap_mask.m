function helimask = get_helimap_mask(ref_dim, ref_pixel, tm_num, num_pfs, num_starts, ...
                     ref_com, repeat_distance, coords, ...
                     phi,theta,psi,...
                     d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat, ...
                     elastic_params, min_resolution, cos_edge_power)

if(nargin <= 17)
  cos_edge_power = 2;
end

n_good_boxes = prod(size(phi));

repeats_to_extend_prior = 0;
repeats_to_extend_post = 0;

if(tm_num == 1)
  num_box_repeats = 2;
  repeats_to_extend_prior = 1;
  box_repeat_origin = 2;
  target_box_num = 1;
elseif(tm_num == n_good_boxes + 1)
  num_box_repeats = 2;
  repeats_to_extend_post = 1;
  box_repeat_origin = 1;
  target_box_num = n_good_boxes;
else
  num_box_repeats = 2;
  box_repeat_origin = 2;
  target_box_num = tm_num;
end

extended_box_coords = zeros(num_box_repeats*num_pfs, 2);

for current_box_repeat = ...
  1+repeats_to_extend_prior:num_box_repeats-repeats_to_extend_post

  current_num_repeats = 1;
  current_repeat_origin = 1;
  if(repeats_to_extend_prior != 0 &&...
    current_box_repeat == 1+repeats_to_extend_prior)
    current_num_repeats = 1+repeats_to_extend_prior;
    current_repeat_origin = 1+repeats_to_extend_prior;
  end
  if(repeats_to_extend_post != 0 &&...
    current_box_repeat == num_box_repeats-repeats_to_extend_post)
    current_num_repeats = 1+repeats_to_extend_post;
    current_repeat_origin = 1;
  end

  box_num = target_box_num + (current_box_repeat - box_repeat_origin);    
  radius_scale_factor = 1;
  monomer_offset = 0;
  [x y] = ...
   microtubule_parametric([current_num_repeats current_repeat_origin],...
                          num_pfs,num_starts,ref_com,repeat_distance,...
                          radius_scale_factor,monomer_offset,...
                          [phi(box_num) d_phi_d_repeat(box_num)],...
                          [theta(box_num) d_theta_d_repeat(box_num)],...
                          [psi(box_num) d_psi_d_repeat(box_num)],...
                          elastic_params);

  x = x/ref_pixel + coords(box_num,1) - coords(target_box_num,1);
  y = y/ref_pixel + coords(box_num,2) - coords(target_box_num,2);

  first_repeat = current_box_repeat + 1 - current_repeat_origin;
  last_repeat =  current_box_repeat + 1 - current_repeat_origin + ...
                   current_num_repeats - 1;

  extended_box_coords( (first_repeat-1)*num_pfs + 1 : last_repeat*num_pfs,:) = [x; y]';
end

subunit1 = num_pfs;
subunit2 = (num_box_repeats-1)*num_pfs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the coordinates around which the mask is to be made
%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(tm_num == 1)
  x_junction1 = sum(extended_box_coords(1:num_pfs,1),1)/num_pfs;
  y_junction1 = sum(extended_box_coords(1:num_pfs,2),1)/num_pfs;
  x_junction2 = sum(extended_box_coords(1:2*num_pfs,1),1)/(2*num_pfs);
  y_junction2 = sum(extended_box_coords(1:2*num_pfs,2),1)/(2*num_pfs);
elseif(tm_num == n_good_boxes + 1)
  x_junction1 = sum(extended_box_coords(1:2*num_pfs,1),1)/(2*num_pfs);
  y_junction1 = sum(extended_box_coords(1:2*num_pfs,2),1)/(2*num_pfs);
  x_junction2 = sum(extended_box_coords(num_pfs+1:2*num_pfs,1),1)/num_pfs;
  y_junction2 = sum(extended_box_coords(num_pfs+1:2*num_pfs,2),1)/num_pfs;
else
  x_junction1 = sum(extended_box_coords(1:num_pfs,1),1)/num_pfs;
  y_junction1 = sum(extended_box_coords(1:num_pfs,2),1)/num_pfs;
  x_junction2 = sum(extended_box_coords(num_pfs+1:2*num_pfs,1),1)/num_pfs;
  y_junction2 = sum(extended_box_coords(num_pfs+1:2*num_pfs,2),1)/num_pfs;
end

x_junction1
y_junction1
x_junction2
y_junction2

x_junction = (x_junction1 + x_junction2)/2;
y_junction = (y_junction1 + y_junction2)/2;

if(target_box_num <= 1)
  psi_junction1 = psi(1);
  psi_junction2 = psi(1);
elseif(tm_num >= n_good_boxes + 1)
  psi_junction1 = psi(n_good_boxes);
  psi_junction2 = psi(n_good_boxes);
else
  psi_junction1 = psi(target_box_num-1);
  psi_junction2 = psi(target_box_num);
end

x_junction
x_junction1
x_junction2

psi_junction = (psi_junction1 + psi_junction2)/2;
vec = [cos(-psi_junction*pi/180) sin(-psi_junction*pi/180)];
vec1 = [cos(-psi_junction1*pi/180) sin(-psi_junction1*pi/180)];
vec2 = [cos(-psi_junction2*pi/180) sin(-psi_junction2*pi/180)];

[x y] = ndgrid(-floor(ref_dim(1)/2):-floor(ref_dim(1)/2)+ref_dim(1)-1,...
               -floor(ref_dim(2)/2):-floor(ref_dim(2)/2)+ref_dim(2)-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make the mask itself. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

helimask = ones(ref_dim);

%%%%%%%%%%%%%
% First, the longitudinal edges. Note that we may either desire cos or cos^2, depending
%  on what the mask is being used for. Default is cos^2. 
%%%%%%%%%%%%%

helimask(find( abs((x - x_junction) * vec(1) + (y - y_junction) * vec(2)) > ...
                repeat_distance/(2*ref_pixel))) = 0;

cos_edge_dim = min_resolution / (2*ref_pixel);

cos_edge = cos( pi/cos_edge_dim * ...
               (cos_edge_dim/2 + ...
                ((x - x_junction1) * vec1(1) + (y - y_junction1) * vec1(2))));
cos_edge = ((cos_edge+1)/2).^cos_edge_power;

%cos_edge_area = find( (x - x_junction1) * vec1(1) + (y - y_junction1) * vec1(2) > ...
%                         -cos_edge_dim/2 & ...
%                      (x - x_junction1) * vec1(1) + (y - y_junction1) * vec1(2) < ...
%                         cos_edge_dim/2);

%helimask(cos_edge_area) = cos_edge(cos_edge_area);

cos_edge = cos( pi/cos_edge_dim * ...
               (cos_edge_dim/2 + ...
                (-(x - x_junction2) * vec2(1) - (y - y_junction2) * vec2(2))));
cos_edge = ((cos_edge+1)/2).^cos_edge_power;

%cos_edge_area = find( (x - x_junction2) * vec2(1) + (y - y_junction2) * vec2(2) > ...
%                         -cos_edge_dim/2 & ...
%                      (x - x_junction2) * vec2(1) + (y - y_junction2) * vec2(2) < ...
%                         cos_edge_dim/2);

%helimask(cos_edge_area) = cos_edge(cos_edge_area);

%%%%%%%%%%%%%
% Then, the lateral edges:
%%%%%%%%%%%%%

seg_diag = sqrt( (repeat_distance/ref_pixel)^2 + ref_dim(1).^2);

seg_width = ref_dim(1)/seg_diag * ref_dim(1) - 2 * cos_edge_dim;

vec = [x_junction1 - x_junction2 y_junction1 - y_junction2];
vec = vec/(sqrt(sum(vec.^2)));

% Rotate vector by 90°
vec = [-vec(2) vec(1)];

helimask(find( abs(vec(1)*x + vec(2)*y) >= seg_width/2 + cos_edge_dim)) = 0;

cos_edge = cos( pi/cos_edge_dim * ...
                (abs((x - x_junction) * vec(1) + (y - y_junction) * vec(2)) - ...
                 seg_width/2));
cos_edge = ((cos_edge+1)/2).^cos_edge_power;

cos_edge_area = find( abs((x - x_junction) * vec(1) + (y - y_junction) * vec(2)) > ...
                         seg_width/2 & ...
                      abs((x - x_junction) * vec(1) + (y - y_junction) * vec(2)) < ...
                         seg_width/2 + cos_edge_dim);

helimask(cos_edge_area) = helimask(cos_edge_area).*cos_edge(cos_edge_area);
