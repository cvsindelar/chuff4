function [overlap_map_list, overlap_map] = get_overlap_map(data_box,synth_background, refs, error_tolerance, max_overlaps)

%%%%%%%%%%%%%%%%%%%%%%
% Generate overlap map, denotes which particles overlap with each other
%%%%%%%%%%%%%%%%%%%%%%

if(nargin < 4) 
  error_tolerance = 0.01; % Default is to discard overlaps with < 1% error
end

size_ref = size(refs);
lib_size = size_ref(3);
n_classes = size_ref(4);

if(nargin < 5) 
  max_overlaps = lib_size;  % Default is to keep as many overlaps as may exist
end

temp_map = zeros(lib_size,lib_size);
temp_map1 = zeros(lib_size,lib_size);
temp_map2 = zeros(lib_size,lib_size);
overlap_map = zeros(lib_size,lib_size);

for ind=1:lib_size
  if(ind == 1)
    ref_background = synth_background;
  else
    ref_background = ref_background + refs(:,:,ind,1);
  end
end

fprintf(stdout,'Computing overlap map:\n');
if(isoctave)
  fflush(stdout);
end

progressbar = 0;

deltaL = zeros([lib_size lib_size]);
deltaL_approx = zeros([lib_size lib_size]);

for i=1:lib_size
  for j=i+1:lib_size
     ref_i1 = squeeze(refs(:,:,i,1));
     ref_i2 = squeeze(refs(:,:,i,2));
     ref_j1 = squeeze(refs(:,:,j,1));
     ref_j2 = squeeze(refs(:,:,j,2));
     L11 = sum(sum((ref_background - data_box).^2));
     L21 = sum(sum((ref_background - ref_i1 + ref_i2 - data_box).^2));
     L12 = sum(sum((ref_background - ref_j1 + ref_j2 - data_box).^2));
     L22 = sum(sum((ref_background - ref_j1 + ref_j2 - ref_i1 + ref_i2 - data_box).^2));

     deltaL(i, j) = L22 - L11;;
     deltaL_approx(i, j) = (L21 - L11) + (L12 - L11);
  end

  if(i/lib_size > progressbar)
    fprintf(stdout, '%3.0f%% ', 100*progressbar);
    if(isoctave)
      fflush(stdout);
    end
    progressbar = progressbar + 0.1;
  end
end

fprintf(stdout, ' done.\n');
fflush(stdout);

deltaL = deltaL + tril(deltaL');
deltaL_approx = deltaL_approx + tril(deltaL_approx');
% overlap_map = abs( (deltaL - deltaL_approx) ./ deltaL) > error_tolerance;
overlap_map = abs( (deltaL - deltaL_approx) / max(abs(deltaL(:))));

overlap_map = overlap_map + eye(size(overlap_map));

%%%%%%%%%%%%%%%%%%%%%
% Throw away the least-important scores: first, keep only a max of 'max_overlaps' 
%  for each particle
%%%%%%%%%%%%%%%%%%%%%

max_discard_error = 0;

overlap_map_list = zeros([lib_size lib_size]);
for i=1:lib_size
  overlap_map_list(i,:) = overlap_map(i,:);
  [overlap_map_list_sort, discard_indices] = sort(overlap_map_list(i,:));
  discard_indices = discard_indices(1:(lib_size - max_overlaps));
  max_discard_error = max([max_discard_error overlap_map(i,discard_indices)]);
  overlap_map(i,discard_indices) = 0;
end

%%%%%%%%%%%%%%%%%%%%%
% Discard any other points below the error tolerance
%%%%%%%%%%%%%%%%%%%%%

overlap_map(overlap_map < error_tolerance) = 0;

max_dim = -1;
overlap_map_list = zeros([lib_size lib_size]);
for i=1:lib_size
  overlap_map_list(i,1:prod(size(find(overlap_map(i,:))))) = find(overlap_map(i,:));
  max_dim = max([max_dim prod(size(find(overlap_map(i,:))))]);
end

max_num_overlaps = max_dim
max_discard_error
overlap_map_list = overlap_map_list(:,1:max_dim);
