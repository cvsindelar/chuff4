function apply_movie_shifts(mic, output_mic, gainref, shifts_file, first_frame, last_frame, pix_size);

m = double(ReadMRC(mic));
s = size(m);
if(prod(size(s)) == 2)
  s = [s(1) s(2) 1];
end

first_frame_true = first_frame+1;
if(last_frame == 0)
  last_frame_true = s(3);
else
  last_frame_true = last_frame+1;
end

first_frame_true
last_frame_true

if(strcmp(shifts_file,'null') != 1)
  shifts = -dlmread(shifts_file);
  [X,Y]=ndgrid(-s(1)/2:s(1)/2-1,-s(2)/2:s(2)/2-1);
else
  shifts = zeros([last_frame_true-first_frame_true+1 2]);
end

n_shifts = size(shifts);
if(n_shifts(1) != last_frame_true-first_frame_true+1)
  printf('Error: Number of available shifts, %d, is not the same as the number requested, %d\n', n_shifts(1), last_frame_true-first_frame_true+1);
end

if(strcmp(gainref,'null') != 1)
  gainref = double(ReadMRC(gainref)');
  gainref = gainref(1:s(1),1:s(2));

  for i=1:s(3)
    if(isoctave)
      i
      fflush(stdout);
    end

    m(:,:,i) = gainref.*m(:,:,i);
  end
end

if(strcmp(shifts_file,'null') != 1)
  j=1;
  for i=first_frame_true:last_frame_true
    if(isoctave)
      j
      fflush(stdout);
    end

    FSH=exp((-1j*2*pi)*ifftshift((shifts(j,1)*X/s(1)+shifts(j,2)*Y/s(2))));

    m(:,:,i) = real(ifftn(fftn(m(:,:,i)).*FSH));
    j=j+1;
  end
end

WriteMRC(sum(m(:,:,first_frame_true:last_frame_true),3), pix_size, output_mic);
