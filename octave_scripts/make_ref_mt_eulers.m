function make_ref_mt_eulers(num_pfs,num_tub_starts,repeat_dist13pf,ref_com_file,dir, num_repeats)

num_starts = num_tub_starts/2;

if(nargin < 6)
  num_repeats = 10 + 4*num_starts;
end

ref_com = dlmread(ref_com_file);
index = find(ref_com(:,1) ==  1);
index = index(prod(size(index)));
ref_com = ref_com(index, 3:5);

% ref_com = ref_com_file;

radius_scale_factor = 1;
tub_monomer_offset = 0;
phi_params = 0;
theta_params = 0;
psi_params = 0;
elastic_params = [0 0 0 0];
%
%
[x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub] = ...
    microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                           repeat_dist13pf,radius_scale_factor,tub_monomer_offset, ...
                           phi_params,theta_params,psi_params,elastic_params);
clear doc;

doc(:,1) = phi_tub*180/pi;;
doc(:,2) = theta_tub*180/pi;
doc(:,3) = psi_tub*180/pi;
doc(:,4) = x_tub;
doc(:,5) = y_tub;
doc(:,6) = z_tub;
doc(find(abs(doc(:)) < 1e-2)) = 0;

doc_filename = sprintf("%s/mt_eulers_pf%d_start%d_doc.spi",dir,num_pfs,num_tub_starts);
writeSPIDERdoc(doc_filename,doc,{ 'psi'; 'theta'; 'phi'; 'x'; 'y'; 'z'});
