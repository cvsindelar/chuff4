function helimap_initialize(run_dir, job_dir, nuke, debug, chumps_round, ...
                    sparx_graph_file, frealign_graph_file, sparx_bin_factor, ...
                    box_doc_file, do_repair_psi, output_bin_factor, ...
                    n_classes, bin_factor, ...
                    fit_order, data_redundancy, max_n_outliers, ...
                    twist_tolerance, coord_error_tolerance,...
                    phi_error_tolerance, theta_error_tolerance, psi_error_tolerance,...
                    min_resolution, recon_pad_factor, ref_pad_factor,...
                    focal_mate,...
                    notebook_dir, use_ctf, gold, test_mode)

small_subunit_dim = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following variables are global for the listed reasons:
%
%  'tm_raw' can be very large, so we want to avoid passing copies of it to functions via
%  matlab's data-passing convention (can't use C-type aliases)
% 
% 'debug' is just a flag we want any function to know about, without having to pass it all the
%  time.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

stdout = 1;

%%%%%%%%%%%%%%%%%%%%%
% Test_mode: 'perfect','perfect_nosigma2', 'wedgetest_sinc','wedgetest_interp', 'perfect_sinc', 'perfect_interp', 'sinc','interp','sinc_sigma2','interp_sigma2'
% In test runs, 'nosigma2' has been used for all except 'perfect' and 'interp_sigma2' and 'sinc_sigma2'

if(nargin < 19)
  notebook_dir = 'notebook'
end

if(nargin < 20)
  test_mode = 'sinc'
end

interpmode = 'sinc'
sigma2mode = 'sigma2'

job_dir = trim_dir(job_dir);

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%%%
% The following lines create results directories, and check if any data 
%  was saved previously. Nuke=1 will override previous results, else error exit.
%%%%%%%%%%%%%%%%%%%%
[helimap_dir, reference_dir, filament_dir] = ...
  save_helimap_alignment(run_dir, 0, job_dir);

fil_info_file = sprintf('%s/fil_info.mat',filament_dir);
if(exist(fil_info_file) && !nuke)
  printf('ERROR: output "%s" exists already...', fil_info_file);
  exit(2)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize coordinates and classes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < 5)
  nuke = 0;
end

if(nargin < 12)
  n_classes = 2;
end

if(nargin < 13)
  bin_factor = 1;
end

if(nargin < 14)
  use_ctf = 1;
end

if(nargin < 15)
  focal_mate = 0;
end

if(nargin < 16)
  min_resolution = 16;
end

if(nargin < 17)
  recon_pad_factor = 1;
end

if(nargin < 18)
  ref_pad_factor = 4;
end

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

elastic_params = [0 0 0 0];
radius_scale_factor = 1;

%%%%%%%%%%%%%%%%%%%%
% Read in and fix up the conventional coordinate refinement
%%%%%%%%%%%%%%%%%%%%

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
ref_pixel = dlmread(sprintf('%s/info.txt',ref_dir));
ref_pixel = ref_pixel(2);

com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
com_info = readSPIDERdoc_dlmlike(com_doc_name);
index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
is_mt = 0;
if is_mt > 0
  [num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
    est_repeat_distance] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);
else
  [coords, phi, theta, psi, directional_psi] = ...
    read_chuff_alignment(sparx_graph_file, frealign_graph_file, box_doc_file, sparx_bin_factor, do_repair_psi, 1);    
  est_repeat_distance = 26.7;
  coords = coords / output_bin_factor; % TODO: This should be before smooth or after smooth?
  num_pfs = 1;
  num_starts = 1;
end
est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;


%%%%%%%%%%%%%%%%%%%%
% The following line is to compute the expected twist and repeat distance for the given
%  microtubule symmetry type
%%%%%%%%%%%%%%%%%%%%
if is_mt > 0
    [expected_twist_per_subunit, expected_rise_per_subunit, ...
      expected_mt_radius,expected_axial_repeat_dist,expected_twist_per_repeat] = ...
      mt_lattice_params(num_pfs,  num_starts, helical_repeat_distance, ...
      sqrt(ref_com(1)**2+ref_com(2)**2));
    best_subunits_per_repeat = 1;
else
    % for actin, should get expected_twist_per_repeat, expected_twist_per_subunit, expected_rise_per_subunit, expected_axial_repeat_dist
    
    expected_twist_per_subunit = -helical_twist / 180 * pi;
    expected_rise_per_subunit = helical_repeat_distance;
    best_subunits_per_repeat = 1; %TODO: should be calculate or provided.
    expected_twist_per_repeat = expected_twist_per_subunit * best_subunits_per_repeat;
    expected_axial_repeat_dist = expected_rise_per_subunit * best_subunits_per_repeat;
    expected_mt_radius = 260;
end
%%%%%%%%%%%%%%%%%%%%
% Note that mt_lattice_params gives the helical twist in a right-handed helical
%  convention (right-handed coordinate system) while the SPIDER Euler convention required
%  to be input into chuff is left-handed; it is also returned in radians.
% Therefore, we reverse the sign of expected_twist_per_repeat in the below expression,
%  and convert to degrees.
%%%%%%%%%%%%%%%%%%%%

[coords,phi,theta,psi,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance,...
 psi_ref, psi_ref_smooth, discontinuities] = ...
   fixup_chuff_alignment(coords,phi,theta,psi,...
                         directional_psi,micrograph_pixel_size, ...
                         expected_axial_repeat_dist,...
                         -expected_twist_per_repeat*180/pi, best_subunits_per_repeat, ...
                         'smooth_all', fit_order, data_redundancy, max_n_outliers, ...
                         twist_tolerance, coord_error_tolerance,...
                         phi_error_tolerance, theta_error_tolerance, psi_error_tolerance);
n_good_boxes = prod(size(phi));

%%%%%%%%%%%%%%%%%%%%
% Now estimate the *canonical* repeat distance (for a 13 pf microtubule), which is 
%  taken as a the ground truth for all MT symmetry types...
%%%%%%%%%%%%%%%%%%%%

est_repeat_distance_13pf = est_repeat_distance * helical_repeat_distance / expected_axial_repeat_dist;

%%%%%%%%%%%%%%%%%%%%
% Decide on the box size
%%%%%%%%%%%%%%%%%%%%

box_dim = ceil(1.5*2*filament_outer_radius/ref_pixel);
box_dim = 16*ceil(box_dim/16);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);
if (small_subunit_dim)
    subunit_dim = 2.3 * (filament_outer_radius - abs(sqrt(sum(ref_com.^2,2)))) / ref_pixel;
    subunit_dim = 16*ceil(subunit_dim/16)
else
    subunit_dim = ref_dim(1);
end

if(debug)
  subunit_dim = box_dim(1);
end

subunit_dim = [subunit_dim(1) subunit_dim(1) subunit_dim(1)];

if(isempty(whos('filament_inner_radius')))
  filament_inner_radius = filament_outer_radius - ...
    2*(filament_outer_radius - abs(sqrt(sum(ref_com.^2,1))));
  if(filament_inner_radius < 0)
    filament_inner_radius = 0;
  end
end

%%%%%%%%%%%%%%%%%%%%
% Assign initial structural classes
%%%%%%%%%%%%%%%%%%%%
n_classes
if(n_classes == 1)
  tot_helimap = ones(num_pfs, n_good_boxes, 1, 2);

elseif(isequal(test_mode,'perfect') || ...
       isequal(test_mode,'perfect_nosigma2'))
  tot_helimap = zeros(num_pfs, n_good_boxes, n_classes, 2);

  [micrograph_name,defocus,astig_mag,astig_angle,b_factor, find_dir] = ...
    chumps_micrograph_info(job_dir,focal_mate,chumps_round);
  clear micrograph_name defocus astig_mag astig_angle b_factor;

  findmap_file = sprintf('%s/tot_decorate_map.spi',find_dir);
  tot_decorate_map = readSPIDERfile(findmap_file);
  s = size(tot_decorate_map);
  tot_helimap(:,:,1,1) = 1-tot_decorate_map(1:2:s(1),1:s(2));
  tot_helimap(:,:,1,2) = 1-tot_decorate_map(1:2:s(1),1:s(2));
  tot_helimap(:,:,2,1) = tot_decorate_map(1:2:s(1),1:s(2));
  tot_helimap(:,:,2,2) = tot_decorate_map(1:2:s(1),1:s(2));

else
  tot_helimap = zeros(num_pfs, n_good_boxes, n_classes, 2);
  tot_helimap(:,:,1,1) = round(rand(size(tot_helimap(:,:,1,1))));
  tot_helimap(:,:,2,1) = 1-tot_helimap(:,:,1,1);
  tot_helimap(:,:,1,2) = round(rand(size(tot_helimap(:,:,1,2))));
  tot_helimap(:,:,2,2) = 1-tot_helimap(:,:,1,2);
end
%%%%%%%%%%%
% Randomly divide the subunits into two halves for use with the 'gold standard'
%%%%%%%%%%%
size(tot_helimap)
if(gold <= 0)
  goldmap = 1 + round(rand(size(tot_helimap(:,:,1,1))));
else
  goldmap = gold * ones(size(tot_helimap(:,:,1,1)));
end
save_helimap_alignment(run_dir, 0, job_dir, ...
                       nuke, notebook_dir, test_mode, ...
                       n_classes, bin_factor, ...
                       focal_mate, use_ctf, chumps_round,...
                       min_resolution, recon_pad_factor, ref_pad_factor,...
                       box_dim, ref_dim, small_subunit_dim, subunit_dim,...
                       ref_pixel, ref_com, filament_inner_radius,...
                       est_pix_per_repeat,num_pfs,num_starts,...
                       coords,phi,theta,psi,...
                       d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat, ...
                       est_repeat_distance_13pf, est_repeat_distance, ...
                       elastic_params, radius_scale_factor, ...
                       tot_helimap, goldmap);
