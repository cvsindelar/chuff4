function final_vol = ...
  recon_fi_finalize(numerator, denominator, pad_factor, fsc)

% output_vol = ...
%  recon_fi_finalize(numerator, denominator, pad_factor)
%
% - pad_factor defaults to 2 if not specified, should be an integer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parse input and initialize parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < 3)
  pad_factor = 2;
end

if(nargin < 4)
  frealign_fudge = 0.1;
  noise_est = [];
end

% NOTE: May want to always set fsc(1) = 1 here...

dim = size(numerator);
dim = dim(1);
r = [dim dim dim];
s = r * pad_factor;

final_vol = fftshift(real(ifftn(ifftshift(numerator./(denominator+frealign_fudge)))));
final_vol = final_vol ./ get_correct3d(r, s);
