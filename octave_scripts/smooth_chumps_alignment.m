function [coords_est,phi_est,theta_est,psi_est,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
  smooth_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_pix_per_repeat)

smoothing_hwidth = 5; % Halfwidth for smoothing euler angles
deriv_smoothing_hwidth = 5; % Halfwidth for smoothing euler angles

%%%%%%%%%%%%%%%%%%%%%
% Regularize the spacing between repeats, shifting each one to
%  make the consecutive distances ~80A; 
%  also, estimate psi from the particle coordinates
%%%%%%%%%%%%%%%%%%%%%

n_good_boxes = prod(size(phi));

mt_vec = coords(n_good_boxes,:) - coords(1,:);
mt_vec = mt_vec/sqrt(mt_vec(1)**2+mt_vec(2)**2);

psi_est = zeros([1 n_good_boxes]);

dist_to_last = zeros([1 n_good_boxes]);

dist_to_last = zeros([1 n_good_boxes]);
for j=2: n_good_boxes
  vec_to_last = coords(j,:) - coords(j-1,:);
  dist_to_last(j) = norm(vec_to_last);
end

%%%%%%%%%%%%%%%%%
% Adjust coordinates so that they are uniformly spaced (eliminate gaps and 
%  quasi-identical subunit alignments)
%%%%%%%%%%%%%%%%%

j=2;
while(j <= n_good_boxes)
  vec_to_last = coords(j,:) - coords(j-1,:);
  psi_est(j) = -180/pi*atan2(vec_to_last(2),vec_to_last(1));

  dist_to_last(j) = norm(vec_to_last);
  signed_dist_to_last = sum(vec_to_last.*mt_vec);

  tub_monomer_spacing = round(2*dist_to_last/est_pix_per_repeat);

  if(tub_monomer_spacing(j) == 2 && signed_dist_to_last > 0)
    ;
  else
    if(tub_monomer_spacing(j) != 0 && signed_dist_to_last > 0)
      coords(j,:) = coords(j-1,:) + (coords(j,:)-coords(j-1,:))*2/tub_monomer_spacing(j);
%%%%%%%%%%
% Add an extra box to take up the slack...
%%%%%%%%%
      n_good_boxes = n_good_boxes + 1;

      coords(j+1:n_good_boxes,:) = coords(j:n_good_boxes-1,:);
      coords(j+1,:) = coords(j,:) + (coords(j,:)-coords(j-1,:));
      phi(j+1:n_good_boxes) = phi(j:n_good_boxes-1);
      theta(j+1:n_good_boxes) = theta(j:n_good_boxes-1);;
      psi(j+1:n_good_boxes) = psi(j:n_good_boxes-1);
      psi_est(j+1:n_good_boxes) = psi_est(j:n_good_boxes-1);
    else
%%%%%%%%%%%%%%%%%%%%
% If there is no distance change, get rid of the current box and shift all information
%  to the left.
%%%%%%%%%%%%%%%%%%%%

      n_good_boxes = n_good_boxes - 1;
      if(j <= n_good_boxes)
        coords(j:n_good_boxes-1,:) = coords(j+1:n_good_boxes,:);
        phi(j:n_good_boxes-1) = phi(j+1:n_good_boxes);
        theta(j:n_good_boxes-1) = theta(j+1:n_good_boxes);;
        psi(j:n_good_boxes-1) = psi(j+1:n_good_boxes);
        psi_est(j:n_good_boxes-1) = psi_est(j+1:n_good_boxes);
      end
      coords = coords(1:n_good_boxes,:);
      phi = phi(1:n_good_boxes);
      theta = theta(1:n_good_boxes);
      psi = psi(1:n_good_boxes);
      psi_est = psi_est(1:n_good_boxes);
      j=j-1;
    end
  end
  vec_to_last = coords(j,:) - coords(j-1,:);
  psi_est(j) = -180/pi*atan2(vec_to_last(2),vec_to_last(1));

  dist_to_last(j) = norm(coords(j,:) - coords(j-1,:));
  j = j + 1;
end

dist_to_last = zeros([1 n_good_boxes]);
for j=2:n_good_boxes
  vec_to_last = coords(j,:) - coords(j-1,:);
  dist_to_last(j) = norm(vec_to_last);
  tub_monomer_spacing = round(2*dist_to_last/est_pix_per_repeat);
end

pix_per_repeat_from_params = helical_repeat_distance/micrograph_pixel_size
pix_per_repeat_from_coords = sum(dist_to_last(2:prod(size(dist_to_last))))/(prod(size(dist_to_last))-1)
est_pix_per_repeat

%%%%%%%%%%%%%%%%%%%%%
% Smooth the Euler angles
%%%%%%%%%%%%%%%%%%%%%

psi_est(1) = psi_est(2);
psi_est_temp = psi_est;

for j=1:n_good_boxes
  cur_phi_est = 0;
  cur_theta_est = 0;
  cur_psi_est = 0;
  count = 0;

  if(phi(j) < 90 || phi(j) > 270)
    phi_temp_offset = 180;          % to avoid wraparound issues near 0 and 360
  else
    phi_temp_offset = 0;
  end
  if(psi_est_temp(j) < 90 || psi_est_temp(j) > 270)
    psi_temp_offset = 180;          % to avoid wraparound issues near 0 and 360
  else
    psi_temp_offset = 0;
  end
  for k=-smoothing_hwidth:smoothing_hwidth
    if(j+k >= 1 && j+k <= n_good_boxes)
      cur_phi_est = cur_phi_est + mod(phi(j+k) + phi_temp_offset,360);
      cur_theta_est = cur_theta_est + theta(j+k);
      cur_psi_est = cur_psi_est + mod(psi_est_temp(j+k) + psi_temp_offset,360);
      count = count + 1;
    end
  end

  phi_est(j) = mod(cur_phi_est/count - phi_temp_offset,360);
  theta_est(j) = cur_theta_est/count;
  psi_est(j) = mod(cur_psi_est/count - psi_temp_offset,360);
end

%%%%%%%%%%%%%%%%%%%%%
% Estimate the Euler angle derivatives
%%%%%%%%%%%%%%%%%%%%%

d_phi_d_repeat = zeros([1 n_good_boxes]);
d_theta_d_repeat = zeros([1 n_good_boxes]);
d_psi_d_repeat = zeros([1 n_good_boxes]);

for j=1:n_good_boxes
  d_phi_d_repeat(j) = 0;
  d_theta_d_repeat(j) = 0;
  d_psi_d_repeat(j) = 0;
  count = 0;

  if(phi(j) < 90 || phi(j) > 270)
    phi_temp_offset = 180;          % to avoid wraparound issues near 0 and 360
  else
    phi_temp_offset = 0;
  end
  if(psi_est_temp(j) < 90 || psi_est_temp(j) > 270)
    psi_temp_offset = 180;          % to avoid wraparound issues near 0 and 360
  else
    psi_temp_offset = 0;
  end
  for k=-deriv_smoothing_hwidth:deriv_smoothing_hwidth
    if(j+k-1 >= 1 && j+k <= n_good_boxes)
      d_phi_d_repeat(j) = d_phi_d_repeat(j) + ...
         mod(phi_est(j+k)+phi_temp_offset,360) - mod(phi_est(j+k-1)+phi_temp_offset,360);
      d_theta_d_repeat(j) = d_theta_d_repeat(j)+ theta_est(j+k)-theta_est(j+k-1);
      d_psi_d_repeat(j) = d_psi_d_repeat(j) + ...
         mod(psi_est(j+k)+psi_temp_offset,360) - mod(psi_est(j+k-1)+psi_temp_offset,360);
      count = count + 1;
    end
  end
  d_phi_d_repeat(j) = d_phi_d_repeat(j)/count;
  d_theta_d_repeat(j) = d_theta_d_repeat(j)/count;
  d_psi_d_repeat(j) = d_psi_d_repeat(j)/count;
end

%%%%%%%%%%%%%%%
% Convert derivatives to units of distance
%%%%%%%%%%%%%%%

d_phi_d_repeat = d_phi_d_repeat/helical_repeat_distance;
d_theta_d_repeat = d_theta_d_repeat/helical_repeat_distance;
d_psi_d_repeat = d_psi_d_repeat/helical_repeat_distance;

%%%%%%%%%%%%%%%
% If directional_psi is 90 (rather than 270), 
%  the MT segment points backwards relative to the 
%  path of the boxes...
%%%%%%%%%%%%%%%

if(directional_psi == 90)
  d_phi_d_repeat = -d_phi_d_repeat;
  d_theta_d_repeat = -d_theta_d_repeat;
  d_psi_d_repeat = -d_psi_d_repeat;
else
  psi_est = psi_est + 180;
end

coords_est = coords;
