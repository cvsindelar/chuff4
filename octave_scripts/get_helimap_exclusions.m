function helimap_exclusions = ...
  get_helimap_exclusions(num_pfs, num_starts, est_repeat_distance,...
                         coords, ref_com, ...
                         phi, theta, psi, ...
                         d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat)

'debug'
  local_repeat_distance = est_repeat_distance
'debug'

n_particles = prod(size(phi));

helimap_exclusions = -ones(num_pfs, n_particles);

elastic_params = [0 0 0 0];
radius_scale_factor = 1;
monomer_offset = 0;

if(isoctave)
  run 'chuff_parameters.m';
else
  run 'chuff_parameters';
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

total_coords = zeros([num_pfs*n_particles 2]);
total_phi = zeros([1 num_pfs*n_particles]);
total_theta = zeros([1 num_pfs*n_particles]);
total_psi = zeros([1 num_pfs*n_particles]);

for box_num = 1:n_particles
  [x_final y_final z_final phi_final theta_final psi_final r_final ...
   x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist repeat_index2 pf_index2 ...
   box_repeat_origin] = ...
     microtubule_parametric(1,num_pfs,num_starts,ref_com,...
                          local_repeat_distance,radius_scale_factor,monomer_offset,...
                          [phi(box_num) d_phi_d_repeat(box_num)],...
                          [theta(box_num) d_theta_d_repeat(box_num)],...
                          [psi(box_num) d_psi_d_repeat(box_num)],elastic_params);

  for ind = 1:num_pfs
    total_coords(ind + (box_num-1)*num_pfs,:) = ...
                            [coords(box_num,1) + x_final(ind)/micrograph_pixel_size ...
                             coords(box_num,2) + y_final(ind)/micrograph_pixel_size];
  end

  total_phi(1 + (box_num-1)*num_pfs:box_num*num_pfs) = phi_final*180/pi;
  total_theta(1 + (box_num-1)*num_pfs:box_num*num_pfs) = theta_final*180/pi;
  total_psi(1 + (box_num-1)*num_pfs:box_num*num_pfs) = psi_final*180/pi;
end

n_total = prod(size(total_phi))
for ind = 1:n_total
  lbound = ind - 2*num_pfs;
  if(lbound < 1)
    lbound = 1;
  end
  ubound = ind + 2*num_pfs;
  if(ubound > n_total)
    ubound = n_total;
  end
  for ind2 = [lbound:ind-1 ind+1:ubound]
    if(norm(total_coords(ind,:) - total_coords(ind2,:), 2) < helimap_exclusions(ind) || ...
       helimap_exclusions(ind) == -1)
      helimap_exclusions(ind) = norm(total_coords(ind,:) - total_coords(ind2,:), 2);
    end
  end
end

% dlmwrite('total_coords.txt', total_coords * micrograph_pixel_size, ' ');

helimap_exclusions = helimap_exclusions * micrograph_pixel_size;
