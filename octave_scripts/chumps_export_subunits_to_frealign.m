num_args = 6;
if(nargin < num_args)
  fprintf('PROGRAMMING ERROR: wrong number of arguments to octave script! (%d)\n', nargin);
%  exit(2)
end

output_directory = argv(){1}
output_header = argv(){2};
bin_factor = str2num(argv(){3});
map_option = argv(){4};
focal_mate = str2num(argv(){5});
num_subvols = str2num(argv(){6})
num_mts = nargin - num_args;

target_box_num = 10;

use_ctf = 0;

tubrefs = [];
kinrefs = [];
tub_ref_file = 'chumps_round1/ref_tub_subunit/ref_tot.spi';
kin_ref_file = 'chumps_round1/ref_kin_subunit/ref_tot.spi';
% tubrefs = readSPIDERfile(tub_ref_file);
% kinrefs = readSPIDERfile(kin_ref_file);

for subvol_index=1:num_subvols
  frealign_parameter_file = sprintf('%s/%s_v%d_0.par',output_directory,output_header,subvol_index);
  f = fopen(frealign_parameter_file, 'w');
  fclose(f);

  output_stack = sprintf('%s/frealign_image_stack_v%d.spi',output_directory,subvol_index);
  if(prod(size(stat(output_stack))) != 0)
    fprintf(stdout, 'ERROR: stack file exists already: "%s"\n', output_stack);
    return
  end
end

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m';

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification

%%%%%%%%%%%%%%%%%%%%%%
% Get reference info, set pixel sizes
%%%%%%%%%%%%%%%%%%%%%%
com_doc_name = 'chumps_round1/ref_tub_subunit/tub_cgr_doc.spi';
com_info = dlmread(com_doc_name);
index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

ref_params_name = 'chumps_round1/ref_tub_subunit/ref_params.spi';
ref_pixel_info = dlmread(ref_params_name);
index = find(ref_pixel_info(:,1) == 2);
ref_pixel = ref_pixel_info(index(1),5);
index = find(ref_pixel_info(:,1) == 1);
min_theta = ref_pixel_info(index(1),5);
index = find(ref_pixel_info(:,1) == 3);
d_angle = ref_pixel_info(index(1),3);

final_pixel_size = micrograph_pixel_size*bin_factor;

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
box_dim = 64*ceil(box_dim/64);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);
pad_ref_dim = 2*ref_dim;

% TEMPORARY!!
%  pad_ref_dim = 4*ref_dim;

%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize variables for loop
%%%%%%%%%%%%%%%%%%%%%%%%%

scale_avg = 0;
n_scale_avg = 0;
tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

% ctf_doc = dlmread('ctf_files/ctf_docfile_for_frealign.spi');

tot_n_particles = ones([1 num_subvols]);
subvol_index = 1;

for mt_num = 1:num_mts
%%%%%%%%%%%%%%%
% Read in filenames, etc.
%%%%%%%%%%%%%%%

  job_dir = argv(){mt_num+num_args};

  if(job_dir(prod(size(job_dir))) == '/')
    job_dir=job_dir(1:prod(size(job_dir))-1);
  end
  [start] = regexp(job_dir,'[^/]*$','start');
  job_dir = job_dir(start:prod(size(job_dir)));

  [mic_name,defocus,astig_mag,astig_angle,b_factor] = ...
    chumps_micrograph_info(job_dir,focal_mate);

  if(focal_mate == 1)
    [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
    mic_name = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
  end

  box_file_name = sprintf('scans/%s.txt',job_dir);

  mt_type_info = dlmread(sprintf('chumps_round1/%s/selected_mt_type.txt',job_dir));
  num_pfs = mt_type_info(1);
  num_starts = mt_type_info(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read in box info and extract particles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

  if(focal_mate == 0)
    find_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
  else
    find_dir = sprintf('chumps_round1/%s/fmate_find',job_dir);
  end
  ctf_doc = dlmread(sprintf('scans/%s_ctf_doc.spi',job_dir(1:start-1)));

  mic_number = job_dir(1:start-1);
  [start] = regexp(mic_number,'[1-9][0-9]*$','start');
  mic_number = str2num(mic_number(start:prod(size(mic_number))));

%  mic_dim = ReadMRCheader(mic_name);
  mic = ReadMRC(mic_name);
  mic_pad = 2*box_dim;
  mic = dct_pad(mic, mic_pad);
  mic_dim = size(mic);

  index = find(ctf_doc(:,1) == 1);
  if(prod(size(index)) == 0)
    fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
    return
  end
  index = index(prod(size(index)));
  defocus1 = ctf_doc(index,3);
  defocus2 = ctf_doc(index,4);
  astig_angle = ctf_doc(index,5);

  presa = 50.0;
  dpres = 50.0;

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

  [num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
    est_repeat_distance] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,1,micrograph_pixel_size);

  if(num_pfs == 0)
    continue
  end

  est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

  n_good_boxes = prod(size(phi))


  [coords,phi,theta,psi] = ...
   pare_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,
                         est_pix_per_repeat);
  [coords,phi,theta,psi,...
   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
    smooth_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,
                         est_pix_per_repeat);

  n_good_boxes = prod(size(phi))

  radon_scale = dlmread(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));
  index = find(radon_scale(:,1) == 1);
  radon_scale = radon_scale(index(prod(size(index))),3);
  scale_avg = scale_avg + radon_scale;
  n_scale_avg = n_scale_avg + 1;

%%%%%%%%%%%%%%%%%%
% Read in decoration map
%%%%%%%%%%%%%%%%%%
  max_overlap = 2*num_pfs;
  n_kins_to_find = 2*num_pfs; % 6*num_pfs;
  num_repeats = ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
% TEMPORARY!!
%  num_repeats = 2*ceil( (n_kins_to_find+2*max_overlap)/num_pfs );

  kin_lib_size = 2*num_repeats*num_pfs;

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;

  map_file = sprintf('%s/tot_decorate_map.spi',find_dir);
%  map_file = 'tot_decorate_map.spi';
  tot_decorate_map = readSPIDERfile(map_file);
%  sorted_map_file = sprintf('%s/sorted_map_composite.spi',find_dir);
  sorted_map_file = 'sorted_map.spi';
  sorted_map = readSPIDERfile(map_file);

  fprintf(stdout,'\n%s\n', job_dir);

  real_box_num = 0;
  for box_num=1:n_good_boxes


    fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
    fflush(stdout);

%if(box_num == target_box_num)

    ctf = -ones(pad_ref_dim);
    ctf_flip = sign(ctf);

    filter = ones(pad_ref_dim);
    ref_pixel = micrograph_pixel_size * bin_factor;  % not used

    [kin_repeat_index, kin_pf_index,...
     x_tub, y_tub, phi_tub, theta_tub, psi_tub] = ...
      get_findkin_refs(box_num, ref_dim,num_repeats,num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com);

    mini_sorted_map = zeros([num_pfs 20]);
    for k=1:10
      mini_sorted_map((2*k-1)*num_pfs+1:2*k*num_pfs) = 1;
    end

    dec_origin = floor(kin_lib_size/2) - floor(n_kins_to_find/2);
    dec_repeat_origin = kin_repeat_index(dec_origin + 1);

    mini_repeat_index = kin_repeat_index(dec_origin+1 : dec_origin+n_kins_to_find);
    mini_pf_index = kin_pf_index(dec_origin+1 : dec_origin+n_kins_to_find);

    sorted_index = num_pfs*(mini_repeat_index-1)+mini_pf_index;

    repeat_map = mini_sorted_map(sorted_index);

debug = 0;
if(debug == 0)

    for i=1:n_kins_to_find
      if(repeat_map(i))
        phi_particle = phi_tub(dec_origin+i)*180/pi;
        theta_particle = theta_tub(dec_origin+i)*180/pi;
        psi_particle = psi_tub(dec_origin+i)*180/pi;

%%%%%%%%%%%%%%%%%%
% Read in boxed image, after re-centering the coordinates using the found
%  coords
%%%%%%%%%%%%%%%%%%

        particle_coords = coords(box_num,:) + ...
          [x_tub(dec_origin+i) y_tub(dec_origin+i)]/micrograph_pixel_size;
        int_coords = round(particle_coords);
        frac_coords = particle_coords - int_coords;

        box_origin = int_coords-floor(box_dim/2);

        orig_box = mic(box_origin(1)+mic_pad:box_origin(1)+mic_pad+box_dim-1,...
                       box_origin(2)+mic_pad:box_origin(2)+mic_pad+box_dim-1);

        box = real(ifftn(fftn(orig_box) .* FourierShift(box_dim(1), -frac_coords)));

        box = BinImageCentered(box, bin_factor);

        frealign_parameter_file = ...
          sprintf('%s/%s_v%d_0.par',output_directory,output_header,subvol_index);

        f = fopen(frealign_parameter_file, 'a');
        fprintf(f, ...
               '%7d%8.2f%8.2f%8.2f%8.2f%8.2f%8.0f%6d%9.1f%9.1f%8.2f%7.2f%6.2f\n', ...
               tot_n_particles(subvol_index), psi_particle, theta_particle, phi_particle, 0, 0, ...
               target_magnification/bin_factor, mic_number, ...
               defocus1, defocus2, astig_angle, presa, dpres);

        tilt_scale_avg = tilt_scale_avg + 1/sin(theta_particle*pi/180);
        n_tilt_scale_avg = n_tilt_scale_avg + 1;

        box = (box - mean(box(:)))/std(box(:),1);

        output_stack = sprintf('%s/frealign_image_stack_v%d.spi',output_directory,subvol_index);
        if(tot_n_particles(subvol_index) == 1)
          writeSPIDERfile(output_stack,box);
        else
          appendSPIDERvol(output_stack,box);
        end

        tot_n_particles(subvol_index) = tot_n_particles(subvol_index) + 1;
      end % if(repeat_map...
    end % for i=1:n_kins_to_find

end % if(debug == 0)
% end % if(box_num == target_box_num)

    fclose('all');

    subvol_index = subvol_index + 1;
    if(subvol_index > num_subvols)
      subvol_index = 1;
    end
  end % for box_num...

end % for mt_num ...

scale_avg = scale_avg/n_scale_avg
tilt_scale_avg = tilt_scale_avg/n_tilt_scale_avg

micrograph_pixels_per_repeat = ...
  helical_repeat_distance/(scale_avg*tilt_scale_avg)/micrograph_pixel_size

info_file = sprintf('%s/info.txt',output_directory);
f = fopen(info_file,'w');
fprintf(f,'output_header %s\n', output_header);
fprintf(f,'bin_factor %d\n',bin_factor);
fprintf(f,'micrograph_pixels_per_repeat %8.3f\n',micrograph_pixels_per_repeat);
fprintf(f,'voxel_size %8.3f\n',final_pixel_size);
fprintf(f,'estimated_repeat_distance %8.3f\n',micrograph_pixels_per_repeat*micrograph_pixel_size);
fclose(f);

return

