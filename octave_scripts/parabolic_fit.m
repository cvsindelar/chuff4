function [params] = parabolic_fit(x,y)
% params = parabolic_fit(x,y)
% params = [a b c]; fit(x) = a*x^2 + b*x + c
% Least-squares fitting to a parabola

a =  [((x(:)).^2)'; x(:)'; ones([1 prod(size(x))])]';

if(rcond(transpose(a)*a) < 0.5)
rcond(transpose(a)*a)
  params = linear_fit(x,y);
  params = [0; params(:)];
else
'yay'
  params = (transpose(a)*a) \ (transpose(a)*y(:));
% params = inv(transpose(a)*a)*(transpose(a)*y(:));
end


