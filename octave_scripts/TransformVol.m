function [new_im] = TransformVol(old_im, eulers, shifts, method, blocksize)
%function [new_im] = TransformVol(old_im, eulers, shifts, method, blocksize)
% old_im: old image
% M: 4x4 homogeneous backward affine transformation matrix
% interp_method (optional): interpolation method'nearest', 'linear', 
% 'spline', 'cubic'. Add * for more speed, see help interp3 for details

% Charles Sindelar, Adapted from code by Martijn Steenwijk 
% Date: May 2015

if nargin < 2 || nargin > 4
   error('Wrong number of input arguments');
end

if nargin < 3
    shifts = [0 0 0];
end

if nargin < 4
    method = 'linear';
end

if nargin < 5
    blocksize = 48;
end

[n ny nz] = size(old_im);

M1 = [1 0 0 1+floor(n/2); ...
      0 1 0 1+floor(n/2); ...
      0 0 1 1+floor(n/2); ...
      0 0 0 1];
M1(1:3,1:3) = EulerMatrix(-[eulers(3) eulers(2) eulers(1)]);
M2 = [1 0 0 -1-floor(n/2)-shifts(1);...
      0 1 0 -1-floor(n/2)-shifts(2);...
      0 0 1 -1-floor(n/2)-shifts(3);...
      0 0 0 1];

%lx = 1; ly = 1; lz = 1;
%ux = n; uy = n; uz = n;

nb=ceil(n/blocksize);

new_im = zeros([n n n]);

for bz=0:nb-1
  lz=1+bz*blocksize;
  uz=min(n,(bz+1)*blocksize);
for by=0:nb-1
  ly=1+by*blocksize;
  uy=min(n,(by+1)*blocksize);
for bx=0:nb-1
  lx=1+bx*blocksize;
  ux=min(n,(bx+1)*blocksize);
        
%Get all points in destination to sample
  [xg yg zg] = ndgrid(lx:ux,ly:uy,lz:uz);

  xyz = [reshape(xg,numel(xg),1)'; reshape(yg,numel(yg),1)'; reshape(zg,numel(zg),1)'];
  xyz = [xyz; ones(1,size(xyz,2))];
%transform into source coordinates
  uvw = M1*M2 * xyz;

%Remove homogeneous
  uvw = uvw(1:3,:)';

%Sample
  xi = reshape(uvw(:,1), ux-lx+1,uy-ly+1,uz-lz+1);
  yi = reshape(uvw(:,2), ux-lx+1,uy-ly+1,uz-lz+1);
  zi = reshape(uvw(:,3), ux-lx+1,uy-ly+1,uz-lz+1);

% interp3 treats x and y in right-handed coordinate system, not in matrix
% index order, so we need to swap them here.
  new_im(lx:ux,ly:uy,lz:uz) = interp3(old_im,yi,xi,zi,method);

%Check for NaN background pixels - replace them with a background of 0
  idx = find(isnan(new_im));
  if(~isempty(idx))
    new_im(idx) = 0;
  end
end
end
end
