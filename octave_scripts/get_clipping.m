function [wi_xyz_clip, wi_oxyz_clip, source_oxyz, background_oxyz] = ...
  get_clipping(wi_xyz, wi_oxyz, bigbox_xyz)

if(prod(size(wi_xyz)) < 2)
  wi_xyz = [wi_xyz 1];
end
if(prod(size(wi_xyz)) < 3)
  wi_xyz = [wi_xyz 1];
end

if(prod(size(wi_oxyz)) < 2)
  wi_oxyz = [wi_oxyz 1];
end
if(prod(size(wi_oxyz)) < 3)
  wi_oxyz = [wi_oxyz 1];
end

if(prod(size(bigbox_xyz)) < 2)
  bigbox_xyz = [bigbox_xyz 1];
end
if(prod(size(bigbox_xyz)) < 3)
  bigbox_xyz = [bigbox_xyz 1];
end

wi_xyz_clip = wi_xyz;
wi_oxyz_clip = wi_oxyz;
source_oxyz = [1 1 1];

get_background = 0;

for dim=1:3
  if( wi_oxyz(dim) < 1 || wi_oxyz(dim)+wi_xyz(dim)-1 > bigbox_xyz(dim))
%%%%%%%%%
% Handle clipping if image falls partially outside micrograph;
% If fully outside micrograph, will return negative numbers for the window size (wi_xyz_clip).
%%%%%%%%%
    if(wi_oxyz(dim) < 1)
      wi_xyz_clip(dim) = wi_oxyz(dim) + wi_xyz(dim) - 1;
      wi_oxyz_clip(dim) = 1;
      source_oxyz(dim) = 1 + wi_xyz(dim) - wi_xyz_clip(dim);
    end
    if(wi_oxyz(dim)+wi_xyz(dim)-1 > bigbox_xyz(dim))
      wi_xyz_clip(dim) = bigbox_xyz(dim) - wi_oxyz_clip(dim) + 1;
    end

%%%%%%%%%%%
% Define a "background" region in order to fill in any part of the box that 
%  falls so far outside the micrograph that we need to fill in.
% Currently, if the box is less than halfway outside the micrograph, we 
%  fill it in with mirrored pixels from the box itself, so no need for
%  background unless the box is *more* than halfway outside the micrograph.
%%%%%%%%%%%

%    if(2*(wi_xyz_clip(dim) - 1) < wi_xyz(dim) && wi_xyz(dim) != 1)
%      get_background = 1;
%    end
    get_background = 1;
  end
end

%%%%%%%%%%%%%%%%%%%%%%
% Detect the case where the big box may not be big enough to find a background
%  square that doesn't overlap with the target window box
%%%%%%%%%%%%%%%%%%%%%%

for dim=1:3
  if(2*wi_xyz(dim) > bigbox_xyz(dim) && wi_xyz(dim) != 1)
    get_background = 0;
  end
end

for dim=1:3
  if(wi_xyz(dim) == 1)
    background_oxyz(dim) = 1;
  else
    if(wi_oxyz(dim) + 2*wi_xyz(dim) <= bigbox_xyz(dim))
% 'ind' maps us back into the volume
      ind = floor((1-wi_oxyz(dim)) / wi_xyz(dim)) + 1;
      background_oxyz(dim) = wi_oxyz(dim) + ind*wi_xyz(dim);
    elseif(wi_oxyz(dim) > wi_xyz(dim))
% 'ind' maps us back into the volume
      ind = floor( (bigbox_xyz(dim)- wi_oxyz(dim) - wi_xyz(dim)) / wi_xyz(dim));
      background_oxyz(dim) = wi_oxyz(dim) + ind*wi_xyz(dim);
    else
      get_background = 0;
    end
  end
end

if(get_background == 0)
  background_oxyz = -1;
end
