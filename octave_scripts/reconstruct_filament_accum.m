function reconstruct_filament_accum(reference_dir, filament_index, n_filaments, micrograph_name, ...
                num_pfs, num_starts, est_repeat_distance_13pf,...
                coords, ref_com, phi, theta, psi, ...
                d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat, ...
                bin_factor, tot_helimap, goldmap, norm_params, ...
                recon_pad_factor, sigma2, use_ctf, defocus, astig_mag, astig_angle)

global vol_array numerator_array denominator_array debug test_mode;
n_good_boxes = prod(size(phi));
num_pfs = 1;
if(nargin < 17)
  norm_params = repmat([1; 0], [1 n_good_boxes]);
end

if(nargin < 20)
  use_ctf = 0;
  defocus = 0;
  astig_mag = 0;
  astig_angle = 0;
end

if(isoctave)
  run 'chuff_parameters.m';
else
  run 'chuff_parameters';
end
micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

final_pixel_size = micrograph_pixel_size*bin_factor;
box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);

box_dim = 16*ceil(box_dim/16);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];
ref_dim = floor(box_dim/bin_factor);

stack = zeros([ref_dim num_pfs*n_good_boxes]);

total_phi = zeros([1 num_pfs*n_good_boxes]);
total_theta = zeros([1 num_pfs*n_good_boxes]);
total_psi = zeros([1 num_pfs*n_good_boxes]);

%sites_per_repeat = num_pfs;
n_classes = size(tot_helimap);
n_classes = n_classes(3);
for box_num=1:n_good_boxes

  fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
  if(isoctave())
    fflush(stdout);
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get coordinates for each subunit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;
  monomer_offset = 0;

%  [x_final y_final z_final phi_final theta_final psi_final r_final ...
%   x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist repeat_index2 pf_index2 ...
%   box_repeat_origin] = ...
%     microtubule_parametric(1,num_pfs,num_starts,ref_com,...
%                          est_repeat_distance_13pf,radius_scale_factor,monomer_offset,...
%                          [phi(box_num) d_phi_d_repeat(box_num)],...
%                          [theta(box_num) d_theta_d_repeat(box_num)],...
%                          [psi(box_num) d_psi_d_repeat(box_num)],elastic_params);
    [x_sub y_sub z_sub phi_sub theta_sub psi_sub...
        x_mt y_mt z_mt phi_mt theta_mt psi_mt origin_repeat] = filament_parametric(1, ref_com, helical_twist, helical_repeat_distance, [phi(box_num) d_phi_d_repeat(box_num)],...
        [theta(box_num) d_theta_d_repeat(box_num)],...
        [psi(box_num) d_psi_d_repeat(box_num)]
    );
    phi_final = phi_sub;
    theta_final = theta_sub;
  total_phi(1 + (box_num-1)*num_pfs:box_num*num_pfs) = phi_final*180/pi;
  total_theta(1 + (box_num-1)*num_pfs:box_num*num_pfs) = theta_mt*180/pi;
%  total_theta(1 + (box_num-1)*num_pfs:box_num*num_pfs) = theta_final*180/pi;
%  total_psi(1 + (box_num-1)*num_pfs:box_num*num_pfs) = psi_final*180/pi;
    
  for ind = 1:num_pfs

ind
    box = read_chumps_box(micrograph_name,1,...
                          [coords(box_num,1) + x_mt(ind)/micrograph_pixel_size ...
                           coords(box_num,2) + y_mt(ind)/micrograph_pixel_size],...
                          box_dim, bin_factor, -psi_mt(ind)*180/pi);
    if(box == -1)
      continue;
    end
    box = invert_density * (box-norm_params(2,box_num))/norm_params(1,box_num);
    %box = box - mean(box(:));

%%%%%%%%%%%%%%%%%%%
% Note: we NEED the outside background to be zero, for these volumes.  This is 
%  what the noise will be centered around!!  Thus, the following line should never be
%  used:
%%%%%%%%%%%%%%%%%%%
%    background = get_helical_background(box, psi_mt(ind)*180/pi, ...
%                                                 filament_outer_radius/final_pixel_size);
%%%%%%%%%%%%%%%%%%%
    background = 0;

    stack(:,:,ind + (box_num-1)*num_pfs) = ...
      soften_box_boundary(box, background);
                          
%    boxes(:,:,box_num) = stack(:,:,ind + (box_num-1)*num_pfs);
%    boxes(:,:,box_num) = box;
  end
end

selrand = rand([1 num_pfs*n_good_boxes]);
selrand = selrand(:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the CTF's
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

electron_lambda = EWavelength(accelerating_voltage);
if(defocus > 0 && use_ctf ~= 0)
  b_factor = 0;
  ctf = CTF_ACR(recon_pad_factor*ref_dim(1), final_pixel_size, electron_lambda, defocus/10000, ...
                           spherical_aberration_constant, ...
                           b_factor, amplitude_contrast_ratio, 0);
else
  ctf = ones(recon_pad_factor*ref_dim);
end

if(filament_index < n_filaments)
  vol_array = [];
else
  vol_array = zeros([ref_dim(1) ref_dim(1) ref_dim(1) n_classes 2]);
end
for igold = 1:2
 
%  best_helimap = zeros(size(tot_helimap(:,:,1)));
%  for indi = 1:num_pfs
%  for indj = 1:n_good_boxes
%    tempscores = squeeze(tot_helimap(indi,indj,:));
%    best_helimap(indi,indj) = find(tempscores == max(tempscores(:)));
%  end
%  end

%%%%%%%%%%%%%%%%%%
% We skip the subsequent loop if there are no matches to igold- this just saves time
%%%%%%%%%%%%%%%%%%
  if(prod(size(find(goldmap == igold)) == 0))
    continue
  end

  for iclass = 1:n_classes
    iclass
%    ind = find(best_helimap(:) == iclass)
    class_probs = tot_helimap(:,:,iclass);
    class_probs(find(goldmap != igold)) = 0;

%%%%%%%%%%%%%%
%  test_vol = ...
%    recon_fi_spw([total_psi; total_theta; total_phi]',...
%                 stack, recon_pad_factor, use_ctf, final_pixel_size, ...
%                 accelerating_voltage, spherical_aberration_constant, amplitude_contrast_ratio);
%writeSPIDERfile('test_vol.spi',test_vol);
%%%%%%%%%%%%%%

    if(isequal(test_mode, 'perfect_nosigma2') || isequal(test_mode, 'nosigma2'))    
      [numerator, denominator] = ...
        recon_fi_accum(stack, total_psi, total_theta, total_phi,...
                     zeros([1 prod(size(total_psi))]),zeros([1 prod(size(total_psi))]),...
                     ones(size(sigma2(:,:,igold))), ctf, recon_pad_factor, class_probs);
    else
      [numerator, denominator] = ...
        recon_fi_accum(stack, total_psi, total_theta, total_phi,...
                     zeros([1 prod(size(total_psi))]),zeros([1 prod(size(total_psi))]),...
                     sigma2(:,:,igold), ctf, recon_pad_factor, class_probs);
    end

    numerator_file_real = sprintf('%s/numerator_class%d_gold%d_real.spi', ...
                                  reference_dir, iclass, igold);
    numerator_file_imag = sprintf('%s/numerator_class%d_gold%d_imag.spi', ...
                                  reference_dir, iclass, igold);
    denominator_file = sprintf('%s/denominator_class%d_gold%d.spi', ...
                               reference_dir, iclass, igold);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wait until the next higher filament is finished reconstructing, and then add
%  to that result (does not apply to the top filament number = n_filaments).  
%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filament_index, n_filaments
    if(filament_index != n_filaments)
      prev_completion_record = sprintf('%s/tallies/filament%d_class%d_gold%d_complete',
                                       reference_dir, filament_index + 1, iclass, igold);
      while(~exist(prev_completion_record))
        sleep(0.2)
      end
      numerator = numerator + readSPIDERfile(numerator_file_real) + ...
                              i * readSPIDERfile(numerator_file_imag);
      denominator = denominator + readSPIDERfile(denominator_file);
    end

    writeSPIDERfile(numerator_file_real, real(numerator));
    writeSPIDERfile(numerator_file_imag, imag(numerator));
    writeSPIDERfile(denominator_file, denominator);

    completion_record = sprintf('%s/tallies/filament%d_class%d_gold%d_complete',
                                reference_dir, filament_index, iclass, igold);
    fid = fopen(completion_record, 'w')
    fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If we made it all the way down to filament number 1, we are ready to finish the reconstruction
%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if(filament_index == 1)
      numerator_array(:,:,:,iclass,igold) = numerator;
      denominator_array(:,:,:,iclass,igold) = denominator;

      vol_array(:,:,:,iclass, igold) = recon_fi_finalize(numerator, denominator, recon_pad_factor);
    end
  end % n_classes

end % igold
