function AppendImagic(map, basename)
% function AppendImagic(map, basename);
% Append a 2D image or a stack of 2D images to an existing stack file.
% We assume that all images in a stack are identical in size.  The data are
% written as reals.
% 

[n1 n2 nimages]=size(map);

[nx slen]=size(basename);
% strip any 'img' or 'hed' extension from the base name
if (slen>4) && (basename(slen-3)=='.') % has an extension
    if (basename(slen-2:slen)=='hed') || (basename(slen-2:slen)=='img')
        basename=basename(1:slen-4);  % remove the extension.
    end;
end;
hdrname=strcat(basename,'.hed');

newfile = 0;
% Open the header
order='ieee-le';
hdr=fopen(hdrname,'r',order);  % try little-endian
if hdr <= 0
  last_nimages = 0;
  newfile = 1;

  [hdr, errmsg] =fopen(hdrname,'w','ieee-le');  % little-endian
  if(hdr == -1)
    fprintf(stdout, '%s\n', errmsg);
    return
  end
else
% Try reading it
  idat=fread(hdr,14,'int32');

  if abs(idat(4))>6e4 % idat(4) is headers/image, typically = 1.
    fclose(hdr);  % wrong byte order, start over.

    fprintf(stdout, 'ERROR: big-endian Imagic files not current supported for AppendImagic function');
    exit(2)

    order='ieee-be';
    hdr=fopen(hdrname,'r',order);  % big-endian
    idat=fread(hdr,14,'int32');
  end;

  last_nimages=idat(2)+1;
  [hdr, errmsg] =fopen(hdrname,'r+b','ieee-le');  % little-endian

  if(idat(13) != n1 || idat(14) != n2)
    fprintf(stdout, 'ERROR: image size does not match that in file (AppendImagic)\n');
    exit(2)
  end
end

% Fill in the 'minimal' header information
t=clock;
idat=zeros(256,1);
% idat(1)=1;  % Image location number
% idat(2)=max(0,nimages-1); % number of images following.
idat(4)=1; % number of headers per images
idat(5)=t(3);  % day
idat(6)=t(2);  % month
idat(7)=t(1);  % year
idat(8)=t(4);  % hour
idat(9)=t(5);  % minute
idat(10)=round(t(6)); % second
idat(12)=n1*n2;
idat(13)=n1;  % x pixels
idat(14)=n2;  % y pixels
typeString='REAL';

idat(61)=1;  % number of planes
idat(69)=33686018;  % machine type

nimages = last_nimages + nimages;
for i=1:nimages  % write multiple headers if necessary
    idat(1)=i;          % image location number
    idat(2)=nimages-i; % number of images following
    cnt=fwrite(hdr,idat(1:14),'int32');
    cnt=fwrite(hdr,typeString,'char');
    cnt=fwrite(hdr,idat(16:256),'int32');
end;

fclose(hdr);

% Write the data file
imgname=strcat(basename,'.img');

if(newfile == 1)
  img=fopen(imgname,'wb','ieee-le');  % little-endian
else
  img=fopen(imgname,'r+b','ieee-le');  % little-endian
  fseek(img, 0, 'eof');
end

cnt=fwrite(img,map,'float32');
fclose(img);
