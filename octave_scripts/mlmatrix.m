function [pos,modmap,fmap] = mlmatrix(fmap,tmap,rkin,psi,x,y)
% calculate the MLE of helix partially decorated with protein partners;
% fmap, tmap, rkin, psi, x, y are correspond to raw image, net microtube
% without decorated protein, probablity decorated protein in helix, helix
% direction, coordinates for decoration.
% the fmap is considered as a square and the 1D pixelszie is even.

%% match coordinates in map;

[s1 s2]=size(fmap);
pixsize=2.6863;
binsize=2;
x=x/pixsize/binsize+s1/2;
y=y/pixsize/binsize+s2/2; 

%% calculate helix direction

[s3 s4]=size(psi);
s4=round(s4/2);
psi1=-(psi(s4)-180)*pi/180; u=[cos(psi1) sin(psi1)];

%% generate 2D mask

pitchsize=40.5;                    % 40.5 is the ideal pitch;
pitch=pitchsize/pixsize/binsize;   
pm=ceil(pitch/2);

maskf=zeros(s1,s1);              % front mask
maskb=maskf;                     % rear mask
maskf(:,s1/2+1:s1/2+pm)=1; 
maskb(:,s1/2+1-pm:s1/2)=1;

maskf=twodrotation(maskf,psi1-pi/2); 
maskb=twodrotation(maskb,psi1-pi/2); 
% maskf=twodrotation(maskf,psi1-pi/2); 
% maskb=twodrotation(maskb,psi1-pi/2); 

%% add white noise
wn=rand([s1 s1])-0.5;
mfmap=mean(fmap(:)); 
fmapold=fmap;
fmap=fmap+wn.*(mfmap/0.1);

%% single kinesin map for modeling, total one repeat includes 'np' kinesins
% calculate the position of each piece of microtube decorated with kinesin

np=5;                     % particle number in per subunit
[p1 p2]=size(x);
ns=18;                 % number of subunits cut from raw helix. It should be an even;
unith=ns/2;
dist=zeros(p2,1);
refk=zeros(s1,s1,np,ns);

for i=1:p2
        k=[x(i)-64 y(i)-64];
        dist(i)=dot(k,u);
end

 dist1=-dist;
for i=1:unith   
     ind=(find(dist<=pitch*i & dist>pitch*i-pitch));
     ind1=(find(dist1<=pitch*i & dist1>pitch*i-pitch));
     refk(:,:,:,unith+1-i)=rkin(:,:,ind);
     refk(:,:,:,unith+i)=rkin(:,:,ind1);   
end 

i=-(unith-0.5):unith+0.5;
shiftc=round([i*pitch*u(1); i*pitch*u(2)]);   % coordinates for mask shift;

%% shift front and rear mask;

fmpad=zeros(s1*2,s1*2);              % padding mask for 2D rotation
bmpad=fmpad;
fmpad(65:192,65:192)=maskf; 
bmpad(65:192,65:192)=maskb;
fm=zeros(128,128,ns);                  % front mask 
bm=fm;                                 % rear mask 

for i=1:ns  
     center=s1+shiftc(:,i);
     fm(:,:,i)=fmpad(center(1)-s1/2+1:center(1)+s1/2,center(2)-s1/2+1:center(2)+s1/2);
     bm(:,:,i)=bmpad(center(1)-s1/2+1:center(1)+s1/2,center(2)-s1/2+1:center(2)+s1/2); 
end

%% calculate max-likehood score for each subunit;

npoint=s1*s1;
cst=zeros(2^np,2^np,ns); 
maskt=zeros(s1,s1);

for i=1:ns    
i
fflush(stdout);
      kb=refk(:,:,:,i);        % initial images of kinesin particles
      
     if i==ns;
         kf=refk(:,:,:,1); 
         fbm=bm(:,:,i)+fm(:,:,1);
     else 
         kf=refk(:,:,:,i+1); 
         fbm=bm(:,:,i)+fm(:,:,i+1);
     end

          af=fmap.*fbm;
          at=tmap.*fbm;
          maskt=maskt+fbm;
          [modb modf ind]=referencemodel(kf,kb,fbm,np);
      
          
    for j=1:2^np
        for k=1:2^np 
          am=modb(:,:,j)+modf(:,:,k); 
          b=af-at-am; 
          cst(j,k,i)=sum(sum(b.^2))/npoint;
        end
    end             
end

cstn=10*cst/max(cst(:));
st = exp(-cstn);
kin=(fmap-tmap).*maskt;       % selected particle in raw micrographs for MLE finding

%% total max-likehood score for each conformation corerespond to each subunit; 

mats=zeros(2^np,ns);
pos=zeros(np,ns);

for i=1:ns   
         
         stn=st;
         sn=1;
    
    if i>1;
    stn(:,:,1:ns+1-i)=st(:,:,i:ns); 
    stn(:,:,ns-i+2:ns)=st(:,:,1:i-1);
    end
    
        j=1;              
    while j<=ns    
    snf=stn(:,:,j); 
    sn=sn*snf; 
    j=j+1;
    end
    
    for k=1:2^np; 

            mats(k,i)=sn(k,k);

    end
    score=mats(:,i);
    [val index]=max(score(:));
    pos(:,i)=ind(:,index);
end

%% show the MLE results map

modmap=zeros(s1,s1);
for i=1:ns    
     pos1=pos(:,i);
     for j=1:np
         pos2=pos1(j);
         if pos2==1;
     modmap=modmap+refk(:,:,j,i); 
         end
     end
end

return
