function chuff_smooth_coords(sparx_graph_file, frealign_graph_file, ...
            box_doc_file, output_file, ...
            job_dir, output_stack_dim, prerotate, ...
            sparx_bin_factor, output_bin_factor, ...
            guessed_helical_twist, subunits_per_repeat, ...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length)

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(nargin < 20)
  min_seg_length = 10;
end

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

do_repair_psi = 1;

rise_per_subunit = helical_repeat_distance;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Each 'particle', or repeat, contains one or more subunits. By default, we find the
%  number of subunits per repeat that minimizes the twist from one particle to
%  the next.
%%%%%%%%%%%%%%%%%%%%%%%%%%

if(subunits_per_repeat <= 0)
  subunits_per_repeat = ceil( (turns_per_repeat*360 - 180) / guessed_helical_twist);
  best_twist = 361;
  best_subunits_per_repeat = -1;
  while(abs(subunits_per_repeat*guessed_helical_twist) < turns_per_repeat * 360 + 180)
    cur_twist = mod(subunits_per_repeat*guessed_helical_twist,360);
    if(abs(cur_twist) < abs(best_twist))
      best_twist = cur_twist
      best_subunits_per_repeat = subunits_per_repeat;
    end
    if(abs(cur_twist-360) < abs(best_twist))
      best_twist = cur_twist - 360
      best_subunits_per_repeat = subunits_per_repeat;
    end
    subunits_per_repeat = subunits_per_repeat + 1;
  end
else
  best_subunits_per_repeat = subunits_per_repeat;
end

[coords, phi, theta, psi,directional_psi] = ...
  read_chuff_alignment(sparx_graph_file, 'null', box_doc_file,sparx_bin_factor, ...
                       do_repair_psi, 1);


[coords_frealign, phi_frealign, theta_frealign, psi_frealign,directional_psi] =   ...
  read_chuff_alignment(sparx_graph_file, frealign_graph_file, box_doc_file,...
                       sparx_bin_factor, do_repair_psi, 1);

[coords_est,phi_est,theta_est,psi_est,d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
 est_repeat_distance,  psi_ref] = ...
  fixup_chuff_alignment(coords,phi_frealign,theta_frealign,psi_frealign,...
                        directional_psi,  micrograph_pixel_size,rise_per_subunit,...
                        guessed_helical_twist, best_subunits_per_repeat, ...
                        'null');

[coords_est_smooth,phi_est_smooth,theta_est_smooth,psi_est_smooth,...
 d_phi_d_repeat_smooth,d_theta_d_repeat_smooth,d_psi_d_repeat_smooth,...
 est_repeat_distance_smooth, psi_ref, psi_ref_smooth, discontinuities, outliers] = ...
  fixup_chuff_alignment(coords,phi_frealign,theta_frealign,psi_frealign,...
                        directional_psi,micrograph_pixel_size,rise_per_subunit,...
                        guessed_helical_twist, best_subunits_per_repeat, ...
                        'smooth_all', fit_order, data_redundancy, max_outliers_per_window, ...
                        twist_tolerance, coord_error_tolerance,...
                        phi_error_tolerance, theta_error_tolerance, psi_error_tolerance);

n_particles = prod(size(phi_est));

%%%%%%%%%%%%%%%%%%%%
% ID the good segments of the filament
%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%
% Break filament down into good segments
%%%%%%%%%
goodcount = 0;
segids = zeros([n_particles 1]);
segid = 0;
for ind=1:n_particles
  if(!discontinuities(ind))
    if(goodcount <= 0)
      segid = segid + 1;
    end
    goodcount = goodcount + 1;
    segids(ind) = segid;
  else
    segids(ind) = 0;
    goodcount = 0;
  end
end

n_segs = segid;

%%%%%%%%%%%%%%%%%%%%%%%
% Compute the distances between each point and the last
%%%%%%%%%%%%%%%%%%%%%%%
coords_delta = zeros([n_particles 1]);
coords_delta(1:n_particles-1) = ...
                      sqrt(sum((coords_est(1:prod(size(phi_est))-1,:) - ...
                                coords_est(2:prod(size(phi_est)),:)).^2,2));
coords_delta(n_particles) = coords_delta(n_particles-1);

coords_smooth_delta = zeros([n_particles 1]);
coords_smooth_delta(1:n_particles-1) = ...
                      sqrt(sum((coords_est_smooth(1:prod(size(phi_est))-1,:) - ...
                                coords_est_smooth(2:prod(size(phi_est)),:)).^2,2));
coords_smooth_delta(n_particles) = coords_smooth_delta(n_particles-1);

%%%%%%%%%%%%%%%%%%%%%%%
% Compute the difference between each phi and the last, which gives the measured twist;
%  Then, compare this with the expected twist.
%%%%%%%%%%%%%%%%%%%%%%%

phi_est = mod(phi_est, 360);
phi_est_smooth = mod(phi_est_smooth, 360);

phi_delta = zeros([n_particles 1]);
phi_delta(1:n_particles-1) =  ...
                     mod(phi_est(1:prod(size(phi_est))-1) - ...
                           phi_est(2:prod(size(phi_est))) + 180, ...
                         360) - 180;
phi_delta(n_particles) = phi_delta(n_particles-1);

net_phi_delta = cumsum(phi_delta) - phi_delta(1);

proto_twist = subunits_per_repeat*guessed_helical_twist;
expected_phi_delta = [0:n_particles-1]*proto_twist;

phi_delta_diff = net_phi_delta(:) - expected_phi_delta(:);
phi_delta_diff = mod(phi_delta_diff + 180, 360) - 180;

% phi_delta_diff = phi_delta_diff - median(phi_delta_diff(:));
% phi_delta_diff = mod(net_phi_delta(:) - expected_phi_delta(:),360);
% phi_delta_diff(find(phi_delta_diff < -180) ) = phi_delta_diff(find(phi_delta_diff < -180)) + 360;
% phi_delta_diff(find(phi_delta_diff > 180) ) = phi_delta_diff(find(phi_delta_diff > 180)) - 360;

%%%%%%%%%%%%%%%%%%%%%%%
% Do the same for the smoothed phi
%%%%%%%%%%%%%%%%%%%%%%%

phi_smooth_delta = zeros([n_particles 1]);
phi_smooth_delta(1:n_particles-1,:) =  ...
                     mod(phi_est_smooth(1:prod(size(phi_est))-1) - ...
                           phi_est_smooth(2:prod(size(phi_est))) + 180, ...
                         360) - 180;
phi_smooth_delta(n_particles) = phi_smooth_delta(n_particles-1);

net_phi_smooth_delta = cumsum(phi_smooth_delta) - phi_smooth_delta(1);

phi_smooth_delta_diff = net_phi_smooth_delta(:) - expected_phi_delta(:);

%%%%%%%%%%%%%%%%%%%%%%%
% Now identify outliers in phi_delta_diff, which correspond to outliers in phi
%  but shifted left by one.
%%%%%%%%%%%%%%%%%%%%%%%

phi_diff_outliers = circshift(outliers(:,1), -1);
phi_diff_outliers(prod(size(phi_diff_outliers))) = ...
  phi_diff_outliers(prod(size(phi_diff_outliers)) - 1);

%%%%%%%%%%%%%%%%%%%%%%%
% Align the phi_smooth_delta_diff and phi_delta_diff plots with each other, for each segment
%%%%%%%%%%%%%%%%%%%%%%%

for ind=1:n_segs

save 'debug.mat'

  seglist = find(segids == ind & phi_diff_outliers != 1);

  if(!isempty(seglist))
    phi_smooth_delta_diff(find(segids == ind)) = ...
      phi_smooth_delta_diff(find(segids == ind)) - ...
      phi_smooth_delta_diff(seglist(1));

    delta = phi_delta_diff(seglist) - phi_smooth_delta_diff(seglist);

    phi_delta_diff(find(segids == ind)) = ...
      phi_delta_diff(find(segids == ind)) - sum(delta(:))/prod(size(delta));
  end
end  

%%%%%%%%%
% Discard segments that are too short
%%%%%%%%%
for ind = 1:segid
  if(prod(size(find(segids == ind))) < min_seg_length)
    segids(find(segids == ind)) = 0;
  end
end

%%%%%%%%%
% Renumber the remaining segments
%%%%%%%%%
segid = 0;
for ind=1:n_particles
  if(segids(ind) && segids(ind) != segid)
    segid = segid + 1;
    segids(find(segids == segids(ind))) = segid;
  end
end

n_segs = segid;

%%%%%%%%%%%%%%%%%%%%
% Now fix the psi plots so they are continuous (avoid wraparound at 360)
%%%%%%%%%%%%%%%%%%%%

psi_mid = floor(n_particles/2)+1;
psi_low = psi_mid - 5;
psi_high = psi_mid + 5;
if(psi_low < 1)
  psi_low = 1;
end
if(psi_high > n_particles)
  psi_high = n_particles;
end
psi_mid_val = median(psi_est_smooth(psi_low:psi_high));

psi_est_smooth(find(psi_est_smooth > psi_mid_val + 180)) = ...
  psi_est_smooth(find(psi_est_smooth > psi_mid_val + 180)) - 360;
psi_est_smooth(find(psi_est_smooth < psi_mid_val - 180)) = ...
  psi_est_smooth(find(psi_est_smooth < psi_mid_val - 180)) + 360;

psi_ref(find(psi_ref > psi_mid_val + 180)) = ...
  psi_ref(find(psi_ref > psi_mid_val + 180)) - 360;
psi_ref(find(psi_ref < psi_mid_val - 180)) = ...
  psi_ref(find(psi_ref < psi_mid_val - 180)) + 360;

psi_ref_smooth(find(psi_ref_smooth > psi_mid_val + 180)) = ...
  psi_ref_smooth(find(psi_ref_smooth > psi_mid_val + 180)) - 360;
psi_ref_smooth(find(psi_ref_smooth < psi_mid_val - 180)) = ...
  psi_ref_smooth(find(psi_ref_smooth < psi_mid_val - 180)) + 360;

phi_smooth_deltadelta(2:n_particles) =  ...
                     phi_smooth_delta(1:prod(size(phi_est))-1) - ...
                           phi_smooth_delta(2:prod(size(phi_est)));

%%%%%%%%%%%%%%%%%%%%
% Print out the plot file
%%%%%%%%%%%%%%%%%%%%

fid = fopen(output_file, 'w');
len = prod(size(output_file));
fid_disc_twist = fopen(sprintf('%s_discontinuities_twist.txt', output_file(1:len-4)), 'w');
fid_disc_phi = fopen(sprintf('%s_discontinuities_phi.txt', output_file(1:len-4)), 'w');
fid_disc_psi = fopen(sprintf('%s_discontinuities_psi.txt', output_file(1:len-4)), 'w');

if(n_segs > 0)
  fid_good = fopen(sprintf('%s_good.txt', output_file(1:len-4)), 'w');
end

first_good_particle = find(discontinuities == 0);
if(!isempty(first_good_particle))
  first_good_particle = first_good_particle(1);
else
  first_good_particle = 1;
end

for ind = 1:n_particles
  fprintf(fid, '%4d %8.2f %8.2f %8.2f %8.2f %8.2f   %8.2f %8.2f   %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.5f %8.5f %8.5f %8.2f\n', ...
	  ind, phi_est_smooth(ind), theta_est_smooth(ind), psi_est_smooth(ind), ...
          coords_est_smooth(ind,1), coords_est_smooth(ind,2), ...
          micrograph_pixel_size*coords_smooth_delta(ind), phi_smooth_delta(ind), ...
          phi_est(ind), theta_est(ind), psi_ref(ind), ...
          coords_est(ind,1), coords_est(ind,2), ...
          micrograph_pixel_size*coords_delta(ind), phi_delta(ind), ...
          phi_delta_diff(ind), ...
          phi_smooth_delta_diff(ind), ...
          psi_ref_smooth(ind));

  
  if(discontinuities(ind))
    fprintf(fid_disc_twist, '%4d %8.2f\n', ind, ...
            phi_smooth_delta_diff(ind) - phi_smooth_delta_diff(first_good_particle));
    fprintf(fid_disc_phi, '%4d %8.2f\n', ind, phi_smooth_delta(ind));
    fprintf(fid_disc_psi, '%4d %8.2f\n', ind, psi_ref_smooth(ind));
  elseif(segids(ind) > 0)
    fprintf(fid_good, '%d %8.2f %8.2f %8.2f %8.2f %8.2f %8.5f\n', 
            ind, phi_est_smooth(ind), theta_est_smooth(ind), psi_ref_smooth(ind), ...
            coords_est_smooth(ind,1), coords_est_smooth(ind,2), phi_smooth_deltadelta(ind));
  end
end

if(n_segs > 0)
  fid_all_good = fopen(sprintf('%s_all_good.txt', output_file(1:len-4)), 'w');

%%%%%%%%%%%%%%%%%%%%%%%%
% Now we fill out the Eulers and coordinates so they represent ALL the subunits.
% We will interpolate between each of the previously determined, smoothed, particle coordinates,
%  each of which represents 1 or more subunits, in order to get estimates for each subunit.
%  This means there will be (best_subunits_per_repeat * n_particles_seg - 1) 
%  total subunits per segment, for a total of 
%  best_subunits_per_repeat * n_particles - n_segs;
%%%%%%%%%%%%%%%%%%%%%%%%
  n_all_subunits = ...
    best_subunits_per_repeat * prod(size(find(segids != 0))) - n_segs;

  phi_all_good = zeros([n_all_subunits 1]);
  theta_all_good = zeros([n_all_subunits 1]);
  psi_all_good = zeros([n_all_subunits 1]);
  coords_all_good = zeros([n_all_subunits 2]);

  good_ind = 1;
  for ind = 1:n_particles
    if(segids(ind))

      coords_all_good(good_ind,:) = coords_est_smooth(ind,:);
      phi_all_good(good_ind) = phi_est_smooth(ind);
      theta_all_good(good_ind) = theta_est_smooth(ind);
      psi_all_good(good_ind) = psi_ref_smooth(ind);

%%%%%%%%%%%%%%%%
% Interpolation is only done if the current particle and the one following belong to the 
%  same segment.
%%%%%%%%%%%%%%%%
      if(ind < n_particles && segids(ind) == segids(ind+1))
        begin_coords = coords_est_smooth(ind, :);
        end_coords = coords_est_smooth(ind+1, :);
        begin_phi = phi_est_smooth(ind);
        end_phi = phi_est_smooth(ind+1);
        begin_theta = theta_est_smooth(ind);
        end_theta = theta_est_smooth(ind+1);
        begin_psi = psi_ref_smooth(ind);
        end_psi = psi_ref_smooth(ind+1);

%%%%%%%%%%%%%%%%
% Due to the fact that phi and psi can roll over 360, we need to add extra logic
%%%%%%%%%%%%%%%%
        if(end_psi - begin_psi > 180)
          end_psi = end_psi - 360;
        elseif(end_psi - begin_psi < -180)
          end_psi = end_psi + 360;
        end

        diff1 = abs(floor(best_subunits_per_repeat*guessed_helical_twist / 360) * 360 + ...
                      begin_phi - end_phi - ...
                    best_subunits_per_repeat*guessed_helical_twist);
        diff2 = abs(floor(best_subunits_per_repeat*guessed_helical_twist / 360 + 1) * 360 + ...
                      begin_phi - end_phi - ...
                    best_subunits_per_repeat*guessed_helical_twist);

        if(diff1 < diff2)
          end_phi = end_phi + ...
                     floor(best_subunits_per_repeat*guessed_helical_twist / 360) * 360;
        else
          end_phi = end_phi + ...
                     floor(best_subunits_per_repeat*guessed_helical_twist / 360 + 1) * 360;
        end

        for ind2 = 2:best_subunits_per_repeat
          coords_all_good(good_ind + ind2 - 1,:) = ...
            begin_coords + (end_coords - begin_coords) * (ind2 - 1)/best_subunits_per_repeat;
          phi_all_good(good_ind + ind2 - 1) = ...
            begin_phi + (end_phi - begin_phi) * (ind2 - 1)/best_subunits_per_repeat;
          theta_all_good(good_ind + ind2 - 1) = ...
            begin_theta + (end_theta - begin_theta) * (ind2 - 1)/best_subunits_per_repeat;
          psi_all_good(good_ind + ind2 - 1) = ...
            begin_psi + (end_psi - begin_psi) * (ind2 - 1)/best_subunits_per_repeat;
        end
        good_ind = good_ind + best_subunits_per_repeat;
      else
        good_ind = good_ind  + 1
      end % if(ind < n_particles ...

    end % if(segids(ind))
  end % for ind = 1:n_particles

  for ind = 1:n_all_subunits
    fprintf(fid_all_good, '%d %8.2f %8.2f %8.2f %8.2f %8.2f\n', 
            ind, phi_all_good(ind), theta_all_good(ind), ...
            psi_all_good(ind), ...
            coords_all_good(ind,1), coords_all_good(ind,2));
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Write out the stack of images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(output_stack_dim != 0 && n_segs > 0)

  output_stack = zeros([output_stack_dim(1) output_stack_dim(1) n_all_subunits]);
  micrograph_name = chumps_micrograph_info(job_dir,0,1);

  printf('Reading micrograph...');
  fflush(stdout);
  micrograph = ReadMRC(micrograph_name);

  if(output_bin_factor != 1)
    micrograph = BinImageCentered(micrograph, output_bin_factor);
  end

  for ind = 1:n_all_subunits
    if(prerotate)
      angle = -psi_all_good(ind);
    else
      angle = 0;
    end

    output_stack(:,:,ind) = ...
      copy_chumps_box(micrograph, ind, coords_all_good, ...
                        output_stack_dim, output_bin_factor, 2, angle);
  end

  output_stack = output_stack - sum(output_stack(:)/prod(size(output_stack)));

%  writeSPIDERfile(sprintf('%s_bin%d.spi', output_file(1:len-4), output_bin_factor), ...
%                          output_stack, 'stack');
  WriteImagic(output_stack, ...
              sprintf('%s_bin%d', output_file(1:len-4), output_bin_factor));
end
