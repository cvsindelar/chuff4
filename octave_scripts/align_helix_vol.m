function align_helix_vol(fname_v1, fname_v2,fname_mask, outer_radius_A, max_dist_A, pixel_size)

v1=readSPIDERfile(fname_v1);
v2=readSPIDERfile(fname_v2);
mask=readSPIDERfile(fname_mask);

s = size(v1)

pixel_size
o = floor(1+s/2)
max_int_r = ceil(outer_radius_A/pixel_size)
max_int_dz = ceil(max_dist_A/2)

wi_xy = 2*max_int_r
wi_z = s(3)-2*max_int_dz

wi_oxy = 1 + o(1) - floor(wi_xy/2)
wi_oz = 1 + o(3) - floor(wi_z/2)

v1_w = v1(wi_oxy:wi_oxy+wi_xy-1,wi_oxy:wi_oxy+wi_xy-1,1:s(3));
v2_w = v2(wi_oxy:wi_oxy+wi_xy-1,wi_oxy:wi_oxy+wi_xy-1,1:s(3));
mask_w = mask(wi_oxy:wi_oxy+wi_xy-1,wi_oxy:wi_oxy+wi_xy-1,1:s(3));

delta_phi = 360*pixel_size/(2*pi*outer_radius_A)/4
max_phi = 360*max_dist_A/(2*pi*outer_radius_A)
n_phi = ceil(max_phi/delta_phi)

max_ccc = -1;

mask_w = ones(size(mask_w));

for phi_i=-n_phi:n_phi
  phi = phi_i*delta_phi;

  v2_rot = rotate3d(v2_w,[phi 0 0 ]);

  for dz=-max_int_dz:max_int_dz
    v2_rotshift = circshift(v2_rot,[0 0 dz]);

%mean(v1_w(:,:,wi_oz:wi_oz+wi_z-1)(:).*mask_w(:,:,wi_oz:wi_oz+wi_z-1)(:))
%mean(v2_rotshift(:,:,wi_oz:wi_oz+wi_z-1)(:).*mask_w(:,:,wi_oz:wi_oz+wi_z-1)(:))
%mean(mask_w(:))

    ccc_val = ccc(v1_w(:,:,wi_oz:wi_oz+wi_z-1).*mask_w(:,:,wi_oz:wi_oz+wi_z-1), ...
                  v2_rotshift(:,:,wi_oz:wi_oz+wi_z-1).*mask_w(:,:,wi_oz:wi_oz+wi_z-1));

    if(ccc_val > max_ccc)
      max_ccc = ccc_val
      best_phi = phi;
      best_dz = dz;
      best_v2 = v2_rotshift;
    end  
  end
end

max_ccc
best_phi
best_dz
writeSPIDERfile("best_vol.spi",circshift(rotate3d(v2,[best_phi 0 0]), [0 0 best_dz]));
