function [best_angle,best_scale,dx,dy,best_m2align,mask] = ...
      align_focal_pairs(m1, m2, pixel_size,...
                        filter_resolution,n_iterations,angle_spec,scale_spec,
                        rot_bin_factor,scale_bin_factor);

max_shift = 10;
shift_offset_x = 0;
shift_offset_y = 0;

m1 = double(RemoveOutliers(m1,2));
m2 = double(RemoveOutliers(m2,2));

whos m1 m2

supersample = 2;

if(rot_bin_factor == 0)
  rot_bin_factor = floor(6/pixel_size);
  if(rot_bin_factor < 1)
    rot_bin_factor = 1;
  end
end
if(scale_bin_factor == 0)
  scale_bin_factor = floor(6/pixel_size);
  if(scale_bin_factor < 1)
    scale_bin_factor = 1;
  end
end

rot_bin_factor
scale_bin_factor

angle_offset = angle_spec(1);
if(prod(size(angle_spec)) > 1)
  angle_halfw = angle_spec(2);
else
  angle_halfw = 1;
endif

if(prod(size(angle_spec)) > 2)
  n_angle = angle_spec(3);
else
  n_angle = 11;
endif

scale_offset = scale_spec(1);
if(prod(size(scale_spec)) > 1)
  scale_halfw = scale_spec(2);
else
  scale_halfw = 0.1;
endif

if(prod(size(scale_spec)) > 2)
  n_scale = scale_spec(3);
else
  n_scale = 11;
endif

s = size(m1);
bigdim = s;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make a soft edged border mask for the micrographs
%%%%%%%%%%%%%%%%%%%%%%%%%%%

mask = ones(s);

edge_size_A = 1000;
edge_size = 2*floor(edge_size_A/pixel_size);
cos_edge = [1:edge_size];
cos_edge = sin(cos_edge/edge_size * pi);
cos_edge_t = cos_edge';

top_edge = repmat(cos_edge(1:floor(edge_size/2)), [s(2) 1]);
bot_edge = repmat(cos_edge(floor(edge_size/2)+1:edge_size), [s(2) 1]);
left_edge = repmat(cos_edge_t(1:floor(edge_size/2),1), [1 s(1)]);
right_edge = repmat(cos_edge_t(floor(edge_size/2)+1:edge_size,1), [1 s(1)]);

mask(1:floor(edge_size/2),:) = left_edge .* mask(1:floor(edge_size/2),:);
mask(s(1)-floor(edge_size/2)+1:s(1),:) = right_edge .* mask(s(1)-floor(edge_size/2)+1:s(1),:);
mask(:,1:floor(edge_size/2)) = top_edge .* mask(:,1:floor(edge_size/2));
mask(:,s(1)-floor(edge_size/2)+1:s(1)) = bot_edge .* mask(:,s(1)-floor(edge_size/2)+1:s(1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare the images for comparison
%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(filter_resolution != 0)
  m1filt = SharpFilt(m1, pixel_size/filter_resolution, 3/s(1));
  m2filt = SharpFilt(m2, pixel_size/filter_resolution, 3/s(1));
else
  m1filt = m1;
  m2filt = m2;
end

m1avg = sum(m1(:))/numel(m1(:));
m2avg = sum(m2(:))/numel(m2(:));

m1filt = (1-mask) * m1avg + mask .* m1filt;

m1filt_rot_bin = BinImage(m1filt,rot_bin_factor);
m1filt_scale_bin = BinImage(m1filt,scale_bin_factor);
m2filt_bin = BinImage(m2filt,rot_bin_factor);

mask_rot_bin = BinImage(mask,rot_bin_factor);
mask_scale_bin = BinImage(mask,scale_bin_factor);

s_rot_bin = size(m1filt_rot_bin);
s_scale_bin = size(m1filt_scale_bin);

best_angle = angle_offset;
best_scale = scale_offset;

curr_angle_halfw = angle_halfw;
curr_scale_halfw = scale_halfw;

for iteration = 1:n_iterations

shortcut = 0;
if(shortcut == 0)
%%%%%%%%%%%%%%%%%%%%%% shortcut for debugging only!

best_score = -2;
best_m2rot = circshift(rotate2d(m2filt, -best_angle), [0 0]);

%%%%%%%%%%%%%%%%% scale search loop
for i_scale=1:n_scale
  if(n_scale == 1)
    scale = scale_offset
  else
    scale = scale_offset + (i_scale-(1+floor(n_scale/2))) * 2*curr_scale_halfw/(n_scale-1);
  end

  m2align = TransformImage(best_m2rot, 0, scale);
  m2align = BinImage(m2align, scale_bin_factor);
%  best_shift_score = -2;
%  for i = -max_shift:max_shift
%  for j = -max_shift:max_shift
%    m2shift = circshift(m2align, [i j]);
%    shift_score = corr2(m1filt_scale_bin(:).*mask_scale_bin(:), m2shift(:).*mask_scale_bin(:));
%    if(shift_score > best_shift_score && abs(i) > 1 && abs(j) > 1)
%      dx = i;
%      dy = j;
%      best_shift_score = shift_score
%    end
%  end
%  end
%  dx
%  dy
%  best_shift_score

%  m2align = (1-mask_scale_bin) * m2avg + mask_scale_bin .* m2align;
  cc = abs(ifftshift(ifftn(fftn(m1filt_scale_bin) .* conj(fftn(m2align)))));

%writeSPIDERfile(sprintf('debug%d_%d.spi',iteration, i_scale),m2align);
%writeSPIDERfile(sprintf('debug_b.spi'),m1filt_scale_bin);
%writeSPIDERfile(sprintf('debug_c.spi'),mask_scale_bin);

% mask away strong peak at the precise origin- and hope true peak isn't there!
  background = cc(1);
  cc(1+floor(s_scale_bin(1)/2)-1:1+floor(s_scale_bin(1)/2)+1,...
     1+floor(s_scale_bin(2)/2)-1:1+floor(s_scale_bin(2)/2)+1) = background;

  [xmax ymax] = find(cc == max(max(max(cc))));
  dx = round(xmax(1)-(floor(s_scale_bin(1)/2)+1))
  dy =  round(ymax(1)-(floor(s_scale_bin(2)/2)+1))

  m2align = circshift(m2align, [dx dy]);
  score = corr2(m1filt_scale_bin(:).*mask_scale_bin(:), m2align(:).*mask_scale_bin(:));

  if(i_scale == 1)
    first_score = score;
  end
  if(score > best_score)
    best_scale = scale;
    best_score = score;
  end

  printf('%8.5f %8.5f %6d %6d %3d %3d %8.5f %e\n', scale,best_angle, xmax,ymax,dx,dy,score,score-first_score);
  fflush(stdout);
end
%%%%%%%%%%%%%%%%% scale search loop

%%%%%%%%%%%%%%%%% rotation search loop
best_score = -2;

best_m2scale = TransformImage(m2filt, 0, best_scale);
best_m2scale = BinImage(best_m2scale, rot_bin_factor);

whos m2scale best_m2scale

for i_angle=1:n_angle
  if(n_angle == 1)
    angle = angle_offset
  else
    angle = angle_offset + (i_angle-(1+floor(n_angle/2))) * 2*curr_angle_halfw/(n_angle-1);
  end

  m2align = rotate2d(best_m2scale, -angle);
%  m2align = (1-mask_rot_bin) * m2avg + mask_rot_bin .* m2align;
  cc = abs(ifftshift(ifftn(fftn(m1filt_rot_bin) .* conj(fftn(m2align)))));

% mask away strong peak at the precise origin- and hope true peak isn't there!
  background = cc(1);
  cc(1+floor(s_rot_bin(1)/2)-1:1+floor(s_rot_bin(1)/2)+1,1+floor(s_rot_bin(2)/2)-1:1+floor(s_rot_bin(2)/2)+1) = background;

  [xmax ymax] = find(cc == max(max(max(cc))));
  dx = round(xmax(1)-(floor(s_rot_bin(1)/2)+1));
  dy =  round(ymax(1)-(floor(s_rot_bin(2)/2)+1));

  m2align = circshift(m2align, [dx dy]);
  score = corr2(m1filt_rot_bin(:).*mask_rot_bin(:), m2align(:).*mask_rot_bin(:));

  if(i_angle == 1)
    first_score = score;
  end
  if(score > best_score)
    best_angle = angle;
    best_score = score;
    best_dx = dx*rot_bin_factor;
    best_dy = dy*rot_bin_factor;
  end

  printf('%8.5f %8.5f %6d %6d %3d %3d %8.5f %e\n', best_scale, angle,xmax,ymax,dx,dy,score,score-first_score);
  fflush(stdout);
end
%%%%%%%%%%%%%%%%% rotation search loop

%%%%%%%%%%%%%%%%%%%%%% shortcut
else
  best_dx = -3;
  best_dy = 6;
  best_angle = -0.32000;
  best_scale = 1.00528
  best_m2rot = circshift(rotate2d(m2filt, -best_angle), [best_dx best_dy]);
end
%%%%%%%%%%%%%%%%%%%%%% shortcut

curr_scale_halfw = curr_scale_halfw / (n_angle-1) * 2;
scale_offset = best_scale;

curr_angle_halfw = curr_angle_halfw / (n_angle-1) * 2;
angle_offset = best_angle;

end

%%%%%%%%%%%%%%%%%%%%%% super-sample, find the sub-pixel shifts, and return the result

fprintf(stdout, 'Supersampling...\n');
fflush(stderr);

s_supersample = supersample*s;

%m1super = zeros(s_supersample);
%origin = 1 + size(m1super)/2-floor(s/2);
%m1super(origin(1):origin(1)+s(1)-1,origin(2):origin(2)+s(2)-1) = fftshift(fftn(m1filt));
%m1super = ifftshift(m1super);

m1super = scale2d(m1filt, supersample);

%m2super = zeros(s_supersample);
%m2super(origin(1):origin(1)+s(1)-1,origin(2):origin(2)+s(2)-1) = fftshift(fftn(best_m2rot));
%m2super = real(ifftn(ifftshift(m2super)));
m2super = scale2d(best_m2rot,supersample);

fprintf(stdout, 'Finding best shifts...\n');
fflush(stderr);

m2super = TransformImage(m2super, 0, best_scale);

cc = abs(ifftshift(ifftn(fftn(m1super) .* conj(fftn(m2super)))));
% mask away strong peak at the precise origin- we hope that the true peak isn't there!

w = 5;
background = cc(1);
cc(1+floor(s_supersample(1)/2)-w:1+floor(s_supersample(1)/2)+w,...
   1+floor(s_supersample(2)/2)-w:1+floor(s_supersample(2)/2)+w) = background;

%writeSPIDERfile('cc.spi',cc);
%writeSPIDERfile('m1super.spi',m1super);
%writeSPIDERfile('m2super.spi',m2super);

[xmax ymax] = find(cc == max(max(max(cc))))

dx = round(xmax(1)-(floor(s_supersample(1)/2)+1))
dy =  round(ymax(1)-(floor(s_supersample(2)/2)+1))

m2super = circshift(m2super, [dx dy]);
%writeSPIDERfile('m2super2.spi',m2super);

best_m2align = m2super(1:supersample:s_supersample,1:supersample:s_supersample);
best_m2align = real(ifftn(fftn(best_m2align).*FourierShift(s(1),[1/supersample 1/supersample])));

%best_m2align = circshift(m2super,[1 1]), supersample)
%best_m2align = BinImageCentered(circshift(m2super,[1 1]), supersample);

dx = dx/supersample;
dy = dy/supersample;
return
