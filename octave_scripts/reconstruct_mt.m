function [vol_array] = reconstruct_mt(micrograph_name, num_pfs, num_starts, est_repeat_distance_13pf,...
                coords, ref_com, phi, theta, psi, ...
                d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat, ...
                bin_factor, tot_helimap, norm_params, ...
                pad_factor, use_ctf, defocus, astig_mag, astig_angle)

n_good_boxes = prod(size(phi));

if(nargin < 14)
  norm_params = repmat([1; 0], [1 n_good_boxes]);
end

if(nargin < 18)
  use_ctf = 0;
  defocus = 0;
  astig_mag = 0;
  astig_angle = 0;
end

if(isoctave)
  run 'chuff_parameters.m';
else
  run 'chuff_parameters';
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

final_pixel_size = micrograph_pixel_size*bin_factor;

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
box_dim = 16*ceil(box_dim/16);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];
ref_dim = floor(box_dim/bin_factor);

stack = zeros([ref_dim num_pfs*n_good_boxes]);

total_phi = zeros([1 num_pfs*n_good_boxes]);
total_theta = zeros([1 num_pfs*n_good_boxes]);
total_psi = zeros([1 num_pfs*n_good_boxes]);

sites_per_repeat = num_pfs;
n_classes = size(tot_helimap);
n_classes = n_classes(3);

%if(fopen('reconstruct_mt.binmat') > 0)
if(0 == 1)
  'loading data'
  load 'reconstruct_mt.binmat';
else

for box_num=1:n_good_boxes

  fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
  if(isoctave())
    fflush(stdout);
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get coordinates for each subunit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;
  monomer_offset = 0;

  [x_final y_final z_final phi_final theta_final psi_final r_final ...
   x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist repeat_index2 pf_index2 ...
   box_repeat_origin] = ...
     microtubule_parametric(1,num_pfs,num_starts,ref_com,...
                          est_repeat_distance_13pf,radius_scale_factor,monomer_offset,...
                          [phi(box_num) d_phi_d_repeat(box_num)],...
                          [theta(box_num) d_theta_d_repeat(box_num)],...
                          [psi(box_num) d_psi_d_repeat(box_num)],elastic_params);

  total_phi(1 + (box_num-1)*num_pfs:box_num*num_pfs) = phi_final*180/pi;
  total_theta(1 + (box_num-1)*num_pfs:box_num*num_pfs) = theta_mt*180/pi;
%  total_theta(1 + (box_num-1)*num_pfs:box_num*num_pfs) = theta_final*180/pi;
%  total_psi(1 + (box_num-1)*num_pfs:box_num*num_pfs) = psi_final*180/pi;

  for i = 1:num_pfs

i
    box = read_chumps_box(micrograph_name,1,...
                          [coords(box_num,1) + x_mt(i)/micrograph_pixel_size ...
                           coords(box_num,2) + y_mt(i)/micrograph_pixel_size],...
                          box_dim, bin_factor, -psi_mt(i)*180/pi);

%    box = read_chumps_box(micrograph_name,1,...
%                          [coords(box_num,1) + x_final(i)/micrograph_pixel_size ...
%                           coords(box_num,2) + y_final(i)/micrograph_pixel_size], ...
%                          box_dim, bin_factor, -psi_final(i)*180/pi);
    if(box == -1)
      continue;
    end

    box = invert_density * (box-norm_params(2,box_num))/norm_params(1,box_num);

    stack(:,:,i + (box_num-1)*num_pfs) = box;
  end
end

end % load reconstruct_mt.binmat

selrand = rand([1 num_pfs*n_good_boxes]);
selrand = selrand(:);

vol_array = zeros([ref_dim ref_dim(1) n_classes]);

%save -binary 'reconstruct_mt.binmat' total_psi total_theta total_phi stack tot_helimap ...
%                          pad_factor  use_ctf  final_pixel_size  ...
%                          accelerating_voltage  spherical_aberration_constant  ...
%                          amplitude_contrast_ratio;

% for igold = 1:2
 
best_helimap = zeros(size(tot_helimap(:,:,1)));

for indi = 1:num_pfs
for indj = 1:n_good_boxes
  tempscores = squeeze(tot_helimap(indi,indj,:));

  best_helimap(indi,indj) = find(tempscores == max(tempscores(:)));
end
end

for indi = 1:n_classes
indi
  ind = find(best_helimap(:) == indi)

  vol_array(:,:,:,indi) = ...
    recon_fi_spw([total_psi(ind); total_theta(ind); total_phi(ind)]',...
                 stack(:,:,ind), pad_factor, use_ctf, final_pixel_size, ...
                 accelerating_voltage, spherical_aberration_constant, amplitude_contrast_ratio);
end
