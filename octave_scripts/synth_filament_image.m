function synth_filament_image(psi_val,psi_amplitude,theta_val,theta_amplitude,phi_val  ,phi_amplitude  ,num_starts,mic_dim,occupancy_frac,tub_ref_dir,invert_density,output_file, use_gridding)


psi_params = [psi_val psi_amplitude];
theta_params = [theta_val theta_amplitude];
phi_params = [phi_val phi_amplitude];

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m';
else
  run 'chuff_parameters';
end

%%%%%%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%%%%%%

radius_scale_factor = 1;
monomer_offset = 0;

select_pf = 0;     % [num_pfs 1];

%%%%%%%%%%%%%%%%%%%%%%%
% Read document file info (com, ref_pixel_size)
%%%%%%%%%%%%%%%%%%%%%%%

% tub_coords = PDBRead('chumps_round1/ref_tub_subunit/tub.pdb');
% x = struct2cell(tub_coords.ATOM);
% X = cell2mat({ x{9,1,:}});
com_info = readSPIDERdoc_dlmlike(sprintf('%s/tub_cgr_doc.spi',tub_ref_dir));
index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

% pixel_info = readSPIDERdoc_dlmlike(strcat(tub_ref_dir,'/ref_params.spi'));
% index = find(pixel_info(:,1) == 1);
% min_theta = pixel_info(index(1),5)
% d_angle = pixel_info(index(1),7)

% index = find(pixel_info(:,1) == 2);
% ref_pixel_size = pixel_info(index(1),5);

ref_pixel_size = dlmread(sprintf('%s/info.txt',tub_ref_dir));
ref_pixel_size = ref_pixel_size(2);

if( abs(ref_pixel_size - 10000*scanner_pixel_size/target_magnification)/ref_pixel_size > 0.01)
  fprintf('Warning: reference directories must have the target pixel size\n');
ref_pixel_size
10000*scanner_pixel_size/target_magnification
%  exit(2)
end

%%%%%%%%%%%%%%%%%%%%%%%
% Generate tubulin coordinates
%%%%%%%%%%%%%%%%%%%%%%%

num_repeats = floor( (min(mic_dim)*ref_pixel_size/helical_repeat_distance))

%[x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
% x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist ...
% mt_repeat_index mt_pf_index origin_repeat phi_first_pf] =...
%   microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
%                          helical_repeat_distance,radius_scale_factor,monomer_offset,...
%                          phi_params,theta_params,psi_params,elastic_params);

[x_tub y_tub z_tub phi_tub theta_tub psi_tub ...
    x_mt y_mt z_mt phi_mt theta_mt psi_mt origin_repeat] = ...
    filament_parametric(num_repeats, ref_com, helical_twist, helical_repeat_distance, phi_params, theta_params, psi_params);

occupancy = rand([num_repeats 1]) <= occupancy_frac;

%%%%%%%%%%%%%%%%%%%%%%%
% Generate images
%%%%%%%%%%%%%%%%%%%%%%%

tub_ref_vol = readSPIDERfile(strcat(tub_ref_dir, '/tub_centered_vol.spi'));
%kin_ref_vol = readSPIDERfile(strcat(kin_ref_dir, '/kin_centered_vol.spi'));

mic = generate_micrograph_gridproj(mic_dim, tub_ref_vol, ref_pixel_size, select_pf,...
                          x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub);

% kin = generate_micrograph_gridproj(mic_dim, kin_ref_vol, ref_pixel_size, select_pf,...
%                           x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,[0 0],occupancy);

% tub_ref_file = strcat(tub_ref_dir, '/ref_tot.spi');
% kin_ref_file = strcat(kin_ref_dir, '/ref_tot.spi');
% refs = readSPIDERfile(tub_ref_file);
% kinrefs = readSPIDERfile(kin_ref_file);
% 
% mic = generate_micrograph(mic_dim, refs, ref_pixel_size, min_theta, d_angle, select_pf,...
%                           x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub);
% 
% kin = generate_micrograph(mic_dim, kinrefs, ref_pixel_size, min_theta, d_angle, select_pf,...
%                           x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,[0 0],occupancy);

%%%%%%%%%%%%%%%%%%%%%%%
% Write the file
%%%%%%%%%%%%%%%%%%%%%%%

% WriteMRC(mic+kin, ref_pixel_size, output_file);
writeSPIDERfile(output_file, invert_density*(mic));

%%%%%%%%%%%%%%%%%%%%%%%
% Save the descriptive parameters (including decoration pattern)
%%%%%%%%%%%%%%%%%%%%%%%

output_occ_file = sprintf('%s_occ.txt',output_file(1:prod(size(output_file))-4));
output_param_file = sprintf('%s_param.txt',output_file(1:prod(size(output_file))-4));
output_coord_file = sprintf('%s_coords.txt',output_file(1:prod(size(output_file))-4));

occupancy = reshape(occupancy, [1 num_repeats]);

if(isoctave)
  save('-text',output_occ_file,'occupancy');

  save('-text',output_param_file,...
       'num_repeats','ref_com', 'num_starts', ...
       'helical_repeat_distance',...
       'phi_params','theta_params','psi_params',...
       'mic_dim', 'ref_pixel_size',...
       'select_pf') ;

  save('-text',output_coord_file,...
       'x_tub','y_tub','z_tub','phi_tub','theta_tub','psi_tub',...
       'x_mt','y_mt','z_mt','phi_mt','theta_mt','psi_mt');
else
  save(output_occ_file,'occupancy','-ascii');

  save(output_param_file,...
       'num_repeats','ref_com',...
       'helical_repeat_distance','radius_scale_factor',...
       'phi_params','theta_params','psi_params',...
       'mic_dim', 'ref_pixel_size','num_starts', ...
       'select_pf', '-ascii');


  save(output_coord_file,...
       'x_tub','y_tub','z_tub','phi_tub','theta_tub','psi_tub','-ascii',...
       'x_mt','y_mt','z_mt','phi_mt','theta_mt','psi_mt','-ascii');
end


return

