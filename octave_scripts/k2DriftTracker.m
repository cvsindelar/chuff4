% k2DriftTracker
% Create a summed image from a K2 movie.
% From DriftTrackingWholeFrame5 for the DE camera.
% F.S. 11 Sep 13

clear;  % Clear out other variables, as this script needs about 4 GB itself.
postName='';
restoreImages=1;  % Compute the full-size aligned images
dsw=4;            % Downsampling of working images
niters=4;

aliDir='Aligned/';
jpegDir=[aliDir 'jpeg/'];
saveFigures=1;

% Have the user select some mi files: boilerplate
if ~exist('fname','var') || ~exist('doBatchProcessing','var') || ~doBatchProcessing
    [fname, pa]=uigetfile('*.*','Select movie files','multiselect','on');
    if isnumeric(pa) % File selection cancelled
        return
    end;
    [rootPath, moviePath]=ParsePath(pa);
    if ~iscell(fname)
        fname={fname};
    end;
    cd(rootPath);
end;

if ~exist(aliDir,'dir')
    mkdir(aliDir);
end;
if ~exist(jpegDir,'dir')
    mkdir(jpegDir);
end;

nfiles=numel(fname);
tic
for findex=1:nfiles
    
    figure(1);
    SetGrayscale;
    
    [pa, baseFilename]=fileparts(fname{findex});
    movName=[moviePath fname{findex}];
    disp(['Reading ' movName]);
    [mv,pixA]=ReadEMFile(movName);
    
    n0=size(mv);
    nim=n0(3);
    disp('Convert to double');
    m0=double(mv);
    mfMean=mean(m0(:));
    n=[1 1]*NextNiceNumber(n0(2));  % raw image size
    
    dn=(n(2)-n0(2))/2;  % First element shift
    m0=Crop(m0-mfMean,n,1);  % pad the entire stack.
    
    % Compute all the fts
    disp('Computing FTs');
    fm0=fft2(m0);
    %%
    clf;
    subplot(2,3,1);
    imac(imscale(BinImage(sum(m0,3),4),256,1e-5));
    axis off;
    title(baseFilename,'interpreter','none');
    
    % Compute all the ccfs
    fmSum=sum(fm0,3);
    crossSpect=zeros([n nim],'single');
    ncc=256;
    ccs0=zeros(ncc,ncc,nim,'single');
    ccs =zeros(ncc,ncc,nim,'single');
    ctr=ncc/2+1;
    disp('Computing CCFs');
    subplot(2,3,2);
    for i=1:nim
        cs=fm0(:,:,i).*conj((fmSum-fm0(:,:,i)));
        cs(1,1:120:n(2))=0;  % Remove the spikes
        crossSpect(:,:,i)=cs;
        cc=Crop(fftshift(real(ifftn(cs))),ncc);
        ccs0(:,:,i)=cc;
        imacs(GaussFilt(Crop(cc,64),.2));
        title(i);
        drawnow;
    end;
    
    
    % Estimate the artifact amplitude
    
    us=-.21;  % undershoot values
    us2=-.08;
    mul=[1 1 1 1 1; 1 0 0 0 1; 1 1 0 1 1; 1 0 0 0 1; 1 1 1 1 1]';
    pmul=Crop(mul,ncc);
    frac=[us us2 us; 0 1 0; us us2 us]';
    pfrac=Crop(frac,ncc);
    amps=zeros(nim,1);
    ccMean=mean(ccs0,3);
    %     Estimate the value of the peak artifact
    pkv=ccMean(ctr,ctr)-(pmul(:)'*ccMean(:))/sum(mul(:));
    ccCorr=pkv*pfrac;
    subplot(2,3,3);
    for i=1:nim
        ccs(:,:,i)=ccs0(:,:,i)-ccCorr;
        imacs(GaussFilt(Crop(ccs(:,:,i),64),.2));
        title('Corrected');
        drawnow;
    end;
    %%
    
    % Downsample the images
    disp('Downsampling');
    nw=n/dsw;
    nwx=nw(1);
    nwy=nw(2);
    dsWindow=ifftshift(fuzzymask(nw,2,0.95*nw(1),.1*nw(1)));
    wfCorr=dsWindow.*Cropo(fftn(ifftshift(Crop(ccCorr,n))),nw);
    wF0=zeros([nw nim]);
    wI0=zeros([nw nim]);
    for i=1:nim
        wF0(:,:,i)=dsWindow.*Cropo(fm0(:,:,i),nw);  % truncated ft
        wI0(:,:,i)=real(ifftn(wF0(:,:,i)));
    end;
    
    %%  Initialize tracking
    wI=wI0;
    
    xsh=zeros(nim,1);
    ysh=zeros(nim,1);
    dxTotal=single(zeros(nim,1));
    dyTotal=single(zeros(nim,1));
    dxAccum=zeros(nim,niters);  % accumulated shifts for plots
    dyAccum=zeros(nim,niters);
    
    fShifts=complex(single(ones([nw nim])));  % Fourier shift arrays
    wISpec=zeros([nw nim]);  % Fourier-shifted spectrum models
    cciLocal=zeros(15,15,niters);  % 1st ccs for figure
    
    %
    disp('Tracking');
    for iter=1:niters
        disp(['Iteration ' num2str(iter)]);
        
        % Construct the independent means
        meanInds=1:nim;  % indices of images to sum for means.
        meanN=numel(meanInds)-1;
        wIMean=zeros([nw nim]);
        for i=1:nim
            meanIndsX=meanInds(meanInds~=i);  % Delete one entry
            wIMean(:,:,i)=mean(wI(:,:,meanIndsX),3);
            wISpec(:,:,i)=wfCorr.*fShifts(:,:,i).*conj(mean(fShifts(:,:,meanIndsX),3));
        end;
        
        %% Compute the weighting filter
        [H1, rs]=k2GetWeightingFilter3(wI,wIMean,wISpec);
        
        %% show the weighting filter.
        subplot(233);
        plot(Radial2(fftshift(H1)));
        title('H1');
        
        %% Compute the shifts from the cross-correlation peaks
        ccSum=zeros(nw);
        for i=1:nim
            crossSpec=H1.*fftn(wI(:,:,i)).*conj(fftn(wIMean(:,:,i)));
            cc=fftshift(real(ifftn(crossSpec-H1.*wISpec(:,:,i))));
            ccSum=ccSum+cc;
            [mxv ix iy]=max2di(cc);
            xsh(i)=ix-nwx/2-1;
            ysh(i)=iy-nwy/2-1;
            %             if i==1
            %                 subplot(236);
            %                 cciLocal(:,:,iter)=Crop(ccSum,size(cciLocal,1));
            %                 imacs(cciLocal(:,:,iter));
            %                 title('Frame 1 CC');
            %                 crossSpec1=fftn(wI(:,:,i))...
            %                     .*conj(fftn(wIMean(:,:,i)))-wISpec(:,:,i);
            %             end;
            %             drawnow;
        end;
        kRelax=1;
        % Accumulate the shifts
        dxTotal=dxTotal+kRelax*xsh;  % used for image correction
        dxAccum(:,iter)=dxTotal;     % 2d array for plotting
        dyTotal=dyTotal+kRelax*ysh;
        dyAccum(:,iter)=dyTotal;
        
        %  Display the progress.
        %     Show the averaged cross-correlation, to watch convergence
        showRawCC=0;
        if showRawCC  % compute the CC without using the weighting filter.
            for i=1:nim
                crossSpec=fftn(wI1(:,:,i)).*conj(fftn(wIMean(:,:,i)))/prod(nw);
                cc=fftshift(real(ifftn(crossSpec-wISpec(:,:,i))));
                ccSum=ccSum+cc;
            end;
        end;
        %     subplot(236);
        %     imacs(Crop(ccSum,32));
        %     title('Mean cross correlation');
        %     drawnow;
        
        subplot(234);  % show the cumulative translations
        plot(-dsw*dxAccum);
        title('X Shift');
        ylabel('Original pixels');
        xlabel('Frame no.');
        cellTxt=cell(niters,1);
        for j=1:niters
            cellTxt{j}=num2str(j);
        end;
        legend(cellTxt,'location','northeast');
        
        subplot(235);
        plot(-dsw*dyAccum);
        title('Y Shift');
        xlabel('Frame no.');
        
        drawnow;
        
        %% Shift the working images, and update the Fourier shifts
        for i=1:nim
            fSh=FourierShift(nw,-[dxTotal(i) dyTotal(i)]);
            wI(:,:,i)=real(ifftn(fftn(wI0(:,:,i)).*fSh));
            fShifts(:,:,i)=fSh;
        end;
    end; % for iter
    %%  Restore the full-size images
    
    if restoreImages
        disp('Getting full-sized images');
        %%
        fsum=zeros(n);
        fshSum=zeros(n);
        for i=1:nim
            fSh=FourierShift(n,-dsw*[dxTotal(i) dyTotal(i)]);
            fshSum=fshSum+fSh;
            fsum=fsum+fm0(:,:,i).*fSh;
        end;
        imgSum=real(ifftn(fsum))+nim*mfMean;  % restore the mean
        %%
        spectrumDisplayExp=.1;
        bin=16;
        figure(1);
        subplot(2,3,1);
        imac(imscale(BinImage(imgSum,4),256,1e-5));
        axis off;
        title(baseFilename,'interpreter','none');
        
        pspect=BinImage(fftshift(abs(fsum)),bin);
        pspect0=BinImage(fftshift(abs(fmSum)),bin);
        subplot(2,3,2);
        imacs((Crop(pspect0,n/(bin*2))).^spectrumDisplayExp);
        title('Before align: to 1/2 Nyquist');
        subplot(2,3,3);
        imacs((Crop(pspect,n/(bin*2))).^spectrumDisplayExp);
        title('After align');
        %%
        % Write the output
        disp(['Writing ' baseFilename postName '-sumali']);
        WriteMRC(imgSum,pixA,[aliDir baseFilename postName '-sumali.mrc']);
        WriteJpeg(rot90(imgSum),[jpegDir baseFilename postName '-sumali.jpg']);
        disp('done.');
    end;
    %%
    if saveFigures
        set(gcf,'paperpositionmode','auto');
        jName=[jpegDir baseFilename postName '-align.jpg'];
        print('-djpeg','-r300',jName);
    end;
    shiftX=dxTotal*dsw;
    shiftY=dyTotal*dsw;
    save([aliDir baseFilename '-shifts'],'shiftX','shiftY');  % Save the working image set
    %
    
end;
toc;
