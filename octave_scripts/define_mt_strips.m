function [big_strip_min, big_strip_max] = ...
           define_mt_strips(lateral_displace,num_pfs,n_good_boxes,...
                            max_t_matrix_subunits,min_subunit_overlap)
%%%%%%%%%%%%%%%
% This function takes a list of lateral subunit displacements (lateral location relative to the
%  helix axis) and defines a series of strips that are expected to contain 
%  a certain number of subunits ("max_t_matrix_subunits").  The strips are designed to
%  overlap with a minimum of "min_subunit_overlap".
%%%%%%%%%%%%%%%

sort_matrix = zeros([2 prod(size(lateral_displace))]);
sort_matrix(1,:) = 1:prod(size(lateral_displace));
sort_matrix(2,:) = lateral_displace(:);

[ordered,index]=sort(sort_matrix(2,:));
small_strip_min = zeros([1 num_pfs]);
small_strip_max = zeros([1 num_pfs]);

for i=1:num_pfs
 small_strip_min(i) = lateral_displace(index(1+(i-1)*2*n_good_boxes));
 small_strip_max(i) = lateral_displace(index(i*2*n_good_boxes));
end

if(num_pfs <= max_t_matrix_subunits)
  n_big_strips = 1;
  big_strip_min(1) = small_strip_min(1);
  big_strip_max(1) = small_strip_max(num_pfs);
else 
  if(num_pfs <= 2*max_t_matrix_subunits - min_subunit_overlap)
    n_big_strips = 2;
  else
    n_big_strips = 2 + ceil((num_pfs - (2*max_t_matrix_subunits - min_subunit_overlap)) / ...
                            (max_t_matrix_subunits - 2*min_subunit_overlap));
  end

  big_strip_min(1) = small_strip_min(1);
  big_strip_max(1) = small_strip_max(max_t_matrix_subunits);
  big_strip_min(n_big_strips) = small_strip_min(num_pfs - max_t_matrix_subunits + 1);
  big_strip_max(n_big_strips) = small_strip_max(num_pfs);

  current_strip = max_t_matrix_subunits - min_subunit_overlap + 1;
  for i=2:n_big_strips-1
    big_strip_min(i) = small_strip_min(current_strip);
    big_strip_max(i) = small_strip_min(current_strip + max_t_matrix_subunits - 1);
    current_strip = current_strip + max_t_matrix_subunits - 2*min_subunit_overlap;
  end
end

return
