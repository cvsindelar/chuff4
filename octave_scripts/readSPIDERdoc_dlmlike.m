function info = readSPIDERdoc_dlmlike(filename,strip)

if(isoctave)
  info = dlmread(filename);
else
  temp = readSPIDERdoc(filename);
  s = size(temp);
  info = zeros([s(1) s(2)+2]);
  info(1:s(1),3:s(2)+2) = temp;
  info(1:s(1),1) = 1:s(1);
end

if(nargin > 1)
  if(strip != 0)
    s = size(info);
    i = 1;
    while(i <= s(1))
      s = size(info);
      if(info(i,2)+2 != s(2))
         info(i:s(1)-1,:)=info(i+1:s(1),:);
         info = info(1:s(1)-1,:);
      end
      s = size(info);
      i=i+1;
    end
  end
end
