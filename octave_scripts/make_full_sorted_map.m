if(nargin < 3)
  fprintf('PROGRAMMING ERROR: wrong number of arguments to octave script! (%d)\n', nargin);
%  exit(2)
end

focal_mate = str2num(argv(){1});
chumps_round = str2num(argv(){2});
bin = str2num(argv(){3});
num_args = 3;

use_ctf = 0;

tubrefs = [];
kinrefs = [];
tub_ref_file = sprintf('chumps_round%d/ref_tub_subunit_bin%d/ref_tot.spi',chumps_round,bin);
kin_ref_file = sprintf('chumps_round%d/ref_kin_subunit_bin%d/ref_tot.spi',chumps_round,bin);
%tubrefs = readSPIDERfile(tub_ref_file);
%kinrefs = readSPIDERfile(kin_ref_file);

num_mts = nargin - num_args;

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m';

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification

tot_n_particles = 1;
scale_avg = 0;
n_scale_avg = 0;
tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

tot_n_particles = 1;

for mt_num = 1: num_mts
%%%%%%%%%%%%%%%
% Read in filenames, etc.
%%%%%%%%%%%%%%%

  job_dir = argv(){mt_num+num_args};

  if(job_dir(prod(size(job_dir))) == '/')
    job_dir=job_dir(1:prod(size(job_dir))-1);
  end
  [start] = regexp(job_dir,'[^/]*$','start');
  job_dir = job_dir(start:prod(size(job_dir)));

  [micrograph,defocus,astig_mag,astig_angle,b_factor,find_dir] = ...
    chumps_micrograph_info(job_dir,focal_mate,chumps_round);

  box_file_name = sprintf('scans/%s.txt',job_dir);

  mt_type_info = dlmread(sprintf('chumps_round%d/%s/selected_mt_type.txt',chumps_round,job_dir));
  num_pfs = mt_type_info(1);
  num_starts = mt_type_info(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read in box info and extract particles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  mic_dim = ReadMRCheader(micrograph);

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

  com_doc_name = sprintf('chumps_round%d/ref_tub_subunit_bin%d/tub_cgr_doc.spi',chumps_round,bin);
%  com_doc_name = 'chumps_round1/ref_tub_subunit/tub_cgr_doc.spi';
  com_info = dlmread(com_doc_name);
  index = find(com_info(:,1) == 1);
  ref_com = com_info(index(1),3:5);

  ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,bin);
  ref_pixel = dlmread(sprintf('%s/info.txt',ref_dir));
  ref_pixel = ref_pixel(2);

%  ref_params_name = sprintf('chumps_round%d/ref_tub_subunit_bin%d/ref_params.spi',...
%                     chumps_round,bin);
%  ref_params_name = 'chumps_round1/ref_tub_subunit/ref_params.spi';
%  ref_pixel_info = dlmread(ref_params_name);
%  index = find(ref_pixel_info(:,1) == 2);
%  ref_pixel = ref_pixel_info(index(1),5);
%  index = find(ref_pixel_info(:,1) == 1);
%  min_theta = ref_pixel_info(index(1),5);
%  index = find(ref_pixel_info(:,1) == 3);
%  d_angle = ref_pixel_info(index(1),3);

  bin_factor = round(ref_pixel/micrograph_pixel_size);
  final_pixel_size = micrograph_pixel_size*bin_factor;

  box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
  box_dim = 16*ceil(box_dim/16);
  box_dim = box_dim*bin_factor;
  box_dim = [box_dim box_dim];
  ref_dim = floor(box_dim/bin_factor);
  pad_ref_dim = 2*ref_dim;

  [num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
    est_repeat_distance] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);

  est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

  [coords,phi,theta,psi,...
   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
    fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,
                         est_pix_per_repeat,num_pfs,num_starts,ref_com);

  n_good_boxes = prod(size(phi));

  radon_scale = dlmread(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));
  index = find(radon_scale(:,1) == 1);
  radon_scale = radon_scale(index(prod(size(index))),3);
  scale_avg = scale_avg + radon_scale;
  n_scale_avg = n_scale_avg + 1;

%%%%%%%%%%%%%%%%%%
% Count how many "good" boxes are fully inside the micrograph
%%%%%%%%%%%%%%%%%%

  real_n_good_boxes = 0;
  for box_num=1:n_good_boxes
    int_coords = round(coords(box_num,:));
    box_origin = int_coords-floor(box_dim/2);
    if(box_origin(1) < 1 || box_origin(2) < 1 || ...
       box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
      continue
    end
    real_n_good_boxes = real_n_good_boxes + 1;
  end

%%%%%%%%%%%%%%%%%%
% Read in decoration map
%%%%%%%%%%%%%%%%%%
  max_overlap = 0*num_pfs;
  sites_per_repeat = 2*num_pfs;
%  num_repeats = ceil( (sites_per_repeat+2*max_overlap)/num_pfs );
  num_repeats = 1;
  kin_lib_size = 2*num_repeats*num_pfs;

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;

  map_file = sprintf('%s/tot_decorate_map.spi',find_dir);
  tot_decorate_map = readSPIDERfile(map_file);
%  test_sorted_map = readSPIDERfile('test_sorted_map.spi');

  pad_sort_map = 3;
  full_sorted_map = zeros([num_pfs 2*(2*pad_sort_map+n_good_boxes)]);

  [box_repeat_origin, kin_repeat_index, kin_pf_index,...
   x_tub, y_tub, phi_tub, theta_tub, psi_tub] = ...
    get_findkin_refs(1, ref_dim, [n_good_boxes n_good_boxes 1], num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com);

[box_bounds, tot_bounds, sorted_indices] = ...
  map_to_helimap(tot_decorate_map, tot_decorate_map, 1, [1 n_good_boxes], 1, ...
                 num_pfs, sites_per_repeat, kin_repeat_index, kin_pf_index);

  full_sorted_map(sorted_indices) = tot_decorate_map(box_bounds(1):box_bounds(2));

  sorted_map_file = sprintf('%s/sorted_map_full.spi',find_dir);
  writeSPIDERfile(sorted_map_file,full_sorted_map);

  fprintf(stdout,'\n%s\n', job_dir);

end % for mt_num ...

return
