function [num_pfs,num_starts,coords, phi, theta, psi, ...
          directional_psi,est_repeat_distance] = ...
   make_synthetic_chuff_alignment(job_dir,helical_repeat_distance,chumps_round,pixel_size,...
                                   synth_header,emify_header)

job_dir = trim_dir(job_dir);

load(sprintf('%s_param.txt',synth_header));
load(sprintf('%s_coords.txt',synth_header));
[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
ctf_doc_name = sprintf('scans/%s_ctf_doc.spi',job_dir(1:start-1));
doc_name = sprintf('scans/%s_doc.spi',job_dir);
if(nargin > 5)
  load(sprintf('%s.mat',emify_header));
else
  defocus = 0;
  astig_amplitude = 0;
  astig_angle = 0;
end
writeSPIDERdoc(ctf_doc_name, [defocus astig_amplitude astig_angle]);

%%%%%%%%%%%%%
% write doc file
% %%%%%%%%%%%%%%%%

writeSPIDERdoc(doc_name, [(x_mt/pixel_size + mic_dim(1) / 2)' - 50 (y_mt/pixel_size + mic_dim(2) / 2)' - 50 ones(size(x_mt))' * 100,   -(psi_mt / pi * 180)' + 180]);


%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

micrograph = chumps_micrograph_info(job_dir,0,chumps_round);

mic_dim = ReadMRCheader(micrograph);

mt_info_name = sprintf('chumps_round%d/%s/selected_mt_type.txt',chumps_round,job_dir);
num_pfs = 1;
fp = fopen(mt_info_name,'w');
num_tub_starts = num_starts;
fprintf(fp, '%d %d\n', num_pfs, num_tub_starts);
fclose(fp);

phi_mt = phi_mt;
phi = phi_mt(1:num_pfs:prod(size(phi_mt)))*180/pi;
theta = theta_mt(1:num_pfs:prod(size(theta_mt)))*180/pi;
psi = psi_mt(1:num_pfs:prod(size(psi_mt)))*180/pi;

x_mt = x_mt(1:num_pfs:prod(size(x_mt)));
y_mt = y_mt(1:num_pfs:prod(size(y_mt)));
z_mt = z_mt(1:num_pfs:prod(size(z_mt)));

phi = phi(prod(size(phi)):-1:1);
theta = theta(prod(size(theta)):-1:1);
psi = psi(prod(size(psi)):-1:1);

x_mt = x_mt(prod(size(x_mt)):-1:1);
y_mt = y_mt(prod(size(y_mt)):-1:1);
z_mt = z_mt(prod(size(z_mt)):-1:1);

n_good_boxes = prod(size(phi));

radon_scale = [1 2 1]';

if(isoctave)
  f = fopen(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir),'w');
  fprintf(f, '%5d%3d%11.4f', radon_scale(1), radon_scale(2), radon_scale(3));
  fclose(f);
end

align_doc_name = ...
  sprintf('chumps_round%d/%s/individual_align_doc_pf%02d_start%d.spi',...
          chumps_round,job_dir,num_pfs,num_tub_starts);

% align_doc = readSPIDERdoc_dlmlike(align_doc_name,1)

align_doc = [90 - psi; ...
             ones([1 n_good_boxes]); ...
             zeros([1 n_good_boxes]); ...
             zeros([1 n_good_boxes]); ...
             90*ones([1 n_good_boxes]); ...
             theta;
             phi; ...
             -1 * ones([1 n_good_boxes]); ...
             -1 * ones([1 n_good_boxes]); ...
             1 + floor(mic_dim(1)/2) + x_mt/pixel_size; ...
             1 + floor(mic_dim(2)/2) + y_mt/pixel_size; ...
             ]';

writeSPIDERdoc(align_doc_name, align_doc);

good_align_doc_name = ...
  sprintf('chumps_round%d/%s/guess_individual_align_doc_pf%02d_start%d_good.spi',...
          chumps_round,job_dir,num_pfs,num_tub_starts);

writeSPIDERdoc(good_align_doc_name, align_doc(:,5:7));

return
