args = argv();
s = size(args);
if(s(1) != 15)
  fprintf('PROGRAMMING ERROR: wrong number of arguments to octave script! (%d)\n', s(1));
%  exit(2)
end

dx = str2num(argv(){1});
dy = str2num(argv(){2});
psi_params = str2num(argv(){3});
theta_params = str2num(argv(){4});
phi_params = str2num(argv(){5});
job_dir =        argv(){6};
num_pfs =        str2num(argv(){7});
num_starts =     str2num(argv(){8})/2;
repeat_dist13pf = str2num(argv(){9});
ref_com =        [str2num(argv(){10}) str2num(argv(){11}) str2num(argv(){12})];
pixel_size =     str2num(argv(){13});
ref_dir =        argv(){14};
kin_ref_dir =        argv(){15};

radius_scale_factor = 1;
monomer_offset = 0;
elastic_params = [0 0 0 0];
num_repeats = 10;
mic_dim = [500 500];

min_theta = 75;
d_angle = 1;
select_pf = 0; # [num_pfs 1];

refs = readSPIDERfile(sprintf('%s/ref_tot.spi', ref_dir));
kinrefs = readSPIDERfile(sprintf('%s/ref_tot.spi', kin_ref_dir));


for i=1:5
% elastic_params = [(i-1)*0.025 0 0 0]
radius_scale_factor = 1+(i-3)*0.025
  [x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
          x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist] =...
              microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                                     repeat_dist13pf,radius_scale_factor,monomer_offset,...
                                     phi_params,theta_params,psi_params,elastic_params);

  mic = generate_micrograph(mic_dim, refs, pixel_size, min_theta, d_angle, select_pf,...
                                   x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub);
  kin = generate_micrograph(mic_dim, kinrefs, pixel_size, min_theta, d_angle, select_pf,...
                                   x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub);
  output_file = sprintf('test_deform_radius%d.spi',i)
  writeSPIDERfile(output_file, mic+kin);
end
