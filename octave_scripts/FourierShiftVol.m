function P=FourierShiftVol(n,sh)
% function P=FourierShift(n,sh)
% Compute the complex exponential for shifting an image.  n is the size of
% the image, and sh =[dx dy] are the shifts in pixels. Positive values are
% shifts up and to the right.  In the returned matrix, P(1,1) is zero
% frequency.
%
% e.g. to shift by dx, dy pixels,
%   fm=fftn(m);
%   P=FourierShift(size(m,1),[dx dy]);
%   fsh=real(ifftn(fm.*P));
% or, in one line,
%   fsh=real(ifftn(fftn(m).*FourierShift(size(m,1),[dx dy])));
% 
if(prod(size(n)) < 2)
  n = [n 1];
end
if(prod(size(n)) < 3)
  n = [n 1];
end

b = zeros(3,2);
for dim=1:3
  if(n(dim) == 1)
    b(dim,1) = 1;
    b(dim,2) = 1;
  else
    b(dim,1) = -n(dim)/2;
    b(dim,2) = n(dim)/2 - 1;
  end
end

[X,Y,Z]=ndgrid(b(1,1):b(1,2),b(2,1):b(2,2),b(3,1):b(3,2));
P=exp((-1j*2*pi)*ifftshift((sh(1)*X/n(1)+sh(2)*Y/n(2))+sh(3)*Z/n(3)));
