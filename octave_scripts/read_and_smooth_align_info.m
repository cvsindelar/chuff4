function [num_pfs,num_starts,est_repeat_distance,ref_pixel,...
          phi,theta,psi,coords, ...
          d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
            read_and_smooth_align_info(job_dir)

smoothing_hwidth = 5; % Halfwidth for smoothing euler angles

run 'chuff_parameters.m'

pixel_size = 10000*scanner_pixel_size/target_magnification;

box_doc_name = sprintf('scans/%s.box',job_dir);
ref_params_name = 'chumps_round1/ref_tub_subunit/ref_params.spi';
com_doc_name = 'chumps_round1/ref_tub_subunit/tub_cgr_doc.spi';
ctf_doc_name = 'ctf_files/ctf_docfile_for_spider.spi';
mt_info_name = sprintf('chumps_round1/%s/selected_mt_type.txt',job_dir));

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

box_doc = dlmread(box_doc_name);
mt_info = dlmread(mt_info_name);
num_pfs = mt_info(1);
num_tub_starts = mt_info(2);
num_starts = num_tub_starts/2; % All functions require the "true" number of helical starts
                               %  (can be non-integral for tubulin)

radon_scale = dlmread(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));

align_doc_name = ...
  sprintf('chumps_round1/%s/individual_align_doc_pf%d_start%d.spi',...
          job_dir,num_pfs,num_tub_starts);

align_doc = dlmread(align_doc_name);

good_align_doc_name = ...
  sprintf('chumps_round1/%s/guess_individual_align_doc_pf%d_start%d_good.spi',...
          job_dir,num_pfs,num_tub_starts);

good_align_doc = dlmread(good_align_doc_name);

com_info = dlmread(com_doc_name);
pixel_info = dlmread(ref_params_name);

index = find(pixel_info(:,1) == 2);
ref_pixel = pixel_info(index(1),5);

index = find(pixel_info(:,1) == 1);
min_theta = pixel_info(index(1),5);

index = find(pixel_info(:,1) == 3);
d_angle = pixel_info(index(1),3);

index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

index = find(radon_scale(:,1) == 1);
radon_scale = radon_scale(index(prod(size(index))),3);

bin_factor = round(ref_pixel/pixel_size);
final_pixel_size = scanner_pixel_size*10000/target_magnification*bin_factor;

%%%%%%%%%%%%%%%%%%
% Initialize file-related parameters
%%%%%%%%%%%%%%%%%%

n_good_boxes = size(good_align_doc);
n_good_boxes = n_good_boxes(1);

fprintf(stdout,'%s %4d\n', job_dir, n_good_boxes);

%%%%%%%%%%%%%%%
% Read in particle coordinates
%  and estimate the true repeat spacing
%%%%%%%%%%%%%%%

tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

for j=1:n_good_boxes
  eulers = good_align_doc(j,3:5);
  box_num = good_align_doc(j,1);

  index = find(align_doc(:,1) == box_num);
  index = index(prod(size(index)));
  extra_inplane_rot = align_doc(index,3);
  coords(j,:) = align_doc(index,12:13);
%  coords(j,:) = [box_doc(box_num,1) box_doc(box_num,2)] + floor(box_doc(box_num,3)/2);
  shifts = align_doc(index,5:6);

  r = extra_inplane_rot*pi/180;

  shifts = [cos(r)*shifts(1)-sin(r)*shifts(2) ...
            sin(r)*shifts(1)+cos(r)*shifts(2)];

  psi(j) = eulers(1) - extra_inplane_rot;
  theta(j) = eulers(2);
  phi(j) = eulers(3);

  tilt_scale_avg = tilt_scale_avg + 1/sin(theta(j)*pi/180);
  n_tilt_scale_avg = n_tilt_scale_avg + 1;

  coords(j,:) = coords(j,:) - shifts;
  if(j > 1)
    dist_to_last(j) = norm(coords(j,:) - coords(j-1,:));
  end
end

%%%%%%%%%%%%%%%%%%%%%
% Regularize the spacing between repeats
%  and estimate psi from coordinates
%%%%%%%%%%%%%%%%%%%%%

tilt_scale_avg = tilt_scale_avg/n_tilt_scale_avg;
est_repeat_distance = helical_repeat_distance/(radon_scale*tilt_scale_avg);
pix_per_repeat_from_radon = est_repeat_distance/micrograph_pixel_size;

psi_est = zeros(n_good_boxes);

j=2;
while(j <= n_good_boxes)
  vec_to_last = coords(j,:) - coords(j-1,:);
  psi_est(j) = -180/pi*atan2(vec_to_last(2),vec_to_last(1));

  dist_to_last(j) = norm(vec_to_last);
  tub_monomer_spacing = round(2*dist_to_last/pix_per_repeat_from_radon);

  if(tub_monomer_spacing(j) ~= 2)
    if(tub_monomer_spacing(j) != 0)
      coords(j,:) = coords(j-1,:) + (coords(j,:)-coords(j-1,:))*2/tub_monomer_spacing(j);
    else
%%%%%%%%%%%%%%%%%%%%
% If there is no distance change, get rid of the current box and shift all information
%  to the left.
%%%%%%%%%%%%%%%%%%%%

      n_good_boxes = n_good_boxes - 1;
      if(j < n_good_boxes)
        coords(j:n_good_boxes-1,:) = coords(j+1:n_good_boxes,:);
        phi(j:n_good_boxes-1) = phi(j+1:n_good_boxes);
        theta(j:n_good_boxes-1) = theta(j+1:n_good_boxes);;
        psi(j:n_good_boxes-1) = psi(j+1:n_good_boxes);
        psi_est(j:n_good_boxes-1) = coords(j+1:n_good_boxes);
      end
      coords = coords(1:n_good_boxes,:);
      phi = phi(1:n_good_boxes);
      theta = theta(1:n_good_boxes);
      psi = psi(1:n_good_boxes);
      psi_est = psi_est(1:n_good_boxes);
      j=j-1;
    end
  end
  dist_to_last(j) = norm(coords(j,:) - coords(j-1,:));
  j = j + 1;
end

pix_per_repeat_from_params = helical_repeat_distance/micrograph_pixel_size
pix_per_repeat_from_coords = sum(dist_to_last(2:prod(size(dist_to_last))))/(prod(size(dist_to_last))-1)
pix_per_repeat_from_radon

%%%%%%%%%%%%%%%%%%%%%
% Smooth the Euler angles
%%%%%%%%%%%%%%%%%%%%%

psi_est(1) = psi_est(2);
psi_est_temp = psi_est;

for j=1:n_good_boxes
  cur_phi_est = 0;
  cur_theta_est = 0;
  cur_psi_est = 0;
  count = 0;

  for k=-smoothing_hwidth:smoothing_hwidth
    if(j+k-1 >= 1 && j+k <= n_good_boxes)
      cur_phi_est = cur_phi_est + phi(j+k);
      cur_theta_est = cur_theta_est + theta(j+k);
      cur_psi_est = cur_psi_est + psi_est_temp(j+k);
      count = count + 1;
    end
  end
  phi_est(j) = cur_phi_est/count;
  theta_est(j) = cur_theta_est/count;
  psi_est(j) = cur_psi_est/count;
end

%%%%%%%%%%%%%%%%%%%%%
% Estimate the Euler angle derivatives
%%%%%%%%%%%%%%%%%%%%%

d_phi_d_repeat = zeros([1 n_good_boxes]);
d_theta_d_repeat = zeros([1 n_good_boxes]);
d_psi_d_repeat = zeros([1 n_good_boxes]);
for j=1:n_good_boxes
  d_phi_d_repeat(j) = 0;
  d_theta_d_repeat(j) = 0;
  d_psi_d_repeat(j) = 0;
  count = 0;
  for k=-smoothing_hwidth:smoothing_hwidth
    if(j+k-1 >= 1 && j+k <= n_good_boxes)
      d_phi_d_repeat(j) = d_phi_d_repeat(j)+ phi_est(j+k)-phi_est(j+k-1);
      d_theta_d_repeat(j) = d_theta_d_repeat(j)+ theta_est(j+k)-theta_est(j+k-1);
      d_psi_d_repeat(j) = d_psi_d_repeat(j)+ psi_est(j+k)-psi_est(j+k-1);
      count = count + 1;
    end
  end
  d_phi_d_repeat(j) = d_phi_d_repeat(j)/count;
  d_theta_d_repeat(j) = d_theta_d_repeat(j)/count;
  d_psi_d_repeat(j) = d_psi_d_repeat(j)/count;
end

%%%%%%%%%%%%%%%
% Convert derivatives to units of distance
%%%%%%%%%%%%%%%

d_phi_d_repeat = d_phi_d_repeat/helical_repeat_distance;
d_theta_d_repeat = d_theta_d_repeat/helical_repeat_distance;
d_psi_d_repeat = d_psi_d_repeat/helical_repeat_distance;

%%%%%%%%%%%%%%%
% If psi is 90 (rather than 270), the MT segment points backwards relative to the 
%  path of the boxes...
%%%%%%%%%%%%%%%

if(eulers(1) == 90)
  d_phi_d_repeat = -d_phi_d_repeat;
  d_theta_d_repeat = -d_theta_d_repeat;
  d_psi_d_repeat = -d_psi_d_repeat;
end
