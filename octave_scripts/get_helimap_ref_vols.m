function vol_exist = ...
    get_helimap_ref_vols(run_dir, helimap_round);

%%%%%%%%%%%%%%%%%%
% Get reference info
%%%%%%%%%%%%%%%%%%
global subunit_array debug test_mode;

n_states = 0;
subunit_dim = 0;

[helimap_dir, reference_dir] = ...
  save_helimap_alignment(run_dir, helimap_round);

finished = 0;
while(!finished)
  if(helimap_round == 1 && ...
     (isequal(test_mode,'perfect') || isequal(test_mode,'perfect_nosigma2')))
    vol_name = sprintf('%s/ref_helimap_class%d_gold1_perfect.spi', ...
                       reference_dir, n_states + 1)
  else
    vol_name = sprintf('%s/ref_helimap_class%d_gold1.spi', reference_dir, n_states + 1)
  end

  if(exist(vol_name) == 2)
    n_states = n_states + 1
    [h, s1] = readSPIDERheader(vol_name);
    subunit_dim = max([subunit_dim s1])
  else
    finished = 1;
  end
end
n_states

if(n_states < 1)

  vol_exist = 0;
  return

else

  vol_exist = 1;

  subunit_dim = [subunit_dim(1) subunit_dim(1) subunit_dim(1)];

  subunit_array = ...
    zeros([subunit_dim(1) subunit_dim(1) subunit_dim(1) n_states]);

  for igold=1:2
  for ind=1:n_states
    if(helimap_round == 1 && ...
       (isequal(test_mode,'perfect') || isequal(test_mode,'perfect_nosigma2')))
      printf('Loading perfect references...\n');
      vol_name = sprintf('%s/ref_helimap_class%d_gold%d_perfect.spi', reference_dir, ind, igold);
    else
      vol_name = sprintf('%s/ref_helimap_class%d_gold%d.spi', reference_dir, ind, igold);
    end
    subunit_array(:,:,:,ind,igold) = readSPIDERfile(vol_name);
  end
  end
end
