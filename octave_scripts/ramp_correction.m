function corrected_image = ramp_correction(input_image)

sz = size(input_image);
[x y] = ndgrid(1:sz(1));

mat=[ mean(x(:).^2) mean(x(:).*y(:)) mean(x(:)) ;
          mean(x(:).*y(:)) mean(y(:).^2) mean(y(:)) ;
          mean(x(:)) mean(y(:)) 1 ];

vec= [ mean(x(:).*input_image(:))
          mean(y(:).*input_image(:))
          mean(input_image(:)) ];

res=(inv(mat)*vec);
a=res(1);
b=res(2);
c=res(3);

z = a*x + b*y + c;

corrected_image = input_image - z;

