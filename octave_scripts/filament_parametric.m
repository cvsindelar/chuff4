%function [coords, ang, coords_sub, ang_sub, origin_repeat] =...
function [x_sub y_sub z_sub phi_sub theta_sub psi_sub...
        x y z phi theta psi origin_repeat] = ...
        filament_parametric(num_repeats, ref_com, helical_twist, helical_rise, phi_params, theta_params, psi_params)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Generate filament subunits center coordinates along z axis by providing one subunit coordinate and helical parameters.
% 
% function [coords, ang, coords_sub, ang_sub, origin_repeat] =...
%         filament_parametric(num_repeats,ref_com, helical_twist, helical_rise, phi_params, theta_params, psi_params)
% num_repeats   : number of repeat totally generated.In num_repeats(2), the user can optionally specify  which repeat will contain the origin where z = 0
% ref_com       : the center of the provided subunit. [x y z]
% helical_twist : helical parameters for filament
% helical_rise  : helical parameters for filament
% phi_params    : the phi value and phi amplitude for randomization
% theta_params  : the theta value and theta amplitude for randomization
% psi_params    : the psi value and psi amplitude for randomization
%
% all calculated with radian.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(prod(size(num_repeats)) < 2)
  origin_repeat = floor(num_repeats/2) + 1;
else
  origin_repeat = num_repeats(2); % In num_repeats(2), the user can optionally specify 
end                               %  which repeat will contain the origin where z = 0

phi_val = phi_params(1);
theta_val = theta_params(1);
psi_val = psi_params(1);
phi_amplitude = phi_params(2);
num_repeats = num_repeats(1);
radius = sqrt(ref_com(1)^2 + ref_com(2)^2);
phi_ref = ref_com(1) / radius;
phi_ref = acosd(phi_ref);
phi_ref
if ref_com(2) < 0
    phi_ref = 360 - phi_ref;
end
phis = (1 : num_repeats)';
phis = phis - origin_repeat;
z = phis * helical_rise + ref_com(3);

% from here, phi is spider convention
phis = -phis * helical_twist + (rand(size(phis)) - 0.5) * phi_amplitude;


% basically phis here is how much angle phi the new subunit is from the reference subunit. This is not the actual position in the 2D plane relative to x axis
phis = mod(phis, 360);
x = cosd((-phis + phi_ref)) * radius;
y = sind((-phis + phi_ref)) * radius;
coords_sub_ori = [x y z];
coords_fila_ori = [zeros([num_repeats 1]) zeros([num_repeats 1]) z - ref_com(3)];
% till here, x, y, z is the coordinate of the center of subunits in unrotated volume. We will output the position for subunit x_sub, y_sub, z_sub in rotated coordinate.

pi2 = 180 / pi;
euler_matrix = EulerMatrix([phi_val / pi2, theta_val / pi2, psi_val / pi2]);
coords_sub = (euler_matrix * coords_sub_ori')';
coords = (euler_matrix * coords_fila_ori')';
phis += phi_val;
phis_sub = phis;
thetas = ones(size(phis)) * theta_val ;
psis = ones(size(phis)) * psi_val;
ang = [phis thetas psis] / pi2;
ang_sub = [phis_sub thetas psis] / pi2;
x = coords(:,1)';
y = coords(:,2)';
z = coords(:,3)';
x_sub = coords_sub(:, 1)';
y_sub = coords_sub(:, 2)';
z_sub = coords_sub(:, 3)';
phi = ang(:, 1)';
theta = ang(:, 2)';
psi = ang(:, 3)';
phi_sub = ang_sub(:, 1)';
theta_sub = ang_sub(:, 2)';
psi_sub = ang_sub(:, 3)';
