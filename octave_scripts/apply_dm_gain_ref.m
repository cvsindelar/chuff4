function apply_dm_gain_ref(mic, output_mic, gainref, mask_file, pix_size,zbin,mask_edge_width, dmrot);

if(nargin < 8)
  dmrot = -1;
end

s = ReadMRCheader(mic);
if(prod(size(s)) == 2)
  s = [s(1) s(2) 1];
end

if(strcmp(gainref,'null') != 1)
  s2 = ReadMRCheader(gainref);
  if(dmrot < 0)
    if( (s(1) > s(2) && s2(1) < s2(2)) || (s(1) < s(2) && s2(1) > s2(2)))
      dmrot = 1;
    else
      dmrot = 0;
    end
  end

  if(dmrot)
    gainref = double(ReadMRC(gainref)');
  else
    gainref = double(ReadMRC(gainref));
  end
  gainref = gainref(1:s(1),1:s(2));

  m = double(ReadMRC(mic));
  for i=1:s(3)
    m(:,:,i) = gainref.*m(:,:,i);
  end
else
  m = double(ReadMRC(mic));
end

if(strcmp(mask_file,'null') != 1)
  mask = double(readSPIDERfile(mask_file));
  bg = sum(m,3);
  if(prod(size(find(mask == 0))) < 100)
    bg = sum(bg(:))/ prod(s);
  else
    bg = sum(bg(find(mask == 0)))/ (s(3)*prod(size(find(mask == 0))));
  end
  bg_sum = sum(sum(bg));

%  lp_freq = 0.5*(2*pix_size) / mask_edge_width;
%  delta_lp_freq = lp_freq/10;
%'filtering...'
%  bg = SharpFilt(bg,lp_freq,delta_lp_freq);
%'...done'

  for i=1:s(3)
    bg_single = m(:,:,i);
    if(prod(size(find(mask == 0))) < 100)
      bg_single = sum(bg_single(:))/ prod(s(1:2));
    else
      bg_single = sum(bg_single(find(mask == 0)))/ (prod(size(find(mask == 0))));
    end
    m(:,:,i) = mask.*m(:,:,i) + (1-mask).*bg * bg_single/bg_sum;
  end
end

for i=1:zbin:s(3)
  maxj = i+zbin-1;
  if(maxj > s(3))
    maxj = s(3);
  end
  for j=i+1:maxj
    m(:,:,i) = m(:,:,i) + m(:,:,j);
  end
end

m = m(:,:,1:zbin:s(3));

% m = m / std(m(:)) * 65536/4;

WriteMRC(m, pix_size, output_mic);
