function chumps_squashy_refine(job_dir,focal_mate,filter_resolution,use_ctf,chumps_round,bin_factor,...
                 tubrefs)

global box mask box_num ref_dim num_repeats num_pfs num_starts ...
       psi d_phi_d_repeat d_theta_d_repeat d_psi_d_repeat ...
       est_repeat_distance radius_scale_factor ...
       mytubrefs ctf ctf_flip filter ...
       ref_pixel min_theta d_angle ref_com

n_normal_args = 6;

job_dir = trim_dir(job_dir);

mkdir(sprintf('chumps_round%d/%s/squashy/',chumps_round,job_dir));
mkdir(sprintf('chumps_round%d/%s/squashy_fmate/',chumps_round,job_dir));

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m'

highpass_resolution = 200;

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

[micrograph,defocus,astig_mag,astig_angle,b_factor] = ...
  chumps_micrograph_info(job_dir,focal_mate);

[num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
  est_repeat_distance] = ...
  read_chumps_alignment(job_dir,helical_repeat_distance,1,micrograph_pixel_size);

est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

[coords,phi,theta,psi,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
  fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,
                         est_pix_per_repeat);

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
tub_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);

ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,bin_factor);
kin_ref_file = sprintf('%s/ref_tot.spi',ref_dir);

n_kins_to_find = 2*num_pfs; % 6*num_pfs;
max_overlap = 2*num_pfs;

num_repeats = ceil( (n_kins_to_find+2*max_overlap)/num_pfs );
%num_repeats = floor(num_repeats/2)
kin_lib_size = 2*num_repeats*num_pfs;

radius_scale_factor = 1;

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

if(focal_mate == 0)
  output_dir = sprintf('chumps_round1/%s/squashy',job_dir);
  micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
else
  output_dir = sprintf('chumps_round1/%s/squashy_fmate',job_dir);
  micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
end

overlap_file = sprintf('%s/overlap_map.spi',output_dir);

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

com_info = dlmread(com_doc_name);

%%%%%%%%%%%%%%%%%%
% Initialize file-related parameters
%%%%%%%%%%%%%%%%%%

n_good_boxes = prod(size(phi));

fprintf(stdout,'%s %4d\n', job_dir, n_good_boxes);

ref_pixel_info = dlmread(ref_params_name);

index = find(ref_pixel_info(:,1) == 2);
ref_pixel = ref_pixel_info(index(1),5);

index = find(ref_pixel_info(:,1) == 1);
min_theta = ref_pixel_info(index(1),5);

index = find(ref_pixel_info(:,1) == 3);
d_angle = ref_pixel_info(index(1),3);

index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

bin_factor = round(ref_pixel/micrograph_pixel_size);
final_pixel_size = micrograph_pixel_size*bin_factor;

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
box_dim = 64*ceil(box_dim/64);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);
pad_ref_dim = 2*ref_dim;
% pad_ref_dim = ref_dim;

mask = fuzzymask(ref_dim(1),2,0.8*ref_dim/2,50/ref_pixel);

%%%%%%%%%%%%%%%%%%%%%%%
% Compute CTF's
%%%%%%%%%%%%%%%%%%%%%%%

electron_lambda = EWavelength(accelerating_voltage);

if(defocus > 0 && use_ctf != 0)
  ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                       spherical_aberration_constant, ...
                       b_factor, amplitude_contrast_ratio, 0);
  ctf = ifftshift(ctf);

  ctf_flip = sign(CTF_ACR(ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                       spherical_aberration_constant, ...
                       b_factor, amplitude_contrast_ratio, 0));
  ctf_flip = ifftshift(ctf_flip);

  box_ctf = CTF_ACR(box_dim(1), micrograph_pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  box_ctf = ifftshift(box_ctf);
else
  ctf = -ones(pad_ref_dim);
  ctf_flip = -ones(ref_dim);
  box_ctf = ones(box_dim);
end

box_ctf_flip = sign(box_ctf);
%ctf_flip = ones(pad_ref_dim);
%box_ctf_flip = ones(box_dim);

%%%%%%%%%%%%%%%
% Pre-compute filtering terms
%%%%%%%%%%%%%%%

filter = ifftn(ones(pad_ref_dim));
lp_freq = 0.5*(2*ref_pixel) / filter_resolution;
delta_lp_freq = lp_freq/10;
filter = SharpFilt(filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*ref_pixel) / highpass_resolution;
delta_hp_freq = hp_freq/10;
filter = SharpHP(filter,hp_freq,delta_hp_freq);

filter = fftn(filter);

box_filter = ifftn(ones(2*box_dim));
lp_freq = 0.5*(2*micrograph_pixel_size) / filter_resolution;
delta_lp_freq = lp_freq/10;
box_filter = SharpFilt(box_filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*micrograph_pixel_size) / highpass_resolution;
delta_hp_freq = hp_freq/10;
box_filter = SharpHP(box_filter,hp_freq,delta_hp_freq);

box_filter = fftn(box_filter);

%%%%%%%%%%%%%%%%%%%%%%%
% To save time, can pre-load the reference images
%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < n_normal_args+1)
  mytubrefs = readSPIDERfile(tub_ref_file);
else
  mytubrefs = tubrefs;
end

mic_dim = ReadMRCheader(micrograph);

%%%%%%%%%%%%%%%%%%
% Count how many "good" boxes are fully inside the micrograph
%%%%%%%%%%%%%%%%%%

real_n_good_boxes = 0;
for box_num=1:n_good_boxes
  int_coords = round(coords(box_num,:));
  box_origin = int_coords-floor(box_dim/2);
  if(box_origin(1) < 1 || box_origin(2) < 1 || ...
     box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
    continue
  end

  real_n_good_boxes = real_n_good_boxes + 1;
end

%%%%%%%%%%%%%%%%%%%%%%%
% Finally- the refinement loop
%%%%%%%%%%%%%%%%%%%%%%%

results = zeros(real_n_good_boxes,7);

real_box_num = 0;
for box_num= 1:n_good_boxes

  int_coords = round(coords(box_num,:));
  box_origin = int_coords-floor(box_dim/2);
  if(box_origin(1) < 1 || box_origin(2) < 1 || ...
     box_origin(1) + box_dim - 1 > mic_dim(1) || box_origin(2) + box_dim - 1 > mic_dim(2))
    continue
  end
  real_box_num = real_box_num + 1;

  fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);

  box = read_chumps_box(micrograph,box_num,coords,box_dim,...
                        bin_factor,box_filter);
  box = invert_density * box;
  real_box_num = real_box_num + 1;

squashy_eval([phi(box_num) theta(box_num) 0 0 0.08 23])
exit(2)

%%%%%%%%%%%%%%%%%%
% Downhill simplex search on MT parameters
%%%%%%%%%%%%%%%%%%

  options.maxRestarts = 1; % maximum (probablistic or degenerated) restarts
  options.maxEvals = 30; % maximum function evaluations
  options.nPoints = 1; % number of random points per restart

% Nelder-mead parameters
  options.maxIter=20; % maximum iterations per restart
  options.alpha = 1; % reflection coeff
  options.beta = 0.5; % contraction coeff
  options.gamma = 2; % expansion coeff
  options.epsilon = 1e-9; % T2 convergence test coefficient
  options.ssigma = 5e-4; % small simplex convergence test coefficient

  [init_best_ccc,init_ref_tub] = squashy_eval([phi(box_num); theta(box_num); 0; 0; 1]);
  outfile = sprintf('%s/box_%04d.spi',output_dir,box_num);
  writeSPIDERfile(outfile,ifftn(fftn(box).*ctf_flip));
  outfile = sprintf('%s/init_ref_%04d.spi',output_dir,box_num);
  writeSPIDERfile(outfile,ifftn(fftn(init_ref_tub).*ctf_flip).*mask);

  best_eulers = [phi(box_num); theta(box_num); psi(box_num); 0; 0; 1];
  best_ccc = -1;
  for delta_theta=-5:10:5

% Search on theta, phi only at first
    delta = [5; 5];
    xmin = [phi(box_num); 85] - delta;
    xmax = xmin + 2*delta;

    loop_xmin = xmin + [0; delta_theta];
    loop_xmax = xmax + [0; delta_theta];
    [found_eulers, found_score] = gbnm(@squashy_eval,loop_xmin(1:2),loop_xmax(1:2),options);
    found_eulers(3:4) = 0;
    found_ccc = -found_score;
    if(found_ccc > best_ccc)
      best_ccc = found_ccc;
      best_eulers = found_eulers;
    end

    fprintf(stdout,'dtheta %d : %6.2f %6.2f %6.2f %6.2f: %6.4f\n', ...
              delta_theta, found_eulers(1), found_eulers(2), found_eulers(3), found_eulers(4), found_ccc);
% Then search on the elastic parameters and phi, theta simultaneously
    delta = [1; 1; 0; 0];
    loop_xmin = found_eulers - delta;
    loop_xmax = found_eulers + delta;
    loop_xmin(3) = 0;
    loop_xmax(3) = 0.3;
    loop_xmin(4) = 0;
    loop_xmax(4) = 180;

    [found_eulers, found_score] = gbnm(@squashy_eval,loop_xmin(1:4),loop_xmax(1:4),options);
    found_ccc = -found_score;
    if(found_ccc > best_ccc)
      best_ccc = found_ccc;
      best_eulers = found_eulers;
    end
    fprintf(stdout,'dtheta %d : %6.2f %6.2f %6.2f %6.2f: %6.4f\n', ...
              delta_theta, found_eulers(1), found_eulers(2), found_eulers(3), found_eulers(4), found_ccc);
    fflush(stdout);
  end

  fprintf(stdout,'Best : %6.2f %6.2f %6.2f %6.2f : %6.4f\n', ...
            best_eulers(1), best_eulers(2), best_eulers(3), best_eulers(4), best_ccc);
  fflush(stdout);

  [found_best_ccc,found_ref_tub] = squashy_eval(best_eulers);

  outfile = sprintf('%s/found_ref_%04d.spi',output_dir,box_num);
  writeSPIDERfile(outfile,ifftn(fftn(found_ref_tub).*ctf_flip).*mask);

%%%%%%%%%%%%%%%%%%
% Write files
%%%%%%%%%%%%%%%%%%

  results(box_num,1) = box_num;
%  results(box_num,2:7) = best_eulers(:);
%  results(box_num,2:3) = best_eulers(:);
  results(box_num,2:5) = best_eulers(:);
  results(box_num,6) = best_ccc;

  outfile = sprintf('%s/squashy_output.txt',output_dir);
  dlmwrite(outfile,results, ' ');
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accessory functions to make the code more readable...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out_dir = trim_dir(job_dir)

if(job_dir(prod(size(job_dir))) == '/')
  job_dir=job_dir(1:prod(size(job_dir))-1);
end
[start] = regexp(job_dir,'[^/]*$','start');
out_dir = job_dir(start:prod(size(job_dir)));

return

function [ccc, ref_tub] = squashy_eval(x)

global box mask box_num ref_dim num_repeats num_pfs num_starts ...
       psi d_phi_d_repeat d_theta_d_repeat d_psi_d_repeat ...
       est_repeat_distance radius_scale_factor ...
       mytubrefs ctf ctf_flip filter ...
       ref_pixel min_theta d_angle ref_com;

squashy_phi = x(1);
squashy_theta = x(2);
% squashy_psi = x(3);
squashy_psi = psi(box_num);

if(prod(size(x)) >= 4)
  mode2_amp = x(3);
  mode2_dphi = x(4);
else
  mode2_amp = 0;
  mode2_dphi = 0;
end

est_radius_scale_factor = 1;

if(prod(size(x)) >= 6)
  mode3_amp = x(5);
  mode3_dphi = x(6);
else
  mode3_amp = 0;
  mode3_dphi = 0;
end

elastic_params = [mode2_amp mode2_dphi mode3_amp mode3_dphi];

%%%%%%%%%%%%%%%%%%
% Generate the reference images, padded 2x to ensure that 
%  generate_micrograph() has room to place all the subunits
%%%%%%%%%%%%%%%%%%

pad_ref_dim = size(ctf);

monomer_offset = 0;
select_pf = 0; # [num_pfs 1];

[x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
 x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist mt_repeat_index mt_pf_index] =...
   microtubule_parametric(2*num_repeats,num_pfs,num_starts,ref_com,...
                          est_repeat_distance,est_radius_scale_factor,monomer_offset,...
                          [squashy_phi d_phi_d_repeat(box_num)],...
                          [squashy_theta d_theta_d_repeat(box_num)],...
                          [squashy_psi d_psi_d_repeat(box_num)],elastic_params);

if(prod(size(find(theta_tub <= min_theta*pi/180))) != 0 || ...
   prod(size(find(theta_tub >= pi - min_theta*pi/180))) != 0)
  ccc = 1.1;
  ref_tub = zeros(ref_dim);
  return;
end

ref_tub = generate_micrograph(pad_ref_dim, mytubrefs, ref_pixel, min_theta, d_angle, select_pf,...
                          x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                          [0 0]);

ref_tub = real(ifftn(fftn(ref_tub) .* ctf .* filter));
avg = mean(ref_tub(:));
ref_tub = (ref_tub - avg);

%%%%%%%%%%%%%%%%%%
% Window out the final-sized reference images, after applying CTF
%%%%%%%%%%%%%%%%%%

in_x = 1+floor(pad_ref_dim(1)/2)-floor(ref_dim(1)/2);
in_y = 1+floor(pad_ref_dim(2)/2)-floor(ref_dim(2)/2);

ref_tub = ref_tub(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
writeSPIDERfile('ref_tub.spi',ref_tub);

ccc = -corr2(ref_tub.*mask,box);
fprintf(stdout,'%6.2f %6.2f %6.2f %6.4f %6.1f %6.4f: %6.4f\n', squashy_phi, squashy_theta, squashy_psi, mode2_amp, mode2_dphi, est_radius_scale_factor, ccc);
fflush(stdout)
return
