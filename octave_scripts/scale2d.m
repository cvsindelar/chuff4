function out = scale2d(inp,factor)

s = size(inp);
s_final = factor*s;

pow2dim = max(2.^ceil(log2(s)));

if(pow2dim != s(1) || pow2dim != s(2))
  m = ceil(pow2dim/s(1));
  n = ceil(pow2dim/s(2));
  out = repmat(inp, m, n);
  s2 = size(out);
  out = circshift(out,floor(s2/2) - floor(s/2));

  orig = 1 + floor(s2/2) - pow2dim/2;
  out = out(orig(1):orig(1)+pow2dim-1,orig(2):orig(2)+pow2dim-1);

  out = dctresamp2d(out,factor);

  s_out = size(out);
  orig = 1 + s_out/2 - floor(s_final/2);
  out = out(orig(1):orig(1)+s_final(1)-1,orig(2):orig(2)+s_final(2)-1);
else
  out = dctresamp2d(inp,factor);
end
