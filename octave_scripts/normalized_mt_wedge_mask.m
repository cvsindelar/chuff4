function [wedge, adjusted_ref_com, adjusted_eulers, wedge_xform] = ...
   normalized_mt_wedge_mask(ref_dim, num_pfs,num_starts,ref_com,...
                            est_repeat_distance_13pf,final_pixel_size,min_resolution, ...
                            outer_radius_13pf);

pad_factor = 1;

mt_radius_13pf = 120;
outer_radius = abs(outer_radius_13pf - mt_radius_13pf) + ...
                num_pfs/13 * mt_radius_13pf
outer_radius_pix = outer_radius/final_pixel_size

if(outer_radius_pix - sqrt(sum(ref_com(:).^2))) > floor(ref_dim/2)-4
'yay'
  outer_radius_pix = sqrt(sum(ref_com(:).^2)) + floor(ref_dim/2)-4
end

if(nargin < 7)
  min_resolution = 50;
end

generous_repeats = 4;
generous_origin = 1 + floor(generous_repeats/2)*num_pfs;

elastic_params = [0 0 0 0];
radius_scale_factor = 1;
monomer_offset = 0;

%filter = ifftn(ones(pad_factor*[ref_dim ref_dim ref_dim]));
%lp_freq = 0.5/2; % min_resolution;
%delta_lp_freq = lp_freq/10;
%filter = fftn(SharpFilt(filter,lp_freq,delta_lp_freq));
filter = fftshift(spherical_cosmask(pad_factor*[ref_dim ref_dim ref_dim], 0, 4));
filter = fftn(filter) / sum(filter(:));

[x_final y_final z_final phi_final theta_final psi_final r_final ...
 x_mt y_mt z_mt] = ...
   microtubule_parametric([generous_repeats floor(generous_repeats/2)+1],...
                        num_pfs,num_starts,ref_com,...
                        est_repeat_distance_13pf,radius_scale_factor,monomer_offset,...
                        [0 0],...
                        [0 0],...
                        [0 0],elastic_params);

% min_i = min(find(z_mt > -est_repeat_distance * ...
%                      (est_repeat_distance + min_resolution)/est_repeat_distance))

% max_i = max(find(z_mt < est_repeat_distance * ...
%                      (est_repeat_distance + min_resolution)/est_repeat_distance))

rise_pix = sqrt( (x_mt(2)-x_mt(1))^2 + (y_mt(2)-y_mt(1))^2 + (z_mt(2)-z_mt(1))^2)/final_pixel_size;

adjusted_ref_com = [x_final(generous_origin) ...
                    y_final(generous_origin) ...
                    z_final(generous_origin)];
adjusted_eulers = [phi_final(generous_origin) ...
                   theta_final(generous_origin) ...
                   psi_final(generous_origin)];

twist = (psi_final(2) - psi_final(1)) * 180/pi;

pd_oxyz = 1 + floor(pad_factor*ref_dim/2) - floor(ref_dim/2);
pd_oxyz = pd_oxyz(1);

for i = generous_origin:generous_origin 
%for i=[generous_origin-4:generous_origin+4 generous_origin]     % min_i:max_i 

  subunit_num = 1 + i - generous_origin
  fflush(stdout);

%%%%%%%%%%%%%%
% Note that the requested wedge below is "hard"; we soften it subsequently with Fourier filtering
%  (this could be done in 2d, after projection, to save lots of time, but then 
%  the reconstruction itself will also be filtered)
%%%%%%%%%%%%%%

  temp =...
    soft_wedge_mask([ref_dim ref_dim ref_dim],subunit_num,num_pfs,num_starts,...
                           rise_pix,twist,...
                           0, outer_radius_pix, adjusted_ref_com/final_pixel_size);

  temp = real(ifftn(filter.*fftn(temp)));
  wedge = temp(pd_oxyz:pd_oxyz+ref_dim-1, pd_oxyz:pd_oxyz+ref_dim-1, pd_oxyz:pd_oxyz+ref_dim-1);
% writeSPIDERfile(sprintf('wedge%d_no_xform.spi',i),wedge_no_xform);

  if(nargout > 3)
    temp =...
      soft_wedge_mask([ref_dim ref_dim ref_dim],subunit_num,num_pfs,num_starts,...
                             rise_pix,twist,...
                             0, outer_radius_pix, adjusted_ref_com/final_pixel_size,...
                             -adjusted_ref_com/final_pixel_size,...
                             -[adjusted_eulers(3) adjusted_eulers(2) adjusted_eulers(1)] );

% writeSPIDERfile(sprintf('wedge%d.spi',i),temp);
    wedge_xform = real(ifftn(filter.*fftn(temp)));
% writeSPIDERfile(sprintf('wedge%d_filt.spi',i),wedge);
  end

end
