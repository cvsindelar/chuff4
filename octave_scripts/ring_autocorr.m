filename = 'snowflake.spi';
l = length(filename);
l = l(1);
output_image = sprintf('%s_strip.spi',filename(1:l-4));

image = readSPIDERfile(filename);

radius = size(image);
radius = floor(radius(1)/2);

strip_pixel_width = ceil(2*pi*radius);
delta_psi = 360/strip_pixel_width;

strip = zeros([strip_pixel_width 2*radius-1]);
writeSPIDERfile("strip.spi",strip);

for r=1:strip_pixel_width
  image2 = rotate2d(image, (r-1)*delta_psi);
#  writeSPIDERfile("debug.spi",image2);
  strip(r,1:radius) = (radius:-1:1)'.*squeeze(image2(1:radius,radius+1));
  r/strip_pixel_width
end

for r=1:radius-1
  strip(:,radius+r) = strip(:,radius+1-r);
end

writeSPIDERfile(output_image, strip);

