function [seam_map, seam_offset] = find_seams(sorted_map)

s = size(sorted_map);

seam_map = zeros(s);

for pf = 1:s(1)
  if(sum(sorted_map(pf,1:2:s(2))) > sum(sorted_map(pf,2:2:s(2))))
    seam_map(pf,1:2:s(2)) = 1;
  else
    seam_map(pf,2:2:s(2)) = 1;
  end
end

best_sum = -1;
for seam_pos = 0:s(1)-1
  shift_map = sorted_map;
  shift_map(1:seam_pos,:) = circshift(shift_map(1:seam_pos,:), [0 1]);

  current_sum1 = sum(sum(shift_map(:,1:2:s(2))));
  current_sum2 = sum(sum(shift_map(:,2:2:s(2))));

  if(current_sum1 > best_sum)
    best_sum = current_sum1;
    seam_offset = seam_pos;
  end
  if(current_sum2 > best_sum)
    best_sum = current_sum2;
    seam_offset = seam_pos;
  end
end
