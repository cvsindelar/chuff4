function sort_helimap_vols(mask)

global vol_array numerator_array denominator_array subunit_array

vol_size = size(vol_array);
n_classes = vol_size(4);
vol_size = vol_size(1:3);

%%%%%%%%%%%%%%%%%%
% We match the classes in the gold1 calculation to the classes in the
%  gold2 calculation, since they will have been independently chosen/selected
%  and may not occur in the same order. We hope that
%  the same structural classes have emerged in both gold1 and gold2. However, if the
%  classes are different, the resolution will be underestimated- not a bad thing.
%
% A simple idea is to go down the list of 'n_c' correlation scores in reverse order:
% 1. start with the highest correlation score: this defines a pair of m1,n1_m1.
% Then, repeat for each next highest correlation score. Thus, for the next one:
%  2. if one or the other of the pair m,n_m is assigned already, skip it.
%  3. otherwise, assign the next to m2, n2_m2 etc.
%
% The matches thus obtained may not be 'optimal' in every sense, but will certainly be
%  good enough when the classes are sufficiently distinct from each other (which is the case
%  we care about!)
%%%%%%%%%%%%%%%%%%%

cc_matrix = zeros([n_classes n_classes]);

% load 'fsc_array.mat'

for iclass1 = 1:n_classes
  for iclass2 = 1:n_classes
    cc_matrix(iclass1, iclass2) = ...
      ccc(vol_array(:,:,:,iclass1,1).*mask, vol_array(:,:,:,iclass2,2).*mask);
  end
end

vol_array_swap = vol_array(:,:,:,:,2);

for iclass=1:n_classes
  [bestmatch1, bestmatch2] = find(cc_matrix == max(cc_matrix(:)));
  vol_match_table(bestmatch1) = bestmatch2;

  if(bestmatch1 != bestmatch2 && ...
     cc_matrix(bestmatch1, bestmatch2) >= -1 && ...
     cc_matrix(bestmatch2, bestmatch2) >= -1)
    vol_array(:,:,:,bestmatch1,2) = vol_array_swap(:,:,:,bestmatch2);
    vol_array(:,:,:,bestmatch2,2) = vol_array_swap(:,:,:,bestmatch1);
    fprintf(stdout, 'SWAP: %d for %d\n', bestmatch1, bestmatch2);
    fflush(stdout);
  end

  cc_matrix(bestmatch1, :) = -2;
  cc_matrix(:, bestmatch2) = -2;
end
