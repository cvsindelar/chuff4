function mic = generate_micrograph(mic_dim, refs, pixel_size, min_theta, d_angle, select_pf,...
                                   x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                                   extra_shift,occupancy, individual,progressbar)
% function mic = generate_micrograph(mic_dim, refs, pixel_size, min_theta, d_angle, select_pf,...
%                                    x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
%                                     extra_shift,occupancy,individual)
% Notes for using this script with SPIDER:
%  If you use SPIDER to generate the reference projections from a
%   PDB file fitted into a volume using, for example, Chimera:
%   then you MUST take care to UNDO the horrible transformation that SPIDER
%   applies to the PDB file in the 'cp from pdb' command.
%   The Euler transformation that works is phi = 0, theta = 180, psi = 90.
%   So the corresponding SPIDER repair would be:
% pb rt3
%   <pdb_file>.pdb
%   <pdb_file>_undo_spider_xform.pdb
%   0 180 90
%
%   You then would use the transformed pdb file in the 'cp from pdb'

if(nargin < 13)
  extra_shift = [0 0];
end

if(nargin < 14)
  occupancy = 1;
end

if(occupancy == 1)
  occupancy = ones(prod(size(x_tub)));
end

if(nargin < 15)
  individual = 0;
end

if(nargin < 16)
  progressbar = 0;
end

if(nargout > 0)

if(prod(size(select_pf)) == 1)
 select_pf(2) = 1;
end

mic_dim_full = mic_dim;
if(prod(size(mic_dim_full)) == 1)
 mic_dim_full(2) = mic_dim(1);
end

micrograph_origin = floor(mic_dim_full/2)+1;

ref_dim = size(refs);

if(individual == 0)
  mic = zeros([mic_dim_full(1) mic_dim_full(2)]);
else
  mic = zeros([mic_dim_full(1) mic_dim_full(2), prod(size(x_tub))]);
end

for i=1:prod(size(x_tub))

  if(floor(i/50)*50 == i && progressbar ~= 0)
    fprintf('Generating subunit no.: %5d of %5d\n',i, prod(size(x_tub)));
    if(isoctave)
      fflush(stdout);
    end
  end

%%%%%%%%%%%%%%%%
% Look up tubulin projection corresponding to Euler angles
%  NB: get_ref_index takes arguments in ***degrees***, not radians!
%%%%%%%%%%%%%%%%

  ref_index = get_ref_index(phi_tub(i)*180/pi,...
                            theta_tub(i)*180/pi,...
                            min_theta, d_angle);

  int_x_tub = floor(x_tub(i)/pixel_size);
  frac_x_tub = x_tub(i)/pixel_size - int_x_tub;
  int_y_tub = floor(y_tub(i)/pixel_size);
  frac_y_tub = y_tub(i)/pixel_size - int_y_tub;

  wi_ox = micrograph_origin(1) + int_x_tub - floor(ref_dim(1)/2);
  wi_oy = micrograph_origin(1) + int_y_tub - floor(ref_dim(2)/2);

  if(select_pf(1) == 0 || (select_pf(1) ~= 0 && mod(i,select_pf(1)) == select_pf(2)))
    if(occupancy(i) == 1)
%%%%%%%%%%%%%%%%
% NB: mrotshift does a COUNTERCLOCKWISE rotation, the opposite of 
%  what an Euler rotation by psi does (this would be clockwise)
%  Therefore we negate the in-plane rotation angle here.
%%%%%%%%%%%%%%%%

      proj = mrotshift(refs(:,:,ref_index),-psi_tub(i),...
                            [frac_x_tub frac_y_tub],...
                            [floor(ref_dim(1)/2)+1 floor(ref_dim(2)/2)+1]);

      proj = mrotshift(proj,0,...
                       extra_shift,...
                            [floor(ref_dim(1)/2)+1 floor(ref_dim(2)/2)+1]);

      wi_dx = ref_dim(1) - 1;
      wi_dy = ref_dim(2) - 1;
      proj_ox = 1;
      proj_oy = 1;
%%%%%%%%%
% Handle clipping if image falls partially or fully outside micrograph
%%%%%%%%%
      if(wi_ox < 1)
        wi_dx = (wi_ox + ref_dim(1) - 1) - 1;
        wi_ox = 1;
        proj_ox = 1 + ref_dim(1) - (wi_dx + 1);
      end
      if(wi_ox+wi_dx > mic_dim_full(1))
        wi_dx = mic_dim_full(1) - wi_ox;
      end

      if(wi_oy < 1)
        wi_dy = (wi_oy + ref_dim(2) - 1) - 1;
        wi_oy = 1;
        proj_oy = 1 + ref_dim(2) - (wi_dy + 1);
      end
      if(wi_oy+wi_dy > mic_dim_full(2))
        wi_dy = mic_dim_full(2) - wi_oy;
      end

%%%%%%%%
% Insert into micrograph
%%%%%%%%

      if( wi_dx > 1 && wi_dy > 1)
        if(individual == 0)
          mic(wi_ox:wi_ox+wi_dx,wi_oy:wi_oy+wi_dy) = ...
              mic(wi_ox:wi_ox+wi_dx,wi_oy:wi_oy+wi_dy) + ...
              proj(proj_ox:proj_ox+wi_dx,proj_oy:proj_oy+wi_dy);
        else
          mic(wi_ox:wi_ox+wi_dx,wi_oy:wi_oy+wi_dy, i) = ...
              proj(proj_ox:proj_ox+wi_dx,proj_oy:proj_oy+wi_dy);
        end
      end
    end
  end
end

end

return
