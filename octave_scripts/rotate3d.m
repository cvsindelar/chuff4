function vol2 = rotate3d(vol,eulers)
phi = eulers(1);
theta = -eulers(2);
psi = eulers(3);

s = size(vol);

if(mod(phi,2*pi) == 0)
  vol2 = vol;
else
  vol2 = zeros(s);
  for(i=1:s(3));
    vol2(:,:,i)=rotate2d(vol(:,:,i),phi);
  end;
end

if(mod(theta,2*pi) != 0)
  for(i=1:s(2));
    vol2(:,i,:)=rotate2d(squeeze(vol2(:,i,:)),theta);
  end;
end

if(mod(psi,2*pi) != 0)
  for(i=1:s(3));
    vol2(:,:,i)=rotate2d(vol2(:,:,i),psi);
  end;
end
