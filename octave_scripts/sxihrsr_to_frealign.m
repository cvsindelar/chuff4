function sxihrsr_to_frealign(align_info_file,frealign_info_file, output_parameter_file, ...
                             job_dir, box_file, apix, magnification, helical_twist, ...
                             input_bin_factor, frealign_bin_factor, output_bin_factor, frealix)

%%%%%%%%%%%%%%%%%%%%%
% Here, 'angle' is the angle that would rotate the filament segments so that 
%  they are approximately horizontal, running left to right 
%  (corresponding to a projection with Euler angles phi, theta ~= 90, psi ~= 0).
%
% Note that we add 90 to angle, to make the segments vertical, when exporting
%  to sxihrsr, but this gets undone by the euler angles found by sxihrsr
%  (psi ~= 90 or 270) if we are exporting to FREALIGN.
%%%%%%%%%%%%%%%%%%%%%

box_coords = dlmread(box_file);
size_box_coords = size(box_coords);
angle = box_coords(2:size_box_coords(1),6);

max_delta_subunits = 4;

%%%%%%%%%%%%%
% Read in sxihrsr alignment
%  Note that the shifts must be scaled by the ratio of binning factors.
%%%%%%%%%%%%%

align_info = dlmread(align_info_file);

size_align = size(align_info);

filament_number = align_info(1,6);
phi = align_info(:,1);
theta = align_info(:,2);
psi = align_info(:,3);
shifts = align_info(:,4:5) * input_bin_factor/output_bin_factor;

%%%%%%%%%%%%%
% Read in optional, additional frealign alignment
%%%%%%%%%%%%%

if(strcmp(frealign_info_file,'null') != 1)
  frealign_info = dlmread(frealign_info_file);

  size_frealign = size(frealign_info);
  if(size_frealign(1) != size_align(1))
    fprintf(stdout,'ERROR: #particles in frealign file differs from that in sxihrsr alignment... \n');
    return
  end

%  filament_number = align_info(1,6);
%%%%%%%%%%%%%%%%%
% Frealign phi, theta values replace the sxihrsr ones while the frealign psi gets added on.
%  Note that the frealign shifts are defined with the opposite sign convention as other shifts
%%%%%%%%%%%%%%%%%
  phi = frealign_info(:,1);
  theta = frealign_info(:,2);
  frealign_psi = frealign_info(:,3);
  frealign_shifts = -frealign_info(:,4:5) * frealign_bin_factor/output_bin_factor;
else
  frealign_psi = zeros([size_align(1) 1]);
  frealign_shifts = zeros([size_align(1) 2]);
end

%%%%%%%%%%%%%%%%%%%%%
% Find the current particle number from the output parameter file
%%%%%%%%%%%%%%%%%%%%%
if(prod(size(stat(output_parameter_file))) != 0)
  old_params = dlmread(output_parameter_file);
  size_fp = size(old_params);
  tot_n_particles = old_params(size_fp(1),1);

  fid = fopen(output_parameter_file,'a');
else
  fid = fopen(output_parameter_file,'w');
  tot_n_particles = 0;
endif

%%%%%%%%%%%%%%%%%%%%%
% Read box coordinates
%%%%%%%%%%%%%%%%%%%%%
box_coords = dlmread(sprintf('%s.box',job_dir));

%%%%%%%%%%%%%%%%%%%%%
% Get micrograph number
%%%%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'[1-9]+','start');
[last] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
micrograph_number = str2double(sprintf('%s',job_dir(start:last-1)));

%%%%%%%%%%%%%%%%%%%%%
% Read CTF info
%%%%%%%%%%%%%%%%%%%%%
[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
ctf_doc = dlmread(sprintf('%s_ctf_doc.spi',job_dir(1:start-1)));
index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end
index = index(prod(size(index)));
defocus1 = ctf_doc(index,3);
defocus2 = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);

if(frealix == 0)
  for i=1:size_align(1)
    tot_n_particles = tot_n_particles + 1;

%%%%%%%%%%%%
% Astigmatism angle should be rotated in the opposite direction as 
%  whatever we did with rotate2d in chuff_extract_boxes 
%  (see chuff3/info/rotation_sign_conventions.txt)
%%%%%%%%%%%%

      adjusted_astig_angle = astig_angle -(angle(i)+90-psi(i));
      presa = 50.0;
      dpres = 50.0;
      fprintf(fid, ...
           '%7d%8.2f%8.2f%8.2f%8.2f%8.2f%8.0f%6d%9.1f%9.1f%8.2f%7.2f%6.2f\n', ...
           tot_n_particles, 0, theta(i), phi(i), 0, 0, ...
           magnification/output_bin_factor, filament_number, ...
           defocus1, defocus2, adjusted_astig_angle, presa, dpres);
  end

else

  adjusted_psi = zeros([size_align(1) 1]);
  x = zeros([size_align(1) 1]);
  y = zeros([size_align(1) 1]);
  delta_repeat = zeros([size_align(1) 1]);
  phi_dev_err = zeros([2*max_delta_subunits+1 1]);

  for i=1:size_align(1)

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Using sxihrsr, we have found psi angles for particles that have been pre-rotated
%  clockwise by angle+90. This increases the effective psi in the particle images, 
%  because positive psi corresponds to clockwise rotation.  
%  For example, if we were viewing a particle whose psi was actually 23, 
%  the pre-rotation yields a final psi of 23+angle+90.
% Thus we need to subtract angle+90 from the psi values
%  reported by sxihrsr, for the transformation of the originally boxed 
%   (non-transformed) particles.
%%%%%%%%%%%%%%%%%%%%%%%%%%%
      adjusted_psi(i) = frealign_psi(i) + psi(i)-(angle(i)+90);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For frealix, we require parameters for the original, untransformed boxes from the micrographs.
% 
% Using sxihrsr, we have discovered shifts that center the particles 
%  following a rotation by angle+90 *clockwise* when viewing the x,y axes
%  in "math" view (i.e. with the origin in the lower left). 
%
% To determine shifts to apply in the absence of this rotation, we therefore
%  map the transformed coordinate system of the applied shifts (after rotation)
%  back to the unrotated coordinate system. The mapping gives x,y axes that 
%  are rotated *counterclockwise* by angle+90 (in math view). 
% Thus, the shifts are modified by a rotation matrix corresponding to this counterclockwise
%  rotation, which is [cos(angle+90) -sin(angle+90); sin(angle+90) cos(angle+90)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%
      r = (angle(i)+90)*pi/180;

      shift_x = cos(r)*shifts(i,1)-sin(r)*shifts(i,2);
      shift_y = sin(r)*shifts(i,1)+cos(r)*shifts(i,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Similar logic applies for frealign parameters, except that the image was rotated further 
%  (by -psi(i)) before frealign analyzed it.
%%%%%%%%%%%%%%%%%%%%%%%%%%%
      r = (angle(i)+90-psi(i))*pi/180;

      shift_x = shift_x + cos(r)*frealign_shifts(i,1)-sin(r)*frealign_shifts(i,2);
      shift_y = shift_y + sin(r)*frealign_shifts(i,1)+cos(r)*frealign_shifts(i,2);

      x(i) = (box_coords(i,1) + box_coords(i,3)/2)/ output_bin_factor - shift_x;
      y(i) = (box_coords(i,2) + box_coords(i,4)/2) / output_bin_factor - shift_y;
  end

  for i=1:size_align(1)
    if(i == 1)
      phi_deviation(i) = 0;
    else
      min_j = -4;
      max_j = 4;
      min_err = 10000;
      for k = -3*360:360:3*360
      for j=min_j:max_j
        delta_phi(j+max_delta_subunits+1) = phi(i) - phi(i-1);
        delta_phi(j+max_delta_subunits+1) = delta_phi(j+max_delta_subunits+1) + k;
        phi_dev_err(j+max_delta_subunits+1) = abs(delta_phi(j+max_delta_subunits+1) - j*helical_twist);
        if(phi_dev_err(j+max_delta_subunits+1) < min_err)
          min_err = phi_dev_err(j+max_delta_subunits+1);
          phi_deviation(i) = delta_phi(j+max_delta_subunits+1) - j*helical_twist;
          delta_repeat(i) = j;
        end
      end
      end
    end
  end

  delta_repeat = delta_repeat/sign(sum(delta_repeat(:)));

  for i=1:size_align(1)

    if(delta_repeat(i) > 0)
      tot_n_particles = tot_n_particles + 1;
% !Fil.#   WP # Film #           X           Y           Z  PathLength          Phi        Theta          Psi   Subunit #    Score Refine? Units Angstr   par_vers. 4
%     34      1      2   11119.998    6936.677   00000.000   00000.000    00000.000    00000.000    00000.000   00000.000    0.223       T

      fprintf(fid, ...
           '%7d %4d %8.2f %8.2f %8.2f %8.2f %8.2f %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f T\n', ...
           filament_number, micrograph_number,  x(i), y(i), ...
            phi(i),theta(i),adjusted_psi(i), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, delta_repeat(i));
    end
  end
endif
