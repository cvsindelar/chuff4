function [v_spw, fsc, f_particle, coremask] = spw_postprocess(v,vfsc1,vfsc2)

r = size(v);
r_origin = floor(r/2) + 1;

f_coremask = 0.5;              % This should be significantly smaller
                                 %  than the actual volume fraction 
                                 %  taken up by the particle; 0.01 = "safe"
coremask = threshold_mask(v, f_coremask);

mask_fac = 1;
mask3d = fuzzymask(r(1),3,mask_fac*(r(1)-r_origin(1))-4,6);
f_mask3d = sum(sum(sum(mask3d.*mask3d)))/prod(r)

masked_fsc(1) = 0.99999;
masked_fsc(2:floor(r(1)/2)+1) = FSCorr(vfsc1.*mask3d,vfsc2.*mask3d);
ssnr = f_mask3d * masked_fsc./(1-masked_fsc);
fsc = ssnr./(1+ssnr);

cutoff_index = floor(r/2);

best_ccc = -1;
for est_f_particle_3d=[0.005:0.005:0.045,0.05:0.05:1.25]

  final_vol_spw_fsc1 = apply_est_cref2_scaled(vfsc1, fsc, 1, est_f_particle_3d,0.5, cutoff_index);
  ccc_spw_masked_est = ccc(final_vol_spw_fsc1 .* coremask, vfsc2.* coremask);
  clear final_vol_spw_fsc1;

  if(ccc_spw_masked_est > best_ccc)
    best_f_particle_3d = est_f_particle_3d;
    best_ccc = ccc_spw_masked_est;
  end

  fprintf('%6f %10.7e\n', est_f_particle_3d, ccc_spw_masked_est);
end

f_particle = best_f_particle_3d;

fprintf('Best f_particle, score: %6f %10.7e\n', best_f_particle_3d, best_ccc);

v_spw = apply_est_cref2_scaled(v, fsc, 1, f_particle, 1, cutoff_index);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [v_cref] = apply_est_cref2_scaled(v,fsc, f_particle_3d_mask,f_particle_est, data_frac, cutoff_index)
% cref = sqrt(2FSC/(1+FSC))
%   2FSC/(1+FSC) = cref^2
%   2FSC = (1 + FSC) * cref^2
% 2FSC = cref^2 + FSC * cref^2
%   FSC * (2 - cref^2) = cref^2
%   FSC = cref^2/(2-cref^2)
% fsc = fsc_ref.^2./(2-fsc_ref.^2);

% m1 = readSPIDERfile('final_vol_spw.spi');
% m2 = readSPIDERfile('final_vol_spw_mask.spi');
% ref = readSPIDERfile('kin_vol_mini.spi');

v_ft = double(fftshift(fftn(v)));
r = size(v);
r_origin = floor(r/2) + 1;

if(cutoff_index > r(1)/2-1)
  cutoff_index = r(1)/2-1;
end

est_fsc_ref = sqrt(2*fsc./(1+fsc));
psnr = data_frac*f_particle_3d_mask/f_particle_est * est_fsc_ref.^2 ./ (1-est_fsc_ref.^2);

% psnr = f_particle_3d_mask/f_particle_est * 2*fsc./(1-fsc);

pfsc_ref = double(sqrt(psnr./(psnr+1)));

[h k l] = ndgrid((1:r(1)) - r_origin(1), (1:r(2)) - r_origin(2), (1:r(3)) - r_origin(3));
radius = sqrt(h.*h + k.*k + l.*l);

v_ft(radius > cutoff_index) = 0;

%fileid = fopen('debug1.txt','w');
for i=0:r(1)*0.5-1
   j = find(radius > i - 0.5 & radius < i + 0.5);
%   if(pfsc_ref(i+1) >= 0.5 && cutoff == 0)
      if(i+1 <= cutoff_index)
     v_ft(j) = v_ft(j) * pfsc_ref(i+1).^2;
%     fprintf(fileid,'%d %f %f %f %f %f\n', i+1,  fsc(i+1), 2*fsc(i+1)/(1-fsc(i+1)), psnr(i+1), est_fsc_ref(i+1), pfsc_ref(i+1).^2);
%     fprintf('%d %f %f %f %f %f\n', i+1,  fsc(i+1), 2*fsc(i+1)/(1-fsc(i+1)), psnr(i+1), est_fsc_ref(i+1), pfsc_ref(i+1).^2);
%     fprintf('%d %f %f %f %f %f\n', i+1,  fsc(i+1), 2*fsc(i+1)/(1-fsc(i+1)), psnr(i+1), est_fsc_ref(i+1), pfsc_ref(i+1).^2);
   else
     v_ft(j) = 0;
%     fprintf('%d %f\n', i+1,  0);
   end
end

v_cref = real(ifftn(ifftshift(v_ft)));

