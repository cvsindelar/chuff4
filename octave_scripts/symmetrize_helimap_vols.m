function pad_vol = ...
  symmetrize_helimap_vols(num_pfs,num_starts,ref_com,est_repeat_distance_13pf, ref_pixel, ...
                         outer_radius_13pf)

global vol_array

edge_width_pix = 4;
radius_scale_factor = 1;
monomer_offset = 0;

pad_factor = 1;

%%%%%%%%%%%%%%%%%%%
% NOTE: need to fix code so we can use est_repeat_distance instead of helical_repeat_distance
%  -> DONE
%%%%%%%%%%%%%%%%%%%

ref_dim = size(vol_array);
n_classes = ref_dim(4);
pad_zdim = ref_dim(3);
pad_vol = zeros([ref_dim(1) ref_dim(2) pad_zdim]);

generous_lbound = floor((-ref_pixel*ref_dim(3)/2 - ref_com(3)) / est_repeat_distance_13pf) - 1;
generous_ubound = ceil((ref_pixel*ref_dim(3)/2 - ref_com(3)) / est_repeat_distance_13pf) + 1;

[x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub...
 x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist...
 mt_repeat_index mt_pf_index repeat_origin] = ...
   microtubule_parametric(generous_ubound - generous_lbound + 1, ...
                        num_pfs,num_starts,ref_com,...
                        est_repeat_distance_13pf,radius_scale_factor,monomer_offset,...
                        [0 0],[0 0],[0 0],[0 0 0 0]);

rise_pix = sqrt( (x_mt(2)-x_mt(1))^2 + (y_mt(2)-y_mt(1))^2 + (z_mt(2)-z_mt(1))^2)/ref_pixel;

generous_origin = num_pfs * (repeat_origin - 1) + 1;
adjusted_ref_com = [x_tub(generous_origin) ...
                    y_tub(generous_origin) ...
                    z_tub(generous_origin)];

twist = (psi_tub(2) - psi_tub(1)) * 180/pi;

mt_radius_13pf = sqrt(ref_com(1).^2 + ref_com(2).^2);
outer_radius = abs(outer_radius_13pf - mt_radius_13pf) + ...
                num_pfs/13 * mt_radius_13pf;
outer_radius_pix = outer_radius/ref_pixel;

if(outer_radius_pix - sqrt(sum(ref_com(:).^2))) > floor(ref_dim(1)/2)-4
  outer_radius_pix = sqrt(sum(ref_com(:).^2)) + floor(ref_dim(1)/2)-4
end

%%%%%%%%%%%%%%%%%%%
% Measure the average background value (over ALL volumes, so ML scores are not affected)
% Also, pre-compute FFT's of the volume array, and then zero out the volume array
%  in order to start accumulating the symmetrized version
%%%%%%%%%%%%%%%%%%%

[x y z] = ndgrid(-floor(ref_dim(1)/2):-floor(ref_dim(1)/2)+ref_dim(1)-1,...
                 -floor(ref_dim(2)/2):-floor(ref_dim(2)/2)+ref_dim(2)-1,...
                 -floor(ref_dim(3)/2):-floor(ref_dim(3)/2)+ref_dim(3)-1);
% zero at element n/2+1.

R = sqrt(x.^2 + y.^2);

background_pix = find(R <   0.5 + outer_radius_pix &...
                      R >= -0.5 + outer_radius_pix);
avg_background = 0;

for igold = 1:2
for iclass = 1:n_classes
  temp = vol_array(:,:,:,iclass,igold);
  avg_background = avg_background + sum(temp(background_pix));
end
end

avg_background = avg_background / (2*n_classes * prod(size(background_pix)));

vol_array = vol_array - avg_background;

%%%%%%%%%%%%%%%%%%%
% Generate filter to smooth the wedge edges
%%%%%%%%%%%%%%%%%%%

filter = fftshift(spherical_cosmask(pad_factor*ref_dim(1:3), 0, edge_width_pix));
filter = fftn(filter) / sum(filter(:));

%%%%%%%%%%%%%%%%%%%
% Now, symmetrize the volume array
%%%%%%%%%%%%%%%%%%%

new_vol_array = zeros(size(vol_array));

progressbar = 0;

for ind=1:prod(size(z_mt))
  subunit_num = (mt_repeat_index(ind) - repeat_origin) * num_pfs + mt_pf_index(ind);

  wedge_mask = soft_wedge_mask(size(pad_vol),subunit_num,num_pfs,num_starts,...
                             rise_pix,twist,...
                             0, outer_radius_pix, adjusted_ref_com/ref_pixel,...
                             [0 0 0], [0 0 0], pad_vol);

%%%%%%%%%%%%%%%%%%%%%%%%
% Below: to save (a lot of) time, we only shift and rotate pieces of the input volume 
%  that correspond to the final wedge defined by wedge_mask
%%%%%%%%%%%%%%%%%%%%%%%%

  z_cross_sect = sum(sum(wedge_mask, 1), 2);

  if(sum(z_cross_sect(:)) > 0)

    trimz_lower = 1;
    for ind2 = 1:ref_dim(3)
      if(z_cross_sect(ind2) == 0)
        trimz_lower = ind2;
      else
        break;
      end
    end
    trimz_upper = ref_dim(3);
    for ind2 = ref_dim(3):-1:trimz_lower
      if(z_cross_sect(ind2) == 0)
        trimz_upper = ind2;
      else
        break;
      end
    end

% Account for the size of the soft mask
    trimz_lower = trimz_lower - edge_width_pix;
    trimz_upper = trimz_upper + edge_width_pix;

% Extra padding to make the Fourier shift work better
    trimz_lower = trimz_lower - ceil( (trimz_upper - trimz_lower) / 2);
    trimz_upper = trimz_upper + ceil( (trimz_upper - trimz_lower) /2);

    if(trimz_lower < 1)
      trimz_lower = 1;
    end
    if(trimz_upper > ref_dim(3))
      trimz_upper = ref_dim(3);
    end
  
    shift_z = (z_tub(ind) - z_tub(generous_origin))/ref_pixel;
    int_shift_z = floor(shift_z);
    frac_shift_z = shift_z - int_shift_z;
  
    wedge_mask = real(ifftn(filter.*fftn(wedge_mask)));
  
    for igold = 1:2
    for iclass = 1:n_classes

      temp = vol_array(:,:,trimz_lower-int_shift_z : trimz_upper-int_shift_z,iclass,igold);
      temp = real(ifftn(fftn(temp).*FourierShiftVol(size(temp),[0 0 frac_shift_z])));
%      temp = vol_array(:,:,:,igold);
%      temp = real(ifftn(fftn(temp).*FourierShiftVol(size(temp),[0 0 shift_z])));
  
%      new_vol_array(:,:,trimz_lower:trimz_upper,iclass,igold) = ...
%          new_vol_array(:,:,trimz_lower:trimz_upper,iclass,igold) + ...
%          (1-wedge_mask(:,:,trimz_lower:trimz_upper)) .* avg_background + ...
%          wedge_mask(:,:,trimz_lower:trimz_upper) .* ...
%            ERotate3(temp, -[0 0 (psi_tub(ind) - psi_tub(generous_origin))]);

      new_vol_array(:,:,trimz_lower:trimz_upper,iclass,igold) = ...
          new_vol_array(:,:,trimz_lower:trimz_upper,iclass,igold) + ...
            wedge_mask(:,:,trimz_lower:trimz_upper) .* ...
              rotate3d(temp, [(psi_tub(ind) - psi_tub(generous_origin))*180/pi 0 0]);

    end
    end
  end % if(sum(z_cross_sect(:) > 0)

  if(ind/prod(size(z_mt)) > progressbar)
    fprintf(stdout, '%3.0f%% ', 100*progressbar);
    if(isoctave)
      fflush(stdout);
    end
    progressbar = progressbar + 0.1;
  end
end

fprintf(stdout, '\n');

%%%%%%%%%%%%%%%%%%%
% Now taper the Z edges of the helix
%%%%%%%%%%%%%%%%%%%

half_z_max_pix = ref_dim(3)/2 - 1 - edge_width_pix;

z_mask = zeros(ref_dim(1:3));
edge = cos( 2*pi*(z - half_z_max_pix) / (4*edge_width_pix)).^2;

edge( find(z > half_z_max_pix + edge_width_pix| ...
           z < half_z_max_pix)) = 0;
z_mask = z_mask + edge;

edge = cos( 2*pi*(z + half_z_max_pix) / (4*edge_width_pix)).^2;

edge( find(z < -(half_z_max_pix + edge_width_pix) | ...
           z > -(half_z_max_pix))) = 0;
z_mask = z_mask + edge;

z_mask(find( abs(z) <= half_z_max_pix)) = 1;

%%%%%%%%%%%%%%%%%%%
% Copy result to the volume array
%%%%%%%%%%%%%%%%%%%

for igold = 1:2
for iclass = 1:n_classes
  vol_array(:,:,:,iclass,igold) = new_vol_array(:,:,:,iclass,igold) .* z_mask + avg_background;
end
end
