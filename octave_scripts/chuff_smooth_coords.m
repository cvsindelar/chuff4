function chuff_smooth_coords(sparx_graph_file, frealign_graph_file, ...
            box_doc_file, output_file, ...
            job_dir, output_stack_dim, prerotate, stack_format, ...
            sparx_bin_factor, output_bin_factor, ...
            guessed_helical_twist, ...
            turns_per_repeat, input_subunits_per_repeat, ...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length)

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m'
else
  run 'chuff_parameters'
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

do_repair_psi = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Each 'particle', or repeat, contains one or more subunits. By default, we find the
%  number of subunits per repeat that minimizes the twist from one particle to
%  the next, after one goes around "turns_per_repeat" times (default 1).
%%%%%%%%%%%%%%%%%%%%%%%%%%

if(turns_per_repeat == -1)
  turns_per_repeat=1;
end

if(input_subunits_per_repeat <= 0)
  subunits_per_repeat = ceil( abs((turns_per_repeat*360 - 180) / guessed_helical_twist));
  best_twist = 361;
  best_subunits_per_repeat = -1;
  while(abs(subunits_per_repeat*guessed_helical_twist) < turns_per_repeat * 360 + 180)
    cur_twist = mod(subunits_per_repeat*guessed_helical_twist,360);
    if(abs(cur_twist) < abs(best_twist))
      best_twist = cur_twist;
      best_subunits_per_repeat = subunits_per_repeat;
    end
    if(abs(cur_twist-360) < abs(best_twist))
      best_twist = cur_twist - 360;
      best_subunits_per_repeat = subunits_per_repeat;
    end
    subunits_per_repeat = subunits_per_repeat + 1;
  end

  subunits_per_repeat = best_subunits_per_repeat;
else
  subunits_per_repeat = input_subunits_per_repeat;
end

protofilament_twist = mod(subunits_per_repeat*guessed_helical_twist, 360);
if(protofilament_twist < -180) 
  protofilament_twist = protofilament_twist + 360;
end
if(protofilament_twist > 180)
  protofilament_twist = protofilament_twist - 360;
end

protofilament_twist
turns_per_repeat
subunits_per_repeat

[coords, phi, theta, psi, directional_psi] =   ...
  read_chuff_alignment(sparx_graph_file, frealign_graph_file, box_doc_file,...
                       sparx_bin_factor, do_repair_psi, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do the stuff!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

smooth_coords(coords, phi, theta, psi, directional_psi, ...
            protofilament_twist, subunits_per_repeat, ...
            helical_repeat_distance, guessed_helical_twist, micrograph_pixel_size, ...
            job_dir, output_file, ...
            output_stack_dim, prerotate, stack_format, output_bin_factor, ...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length);
