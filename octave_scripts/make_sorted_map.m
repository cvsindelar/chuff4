if(nargin < 2)
  fprintf('PROGRAMMING ERROR: wrong number of arguments to octave script! (%d)\n', nargin);
%  exit(2)
end

focal_mate = str2num(argv(){1});
num_args = 1;

num_mts = nargin - num_args;

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

run 'chuff_parameters.m';

for mt_num = 1: num_mts
%%%%%%%%%%%%%%%
% Read in filenames, etc.
%%%%%%%%%%%%%%%

  job_dir = argv(){mt_num+num_args};

  if(job_dir(prod(size(job_dir))) == '/')
    job_dir=job_dir(1:prod(size(job_dir))-1);
  end
  [start] = regexp(job_dir,'[^/]*$','start');
  job_dir = job_dir(start:prod(size(job_dir)));

  [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

  if(focal_mate == 0)
    find_dir = sprintf('chumps_round1/%s/find_kinesin',job_dir);
  else
    find_dir = sprintf('chumps_round1/%s/fmate_find',job_dir);
  end

%%%%%%%%%%%%%%%%%%
% Read in decoration map
%%%%%%%%%%%%%%%%%%
  n_kins_to_find = 2*num_pfs;

  map_file = sprintf('%s/tot_decorate_map.spi',find_dir);
  tot_decorate_map = readSPIDERfile(map_file);

  sort_map_pad = 10;
  full_sorted_map = zeros([num_pfs 2*(sort_map_pad+n_good_boxes)]);

  for box_num=1:n_good_boxes
    dec_origin = floor(kin_lib_size/2) - floor(n_kins_to_find/2);

    repeat_map = tot_decorate_map( 1+(box_num-1)*n_kins_to_find: ...
                                              box_num*n_kins_to_find);
    mini_repeat_index = kin_repeat_index(dec_origin+1 : dec_origin+n_kins_to_find);
    mini_pf_index = kin_pf_index(dec_origin+1 : dec_origin+n_kins_to_find);

    mini_repeat_index = mini_repeat_index - dec_repeat_origin;
    mini_repeat_index = mini_repeat_index + (box_num-1)*2;

    sorted_index = num_pfs*(mini_repeat_index-1)+mini_pf_index;
    full_sorted_map(sorted_index + sort_map_pad*num_pfs) = repeat_map;

    sorted_map_file = sprintf('%s/sorted_map_full.spi',find_dir);
    writeSPIDERfile(sorted_map_file,full_sorted_map);

%    test_repeat_map = test_sorted_map(sorted_index + sort_map_pad*num_pfs);
%
%    test_decorate_map( 1+(box_num-1)*n_kins_to_find: ...
%                               box_num*n_kins_to_find) = test_repeat_map;
%    writeSPIDERfile('test_decorate_map.spi',test_decorate_map);

    final_map = ref_tub;
    final_map2 = ref_tub;

end % if(box_num == 40)

end % for box_num...

end % for mt_num ...

return
