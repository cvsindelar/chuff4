function [micrograph3, p] = replicate_micrograph(job_dir,use_ctf,focal_mate,bin_factor,...
                             chumps_round, dec_factor)

stdout = 1;

if(nargin < 6)
  dec_factor = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'chuff_parameters.m';
else
  run 'chuff_parameters';
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification

[tubrefs, kinrefs, box_dim, ref_dim, ref_pixel, ref_com, min_theta, d_angle] = ...
  get_subunit_refs(chumps_round, micrograph_pixel_size, filament_outer_radius, bin_factor);

num_mts = 1;

for mt_num = 1: num_mts

%%%%%%%%%%%%%%%
% Get CTF info
%%%%%%%%%%%%%%%
% Padding the references improves the behavior of the CTF at image boundaries
  pad_ref_dim = 4*ref_dim;

  [micrograph_name,defocus,astig_mag,astig_angle,b_factor, find_dir] = ...
    chumps_micrograph_info(job_dir,focal_mate,chumps_round);

  if(use_ctf ~= 0)
    electron_lambda = EWavelength(accelerating_voltage);
    ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
    ctf = ifftshift(ctf);
  else
    [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
    micrograph_name = sprintf('%s.mrc',job_dir(1:start-1));
    if(micrograph_name(1:6) ~= 'scans/')
      micrograph_name = sprintf('scans/%s',micrograph_name);
    end

    ctf = -ones(pad_ref_dim);
  end

  mic_dim = ReadMRCheader(micrograph_name);
  micrograph = ReadMRC(micrograph_name);

  if(bin_factor != 1)
    micrograph = BinImageCentered(micrograph, bin_factor);
  end

%%%%%%%%%%%%%%%%%%
% Get alignment info
%%%%%%%%%%%%%%%%%%

  [num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
    est_repeat_distance] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);

  [coords,phi,theta,psi,...
   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance] = ...
    fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_repeat_distance/micrograph_pixel_size,...
			 num_pfs, num_starts, ref_com);
  n_good_boxes = prod(size(phi));

%%%%%%%%%%%%%%%%%%
% Read in decoration map
%%%%%%%%%%%%%%%%%%

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;

  map_file = sprintf('%s/tot_decorate_map.spi',find_dir);
  tot_decorate_map = readSPIDERfile(map_file);

  fprintf(stdout,'\n%s\n', job_dir);

  micrograph2 = zeros(floor(mic_dim(1)/bin_factor),floor(mic_dim(2)/bin_factor));
  micrograph3 = zeros(floor(mic_dim(1)/bin_factor),floor(mic_dim(2)/bin_factor));

  for box_num=1:n_good_boxes
    fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
    if(isoctave)
      fflush(stdout);
    end

    if(!box_in_micrograph(coords(box_num,:), box_dim, mic_dim))
      printf('  Note: box %d not completely within micrograph. \n',box_num);
    end

    if(box_num > 1 && box_num < n_good_boxes)
      box_num_repeats = 1; % We request the image of a single tubulin repeat, 
                           %  representing a full repeat of subunits beginning at the origin
                           %  and extending in the direction of travel.
                           % Note that requesting a single repeat from get_findkin_refs
                           %  only gets the subunits at one side of the origin,
                           %  with the first subunit located precisely at the origin.
                           % Requesting two repeats would obtain the above repeat plus the
                           %  repeat located on the other side of the origin.
      box_repeat_origin = 1;
    else
%%%%%%%%%%%%%%%%%%%
% For the bounding boxes, extend the microtubule out towards the edges
%%%%%%%%%%%%%%%%%%%
      box_num_repeats = ceil(ref_dim(1)/3*ref_pixel/helical_repeat_distance);
      if(box_num == 1)
        box_repeat_origin = box_num_repeats;
      else
        box_repeat_origin = 1;
      endif
    end

    [tub_kin_repeat_origins, kin_repeat_index, kin_pf_index,...
     x_tub, y_tub, phi_tub, theta_tub, psi_tub, phi_mt, theta_mt, psi_mt, ref_tub, ref_kin] = ...
      get_findkin_refs(box_num, ref_dim,...
                  [box_num_repeats box_num_repeats box_repeat_origin box_repeat_origin],...
                   num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com,...
                   tubrefs,kinrefs,ctf,ones(size(ctf)),ones(size(ctf)),...
                   ref_pixel,min_theta,d_angle);

    s = size(ref_kin);
    kin_lib_size = s(3); % = 2*box_num_repeats*num_pfs

    decorate = zeros(kin_lib_size,1);

    sites_per_repeat = 2*num_pfs;

    tot_num_repeats = prod(size(tot_decorate_map))/(sites_per_repeat);
    if(kin_lib_size != box_num_repeats*sites_per_repeat)
      printf('Doh! lib_size %d n_repeats %d sites_per_rep %d\n', kin_lib_size,num_repeats,sites_per_repeat);
    end

    [box_bounds, tot_bounds] = ...
      box_to_tot_findmap(decorate, [1 box_num_repeats], box_repeat_origin, ...
                     tot_decorate_map, box_num, sites_per_repeat);

    decorate(box_bounds(1):box_bounds(2)) = tot_decorate_map(tot_bounds(1):tot_bounds(2));

    final_map = ref_tub;
    final_map2 = ref_tub;
    for j=box_bounds(1):box_bounds(2)
      final_map = final_map + decorate(j)*ref_kin(:,:,j);
      final_map2 = final_map2 + decorate(j)*ref_kin(:,:,j)*dec_factor;
    end

    micrograph2 = paste_chumps_box(micrograph2,final_map, box_num,coords,...
                        bin_factor,ones(size(final_map)));

% debug
%if(box_num != 3)
    micrograph3 = paste_chumps_box(micrograph3,final_map2, box_num,coords,...
                        bin_factor,ones(size(final_map)));
%end

end % for box_num...

scale_box_dim = floor(2 * 2*filament_outer_radius/(bin_factor*micrograph_pixel_size));
rot_box_dim = floor(sqrt(2)* scale_box_dim);
scale_box_dim = [scale_box_dim scale_box_dim];
rot_box_dim = [rot_box_dim rot_box_dim];

wi_oxy = 1 + floor(rot_box_dim/2) - floor(scale_box_dim/2);

binned_boxes = zeros([scale_box_dim n_good_boxes]);
binned_replicates = zeros([scale_box_dim n_good_boxes]);

real_n = 0;
for box_num = 1:n_good_boxes
  if(!box_in_micrograph(coords(box_num,:), bin_factor*rot_box_dim, mic_dim))
    printf('  Note: box %d not completely within micrograph. \n',box_num);
%    continue
  end
  real_n = real_n+1;

  temp = rotate2d(copy_chumps_box(micrograph,box_num,coords,rot_box_dim,bin_factor),...
                  -psi(box_num));
  binned_boxes(:,:,real_n) = temp(wi_oxy(1):wi_oxy(1)+scale_box_dim(1)-1,...
                                  wi_oxy(2):wi_oxy(2)+scale_box_dim(2)-1);

  temp = rotate2d(copy_chumps_box(micrograph2,box_num,coords,rot_box_dim,bin_factor),...
                  -psi(box_num));
  binned_replicates(:,:,real_n) = temp(wi_oxy:wi_oxy+scale_box_dim(1)-1,...
                                  wi_oxy:wi_oxy+scale_box_dim(1)-1);
end

p = linear_fit(binned_replicates(:),binned_boxes(:));

if(nargout == 0)
    [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
    if(focal_mate)
      fmate_string = '_fmate';
    else
      fmate_string = '';
    end
    micrograph3_name = sprintf('%s_MT%d%s_replicate_down%d.spi',job_dir(1:start-1),...
      mt_num,fmate_string,bin_factor);

    writeSPIDERfile('replicates.spi',p(1)*binned_replicates+p(2));
    writeSPIDERfile('boxes.spi',binned_boxes);

    writeSPIDERfile(sprintf(micrograph3_name,box_num), p(1)*micrograph3+p(2));
end

end % for mt_num ...

return
