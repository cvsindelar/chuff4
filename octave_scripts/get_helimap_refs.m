function [reflib, box_dim, ref_dim, ref_pixel, ref_com, min_theta, d_angle] = ...
    get_helimap_refs(chumps_round, micrograph_pixel_size, filament_outer_radius, bin_factor, helimap_header)

%%%%%%%%%%%%%%%%%%
% Get reference info
%%%%%%%%%%%%%%%%%%
global reflib;

if(nargin < 5)
  helimap_header = sprintf('ref_helimap');
end

n_states = 0;
subunit_dim = 0;
while(exist(sprintf('chumps_round%d/%s%d_bin%d/ref_tot.spi', ...
                    chumps_round, helimap_header, n_states+1, bin_factor)) == 2)
  [h, s1] = readSPIDERheader(sprintf('chumps_round%d/%s%d_bin%d/ref_tot.spi',...
                             chumps_round, helimap_header, n_states+1, bin_factor));
  subunit_dim = max([subunit_dim s1])
  n_states = n_states + 1;
end

for i=1:n_states
  nstackitems = h(26);
end

subunit_dim = [subunit_dim subunit_dim];

n_states

if(n_states < 1)
  fprintf(stdout, 'ERROR: no helimap references!\n');

  return
end

ref_dir = sprintf('chumps_round%d/%s%d_bin%d', ...
                          chumps_round, helimap_header, 1, bin_factor);

com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
com_info = readSPIDERdoc_dlmlike(com_doc_name);
index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
ref_pixel_info = readSPIDERdoc_dlmlike(ref_params_name);
index = find(ref_pixel_info(:,1) == 2);
ref_pixel = ref_pixel_info(index(1),5);
index = find(ref_pixel_info(:,1) == 1);
min_theta = ref_pixel_info(index(1),5);
index = find(ref_pixel_info(:,1) == 3);
d_angle = ref_pixel_info(index(1),3);

box_dim = ceil(1.5*2*filament_outer_radius/ref_pixel);
box_dim = 16*ceil(box_dim/16);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);

if(isempty(reflib))

  reflib = zeros([subunit_dim nstackitems n_states]);

  for i=1:n_states
    ref_dir = sprintf('chumps_round%d/%s%d_bin%d', ...
                       chumps_round, helimap_header, i, bin_factor);

    [h, s1] = readSPIDERheader(sprintf('%s/ref_tot.spi',ref_dir));

    oxy = 1 + floor(subunit_dim/2) - floor(s1(1)/2);
    reflib(oxy:oxy+s1(1)-1,oxy:oxy+s1(1)-1,:,i) = ...
     readSPIDERfile(sprintf('%s/ref_tot.spi', ref_dir));
  end
end
