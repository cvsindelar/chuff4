function [box_repeat_origin, kin_repeat_index, kin_pf_index,...
          x_tub_final, y_tub_final, phi_tub_final, theta_tub_final, psi_tub_final,...
          phi_mt, theta_mt, psi_mt, ref_tub, ref_kin] = ...
  get_findkin_refs(box_num, ref_dim,num_repeats,num_pfs,num_starts,...
                   phi_est, theta_est, psi_est, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com,...
                   tub_ref_vol,kin_ref_vol,ctf,ctf_flip,filter,...
                   ref_pixel)

if(nargin <= 15)
  generate = 0;
else
  generate = 1;
end

num_tub_repeats = num_repeats(1);
if(prod(size(num_repeats)) > 1)
  num_kin_repeats = num_repeats(2);
else
  num_kin_repeats = num_tub_repeats;
end

if(prod(size(num_repeats)) >= 3)
  num_tub_repeats = [num_tub_repeats; num_repeats(3)];
end

if(prod(size(num_repeats)) >= 4)
  num_kin_repeats = [num_kin_repeats; num_repeats(4)];
end

ref_tub = [];
ref_kin = [];

%%%%%%%%%%%%%%%%%%
% Generate the reference images, possibly padded to ensure that 
%  generate_micrograph_gridproj() has room to place all the subunits
%%%%%%%%%%%%%%%%%%

kin_lib_size = 2*num_kin_repeats(1)*num_pfs;

monomer_offset = 0;
select_pf = 0; % [num_pfs 1];

[x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
 x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist mt_repeat_index mt_pf_index ...
 tub_repeat_origin] = ...
   microtubule_parametric(num_tub_repeats,num_pfs,num_starts,ref_com,...
                          est_repeat_distance,radius_scale_factor,monomer_offset,...
                          [phi_est(box_num) d_phi_d_repeat(box_num)],...
                          [theta_est(box_num) d_theta_d_repeat(box_num)],...
                          [psi_est(box_num) d_psi_d_repeat(box_num)],elastic_params);

if(generate ~= 0)
  pad_ref_dim = size(ctf);

  ref_tub = generate_micrograph_gridproj(pad_ref_dim, tub_ref_vol, ref_pixel, select_pf,...
                          x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                          [0 0]);
end

[x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
 x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist mt_repeat_index mt_pf_index ...
 kin_repeat_origin] =...
   microtubule_parametric(num_kin_repeats,num_pfs,num_starts,ref_com,...
                          est_repeat_distance,radius_scale_factor,monomer_offset,...
                          [phi_est(box_num) d_phi_d_repeat(box_num)],...
                          [theta_est(box_num) d_theta_d_repeat(box_num)],...
                          [psi_est(box_num) d_psi_d_repeat(box_num)],elastic_params);
if(generate ~= 0)
  ref_kin = ...
      generate_micrograph_gridproj(pad_ref_dim, kin_ref_vol, ref_pixel, select_pf,...
                          x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,...
                          [0 0],1,1);
end

%%%%%%%%%%%%%%%%%%%%%
% Generate images corresponding to the alternate kinesin binding sites
%  by effectively swapping alpha tubulin for beta tubulin- 
%  we use a monomer-sized axial shift.
%%%%%%%%%%%%%%%%%%%%%

monomer_offset = 1;
[x_tub2 y_tub2 z_tub2 phi_tub2 theta_tub2 psi_tub2 r_tub ...
 x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist] =...
   microtubule_parametric(num_kin_repeats,num_pfs,num_starts,ref_com,...
                          est_repeat_distance,radius_scale_factor,monomer_offset,...
                          [phi_est(box_num) d_phi_d_repeat(box_num)],...
                          [theta_est(box_num) d_theta_d_repeat(box_num)],...
                          [psi_est(box_num) d_psi_d_repeat(box_num)],elastic_params);
if(generate ~= 0)
  ref_kin2 = ...
      generate_micrograph_gridproj(pad_ref_dim, kin_ref_vol, ref_pixel, select_pf,...
                          x_tub2,y_tub2,z_tub2,phi_tub2,theta_tub2,psi_tub2,...
                          [0 0],1,1);
end

if(generate ~= 0)
  ref_tub = real(ifftn(fftn(ref_tub) .* ctf .* ctf_flip .* filter));
  avg = mean(ref_tub(:));
  ref_tub = (ref_tub - avg);
end

%%%%%%%%%%%%%%%%%%
% Window out the final-sized reference images, after applying CTF
%%%%%%%%%%%%%%%%%%


if(generate ~= 0)
  in_x = 1+floor(pad_ref_dim(1)/2)-floor(ref_dim(1)/2);
  in_y = 1+floor(pad_ref_dim(2)/2)-floor(ref_dim(2)/2);
  ref_tub = ref_tub(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
  ref_kin_final = zeros([ref_dim(1) ref_dim(2) kin_lib_size]);
end

%%%%%%%%%%%%%%%%%%%%%
% Make the final library with alternating repeats from the normal
%  and offset images, to keep them roughly in order by axial position
%%%%%%%%%%%%%%%%%%%%%

x_tub_final = zeros([1 kin_lib_size]);
y_tub_final = zeros([1 kin_lib_size]);
phi_tub_final = zeros([1 kin_lib_size]);
theta_tub_final = zeros([1 kin_lib_size]);
psi_tub_final = zeros([1 kin_lib_size]);

for i=1:kin_lib_size/2
  k = 2*i-1;
  if(generate ~= 0)
    temp = real(ifftn(fftn(ref_kin(:,:,i)) .* ctf .* ctf_flip .* filter));
    ref_kin_final(:,:,k) = temp(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
  end
  kin_repeat_index(k) = 2*mt_repeat_index(i)-1;
  kin_pf_index(k) = mt_pf_index(i);

  x_tub_final(k) = x_tub(i);
  y_tub_final(k) = y_tub(i);
  phi_tub_final(k) = phi_tub(i);
  theta_tub_final(k) = theta_tub(i);
  psi_tub_final(k) = psi_tub(i);

  k = 2*i;
  if(generate ~= 0)
    temp = real(ifftn(fftn(ref_kin2(:,:,i)) .* ctf .* ctf_flip .* filter));
    ref_kin_final(:,:,k) = temp(in_x:in_x+ref_dim(1)-1,in_y:in_y+ref_dim(2)-1);
  end
  kin_repeat_index(k) = 2*mt_repeat_index(i);
  kin_pf_index(k) = mt_pf_index(i);

  x_tub_final(k) = x_tub2(i);
  y_tub_final(k) = y_tub2(i);
  phi_tub_final(k) = phi_tub2(i);
  theta_tub_final(k) = theta_tub2(i);
  psi_tub_final(k) = psi_tub2(i);
end

%%%%%%%%%%%%%%%%%%
% Finished generating images
%%%%%%%%%%%%%%%%%%

if(generate ~= 0)
  ref_kin = real(ref_kin_final);
end

box_repeat_origin = [tub_repeat_origin kin_repeat_origin];
