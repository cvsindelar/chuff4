function random_dec(s,thresh)

m = zeros(s);
r = rand(s);
m(find(r < thresh)) = 1;

e = m;
o = m;
e(:,1:2:s(2)) = 0;
o(:,2:2:s(2)) = 0;

writeSPIDERfile('sorted_map_random_even.spi',e);
writeSPIDERfile('sorted_map_random_odd.spi',o);
writeSPIDERfile('sorted_map_random.spi',m);

sum(sum(m))/prod(size(m))
sum(sum(e))/prod(size(m))
sum(sum(o))/prod(size(m))
