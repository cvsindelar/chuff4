function [sigma2,sigma2_1d] = ...
  get_sigma2(igold, goldmap, boxes,synth_boxes,output_dim,norm_params_image,...
             coords,phi,theta,psi,...
             d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat, elastic_params,...
             ref_pixel,helical_repeat_distance,min_resolution, filament_outer_radius, outer)

%%%%%%%%%%%%%%%%%%%%
% Note that for now we do not use igold here, so noise estimates are the same for both
%  subsets of particles
%%%%%%%%%%%%%%%%%%%%
global debug;

if(nargin < 19)
  outer = 1;
end

sigma2_fudge = 0.1;

n_good_boxes = prod(size(phi));
ref_dim = size(boxes);
ref_dim = ref_dim(1:2);

'debug'
fudge = 5;
'debug'
first = 1 + fudge;
last = n_good_boxes - fudge;

sigma2_sum = zeros([output_dim]);

% sigma2 = zeros([ref_dim n_good_boxes]);

  for box_num=first:last

  if(outer)
    helimask = ...
        1-helix_cos_mask_2d(output_dim, -psi(box_num), filament_outer_radius, ...
                            min_resolution, ref_pixel);
  else
   helimask = ...
     get_helimap_mask_simple(output_dim, ref_pixel, ...
                             box_num, helical_repeat_distance, coords, psi, min_resolution);
  end

  f_mask = sum(helimask(:).^2)/prod(size(helimask));

  sigma2 = zeros([output_dim]);

  if(ref_dim <= output_dim)
    temp_sigma2 = ...
           (norm_params_image(1,box_num) * boxes(:,:,box_num) + norm_params_image(2,box_num));
    temp_sigma2 = temp_sigma2 - synth_boxes(:,:,box_num);
    temp_sigma2 = temp_sigma2 - sum(temp_sigma2(:) .* helimask(:)) / sum(helimask(:));
    temp_sigma2 = temp_sigma2 .* helimask;
  else
    wi_oxy = 1 + floor(ref_dim/2) - floor(output_dim/2);

    temp_sigma2 = ...
         norm_params_image(1,box_num) * ...
            boxes(wi_oxy(1):wi_oxy(1)+output_dim(1)-1,...
                  wi_oxy(1):wi_oxy(1)+output_dim(1)-1,box_num) + ...
         norm_params_image(2,box_num);
    temp_sigma2 = temp_sigma2 - synth_boxes(wi_oxy(1):wi_oxy(1)+output_dim(1)-1,...
                  wi_oxy(1):wi_oxy(1)+output_dim(1)-1,box_num);
    temp_sigma2 = temp_sigma2 - sum(temp_sigma2(:) .* helimask(:)) / sum(helimask(:));
    temp_sigma2 = temp_sigma2 .* helimask;
  end

  sigma2(1:ref_dim(1),1:ref_dim(2)) = temp_sigma2;

  sigma2 = 0.5 * 1/f_mask * ...
              abs(fftshift(fft2(sigma2))).^2;

  sigma2_sum = sigma2_sum + sigma2/(last-first+1);
end

[sigma2_1d, sigma2] = rot_avg(sigma2_sum, 1);

sigma2 = ifftshift(sigma2);

sigma2(find(abs(sigma2) < sigma2_fudge)) = sigma2_fudge;
