function chumps_align_focal_pairs(micrograph,filter_resolution,n_iterations,...
                        angle_spec,scale_spec,rot_bin_factor,scale_bin_factor)

run 'chuff_parameters.m'

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;
supersample = 2;

angle_offset = angle_spec(1);
if(prod(size(angle_spec)) > 1)
  angle_halfw = angle_spec(2);
else
  angle_halfw = 1;
endif

if(prod(size(angle_spec)) > 2)
  n_angle = angle_spec(3);
else
  n_angle = 11;
endif

scale_offset = scale_spec(1);
if(prod(size(scale_spec)) > 1)
  scale_halfw = scale_spec(2);
else
  scale_halfw = 0.1;
endif

if(prod(size(scale_spec)) > 2)
  n_scale = scale_spec(3);
else
  n_scale = 11;
endif

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

if(micrograph(1:6) != 'scans/')
  micrograph = sprintf('scans/%s',micrograph);
end
focal_mate = sprintf('%s_focal_mate.mrc',micrograph(1:prod(size(micrograph))-4));
focal_mate_align = sprintf('%s_focal_mate_align.mrc',micrograph(1:prod(size(micrograph))-4))

ctf_doc_name = sprintf('%s_ctf_doc.spi',micrograph(1:prod(size(micrograph))-4));
fmate_ctf_doc_name = sprintf('%s_focal_mate_ctf_doc.spi',micrograph(1:prod(size(micrograph))-4));

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

ctf_doc = dlmread(ctf_doc_name);

index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));
defocus = ctf_doc(index,3);
astig_mag = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);

ctf_doc = dlmread(fmate_ctf_doc_name);

index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));
fmate_defocus = ctf_doc(index,3);
fmate_astig_mag = ctf_doc(index,4);
fmate_astig_angle = ctf_doc(index,5);

printf('Defocus for micrograph: %7.0f ; for focal mate: %7.0f\n',defocus,fmate_defocus);

b_factor = 0;

electron_lambda = EWavelength(accelerating_voltage);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read micrographs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m1 = ReadMRC(micrograph);
m2 = ReadMRC(focal_mate);
s = size(m1);
s_supersample = supersample*s;

if(defocus > 0)
  ctf_flip1 = sign(CTF_ACR(s(1), micrograph_pixel_size, ...
                     electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0));
  ctf_flip2 = sign(CTF_ACR(s(1), micrograph_pixel_size, ...
                     electron_lambda, fmate_defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0));
  ctf_flip1 = ifftshift(ctf_flip1);
  ctf_flip2 = ifftshift(ctf_flip2);
else
  fprintf(stdout, 'ERROR: ctf information not found...\n');
end

m1 = real(ifftn(ctf_flip1.*fftn(m1)));
m2_flip = real(ifftn(ctf_flip2.*fftn(m2)));

[best_angle,best_scale,dx,dy] = ...
  align_focal_pairs(m1,m2_flip,micrograph_pixel_size,...
                        filter_resolution,n_iterations,angle_spec,scale_spec,
                        rot_bin_factor,scale_bin_factor);

best_m2rot = rotate2d(m2, -best_angle);
m2super = scale2d(best_m2rot,supersample);
m2super = TransformImage(m2super, 0, best_scale);
m2super = circshift(m2super, [supersample*dx supersample*dy]);
best_m2align = m2super(1:supersample:s_supersample,1:supersample:s_supersample);
best_m2align = real(ifftn(fftn(best_m2align).*FourierShift(s(1),[1/supersample 1/supersample])));

WriteMRC(best_m2align,micrograph_pixel_size,focal_mate_align);
