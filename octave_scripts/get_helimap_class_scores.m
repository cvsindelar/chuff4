function [new_helimap, norm_factor] = get_helimap_class_scores(tm_raw, num_pfs, n_classes)

global tm_raw;
size_tm = size(tm_raw)
tm_dim = size_tm(1)
n_good_boxes = size_tm(3)-1

new_helimap = zeros([num_pfs n_good_boxes n_classes]);

norm_factor = (max(tm_raw(:)) - min(tm_raw(:)))
tm_norm = tm_raw/norm_factor;
tm_exp = exp(-tm_norm);

%%%%%%%%%%%%%%
% Now zero out the 'special' rows/columns of the first and last transfer matrices; see notes below
%%%%%%%%%%%%%%
%tm_exp(2:(n_classes^num_pfs),:,1) = 0;
%tm_exp(:,2:(n_classes^num_pfs),1) = 0;

progressbar = 0;

for selbox = 1:n_good_boxes

%%%%%%%%%%%%%%%
% Note here: the first dimension of transfer matrix 'n' corresponds to the states of 
%  TM repeat 'n-1'. This is due to an origin convention, which is chosen to match the image
%  coordinate systems of SPIDER and other EM programs: 
%  for example, if an image is n pixels wide, the origin is at (floor(n/2) + 1).
%
%  Hence, although we are not dealing with pixels here but rather repeats, 
%  if two TM repeats are represented in a given segment image, the **second** of these 
%  corresponds to the 'origin'.
%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%
% For the first part of the TM product: we wish the first dimension of the beginning matrix to correspond to 
%  'selbox'; hence, according to the above note, we should start with TM # selbox+1
%
% Note also that TM #n_good_boxes+1 is special, because it contains only one non-zero row- this describes
%  all states of subunit #n_good_boxes. There is no subunit #n_good_boxes+1 for this TM, 
%  so we say that there is only one state for subunit #n_good_boxes+1, the likelihood of which is 
%  exactly one; the likelihood of all other states (corresponding to the other columns) is identically zero.
%%%%%%%%%%%%%%%
  first_tm = selbox+1;
  last_tm = n_good_boxes+1;
  for k=first_tm:last_tm
    if(k == first_tm)
      tm_prod = tm_exp(:,:,k);
    else
%%%%%%%%%%%%%%%
% For each matrix product, the values of the elements will increase on average by the matrix
%  dimension. Thus, to avoid numeric overflow, we normalize the products by this number
%  (which is equal to (n_classes^num_pfs) )
%%%%%%%%%%%%%%%
      tm_prod = tm_prod * tm_exp(:,:,k) / (n_classes^num_pfs);
    end
  end

%%%%%%%%%%%%%%%
% For the second part of the TM product: we wish the last dimension of the last matrix to correspond to 
%  'selbox'; hence, according to the above note, we should finish with with TM # selbox.
%
% Note also that TM #1 is special, because it contains only one non-zero column- this describes
%  all states of subunit #1. There is no subunit n-1 (= 0) for this TM, so we say that there is only one 
%  state for subunit #0, the likelihood of which is exactly one; the likelihood of all other states
%  (corresponding to the other columns) is identically zero.
%%%%%%%%%%%%%%%
  first_tm = 1;
  last_tm = selbox;
  for k=first_tm:last_tm
    if(k == first_tm)
      tm_prod = tm_prod * tm_exp(:,:,k);
    else
      tm_prod = tm_prod * tm_exp(:,:,k) / (n_classes^num_pfs);
    end
  end

  new_helimap(:,selbox,:) = 0;
  for state_value=1:n_classes^num_pfs
    state_vec = get_states_from_num(state_value,n_classes,num_pfs);
    for k=1:num_pfs
      new_helimap(k,selbox,state_vec(k)) = ...
        new_helimap(k,selbox,state_vec(k)) + diag(tm_prod)(state_value);
    end
  end

  for k=1:num_pfs
    max_val = max(squeeze(new_helimap(k,selbox,:)));
    for class = 1:n_classes
      new_helimap(k,selbox,class) = ...
        exp(norm_factor*(log(new_helimap(k,selbox,class))-log(max_val)));
    end
  end

  for k=1:num_pfs
    norm = sum(new_helimap(k,selbox,:));
    new_helimap(k,selbox,:) = new_helimap(k,selbox,:)/norm;
  end

  state_value = find(diag(tm_prod) == max(diag(tm_prod)));
  state_vec = get_states_from_num(state_value(1),n_classes,num_pfs);

%  new_helimap(find(new_helimap < 0.5)) = 0;
%  new_helimap(find(new_helimap >= 0.5)) = 1;

  for k=1:n_classes
    temp = zeros([num_pfs 1]);
    temp(find(state_vec == k)) = 1;
%    new_helimap(:,selbox,k) = temp;
%    new_helimap(:,selbox,k)'
%    temp'
  end

%  s4 = sort(diag(tm_prod));
%  s4(prod(size(s4))-4:prod(size(s4)));
%
%  [i4,j4] = find(tm_exp(:,:,selbox) == max(tm_exp(:,:,selbox)(:)));

  if(selbox/n_good_boxes > progressbar)
    fprintf(stdout, '%3.0f%% ', 100*progressbar);
    if(isoctave)
      fflush(stdout);
    end
    progressbar = progressbar + 0.1;
  end
end

fprintf(stdout, ' done.\n');
fflush(stdout);
