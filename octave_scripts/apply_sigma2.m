function return_box = apply_sigma2(box, sigma2_factor)

%%%%%%%%%%%%%%%%%%%%%%%
% Note on below expression: we apply the CTF to the reference image, and
%  then pre-divide by sigma2 so that we can compute the final likelihood
%  score as a real-space operation rather than in Fourier space, 
%  as in Schere's Eq. 7, in the 2012 Relion paper.
% However, this requires that we pre-divide
%  both FFT's by the ***square root*** of **twice** sigma2, so that when they are squared
%  we get the desired 2*sigma2 in the denominator. 
% Emphasize again that we compute the sum of squared differences in real space.
%%%%%%%%%%%%%%%%%%%%%%%

return_box = fftn(box);
return_box = return_box .* sigma2_factor;
return_box = real(ifftn(return_box));

%%%%%%%%%%%%%%%%%%%%%%%
% Purpose of the following line is to set F000 to zero, otherwise
%  the likelihood expressions can be dominated by the DC component
%  of the images, which has little or nothing to do with the protein
%  signal!!
%%%%%%%%%%%%%%%%%%%%%%%

return_box = return_box - sum(return_box(:))/prod(size(return_box));

return
