function vm = helix_cos_mask(v, pixel_size, edge_width, r_outer, r_inner, half_z_max, estimate_background)
% function vm = helix_cos_mask(v, pixel_size, edge_width, r_outer, r_inner, half_z_max, estimate_background)

v_size = size(v)

if(nargin < 5 || r_outer == 0)
  r_outer = v_size(1)*pixel_size/2 - edge_width;
end
if(nargin < 6)
  r_inner = 0;
end
if(nargin < 7 || half_z_max == 0)
  half_z_max = v_size(3)*pixel_size/2 - edge_width;
end
if(nargin < 7 || half_z_max == 0)
  estimate_background = 0;
end

edge_width_pix = edge_width/pixel_size;
r_inner_pix = r_inner/pixel_size;
r_outer_pix = r_outer/pixel_size;
half_z_max_pix = half_z_max/pixel_size;

if(prod(size(v_size)) < 3)
  if(prod(size(v_size)) < 2)
    v_size = [v_size(1) v_size(1)];
  end
  v_size = [v_size(1) v_size(2) v_size(1)];
end

[x,y,z] = ndgrid(-floor(v_size(1))/2:floor(v_size(1))/2-1, ...
                 -floor(v_size(2))/2:floor(v_size(2))/2-1, ...
                 -floor(v_size(3))/2:floor(v_size(3))/2-1);

r_squared = x.*x + y.*y;

if(estimate_background != 0)
  outer_avg = v(find(r_squared >= r_outer_pix^2 & r_squared <= (r_outer_pix + edge_width_pix)^2));
  if(prod(size(outer_avg)) > 0)
    outer_avg = sum(outer_avg) / prod(size(outer_avg))
  else
    outer_avg = 0
  end
else
  outer_avg = 0
end

if(estimate_background != 0)
  inner_limit = r_inner_pix - edge_width_pix;
  if(inner_limit < 0)
    inner_limit = 0
  end
  inner_avg = v(find( (r_squared >= inner_limit^2) & (r_squared <= r_inner_pix^2)));

  if(prod(size(inner_avg)) > 0)
    inner_avg = sum(inner_avg) / prod(size(inner_avg))
  else
    inner_avg = 0
  end
else
  inner_avg = 0
end

if(estimate_background != 0)
  upper_avg = v(find(z >= half_z_max_pix & z <= half_z_max_pix + edge_width_pix));
  if(prod(size(upper_avg)) > 0)
    upper_avg = sum(upper_avg) / prod(size(upper_avg))
  else
    upper_avg = 0
  end
else
  upper_avg = 0
end

if(estimate_background != 0)
  lower_avg = v(find(z <= -half_z_max_pix & z >= -(half_z_max_pix + edge_width_pix)));
  if(prod(size(lower_avg)) > 0)
    lower_avg = sum(lower_avg) / prod(size(lower_avg))
  else
    lower_avg = 0
  end
else
  lower_avg = 0
end

if(upper_avg == 0 || lower_avg == 0)
  upperlower_avg = 0
else
  upperlower_avg = (upper_avg + lower_avg)/2;
end

origin = floor(v_size/2) + 1;

z_mask = zeros(v_size);
% edge = cos( 2*pi*(z - v_size(3)/2 + edge_width_pix) / (4*edge_width_pix)).^2;
% edge = cos( 2*pi*(z - (v_size(3) - half_z_max_pix) + edge_width_pix) / (4*edge_width_pix)).^2;
edge = cos( 2*pi*(z - half_z_max_pix) / (4*edge_width_pix)).^2;

edge( find(z > half_z_max_pix + edge_width_pix| ...
           z < half_z_max_pix)) = 0;
z_mask = z_mask + edge;

% edge = cos( 2*pi*(z + v_size(3)/2 - edge_width_pix) / (4*edge_width_pix)).^2;
% edge = cos( 2*pi*(z + (v_size(3) - half_z_max_pix) - edge_width_pix) / (4*edge_width_pix)).^2;
edge = cos( 2*pi*(z + + half_z_max_pix) / (4*edge_width_pix)).^2;

edge( find(z < -(half_z_max_pix + edge_width_pix) | ...
           z > -(half_z_max_pix))) = 0;
z_mask = z_mask + edge;

z_mask(find( abs(z) <= half_z_max_pix)) = 1;

if(r_inner >= 0)
  inner_cylmask = 1-fuzzymask(v_size(1), 2, r_inner_pix-edge_width_pix, edge_width_pix/2);
else
  inner_cylmask = ones([v_size(1) v_size(2)]);
end
inner_cylmask = repmat(inner_cylmask, [1 1 v_size(3)]);

outer_cylmask = fuzzymask(v_size(1), 2, r_outer_pix, edge_width_pix/2);
outer_cylmask = repmat(outer_cylmask, [1 1 v_size(3)]);

vm = v.*inner_cylmask + (1-inner_cylmask) * inner_avg;
vm = vm.*outer_cylmask + (1-outer_cylmask) * outer_avg;
vm = vm.*z_mask + (1-z_mask) * upperlower_avg;

