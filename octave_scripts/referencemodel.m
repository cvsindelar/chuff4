function [modb modf ind]=referencemodel(antb,antf,fbm,np)

modt=zeros(128,128,2^np); 
modt1=modt;
modf=modt;
modb=modt;

mod1=zeros(128,128);
mod2=mod1;
ind=zeros(np,2^np); 

for i=1:np
    
    e=2^i; 
    f=e-2^(i-1);
    mod1(:,:)=antf(:,:,i); 
    mod2(:,:)=antb(:,:,i); 
    
    for j=f+1:e
        
    modt(:,:,j)=modt(:,:,j-f)+mod1;
    modb(:,:,j)=modt(:,:,j).*fbm;
    
    modt1(:,:,j)=modt1(:,:,j-f)+mod2;
    modf(:,:,j)=modt1(:,:,j).*fbm;
    
    ind(:,j)=ind(:,j-f); 
    ind(i,j)=1;
    
    end
end

return
