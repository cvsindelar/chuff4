; Generate tilted microtubule reference images
; NB: assume all volume dimensions are the same!

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Input parameters:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr(?reference_base? <ref_base>)                            ; Reference images should be at: <ref_base>_xxxxxx.spi
fr(?input_volume? <vol>)                                   ; Volume to be projected

($voxel_size) = rr(?voxel_size?)                           ; Voxel size for volume
($helical_repeat_dist) = rr(?helical_repeat_dist?)         ; Axial repeat distance in angstroms (volume)
($bin_factor, $oversample_factor) = rr(?bin_factor oversample_factor?)
($filter_resolution) = rr(?filter_resolution?) ; to filter volume
($final_box_dim) = rr(?final_box_dim?) ; optionally pad the final images
                                       ;  so they can be applied to larger
                                       ;  data images
($min_phi, $max_phi, $min_theta, $max_theta, $angle_step) = \
  rr(?min_phi max_phi min_theta max_theta angle_step?)         ; Euler angle range and step
($psi) = rr(?psi?)
($which_split, $n_splits) = rr(?psi?)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Calculate parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

($vol_x, $vol_y, $vol_z) = fi(<vol>, 12 2 1)

$error_exit = 0
if($vol_x .NE. $vol_y) $error_exit = 1
if($vol_x .NE. $vol_z) $error_exit = 1
if($error_exit.EQ.1) then
;  vm(echo 'ERROR: reference volume dimensions {****$vol_x} {****$vol_y} {****$vol_z} must be equal...')
;  en de
endif

downsample_centered(<vol>, _1, $bin_factor)

$binned_voxel_size = $voxel_size*$bin_factor
$mask_edge_width = 20/($binned_voxel_size)

$butterworth_edge = 1
$map_resolution_low = $filter_resolution + $butterworth_edge
$map_resolution_hi = $filter_resolution - $butterworth_edge
$butterworth_low = 0.5*(2*$binned_voxel_size)/$map_resolution_low
$butterworth_hi = 0.5*(2*$binned_voxel_size)/$map_resolution_hi

$butterworth_lopass_filter_code = 7
fq(_1, _2, $butterworth_lopass_filter_code, $butterworth_low $butterworth_hi)

$ref_pixel = $binned_voxel_size*$oversample_factor

($vol_x, $vol_y, $vol_z) = fi(_2, 12 2 1)
$vol_dim = $vol_x

; Pad volume to make sure the binned volume has even, integral dimensions
;  NOTE EVIL SPIDER BUG: 'pj 3q' has the WRONG ORIGIN if the volume dimensions
;   are odd!

$pd_xyz = 2*$oversample_factor*int( ($vol_z+2*$oversample_factor - 1) / (2*$oversample_factor) )
$pd_ox = 1 + int($pd_xyz/2) - int($vol_x/2)
$pd_oy = 1 + int($pd_xyz/2) - int($vol_y/2)
$pd_oz = 1 + int($pd_xyz/2) - int($vol_z/2)
pd(_2, _1, $pd_xyz $pd_xyz $pd_xyz, N, 0, $pd_ox $pd_oy $pd_oz)

$vol_dim = $pd_xyz

$proj_repeat_pixels = $helical_repeat_dist/$ref_pixel

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Generate document file for projection command "pj 3q"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$min_psi = $psi          ; Could make this more general, just handle filaments for now, and do only
$max_psi = $psi          ;   one polarity (because other polarity is related by symmetry)

; $max_i = int(($max_psi-$min_psi)/$angle_step) + 1

if($max_psi.eq.270) then
  $max_i = 2
else
  $max_i = 1
endif

$max_j = int(($max_theta-$min_theta)/$angle_step) + 1
$max_k = int(($max_phi-$min_phi)/$angle_step) + 1

$angle_index = 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Generate document file for projection command "pj 3q"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$max_size = ($max_theta-$min_theta+1)/$angle_step
if($max_size .LT. 1) then
  $max_size = 1
endif
$max_size = $max_size * 360

sd ic new(ref_angle, 5 $max_size)

if($n_splits .NE. 1) then
  sd ic new(ref_angle_split, 5 $max_size)
endif

do LB1 $i = 1, $max_i 
  $psi = $min_psi + 180*($i-1)

  do LB2 $j = 1, $max_j
    $theta = $min_theta + $angle_step*($j-1)

    do LB3 $k = 1, $max_k
      $angle_index = $angle_index + 1
      $phi = $min_phi + $angle_step*($k-1)

      ($angle_index,$psi,$theta,$phi) = sd ic(ref_angle)
    LB3
  LB2
LB1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Save reference projection parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if($which_split .EQ. 1) then
  de(<ref_base>/ref_angle)    ; Delete old angle file
  sd ic copy(ref_angle, <ref_base>/ref_angle)

  (1, $min_phi, $max_phi, $min_theta, $max_theta, $angle_step) = sd(<ref_base>/ref_params)
  (2, $helical_repeat_dist, $proj_repeat_pixels, $ref_pixel, $micrograph_pixel_size) = \
    sd(<ref_base>/ref_params)
  (3, $max_i, $max_j, $max_k) = sd(<ref_base>/ref_params)
endif

$tot_num_refs = $angle_index

if($n_splits .EQ. 1) then
  fr l(<stack_name> <ref_base>/ref_tot)
  fr l(<angle_doc> ref_angle)

  $first_ref = 1
  $last_ref = $tot_num_refs
else
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Split up the tasks if requested
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  fr l(<angle_doc> ref_angle_split)
  fr l(<stack_name> <ref_base>/ref_tot_split{***$which_split})

  $chunk_size = $tot_num_refs/$n_splits

  if($chunk_size .NE. int($chunk_size)) then
    $chunk_size = int($chunk_size) + 1
  endif

;;;;;;;;;;;;
; A final, combining step is specified by $which_split=0
;;;;;;;;;;;;
  if($which_split .EQ. 0) then
    ($exists) = iq fi(<ref_base>/ref_tot)
    if($exists .EQ. 1) then
      vm(/bin/rm <ref_base>/ref_tot.spi)
    endif

    $angle_index = 1

    vm(echo Merging {***$n_splits} files:)
    do LB5 $i = 1, $n_splits
      ($chunk_size) = fi x(<ref_base>/ref_tot_split{***$i}@, 26)
      vm(printf ' '{***$i})
      do LB6 $j = 1, $chunk_size
        cp(<ref_base>/ref_tot_split{***$i}@{*******$j}, \
           <ref_base>/ref_tot@{*******$angle_index})
           $angle_index = $angle_index + 1
      LB6
      vm(/bin/rm <ref_base>/ref_tot_split{***$i}.spi)
;      vm(/bin/rm <ref_base>/ref_angle_temp{***$i}.spi)
;      vm(/bin/rm <ref_base>/ref_angle_split{***$i}.spi)
    LB5

;;;;;;;;;;;;
; Quit if finished with the combining step
;;;;;;;;;;;;
    en de

  endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make a mini angle doc file for each split up job:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  $first_ref = 1 + ($which_split - 1)*$chunk_size
  $last_ref = $which_split*$chunk_size

  if($last_ref .GT. $tot_num_refs) then
    $last_ref = $tot_num_refs
  endif

(1, $chunk_size, $first_ref, $last_ref) = sd(debug_doc)

  $num_refs = $last_ref - $first_ref + 1

  $angle_index = $first_ref
  do LB7 $i = 1,$num_refs
    ($angle_index,$psi,$theta,$phi) = ud ic(ref_angle)
    ($i,$psi,$theta,$phi) = sd ic(ref_angle_split)
     $angle_index = $angle_index + 1
  LB7

endif

$num_refs = $last_ref - $first_ref + 1

;;;;;;;;;;;;
; Define the final image size
;;;;;;;;;;;;

if($final_box_dim .EQ. 0) then
  $final_dim = $vol_dim
else
  $final_dim = $final_box_dim
endif

$final_dim = int($final_dim/$oversample_factor/2) * 2*$oversample_factor

vm(echo Projecting {******$num_refs} images...)

ms(_10@, $vol_dim $vol_dim, $num_refs)

;;;;;;;;;;;;
; Dump one image just so we can see what it looks like
;;;;;;;;;;;;

$wi_xy = $final_dim
$wi_oxy = 1 + int($vol_dim/2) - int($wi_xy/2)
$pd_xy = $final_dim
$pd_oxy = 1 + int($pd_xy/2) - int($vol_dim/2)

if($which_split .EQ. 1) then
  pj 3q(_1,$vol_dim, 1-1, <angle_doc>, _10@******)
  if($final_dim .LE. $vol_dim) then
    wi(_10@1, _2, $wi_xy $wi_xy, $wi_oxy $wi_oxy)
    $mask_radius = $final_dim/2 - 1 - $mask_edge_width
  else
    pd(_10@1, _2, $pd_xy $pd_xy, N, 0, $pd_oxy $pd_oxy)
    $mask_radius = $vol_dim/2 - 1 - $mask_edge_width
  endif
  downsample_centered(_10@1, <ref_base>/ref_tot@1, $oversample_factor)  
endif

;;;;;;;;;;;;
; Now make the projections
;;;;;;;;;;;;

pj 3q(_1,$vol_dim, 1-$num_refs, <angle_doc>, _10@******)
  
; downsample to avoid aliasing artifacts

($exists) = iq fi(<stack_name>)
if($exists .EQ. 1) then
  vm(/bin/rm <stack_name>.spi)
endif

;;;;;;;;;;;;
; Bin and resize the projections
;;;;;;;;;;;;

do LB4 $i = 1, $num_refs

  if($final_dim .LE. $vol_dim) then
    wi(_10@{******$i}, _2, $wi_xy $wi_xy, $wi_oxy $wi_oxy)
    $mask_radius = $final_dim/2 - 1 - $mask_edge_width
  else
    pd(_10@{******$i}, _2, $pd_xy $pd_xy, N, 0, $pd_oxy $pd_oxy)
    $mask_radius = $vol_dim/2 - 1 - $mask_edge_width
  endif

  $ma_oxy = 1 + int($final_dim/2)
;  ma(_2, _3, $mask_radius 0, C, E, 0, $ma_oxy $ma_oxy, $mask_edge_width)
;  cp(_3, _2)

  downsample_centered(_2, _1, $oversample_factor)

  cp(_1, <stack_name>@{******$i})
LB4

; (1, $final_dim, $vol_dim, $mask_edge_width, $mask_radius) = sd(debug_doc)

en de
