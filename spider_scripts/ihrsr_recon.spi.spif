;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Common block for all 3 IHRSR sub-scripts: 
;  ihrsr_makeref.spi.spif, ihrsr_refine.spi.spif, ihrsr_recon.spi.spif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr(?image stack? <image_stack>)

; IMPORTANT - no spaces allowed between <volsuf> and name
fr(?volsuf?<volsuf>)

($Cn) = rr(?Cn)                ; point group symmetry (set to 1 for no Cn symmetry)
($dihedral) rr(?dihedral?)     ; 1 for dihedral symmetry, 0 otherwise
($win_dim) = rr(?win_dim?)     ; windowed img dimension (pixels) - used w/ Dn sym only

($cycle)   = rr(?cycle?)         ; cycle number

($rad_min) = fr(?rad_min?)                    ; Minimum radius for search (Angstroms)
($rad_max) = fr(?rad_min?)                    ; Maximum radius for search (Angstroms)

;;;;;;;;;;;;;;;;;;;;;;
; hsearch_lorentz and himpose parameters
; NOTE - Keep defaults (0.1) unless you really know what you're doing
;;;;;;;;;;;;;;;;;;;;;;

($inc_phi) = fr(?inc_phi?)
($inc_z) = fr(?inc_z?)

($pixel_size) = rr(?pixel_size?)

;;;;;;;;;;;;;;;;;;;;;;
; Search parameters
; NOTE - Following relation must be satisfied
;        $max_ring + $search_range < $img_dim/2 - 2
;
; NOTE - Keep nshift=1 unless you really know what you're doing
;        Rather than comparing against full projection, can divide
;        into slices. Divide original choice for $search_range by 
;        $nshift and then adjust $max_ring to satisfy relation
;;;;;;;;;;;;;;;;;;;;;;

($search_range)    = rr(?search_range?)     ; search range (pixels) divided by # nshifts
($max_ring)        = rr(?$max_ring?)  ; maximum ring (pixels)
($nshift)          = rr(?$nshift?)    ; # shifts used to generate ref projs (Ed's trick)
($max_inplane_dev) = rr(?$max_inplane_dev?)  ; maximum in-plane deviation
($ang_incr)        = rr(?$ang_incr?)  ; increment for azimuthal search (degrees)

;;;;;;;;;;;;;;;;;;;;;;
; Initial guess for helical symmetry parameters
;;;;;;;;;;;;;;;;;;;;;;

($rise) = rr(?$rise?)            ; axial rise - initial estimate (Angstroms)
($deltaphi) = rr(?$deltaphi?)      ; rotation per subunit - initial est (degrees)

;;;;;;;;;;;;;;;;;;;;;;
; Out-of-plane search parameters
; NOTE - Keep noutp = 0 to skip search over out-of-plane angles
;;;;;;;;;;;;;;;;;;;;;;

($noutp) = rr(?$noutp?)             ; Number of out of plane deviations in each direction
($outp_incr) = rr(?$outp_incr?)     ; Step size for out of plane deviations     

($mp) = rr(?mp?)  ;

$pixel_size_int = int($pixel_size)
$pixel_size_frac = int(10*($pixel_size - int($pixel_size)) + 0.5)

if($pixel_size_int .GT. 9) then
  fr l(<pixel_size_string> {**$pixel_size_int}.{*$pixel_size_frac}
else
  fr l(<pixel_size_string> {*$pixel_size_int}.{*$pixel_size_frac}
endif

;;;;;;;;;;;;;;;;
; Set number of Open MP threads
;;;;;;;;;;;;;;;;
md
  set mp
  $mp
;

; Numerical constants
$ninety    = 90.0
$mninety   = -90.0
$zero      = 0.0

; Turn off verbose ouput
md(VB OFF)

; Get rid of preexisting files that may cause problems
VM
rm -f newboxes.dat
VM
rm -f angsym.dat
VM
rm -f fangles_ran.dat
VM
rm -f ftzdsk_ran.dat
VM
rm -f refproj.dat
VM
rm -f tmp.dat
VM
rm -f angleproj.dat


; If new run, make sure we have symdoc file
if($cycle.eq.2) then
  VM
  rm -f symdoc.dat
  sd 1, $deltaphi, $rise
  symdoc
  sd 2, $deltaphi, $rise
  symdoc
  sd e ; Close document file
  symdoc
endif

; Calculate number of ref projs (360/n*$ang_incr) for Cn symm
$nazim = INT(360.0/($Cn*$ang_incr))

; Get size of images in stack file
fi x $nrow,$nsam      ; Extract number of rows and pixels/row from image
<image_stack>@001       ;   image name
(2,12)                  ;   nrow,nsam = header locations 2,12

$img_dim = $nrow

; Make sure that images are square
if($nrow.ne.$nsam) then
  vm(echo "Images in input stack are not square - terminating program")
  en d
endif

; Obtain number of images in stack file
fi x $nimg
  <image_stack>@
  (26)

; Generate symmetry file used for back projection
do $i=1,$Cn
  $angle = ($i-1)*360/$Cn
  sd $i,$zero,$angle,$zero
  angsym
enddo

sd e ; Close document file
  angsym

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; End of common block
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DO LB11 $k=1,$nimg
  UD IC $k,$best_proj,$cc,$inplane_rot,$xshift,$yshift,$img_num
    fparamzds+{***$cycle}

  ; Shift in-plane rotation to be in range 0-360
  if($inplane_rot.gt.360)$inplane_rot = $inplane_rot -360.
  if($inplane_rot.lt.0)$inplane_rot = 360. + $inplane_rot

  ; Eliminate ptles outside range of allowed in-plane deviation
  ; Allow deviations from both 0 and 180 degress
  if($inplane_rot.lt.(180.-$max_inplane_dev))then
    if($inplane_rot.gt.$max_inplane_dev) go to LB11
  endif
  if($inplane_rot.gt.(180.+$max_inplane_dev))then
    if($inplane_rot.lt.(360.-$max_inplane_dev)) go to LB11
  endif

  ; If we made it this far, ptle within allowed in-plane deviation
  $ngood=$ngood+1
  sd $ngood,$best_proj,$cc,$inplane_rot,$xshift,$yshift,$ngood
  fgoodparamzds+{***$cycle}

  ; --- Converting index of the best projection to angles and shifts ---

  ; Figure out what block (starting from zero) we are in.
  ; All values within a block have same out-of-plane angle
  $block = INT(($best_proj-1)/($nazim*$nshift))

  ; Calculate out-of-plane angle
  $outp = $outp_incr*($block-$noutp)

  ; Figure out index within a block
  $best_in_block  = $best_proj - $block*$nazim*$nshift

  ; Calculate "composite" y-shift accounting for shifts in ref projs
  $jshft=INT(($best_in_block-1)/$nazim)
  $composite_yshift=$yshift - $jshft*$rise/($pixel_size*$nshift) 

  ; Calculate azimuthal angle 
  $azimuth=($best_in_block - $jshft*$nazim -1.)*$ang_incr

  ; Combine two sets of rotations
   sa e, $phi,$theta,$psi
     $zero,$azimuth,$zero
     $ninety,$outp,$mninety

  ; Write new document file containing angles

  sd $ngood,$psi,$theta,$phi
    fangles{***$cycle}

  sd $ngood,$best_proj,$outp,$azimuth
    fgoopazim{***$cycle}


  ; Write new stack file w/ images corrected for shift and in-plane rotation
  RT SQ                             ; Rotate and shift - quadratic interpolation
    <image_stack>@{******$img_num}  ;   input image
    newboxes@{******$ngood}         ;   output image
    $inplane_rot                   ;   rotation
    $xshift,$composite_yshift     ;   shift

LB11

ud ice ; Terminate access to in-core image of document file
  fparamzds+{***$cycle}

sd e ; Close document file
  fangles{***$cycle}

sd e ; Close document file
  fgoopazim{***$cycle}

sd e ; Close document file
  fgoodparamzds+{***$cycle}

; Reconstruct map using back projection
bp 3f                  ; back projection, 3D Fourier space 
  newboxes@******	 ;  template for image files	     
  (1-$ngood)		 ;  range of image numbers to use    
  fangles{***$cycle}	 ;  projection angles file	     
  angsym		 ;  apply point group symmetry
  ftzdsk{***$cycle}	 ;  reconstructed volume             

; Now the hard part, impose helical symmetry on threed***
; phistart, zstart (initial guesses at symmetry) 

if($cycle.gt.2) then
VM
hsearch_lorentz ftzdsk{***$cycle}.dat symdoc.dat <pixel_size_string> <rad_min> <rad_max> <inc_phi> <inc_z>
endif

VM
himpose ftzdsk{***$cycle}.dat symdoc.dat <volsuf>{***$cycle}.dat <pixel_size_string> <rad_min> <rad_max>

if($dihedral.eq.1) then ; Apply dihedral symmetry

  ; Get rid of preexisting files that may cause problems
  VM
  rm -f ccdoc.dat
  VM
  rm -f ccdocs.dat
  VM
  rm -f <volsuf>{***$cycle}r.dat
  VM
  rm -f <volsuf>{***$cycle}rs.dat

  rt 90                   ; Generate new volume rotated by 90 degrees
    <volsuf>{***$cycle}  ;   input volume
    <volsuf>{***$cycle}r ;   output (flipped) volume
    (1,$img_dim)         ;   first and last x column
    (1,$img_dim)         ;   first and last y column
    (1,$img_dim)         ;   first and last z column
    x                     ;   rotation axis
    (180)                 ;   rotation angle

  ; Calculate location of top left corner of windowed volumes
  $corner = ($img_dim-$win_dim)/2 + 1

  wi                                ; Window original volume
    <volsuf>{***$cycle}            ;   input volume
    _1                              ;   windowed volume
    ($win_dim,$win_dim,$win_dim) ;   window size
    ($corner,$corner,$corner)    ;   top left coordinates

  wi                                ; Window flipped volume
    <volsuf>{***$cycle}r  	    ;   input volume	    
    _3			  	    ;   windowed volume	    
    ($win_dim,$win_dim,$win_dim) ;   window size
    ($corner,$corner,$corner)    ;   top left coordinates

  ; Get best CC for each choice of dihedral angle (1 degree increment)
  do $phi=1,360 
    RT 3D                    ; Rotate windowed/flipped volume about azimuth
      _3                     ;   input volume
      _2                     ;   output volume
      $phi,$zero,$zero    ;   phi, 0, 0

    CC N                     ; Normalized cross correlation between ...
      _1                     ;   windowed original volume
      _2                     ;   windowed, flipped, rotated volume
      _4                     ;   cross correlation coefficients (output)

    PK 3 $ix,$iy,$iz,$fx,$fy,$fz,$h ; 3D peak search
      _4                                   ;   input file (CCs)
      +                                    ;   look for maxima
      (1,0)                                ;   1 peak, w/o override
      N                                    ;   N = simple peak search
      N                                    ;   not sure why this is needed

    SD $phi,$h,$fx,$fy,$fz  ; Write peak height and coors (key=$phi)
      ccdoc                      ;   to file ccdoc.dat
  enddo

  sd s        ; Sort document file
    ccdoc     ;   input file name
    (1,360)   ;   first key, last key
    (1)       ;   sort based on column 1
    (4,4)     ;   write $fz to the new doc file
    ccdocs    ;   output file name (sorted file)

  sd e        ; Close document
    ccdoc
  sd e        ; Close document
    ccdocs

  ; Get information from last line of sorted document file
  $key=360             
  ud $key,$phi,$junk,$fz
    ccdocs

  ; impose shift $fz and rotation $phi on flipped volume
  RT 3D                    ; Rotate volume about azimuth
    <volsuf>{***$cycle}r  ;   input volume (flipped)
    _1                     ;   output volume (rotated, flipped)
    $phi,$zero,$zero    ;   phi, 0, 0
  SH                       ; Shift volume
    _1                     ;   input volume (defined above)
    <volsuf>{***$cycle}rs ;   output volume (shifted, rotated, flipped)
    $zero,$zero,$fz     ;   xshift, yshift, zshift

  ; Add togther original volume and shifted, rotated, flipped volume
  ad                       ; Add volumes
    <volsuf>{***$cycle}   ;   first input volume
    <volsuf>{***$cycle}rs ;   second input volume
    _1                     ;   output volume
    *                      ;   done

  ; Rescale density of the new volume (divide by 2)
  ar              ; Arithmetic operation
    _1            ;   input volume (summed volume)
    _2            ;   output volume (averaged volume)
    (0.5*p1)      ;   operation (mutliply by 0.5)
 
  ; window out the ends to account for shift of flipped
  ; volume relative to the original volume

  $zdim = $img_dim-int(abs($fz))
  $top_left_z = int(abs($fz)) + 1

  if($fz.ge.0.)then
    wi                            ; Window volume
      _2                          ;  input (averaged) volume
      _1                          ;  output volume 
      $img_dim,$img_dim,$zdim  ;  window dimensions
      (1,1,$top_left_z)          ;  top left corner
  else
    wi                            ; Window volume
      _2                          ;  input (averaged) volume
      _1                          ;  output volume
      $img_dim,$img_dim,$zdim  ;  window dimensions
      (1,1,1)                     ;  top left corner
  endif

  $top_left_z=int(abs($fz)/2.)
  pd                              ; Pad volume out to original dimensions
    _1                            ;  input volume (defined above)
    s<volsuf>{***$cycle}         ;  output volume
    $img_dim,$img_dim,$img_dim ;  padded volume dimensions
    b                             ;  use border value for padding
    (1,1,$top_left_z)            ;  top left coordinates

  ; now cleanup
  UD E
  ccdocs
  VM
  rm -f ccdoc.dat
  VM
  rm -f ccdocs.dat
  VM
  rm -f <volsuf>{***$cycle}r.dat
  VM
  rm -f <volsuf>{***$cycle}rs.dat

endif ; End block of code for dihedral symmetry

enddo ; End loop over cycles

; Cleaning up files
VM
rm -f newboxes.dat
VM
rm -f refproj.dat
VM
rm -f tmp.dat
VM
rm -f angleproj.dat

en d
