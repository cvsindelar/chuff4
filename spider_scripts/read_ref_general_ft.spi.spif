;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Read in reference image, given a base name for the files, and a set of 3 orientation angles phi, theta, psi
;   Assumes all reference images are at psi = 90; psi = 270 are generated by symmetry.
;   Assumes reference images only go from phi = 0 to phi = 179; phi = 180 to 359 are generated by symmetry.
;
;   Special feature:  this routine uses the tilt angle theta to scale the image
;     so the output height is exactly spanned by two dimer repeats.  This is to
;     match the convention of the alignment script, which generates averaged 
;     dimer images whose repeat length *also* agrees with the box height.
;
;     What this means is that tilted microtubule images will be scaled up some relative
;       to the untilted images.
;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Read input parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr(?ref base? <ref_shortbase>)
fr(?ref lib? <ref_lib>)
fr(?ref lib ft? <ref_lib_ft>)
fr(?output file? <output_file>)
fr(?output ft? <output_ft>)

($final_x, $final_y) = rr(?final_x final_y?)        ; Dim's of final padded image, with the height of ONE dimer
($psi,$theta,$phi) = rr(?psi theta phi?)           ; Euler angles of reference projection
($min_phi, $max_phi, $min_theta, $d_angle) = rr(?min_phi max_phi min_theta d_angle?)

($himem) = iq fi(<ref_lib>)

;;;;;;;;;;;;;;;;;;;;;;;;;;
; Define file name-- base name for reference image files...
;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr l(<ref_base> <ref_shortbase>/ref)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Calculate the reference index
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$phi_window = int(($max_phi-$min_phi)/$d_angle) + 1

$ref_index = int(($phi - $min_phi)/$d_angle + 0.51) + 1           ; angle index corresponding to phi

if($ref_index.GT.int(359.99/$d_angle) + 1) then                   ; handle wraparound if phi >= 360
  $ref_index = $ref_index - (int(359.99/$d_angle) + 1)            ;  (or even if phi is slightly less than 360, but
                                                                  ;   would round up to 360)
  $phi = $phi - 360
endif

$ref_index = $ref_index + $phi_window*int( ($theta -$min_theta)/$d_angle + 0.51)  ; add the tilt index

($max_i) = ud n(<ref_base>_angle)

if($ref_index.GT.$max_i) then
vm(echo ERROR: programming error in read_ref_general.spi.spif)
  $psi
  $theta
  $phi
  $ref_index
  en
endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Read in the reference image
;;;;;;;;;;;;;;;;;;;;;;;;;;;

if($himem .EQ. 0) then
  ($ref_x, $ref_y) = fi(<ref_base>_000001, 12 2)                 ; ref image size
else
  ($ref_x, $ref_y) = fi(<ref_lib>, 12 2)                 ; ref image size
;  ($ref_x, $ref_y) = fi(<ref_lib>, 12 2)                 ; ref image size
endif

if(int($ref_x/2).NE.$ref_x/2) then
  vm(echo ERROR: reference images must have even dimensions....)
  en de
endif
if(int($ref_y/2).NE.$ref_y/2) then
  vm(echo ERROR: reference images must have even dimensions....)
  en de
endif

$pd_x = $final_x
$pd_y = $final_y
$pd_ox = 1 + int($pd_x/2) - int($ref_x/2)
$pd_oy = 1 + int($pd_y/2) - int($ref_y/2);

if($himem .EQ. 0) then
  cp(<ref_base>_{******$ref_index}, _70)
else
  ps z(<ref_lib>, _70, $ref_index)
endif

pd(_70, _71, $pd_x $pd_y, N, 0, $pd_ox $pd_oy)

if($psi.EQ.270) then        ; check whether psi is 90 or 270
;;;;;
; Rotate upside-down for psi=270
;;;;;
  rotate2d_90(_71, <output_file>, 180)
else
  cp(_71, <output_file>)
endif

;if($himem .NE. 0) then
;  cp(<ref_lib_ft>@{******$ref_index}, <output_ft>)
;endif

re
;
