; Generate tilted microtubule reference images
; NB: assume all volume dimensions are the same!

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Input parameters:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr(?reference_base? <ref_base>)                            ; Reference images should be at: <ref_base>_xxxxxx.spi
fr(?input_volume? <vol>)                                   ; Volume to be projected

($voxel_size) = rr(?voxel_size?)                           ; Voxel size for volume
($helical_repeat_dist) = rr(?helical_repeat_dist?)         ; Axial repeat distance in angstroms (volume)
($bin_factor) = rr(?bin_factor?)                           ; Binning size
($mt_diameter) = rr(?mt_diameter?)                         ; diameter of a microtubule-kin complex (generous, in Angstroms)
($min_phi, $max_phi, $min_theta, $max_theta, $angle_step) = \
  rr(?min_phi max_phi min_theta max_theta angle_step?)         ; Euler angle range and step

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Calculate parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

($vol_x, $vol_y, $vol_z) = fi(<vol>, 12 2 1)

$error_exit = 0
if($vol_x .NE. $vol_y) $error_exit = 1
if($vol_x .NE. $vol_z) $error_exit = 1
if($error_exit.EQ.1) then
  vm(echo 'ERROR: reference volume dimensions {****$vol_x} {****$vol_y} {****$vol_z} must be equal...')
  en de
endif

$vol_dim = $vol_x

; Pad volume to make sure the binned volume has even, integral dimensions
;  NOTE EVIL SPIDER BUG: 'pj 3q' has the WRONG ORIGIN if the volume dimensions
;   are odd!

$pd_xyz = 2*$bin_factor*int( ($vol_dim+2*$bin_factor - 1) / (2*$bin_factor) )
$pd_oxyz = 1 + int($pd_xyz/2) - int($vol_dim/2)
pd(<vol>, _2, $pd_xyz $pd_xyz $pd_xyz, N, 0, $pd_oxyz $pd_oxyz $pd_oxyz)

$vol_dim = $pd_xyz
$bin_vol_dim = $vol_dim/$bin_factor

$proj_repeat_pixels = $helical_repeat_dist/$voxel_size / $bin_factor
$volume_repeat_pixels = int($helical_repeat_dist/$voxel_size + 0.5) ; Assumes the user gave "good" numbers

$mt_diameter_pixels = int($mt_diameter/$voxel_size / $bin_factor)

$ref_pixel = $voxel_size/$bin_factor   ; voxel size of initial volume (specifies the resolution the images will have)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Save reference projection parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(1, $min_phi, $max_phi, $min_theta, $max_theta, $angle_step) = sd(<ref_base>_params)
(2, $helical_repeat_dist, $proj_repeat_pixels) = sd(<ref_base>_params)

downsample_centered(_2, _1, $bin_factor $bin_factor $bin_factor)
($vol_dim) = fi(_1, 12)        ; NB: assume all vol dimensions are the same!

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Generate document file for projection command "pj 3q"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$min_psi = 90          ; Could make this more general, just handle filaments for now, and do only
$max_psi = 90          ;   one polarity (because other polarity is related by symmetry)

; $max_i = int(($max_psi-$min_psi)/$angle_step) + 1

if($max_psi.eq.270) then
  $max_i = 2
else
  $max_i = 1
endif

$max_j = int(($max_theta-$min_theta)/$angle_step) + 1
$max_k = int(($max_phi-$min_phi)/$angle_step) + 1

(3, $max_i, $max_j, $max_k) = sd(<ref_base>_params)

de(<ref_base>_angle)    ; Delete old angle file

$angle_index = 1

do LB1 $i = 1, $max_i 
  $psi = $min_psi + 180*($i-1)

  do LB2 $j = 1, $max_j
    $theta = $min_theta + $angle_step*($j-1)

    do LB3 $k = 1, $max_k
      $phi = $min_phi + $angle_step*($k-1)
      ($angle_index,$psi,$theta,$phi) = sd(<ref_base>_angle)
      $angle_index = $angle_index + 1
    LB3
  LB2
LB1

sd e(<ref_base>_angle)

($num_refs) = ud n(<ref_base>_angle)                  ; Get number of reference images

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Generate document file for projection command "pj 3q"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vm(echo Projecting {******$num_refs} images...)

pj 3q(_1,$vol_dim, 1-$num_refs, <ref_base>_angle, <ref_base>_******)

en de
