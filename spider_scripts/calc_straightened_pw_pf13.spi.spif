fr(?micrograph? <micrograph>)
fr(?box_header? <box_header>)
fr(?align_doc? <align_doc>)                ; where radon scaling/orientation files are contained
fr(?job_dir? <job_dir>)                    ; where current analysis output is contained

($helical_repeat_distance) = rr(?helical_repeat_distance?) ; In Angstroms
($box_pixel) = rr(?micrograph_pixel_size?)     ; After scaling, as determined by the radon script
($mt_radius) = rr(?microtubule radius?) ; After scaling, as determined by the radon script

$repeat_pixels = $helical_repeat_distance/$box_pixel
$mt_width_pixels = 2*$mt_radius / $box_pixel

($num_boxes) = ud n(<box_header>_doc)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; First find the shift relationship between boxes 1 and 2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(1, $box_x, $box_y, $box_dim) = ud(<box_header>_doc)   ; Get box coords
wi(<micrograph>, _1, $box_dim $box_dim, $box_x $box_y)
(2, $box_x2, $box_y2, $box_dim) = ud(<box_header>_doc)
wi(<micrograph>, _1, $box_dim $box_dim, $box_x $box_y)

$box_dx = $box_x2-$box_x
$box_dy = $box_y2-$box_y
$dx = sqrt( $box_dx*$box_dx + $box_dy*$box_dy)
; vm(echo box_seg_angle: {*****$box_seg_angle} pk_fits: {****$pk_fitx} {****$pk_fity} dx: {*****$dx})

$avg_mag_scale = 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Now piece all the boxes together...
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$pd_y = int($dx * $num_boxes * $avg_mag_scale)
bl(_1, $mt_width_pixels $pd_y, N, 0)
; bl(_11, $mt_width_pixels $pd_y, N, 0)

; (1, $pd_y, $mt_width_pixels, $num_boxes, $dx, $avg_mag_scale) = sd(debug_doc)

do LB3 $i = 1, $num_boxes

  ($i, $rot_angle, $repeat_scale, $best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi,$mag_scale,$dimer_repeat) = ud(<align_doc>)

  $repeat_scale = 1

  if($i.EQ.1) then
    $insert_position = ($num_boxes - 1)*$dx*$avg_mag_scale + 1.0001
    $seg_height = int($dx + 2)*$avg_mag_scale
  endif

  $int_position = int($insert_position)
  $frac_position = $insert_position - $int_position

  ($i, $box_x, $box_y, $box_dim) = ud(<box_header>_doc)
  wi(<micrograph>, _23, $box_dim $box_dim, $box_x $box_y)
  ($ref_max, $ref_min, $ref_avg, $ref_sdev) = fs(_23)      ; File statistics

  ar(_23, _21, (P1 - $ref_avg)/$ref_sdev)        ; Arithmetic transformation
  ra(_21, _22)
  rt sq(_22, _2, $rot_angle $repeat_scale, $best_boxfitx $frac_position)

; Window out the microtubule only
  $wi_x = 1 + int($box_dim/2) - int($mt_width_pixels/2)
  $wi_y = 1 + int($box_dim/2) - int($seg_height/2)
  wi(_2, _3, $mt_width_pixels $seg_height, $wi_x $wi_y)

; Shift the "empty" solvent region to the center and window that out
;  $shift = $box_dim/2
;  sh(_2, _4, $shift 0)
;  $empty_width = $box_dim - $mt_width_pixels
;  wi(_4, _5, $empty_width $seg_height, $wi_x $wi_y)

; Insert windows into the final pictures
  in(_3, _1, 1 $int_position)
;  in(_5, _11, 1 $int_position)

  $frac_position = $frac_position*10000
  if($frac_position .GE.9999.5) then
    $int_position = $int_position - 1
    $frac_position = $frac_position + 0.5 - 10000
  endif

  vm(echo {******$int_position}.{****$frac_position})

  $insert_position = $insert_position - $dx*$repeat_scale
LB3

;ra(_1, _2)
;($ref_max, $ref_min, $ref_avg, $ref_sdev) = fs(_2)      ; File statistics
;ar(_2, _1, (P1 - $ref_avg)/$ref_sdev)        ; Arithmetic transformation
;ra(_11, _2)
;($ref_max, $ref_min, $ref_avg, $ref_sdev) = fs(_2)      ; File statistics
;ar(_2, _11, (P1 - $ref_avg)/$ref_sdev)        ; Arithmetic transformation

; This horrible expression gets the next higher power of 2 from $pd_y:
$pd_y_pw2 = exp( log(2)/log(exp(1)) * (int(log($pd_y)/log(2))+1) )
if($pd_y_pw2 .LT. 1024) then
  $pd_y_pw2 = 1024
endif

; And this is the downsampling to use from the horrible result, above,
;   to produce a 1024x1024 image:
$downsample_factor = exp( log(2)/log(exp(1)) * (int(log($pd_y)/log(2))+ 1 - 10))

if($downsample_factor.LT.1) then
  $downsample_factor = 1
endif

pd(_1, _9, $mt_width_pixels $pd_y_pw2, N, 0, 1 1)
; pd(_11, _10, $mt_width_pixels $pd_y_pw2, N, 0, 1 1)
cp to tif(_9, stackboxes)

;$regular_height = 80*int($pd_y/80)
;wi(_1, _13, $mt_width_pixels $regular_height, 1 1)
;pw(_13, _14)
;cp to tif(_14, noo)

pw(_9, _2)
; pw(_10, _4)

dc s(_2, _3, 1 $downsample_factor)
; dc s(_4, _5, 1 $downsample_factor)

($temp_y) = fi(_3, 2)
ip(_3, _4, $temp_y $temp_y)
; ip(_5, _6, $temp_y $temp_y)

$wi_x = 1024 ; 700
$wi_ox = 1 + int($temp_y/2) - int($wi_x/2)
wi(_4, _3, $wi_x $wi_x, $wi_ox $wi_ox)
; wi(_6, _5, $wi_x $wi_x, $wi_ox $wi_ox)

cp to tif(_3, <job_dir>_straightened_pw)
en de
