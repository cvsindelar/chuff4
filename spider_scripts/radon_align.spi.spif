;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Note that this finds the scaling that bring the 40A layer lines to the 40th pixel
;    (for a box 1600A long), by averaging the information from all the boxes in an image.
;  However, the scale factor found this way is not as accurate as theoretically possible, it gets
;    maybe +/- 1-2% accuracy...
; Note also: this method is insensitive to whether density is inverted or not!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr(?micrograph? <micrograph>)
fr(?box_header? <box_header>)
fr(?job_dir? <job_dir>)                    ; where current analysis output is contained
($box_num) = rr(?box number?)              ; zero means all boxes
($box_pixel) = rr(?box pixel size?)        ; Nominal angstroms per pixel in initial box files
($helical_repeat_dist) = rr(?helical_repeat_dist?) ; Around 82 A for MTs
($scanner_pixel_size) = rr(?scanner_pixel_size?)
($target_repeat_pixels) = rr(?target_repeat_pixels?) ; defines the scale factor that will be calculated
($radon_window) = rr(?radon_window?)       ; orientational search window in degrees
($debug) = rr(?write_debug_info?)          ; write out diagnostic images, etc.

if($box_num.eq.0) then      ; Set the upper and lower limits of the box analysis loop
  $begin_box = 1
  ($end_box) = ud n(<box_header>_doc)
else
  $begin_box = $box_num
  $end_box = $box_num
endif

($file_exists) = iq fi(<job_dir>/radon_scale_doc)
if($file_exists.EQ.1) then
  vm(echo skipping processed microtubule...)
  if($debug .EQ. 0) then
    en de                          ; if user requested debugging, always redo the calculation
  endif
endif

;;;;;;;;;;;;;;;;;;;;;;;;;;
; Parameters (will depend on pixel sampling size)
;;;;;;;;;;;;;;;;;;;;;;;;;;

$layer_line_resolution = $helical_repeat_dist/2  ; For MTs: 40 A layer line is used for scale searching

;;;;;;;;;;;;;;;;;;;;;;;;
; Read box parameters
;;;;;;;;;;;;;;;;;;;;;;;;

(1, $box_x, $box_y, $box_dim) = ud(<box_header>_doc)     ; Get box dimensions

;;;;;;;;;;;;;;;;;;;;;;;;;;
; Algorithm parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;

$layer_line_radius = 80
$pad_box_size = 1000
$radon_sample = 1                        ; angular sampling of Radon transform 

;;;;;;;;;;;;;;;;;;;;;;;;
; Read box segment angle, so we can rotate each box to approximately vertical before doing the radon alignment
;;;;;;;;;;;;;;;;;;;;;;;;

(1, $box_seg_angle) = ud(<box_header>_angle_doc)
$box_seg_angle = 90 + $box_seg_angle

;;;;;;;;;;;;;;;;;;;;;;;;
; Calculate some algorithm parameters
;;;;;;;;;;;;;;;;;;;;;;;;

$adjusted_box_pixel = $layer_line_resolution * $layer_line_radius/$pad_box_size;

                      ;   This is a 'relative' pixel size; i.e. if it is 0.5, then pixels
                      ;    are twice as fine as the original.

$first_iteration = 1; ; this flag says whether we are in the first iteration of the loop...

$cur_angle = 0

;;;;;;;;;;;;;;;;;;;;;;
; The big loop: for each box
;;;;;;;;;;;;;;;;;;;;;;

do LB2 $i=$begin_box,$end_box

  if($i.NE.1) then
    $prev_angle = $cur_angle
  endif

  ($i, $box_x, $box_y, $box_dim) = ud(<box_header>_doc)   ; Get box coordinates
;;;;;;;;;;;;;;;;;;;;;;;
; Get current in-plane angle from coordinates of current and next box
;;;;;;;;;;;;;;;;;;;;;;;
  $temp = $i + 1
  if($temp.LE.$end_box) then
    ($temp, $box_x1, $box_y1) = ud(<box_header>_doc)
    $dx = $box_x1 - $box_x
    $dy = $box_y1 - $box_y
    if($dx.EQ.0) then
      if($dy.GT.0) then
        $cur_angle = 90
      else
        $cur_angle = -90
      endif
    else
      $cur_angle = ata($dy/$dx)
      if($dx.LT.0) then
        if($dy.GE.0) then
          $cur_angle = $cur_angle + 180
        else
          $cur_angle = $cur_angle - 180
        endif
      endif
    endif
    $cur_angle = $cur_angle + 90
  else
    $cur_angle = $prev_angle
  endif

;;;;;;;;;;;;;;;;;;;;;;;
; Get next in-plane angle
;;;;;;;;;;;;;;;;;;;;;;;
  $temp = $i + 2
  if($temp.LE.$end_box) then
    ($temp, $box_x2, $box_y2) = ud(<box_header>_doc)
    $dx = $box_x2 - $box_x1
    $dy = $box_y2 - $box_y1
    if($dx.EQ.0) then
      if($dy.GT.0) then
        $next_angle = 90
      else
        $next_angle = -90
      endif
    else
      $next_angle = ata($dy/$dx)
      if($dx.LT.0) then
        if($dy.GE.0) then
          $next_angle = $next_angle + 180
        else
          $next_angle = $next_angle - 180
        endif
      endif
    endif
    $next_angle = $next_angle + 90
  else
    $next_angle = $cur_angle
  endif

  if($i.EQ.1) then
    $prev_angle = $cur_angle
  endif

;;;;;;;;;;;;;;;;;;;;;;;
; Address wrap-around issues i.e. when the in-plane angle switches from near -180 to near +180
;;;;;;;;;;;;;;;;;;;;;;;

  if($cur_angle - $box_seg_angle.GT.180) then
    $cur_angle = $cur_angle - 360
  else 
    if($cur_angle - $box_seg_angle.LT.-180) then
      $cur_angle = $cur_angle + 360
    endif
  endif

  if($next_angle - $box_seg_angle.GT.180) then
    $next_angle = $next_angle - 360
  else 
    if($next_angle - $box_seg_angle.LT.-180) then
      $next_angle = $next_angle + 360
    endif
  endif

;;;;;;;;;;;;;;;;;;;;;;;
; Now set box_seg_angle to the median of (prev_angle, cur_angle, next_seg_angle)
;  (using "poor man's sorting")
;;;;;;;;;;;;;;;;;;;;;;;

;  vm(echo Box {****$i}: {*****$prev_angle} {*****$cur_angle} {*****$next_angle})

  if($cur_angle.GE.$prev_angle) then
    if($cur_angle.LE.$next_angle) then
      $box_seg_angle = $cur_angle
    endif
  endif

  if($cur_angle.GE.$next_angle) then
    if($cur_angle.LE.$prev_angle) then
      $box_seg_angle = $cur_angle
    endif
  endif

  if($prev_angle.GE.$cur_angle) then
    if($prev_angle.LE.$next_angle) then
      $box_seg_angle = $prev_angle
    endif
  endif

  if($prev_angle.GE.$next_angle) then
    if($prev_angle.LE.$cur_angle) then
      $box_seg_angle = $prev_angle
    endif
  endif

  if($next_angle.GE.$cur_angle) then
    if($next_angle.LE.$prev_angle) then
      $box_seg_angle = $next_angle
    endif
  endif

  if($next_angle.GE.$prev_angle) then
    if($next_angle.LE.$cur_angle) then
      $box_seg_angle = $next_angle
    endif
  endif

;;;;;;;;;;;;;;;;;;;;;;
; Rotate the box file and clip to 500x500
;;;;;;;;;;;;;;;;;;;;;;

  $scale = $box_pixel/$adjusted_box_pixel

  $rtsq_scale = $scale
  $dcs_scale = 1
  do LB4 $k = 1, 10
    if($rtsq_scale.LT.0.5) then
      $rtsq_scale = $rtsq_scale * 2
      $dcs_scale = $dcs_scale * 2
    else
      goto LB5
    endif
  LB4
  LB5

;  downsample_centered(<data_dir>/box_{****$i}, _1, $dcs_scale)
  wi(<micrograph>, _2, $box_dim $box_dim, $box_x $box_y)
  downsample_centered(_2, _1, $dcs_scale)

  ra(_1, _2)
  rt sq(_2, _1, $box_seg_angle $rtsq_scale, 0 0)             ; rotate, shift (and scale) in one step

  ($wi_x) = fi(_1, 12)

  if($rtsq_scale.LT.1)  then
    $wi_x = int($box_dim/$dcs_scale*$rtsq_scale)
    $wi_ox = 1 + int($box_dim/$dcs_scale/2) - int($wi_x/2)

    wi(_1, _2, $wi_x $wi_x, $wi_ox $wi_ox)
    cp(_2, _1)
  endif

  $pd_topleft=1 + int($pad_box_size/2)-int($wi_x/2) ; convert to upper left box coord's for 'pd' command

; Embed image back in original-sized box
  pd(_1, _2, $pad_box_size $pad_box_size, N, 0, $pd_topleft $pd_topleft)

  pw(_2, _3)                        ; Power spectrum

  $radon_pw_size = 2 * (3/2 * $layer_line_radius)     ; Window size to box out the power spectrum before Radon transform
                            ;   Info beyond the 40A layer line is discarded (would provide a weak signal at best...)
                            ;   NB Radon Transform will stop if the size is bigger than 400...

  $radon_pw_topleft = 1 + int($pad_box_size/2) - int($radon_pw_size/2)  ; Upper left corner of power spectrum window used for Radon
; Window out the center part (only 40A layer line and lower resolutions)
  wi(_3, _4, $radon_pw_size $radon_pw_size, $radon_pw_topleft $radon_pw_topleft)

  $mask_center = $radon_pw_size/2+1                  ; center of mask to eliminate F00 in power spectrum 
  $mask_radius = INT($pad_box_size/24+0.5)          ; radius of mask ((box size)/24, seems arbitrary...)
  $gauss_falloff = $mask_radius/3                    ; length of Gaussian falloff for mask

; Mask the center (F00) off ($mask_radius is inner radius, mask stuff inside this (0.0 for outer radius; not used))
  ma(_4, _5, 0.0 $mask_radius, G, E, 0.0, $mask_center $mask_center, $gauss_falloff)

  $circ_mask_radius = INT($radon_pw_size/2)               ; mask radius ($radon_pw_size/2, goes to edge of box)

; Radon transform: parameters are: angular sampling increment;
;                                  output file (NB goes from -90 degrees to +89 degrees, so NROW/2 + 1 is 0 degrees);
;                                  x-dimension of output file (use same dimensions as input file here);
;                                  radius of circular mask;  center offset; 
;                                  "Average" threshhold (all values below avg. set to 0)

  rm 2dn(_5, $radon_sample, _6, $radon_pw_size, $circ_mask_radius, 0 0, A)

  $radon_window_rows = int($radon_window/$radon_sample)                 ; Number of Radon rows corresponding to a 30-degree window
  $radon_bins = 180/$radon_sample                     ; size of the whole radon angular bin range (if not an integer, we're in trouble!)
  $radon_window_top = 1 + int($radon_bins/2) - int($radon_window_rows/2) ; position of the top of the 30-degree window in the transform

  $wi_x = $radon_pw_size
  $wi_y = $radon_window_rows
  $wi_ox = 1 + int($radon_pw_size/2) - int($wi_x/2)
  $wi_oy = $radon_window_top;

; take a 30-degree window around 0 degrees in the Radon transform
  wi(_6, _7, $wi_x $wi_y, $wi_ox $wi_oy)

  $wi_x = 20
  $wi_ox = 1 + int($radon_pw_size/2) - int($wi_x/2)

; take a 30-degree window around 0 degrees in the Radon transform
  wi(_6, _11, $wi_x $wi_y, $wi_ox $wi_oy)

; Search for Radon peak: Params are: input file; # of peaks,center origin override (0/1); 
;                                     ellipse axes (x,y) for CGR calculation; positivity enforced? (Y/N); 
;                                     minimum peak neighbor distance; edge exclusion width; document file name
; NB pk dc does not write out the center of gravity results in the doc file :-(
; It outputs the highest peak position before center of gravity calculation (?!)

  ($pk_x,$pk_y,$pk_max,$pk_ratio,$pk_fitx,$pk_fity,$pk_fitmax) = \
    pk c(_11, 7 0, 1 5, n, 6, 0 0)

  $radon_rot=$pk_fity*$radon_sample                  ; convert radon row to actual angle...

  $radon_rot=$radon_rot + $box_seg_angle;            ; add the "crude" rotation

  ($i,$radon_rot) = sd(<job_dir>/radon_rot_doc)              ; save rotation

  vm(echo Box {****$i}: {*****$radon_rot} {*****$box_seg_angle})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Average the 0-degree projections (with the layer lines)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if($pk_fity.ge.0) then
    $smallbox_topleft = 1 + int($radon_window_rows/2) + int($pk_fity + 0.5) - int(3/2); Upper corner of 3-pixel high box enclosing the Radon peak
  else
    $smallbox_topleft = 1 + int($radon_window_rows/2) + int($pk_fity - 0.5) - int(3/2); Upper corner of 3-pixel high box enclosing the Radon peak
  endif

; take the 3 rows around 0 degrees from the Radon xform
  wi(_7, _8, $radon_pw_size 3, 1 $smallbox_topleft)

  if($first_iteration.EQ.1) then         ; Add the rows together for all boxes
    cp(_8, _9)                      ; (initialize if this is the first box)
  else
    ad(_8, _9, _9, *)
  endif

  if($pk_fity.ge.0) then
    $pk_fity = 1 + int($radon_window_rows/2) + int($pk_fity + 0.5)
  else
    $pk_fity = 1 + int($radon_window_rows/2) + int($pk_fity - 0.5)
  endif

  $annotate_x = int($radon_pw_size/2) + 1
  $x1 = $annotate_x - 17
  $x2 = $annotate_x - 7
  $xb1 = $annotate_x + 7
  $xb2 = $annotate_x + 17

  pt(_7, L, $x1 $pk_fity, $x2 $pk_fity, Y, L, $xb1 $pk_fity, $xb2 $pk_fity, N)     ; Annotate image file

  if($debug .NE. 0) then
    cp to tif(_7, <job_dir>/radon_{****$i}.tif)
    cp to tif(_8, <job_dir>/layerlines_{****$i}.tif)
  endif

  LB3

  $first_iteration = 0;                   ; No longer are we in the first loop iteration

LB2

$layer_line_pos = $mask_center + $layer_line_radius           ; position in radon row of the 40A layer line

; Mask off all but the positive 40A layer line
ma(_9, _8, 5 0, G, E, 0.0, $layer_line_pos 1, $gauss_falloff)

; expand the Radon row to be 21 pixels wide, so peak search works!
;   NB: expanding NROW from 3 to 21: 10 = 1 + int(21/2) - int(3/2)
pd(_8, _7, $radon_pw_size 21, N, 0, 1 9)


if($debug .NE. 0) then
  cp to tif(_9, <job_dir>/layerlines_avg.tif)
  cp to tif(_7, <job_dir>/layerlines_masked.tif)
;  cp(_7, layerlines_masked)
endif

; Search for layer line peak; $pk_fitx, $pk_fity are parabolically fitted x, y coords
($pk_x,$pk_y,$pk_max,$pk_ratio,$pk_fitx,$pk_fity,$pk_fitmax) = \
  pk c(_7, 3 0, 3 3, n, 6, 0 0)

$scale_factor = ($pk_fitx / $layer_line_radius)
                                       ; Scale factor to bring micrograph's 40A layer line to the "true" radius (meaning
                                       ;  the radius that agrees with the helical repeat distance and pixel size given)
                                       ; Example: if padded box was 1600A-- original box being 800A-- this radius might be 40 pixels
                                       ;   Thus, if the layer line was found at 39 pixels, it is 1600A/39 = "41A".  
                                       ;   This means if we were aiming for 10-repeat box, we got slightly
                                       ;   fewer repeats.  So, to get to the "10-repeat" scale, we would have to
                                       ;   scale DOWN our box slightly (which of course introduces a little garbage
                                       ;   in the top and bottom of the box, NB).  Scaling down brings the box to 
                                       ;   a scale where the layer lines occur at 40 pixels, so they are now "40A" 
                                       ;   layer lines.

$true_magnification = 10000*$scanner_pixel_size/($box_pixel*$scale_factor)

$scale_factor = $scale_factor * $target_repeat_pixels / ($helical_repeat_dist/$box_pixel)

                                       ; Now adjust the scale factor to give an integral number of pixels per repeat;
                                       ;  If target spacing is larger than the 'calibrated' spacing 
                                       ;  ('calibrated' means with the 
if($box_num.eq.0) then
  (1, $scale_factor, $true_magnification) = sd(<job_dir>/radon_scale_doc)
else
  (1,$scale_factor, $true_magnification) = sd(<job_dir>/radon_scale_doc_{****$box_num})
endif

en de
;
