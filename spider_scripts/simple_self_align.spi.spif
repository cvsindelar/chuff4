;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Reference alignment and microtubule seam finding (any number of protofilaments)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr(?micrograph? <micrograph>)
fr(?box_header? <box_header>)
fr(?radon_dir? <radon_dir>)                ; where radon scaling/orientation files are contained
fr(?job_dir? <job_dir>)                    ; where current analysis output is contained
($pixel_size, $filter_resolution) = rr(?filter_resolution?)
($tubular) = rr(?tubular?)

$begin_box = 2
($end_box) = ud n(<box_header>_doc)

$bin_factor = 1

$best_magscale = 0
$best_psi = 0
$best_theta = 0
$best_phi = 0

$cum_x = 0

(1,$rot_angle) = ud(<radon_dir>/radon_rot_doc)
(1, $rot_angle, $best_magscale, $cum_x,$best_boxfity,$best_psi,$best_theta,$best_phi,$max_peak) = \
  sd(simple_align_doc)

;;;;;;;;;;;;;;;;;;;;;;;;;;
; The big loop
;;;;;;;;;;;;;;;;;;;;;;;;;;

do LB1 $box_num = $begin_box,$end_box               ; For each box

;;;;;;;;;;;;;;
; Get reference box
;;;;;;;;;;;;;;
  $prev_box = $box_num - 1

  ($prev_box, $orig_box_x, $orig_box_y, $orig_box_dim) = ud(<box_header>_doc)
  $box_dim = 2*$bin_factor*int($orig_box_dim/$bin_factor/2)

  $box_x = $orig_box_x + int($orig_box_dim/2) - int($box_dim/2)
  $box_y = $orig_box_y + int($orig_box_dim/2) - int($box_dim/2)

  wi(<micrograph>, _1, $box_dim $box_dim, $box_x $box_y)
  downsample_centered(_1, _2, $bin_factor)
  ra(_2, _1)
  ($prev_box,$rot_angle) = ud(<radon_dir>/radon_rot_doc)
  rt sq(_1, _11, $rot_angle 1, 0 0)
  
;;;;;;;;;;;;;;
; Get reference box
;;;;;;;;;;;;;;
  ($box_num, $orig_box_x, $orig_box_y, $orig_box_dim) = ud(<box_header>_doc)
  $box_x = $orig_box_x + int($orig_box_dim/2) - int($box_dim/2)
  $box_y = $orig_box_y + int($orig_box_dim/2) - int($box_dim/2)

  wi(<micrograph>, _1, $box_dim $box_dim, $box_x $box_y)
  downsample_centered(_1, _2, $bin_factor)
  ra(_2, _1)
  ($box_num,$rot_angle) = ud(<radon_dir>/radon_rot_doc)
  rt sq(_1, _12, $rot_angle 1, 0 0)

;;;;;;;;;;;;;;;;;;;;;;;;;;
; Smooth top, bottom edges by a cosine mask
;;;;;;;;;;;;;;;;;;;;;;;;;;

  ($box_x,$box_y) = fi(_2, 12 2)
  bl(_5, 1 $box_y, N, 1)
  $ma_ox = 1
  $ma_oy = int($box_y/2)+1
  $r_mask = int(0.8 * $box_y/2)
  $edge = int(0.15 * $box_y)
  ma(_5,_6, $r_mask 0, C, E, 0, $ma_ox $ma_oy, $edge)
  bl(_5, $box_x $box_y, N, 1)
  do LB3 $i=1,$box_x
    in(_6, _5, $i 1)
  LB3

  mu(_11, _5, _13, *)
  mu(_12, _5, _14, *)

  $butterworth_edge = 1
  $map_resolution_low = $filter_resolution + $butterworth_edge
  $map_resolution_hi = $filter_resolution - $butterworth_edge
  $butterworth_low = 0.5*(2*$pixel_size)/$map_resolution_low
  $butterworth_hi = 0.5*(2*$pixel_size)/$map_resolution_hi

  $butterworth_lopass_filter_code = 7
  fq(_13, _15, $butterworth_lopass_filter_code, $butterworth_low $butterworth_hi)
  fq(_14, _16, $butterworth_lopass_filter_code, $butterworth_low $butterworth_hi)

  cc(_15, _16, _1)

; NB pk c args are: input file; of peaks,center origin override (0/1); 
;                   ellipse axes (x,y) for CGR calculation;
;                   positivity enforced? (Y/N); minimum peak neighbor distance; 
;                   edge exclusion width (x, y)

  ($pk_x,$pk_y,$pk_max,$pk_ratio,$pk_fitx,$pk_fity,$pk_fitmax) = pk c(_1, 1 0, 3 3, N, 1, 0 200)

  dc s(_16, _17, 1 $box_y)
  $centery =($box_y/2)+1  ; Calculate top left coordinate in y-dimension

  pd(_17, _18, $box_x $box_y 1, m, 1 $centery)

;  Peak search - output to document file:
;   input file (defined above); look for 2 peaks: center origin override disabled;
;   name of document file

  pk d(_18, 2 0, <radon_dir>/center_temp_doc)

  (1, $pk1) = ud(<radon_dir>/center_temp_doc)

  if($tubular .NE. 0) then
    (2, $pk2) = ud(<radon_dir>/center_temp_doc)
    $x_tube_shift = -INT(($pk1+$pk2)/2) ; Calculate shift
  else
    $x_tube_shift = -$pk1
  endif

  $best_boxfitx = $pk_fitx * $bin_factor
  $best_boxfity = $pk_fity * $bin_factor

  $cum_x = $cum_x + $best_boxfitx

  ($box_num, $rot_angle, $best_magscale, $cum_x,$best_boxfity,$best_psi,$best_theta,$best_phi,$max_peak,$x_tube_shift) = \
  sd(simple_align_doc)

;;;;;;;;;;;;;;;;;;;
; End of the big loop
;;;;;;;;;;;;;;;;;;;
LB1

en de                                  ; Delete results file at the end
;
