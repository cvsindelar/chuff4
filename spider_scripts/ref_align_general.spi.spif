;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Reference alignment and microtubule seam finding (any number of protofilaments)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr(?micrograph? <micrograph>)
fr(?box_header? <box_header>)
fr(?radon_dir? <radon_dir>)                ; where radon scaling/orientation files are contained
fr(?job_dir? <job_dir>)                    ; where current analysis output is contained
fr(?reference_base? <ref_base>)            ; reference images should be at: <ref_base>/ref_xxxxxx.spi
($himem) = rr(?high_memory_and_faster?)
($num_pfs,$num_starts) = rr(?num_pfs num_starts?)
($box_num) = rr(?box number?)              ; zero means all boxes

($use_ctf) = rr(?use_ctf?)                                             ; CTF parameters
($box_pixel) = rr(?box_pixel_size?)
($defocus, $astig, $astig_azimuth) = rr(?defocus astig astig_azimuth?)
($amplitude_contrast_ratio) = rr(?amplitude_contrast_ratio?)           ; CTF parameters
($accelerating_voltage) = rr(?accelerating_voltage?)                   ; CTF parameters
($spherical_aberration_constant) = rr(?spherical_aberration_constant?) ; CTF parameters
($invert_density) = rr(?invert_density?)   ; multiply micrograph density by this factor (+/- 1)

($assigned_psi) = rr(?assigned_psi?)   ; can specify psi here

($final_align) = rr(?final align?)
($write_images) = rr(?write_debug_info?)          ; write out diagnostic images, etc.
($nuke) = rr(?nuke?)          ; recalculate all

if($assigned_psi .GE. 90) then
  fr l(<ref_align_doc> <job_dir>/ref_align_doc_pf{**$num_pfs}_start{*$num_starts}_psi{***$assigned_psi})
  fr l(<final_align_doc> <job_dir>/final_align_doc_pf{**$num_pfs}_start{*$num_starts}_psi{***$assigned_psi})
  fr l(<guess_seam_doc> <job_dir>/guess_ref_align_doc_pf{**$num_pfs}_start{*$num_starts}_psi{***$assigned_psi})
else
  fr l(<ref_align_doc> <job_dir>/ref_align_doc_pf{**$num_pfs}_start{*$num_starts})
  fr l(<final_align_doc> <job_dir>/final_align_doc_pf{**$num_pfs}_start{*$num_starts})
  fr l(<guess_seam_doc> <job_dir>/guess_ref_align_doc_pf{**$num_pfs}_start{*$num_starts})
endif

if($himem .NE. 0) then
;  cp(<ref_base>/ref_tot_ft@,_11@, $max_i)
  ($max_i) = ud n(<ref_base>/ref_angle)
  cp(<ref_base>/ref_tot@,_12@, $max_i)
  fr l(<ref_lib> _12)
else
  fr l(<ref_lib> <ref_base>/ref_tot)
endif

;;;;;;;;;;;;;;;;
; Set the upper and lower limits of the box analysis loop
;;;;;;;;;;;;;;;;

if($box_num.eq.0) then
  $begin_box = 1
  ($end_box) = ud n(<box_header>_doc)
else
  $begin_box = $box_num
  $end_box = $box_num
endif

;;;;;;;;;;;;;;;;;;;
; Set up stuff
;;;;;;;;;;;;;;;;;;;

$electron_lambda = 12.398/sqrt($accelerating_voltage*(1022+$accelerating_voltage))

;;;;;;;;;;;;;;;;;;;
; Algorithm parameters, etc.
;;;;;;;;;;;;;;;;;;;

(1, $min_phi, $max_phi, $min_theta, $max_theta, $d_angle) = ud(<ref_base>/ref_params)
(2, $dimer_repeat, $proj_pixels_per_repeat) = ud(<ref_base>/ref_params)

; Note: box, ref pixel sizes should be compatible with integral values of bin_factor
$bin_factor = int(($dimer_repeat/$proj_pixels_per_repeat)/$box_pixel + 0.5)

$tilt_window = $max_theta-$min_theta+1 ; Angular range of reference tilt angles

$peaks_to_refine = 5                  ; How many peaks around the coarsely found global maximum to examine
                                      ;   more finely

$coarse_phi_sample = 3        ; Angular sampling of coarse rotational search
$coarse_theta_sample = 5      ; Angular sampling of coarse tilt search

;;;;;;;;;;;;;;;;;;;;;;;;;;
; The big loop
;;;;;;;;;;;;;;;;;;;;;;;;;;

do LB1 $box_num = $begin_box,$end_box               ; For each box

  ($box_num, $orig_box_x, $orig_box_y, $orig_box_dim) = ud(<box_header>_doc)   ; Get box dimensions

;;;;;;;;;;;;;;;;;;;;;;
; Skip analysis if analyzed already...
;;;;;;;;;;;;;;;;;;;;;;

  if($final_align .EQ. 0) then
    ($file_exists) = iq fi(<ref_align_doc>)
  else
    ($file_exists) = iq fi(<final_align_doc>)
  endif

  if($file_exists.eq.1) then
    if($final_align .EQ. 0) then
      ($num_processed) = ud n(<ref_align_doc>)
    else
      ($num_processed) = ud n(<final_align_doc>)
    endif
    if($box_num.LE.$num_processed) then 
      if($write_images + $nuke .EQ. 0) then
        vm(echo skipping the processed box {****$box_num})
        goto LB1  ; if user requested debugging, always redo the calculation
      endif
    endif
  endif

;;;;;;;;;;;;;;;;;;;;;;;;;;
; Downsample the box file
;;;;;;;;;;;;;;;;;;;;;;;;;;

  $box_dim = 2*$bin_factor*int($orig_box_dim/$bin_factor/2)
  $box_x = $orig_box_x + int($orig_box_dim/2) - int($box_dim/2)
  $box_y = $orig_box_y + int($orig_box_dim/2) - int($box_dim/2)
  
;  downsample_centered(<data_dir>/box_{****$box_num}, _2, $bin_factor)
  wi(<micrograph>, _1, $box_dim $box_dim, $box_x $box_y)
  downsample_centered(_1, _2, $bin_factor)
  ra(_2, _1)

  ($box_num,$temp,$rot_angle_no_smooth,$skip,$rot_angle) = ud(<radon_dir>/radon_rot_doc)

  rt sq(_1, _2, $rot_angle 1, 0 0)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Normalize the averaged image (and possibly invert density)
;  Multiplies by -1 to correct for the low-res contrast inversion, in case
;  CTF correction is not requested
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ($ref_max, $ref_min, $ref_avg, $ref_sdev) = fs(_2)      ; File statistics
  if($ref_sdev .EQ. 0) then
    vm(echo ERROR: blank image.  Exiting...)
    en de
  endif

  ar(_2, _1, -1*$invert_density*(P1 - $ref_avg)/$ref_sdev)   ; Arithmetic transformation

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Create the ctf file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if($use_ctf .NE. 0) then
    ($ctf_x, $ctf_y) = fi(_1, 12 2)   ; get the box size

    $max_freq = 1/(2*($box_pixel*$bin_factor))
    tf ct(_8, $spherical_aberration_constant, $defocus $electron_lambda, \
           $ctf_x $ctf_y, $max_freq, 0.00000005 0, $astig $astig_azimuth, $amplitude_contrast_ratio 100000000, -1)
    ft(_1, _3)
    mu(_3, _8, _3, *)
    ft(_3, _1)
  endif

;;;;;;;;;;;;;;;;;;;;;;;;;;
; Smooth top, bottom edges by a cosine mask
;;;;;;;;;;;;;;;;;;;;;;;;;;

  ($box_x,$box_y) = fi(_2, 12 2)
  bl(_5, 1 $box_y, N, 1)
  $ma_ox = 1
  $ma_oy = int($box_y/2)+1
  $r_mask = int(0.8 * $box_y/2)
  $edge = int(0.15 * $box_y)
  ma(_5,_6, $r_mask 0, C, E, 0, $ma_ox $ma_oy, $edge)
  bl(_5, $box_x $box_y, N, 1)
  do LB3 $i=1,$box_x
    in(_6, _5, $i 1)
  LB3
  mu(_1, _5, _1, *)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; We are ready to do cross-correlation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$max_peak = -99999;                ; this will store the max peak found

if($final_align .EQ. 1) then

  ($file_exists) = iq fi(<guess_seam_doc>)
  if($file_exists .NE. 1) then
    vm(echo 'ERROR: file not found : ' <guess_seam_doc>);
    vm(echo 'ERROR: please run chumps_select_seam first...');
    vm(echo '  ')
    en de
  endif

  ($box_num, $rot_angle, $best_magscale, $best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi,$temp) = \
  ud(<ref_align_doc>)
  ($box_num, $best_psi,$best_theta,$best_phi) = ud(<guess_seam_doc>)

  $best_boxfitx = $best_boxfitx/$bin_factor
  $best_boxfity = $best_boxfity/$bin_factor
else
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Seam-finding section: (skip if doing final alignment)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if($assigned_psi .GE. 90) then
  $max_k = 1
else
  $max_k = 2
endif

do LB2 $k = 1,$max_k
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Coarse search for protofilament peak
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if($k .EQ. 1) then
    $psi = 90
  else
    $psi = 270
  endif

  if($assigned_psi .GE. 90) then
    $psi = $assigned_psi
  endif

  $theta_start = 90
  $num_theta = 1
  $dtheta = 1

  $phi_start = 0
  $dphi = $coarse_phi_sample
  $num_phi = int( (360/$num_pfs)/$dphi)

  ($max_peak,$best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi) = \
    align_grid_general(_1, <ref_base>, <ref_lib>, \
                        <job_dir>/ccpeak_doc_{****$box_num}, \
                        $psi $theta_start $num_theta $dtheta $phi_start $num_phi $dphi)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Fine search for protofilament peak
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  $dtheta = 1
  $num_theta = 1
  $theta_start = 90

  $dphi = 1
  $num_phi = int( (2*$coarse_phi_sample + 1)/$dphi)
  $phi_start = $best_phi;   ; start of a window centered around the coarsely found proto peak

  ($max_peak,$best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi) = \
    align_grid_general(_1, <ref_base>, <ref_lib>, \
                        <job_dir>/ccpeak_doc_{****$box_num}, \
                        $psi $theta_start $num_theta $dtheta $phi_start $num_phi $dphi)

  if($k .EQ. 1) then
    $proto_phi_psi90 = $best_phi
  else
    $proto_phi_psi270 = $best_phi
  endif
LB2

$max_peak = -99999;                ; $max_peak may not be reached initially
                                   ;  in following steps, due to coarse search

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Global coarse search over all protofilaments, using the found protofilament peak:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  $theta_start = 90
  $theta_start = $best_theta

  $dphi = 360/$num_pfs
  $num_phi = $num_pfs

  $dtheta = $coarse_theta_sample
  $num_theta = int( ($tilt_window - 1)/$coarse_theta_sample )  + 1   ; how many tilt angles to "coarsely" consider
  $theta_start = 90

  do LB6 $k = 1, $max_k                    ; Loop through the 2 symmetry cases ("right-side up" and "upside-down")

    if($k.EQ.1) then
      $phi_start = $proto_phi_psi90;  ; reference index of the finely found protofilament peak
                                      ; NB: will be between 0 and int(360/num_pfs), inclusive 
                                      ;     (lowest tilt angle in reference set)
      $psi = 90                                    ; psi (Euler angle)
    else
      $phi_start = $proto_phi_psi270   ; peak index, but for a MT rotated in-plane 180 degrees
      $psi = 270                                   ; psi (Euler angle)
    endif
    
    if($assigned_psi .GE. 90) then
      $phi_start = $proto_phi_psi90;
      $psi = $assigned_psi
    endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Do the cross-correlation search
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ($max_peak,$best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi) = \
    align_grid_general(_1, <ref_base>, <ref_lib>, \
                        <job_dir>/ccpeak_doc_{****$box_num}, \
                        $psi $theta_start $num_theta $dtheta $phi_start $num_phi $dphi)

;;;;;;;;;;;;;;;;;;;
; End of symmetry loop
;;;;;;;;;;;;;;;;;;;
  LB6

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; End of seam-finding section
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
endif

  $psi = $best_psi

  $phi_start = $best_phi;
  $dphi = $coarse_phi_sample;
  $num_phi = int( (360/$num_pfs)/$dphi)

  $theta_start = 90
  $num_theta = int( ($tilt_window - 1)/$coarse_theta_sample )  + 1   ; how many tilt angles to "coarsely" consider
  $dtheta = $coarse_theta_sample

  ($max_peak,$best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi) = \
    align_grid_general(_1, <ref_base>, <ref_lib>, \
                        <job_dir>/ccpeak_doc_{****$box_num}, \
                        $psi $theta_start $num_theta $dtheta $phi_start $num_phi $dphi)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Fine search to refine the global max
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  $dphi = 1;
  $num_phi = int( (2*$coarse_phi_sample + 1)/$dphi)
  $phi_start = $best_phi                                ; reference index of the finely found protofilament peak
                                                        ; NB: will be between 0 and int(360/num_pfs), inclusive 
                                                        ;     (lowest tilt angle in reference set)
  $dtheta = 1
  $num_theta = int( (2*$coarse_theta_sample + 1)/$dtheta) ; how many tilt angles to consider
  $theta_start = $best_theta

  ($max_peak,$best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi) = \
    align_grid_general(_1, <ref_base>, <ref_lib>, \
                        <job_dir>/ccpeak_doc_{****$box_num}, \
                        $psi $theta_start $num_theta $dtheta $phi_start $num_phi $dphi)

  if($max_peak .GE. 0) then
    $max_peak_print = 10000000 * $max_peak
    vm(echo Box {***$box_num} proto: {****$proto_phi_psi90} Eulers: {****$best_psi} {****$best_theta} {****$best_phi} Score: 0.{*******$max_peak_print})
  else
    $max_peak_print = -10000000 * $max_peak
    vm(echo Box {***$box_num} proto: {****$proto_phi_psi90} Eulers: {****$best_psi} {****$best_theta} {****$best_phi} Score: -0.{*******$max_peak_print})
  endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Write out info
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if($write_images .NE. 0) then
    sh(_1, _10, $best_boxfitx $best_boxfity)

    cp to tif(_10, <job_dir>/avg_box_align_{****$box_num})

    read_ref_general(<ref_base>, <ref_lib>, _10,\
                     $box_x $box_y, $best_psi $best_theta $best_phi)

    ($ctf_x, $ctf_y) = fi(_10, 12 2)   ; get the box size
    tf c(_8, $spherical_aberration_constant, $defocus $electron_lambda, \
           $ctf_x $ctf_y, $max_freq, 0.00000005 0, $astig $astig_azimuth, $amplitude_contrast_ratio 100000000, -1)
    ft(_10, _3)
;    mu(_3, _8, _3, _8, *)
    mu(_3, _8, _3, *)
    ft(_3, _10)
    cp to tif(_10, <job_dir>/ref_match_{****$box_num})
  endif

;;;;;;;;;;;;;;;;;;;
; Save rotation, scale, x & y trans, & the Euler angles...
;;;;;;;;;;;;;;;;;;;

  $best_boxfitx = $bin_factor*$best_boxfitx;
  $best_boxfity = $bin_factor*$best_boxfity;
  $best_magscale = 1

  if($final_align .EQ. 0) then
    ($box_num, $rot_angle, $best_magscale, $best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi,$max_peak) = \
      sd(<ref_align_doc>)
    sd e(<ref_align_doc>)
  else
    ($box_num, $rot_angle, $best_magscale, $best_boxfitx,$best_boxfity,$best_psi,$best_theta,$best_phi,$max_peak) = \
      sd(<final_align_doc>)
    sd e(<final_align_doc>)
  endif

vm(rm <job_dir>/ccpeak_doc_{****$box_num}.spi)

;;;;;;;;;;;;;;;;;;;
; End of the big loop
;;;;;;;;;;;;;;;;;;;
LB1

en de                                  ; Delete results file at the end
;
