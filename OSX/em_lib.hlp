 
C
C-IMAG3Y----------------------------------------------------------------
C
C       Please specify whether the input image file contains a 2D
C       image or a 3D volume (many 2D sections).
C
 
C
C-IMAG3MRCIN------------------------------------------------------------
C
C       Please specify whether the input image file contains a
C       2D image, a stack of 2D images or a 3D volume (multiple
C       2D sections):
C
C       SINGLE_2D   : A single MRC file with only one 2D image
C
C       MULTIPLE_2D : A single MRC file containing a stack of
C                     2D images
C
C       3D_VOLUME   : A single MRC file containing a 3D volume
C
 
C
C-IMAG3DA---------------------------------------------------------------
C
C       Please specify whether the input image file contains a 2D
C       image or a 3D volume (multiple 2D sections).
C
 
C
C-IMAG3MRCOUT-----------------------------------------------------------
C
C       The input file can contain a stack of 2D images. Please
C       specify how these images should be stored in the MRC
C       output file:
C
C       SINGLE_2D   : A set MRC files with only a single 2D image
C                     per file
C
C       MULTIPLE_2D : A single MRC file containing all 2D images
C
 
C
C-IMAG3X----------------------------------------------------------------
C
C       Please specify whether the input image file contains a 2D
C       image or a 3D volume (many 2D sections).
C
 
C
C-IMPSET1---------------------------------------------------------------
C
C       The input image(s) you want to convert are stored in a file
C       format that contains only ONE image per file.
C
C       If you want to convert only one image then answer this question
C       with NO.
C
C       However, it is also possible to read in many input images which
C       are stored in a SET of independent input files. In this case
C       give YES.
C
 
C
C-IMPSET2---------------------------------------------------------------
C
C       If the 2D image / 3D volume is stored in ONLY one file then
C       answer with NO.
C
C       If many 2D images / 2D sections of a 3D volume are stored in
C       a SET of input files then answer with YES.
C
 
C
C-IMPINP0---------------------------------------------------------------
C
C    There are two options:
C
C    INERACTIVE:  Rootname of the input image file has to be specified
C                 interactively by the user
C
C    FILE:        Names of the input files will be read from a text
C                 (ASCII) file
C
 
C
C-IMPTXTF---------------------------------------------------------------
C
C       Please specify the (text) file which contains the names of the
C       image files that should be converted. Please specify the name
C       together with the extension.
C
C       The file is assumed to contain one name per line!
C
C       The file should be in the default directory you are working in.
C       If you want a file from another directory specify that by using
C       the correct prefixes.
C
 
C
C-IMPINP1---------------------------------------------------------------
C
C       For a SET of input images, the filenames are assumed to be
C       constructed as leading characters (rootname) followed by a
C       number such as in the set:
C
C             rootname1.xxx   rootname2.xxx   rootname3.xxx   ...
C
C       Example:
C              Input image file  :  my_image_,1,141
C
C              In this case  my_image_1.xxx  my_image_2.xxx
C              my_image_3.xxx ...  my_image_141.xxx  are
C              converted. "xxx" is the extension which you
C              have to specify later.
C
C       NOTE1: If you specify
C                               none   or    NONE
C
C              as a rootname such as in:
C
C                               none,13,144
C
C              the input filename is assumed to consist ONLY of the
C              numbers 13 to 144   followed by the extension.
C
C       NOTE2: Do not specify the file extension here!
C
 
C
C-IMPLOC1---------------------------------------------------------------
C
C       Please specify the length of the string used to write the
C       image numbers.
C
C       Examples:  If "input_" is the rootname then
C
C        0 means: input_1,  ... input_9,   input_10, ... input_100, ...
C        1 means: input_1,  ... input_9
C        2 means: input_01, ... input_09,  input_10
C        3 means: input_001,... input_009, input_010, ... input_999
C                           etc.
C
 
C
C-IMPEXT1---------------------------------------------------------------
C
C       The extension of the input files is what comes after the dot
C       (possibly including the dot itself).
C
C       Please specify this extension. If the specified input data
C       format has a well defined extension then this extension is
C       proposed as default and you can simply give CR/ENTER.
C
C       NOTE: Be careful that the input image file(s) do not have
C             extensions which might also automatically be used to write
C             the images into the specified output format (".img" or
C             ".hed" for IMAGIC, ".pif" for PORTABLE IMAGE FORMAT,
C             ".shf" for GATAN's simple header format, etc.)
C
 
C
C-IMPINP2---------------------------------------------------------------
C
C       Please specify the input file name together with the extension.
C
C       The input image file is assumed to contain the 3D volume (many
C       2D sections) which should be converted into another format.
C
C       The input image file should be in the default directory you
C       are working in. If you want a file from another directory
C       specify that by using the correct prefixes.
C
C       NOTE:
C
C       The input file should contain only ONE 3D volume per file.
C       Therefore a location numbers have not to be specified.
C
C       Exp.:
C                  Input image file   :  my_image.mrc
C
C                  The 3D volume stored in my_image.mrc is used as
C                  input.
C
 
C
C-IMPINP3---------------------------------------------------------------
C
C       Please specify the input file name.
C
C       The input image file is assumed to contain the 3D volume which
C       should be converted into another format.
C
C       The input image file should be in the default directory you
C       are working in. If you want a file from another directory
C       specify that by using the correct prefixes.
C
C       NOTE:
C
C       The input file should contain only ONE 3D volume per file.
C       Therefore location numbers have not to be specified.
C
C       Exp.:
C                  Data format      :  MDPP
C                  Input image file :  my_image
C
C                  The 3D volume stored in my_image.bmd is used as
C                  input.
C
 
C
C-IMPINP4---------------------------------------------------------------
C
C       Please specify the input file name WITHOUT extension.
C
C       The input image file is assumed to contain the image(s) which
C       should be converted into another format.
C
C       The input image file should be in the default directory you
C       are working in. If you want a file from another directory
C       specify that by using the correct prefixes.
C
C       NOTE:
C
C       The input file can contain more than one image per file.
C       You can therefore specify location numbers to select a certain
C       range of images that should be converted. If you do not
C       specify location numbers all images in the input file will be
C       used.
C
C       Exp.:
C                Input file :  my_image,3,4
C
C                In this case the images stored in locations #3, #4
C                and #5 of the input file called  my_image.xxx  (xxx
C                is the extension which you have to specify later)
C                will be used as input images.
C
 
C
C-IMPEXT5---------------------------------------------------------------
C
C       The extension of the input files is what comes after the dot
C       (possibly including the dot itself).
C
C       Please specify this extension. If the specified input data
C       format has a well defined extension then this extension is
C       proposed as default and you can simply give CR/ENTER.
C
C       NOTE: Be careful that the input image file(s) do not have
C             extensions which might also automatically be used to write
C             the images into the specified output format (".img" or
C             ".hed" for IMAGIC, ".pif" for PORTABLE IMAGE FORMAT,
C             ".shf" for GATAN's simple header format, etc.)
C
 
C
C-IMPINP6---------------------------------------------------------------
C
C       Please specify the input file name together with the extension.
C
C       The input image file is assumed to contain the image(s) which
C       should be converted into another format.
C
C       The input image file should be in the default directory you
C       are working in. If you want a file from another directory
C       specify that by using the correct prefixes.
C
C       NOTE:
C
C       The input file contains only one image (or one 3D volume) per
C       file. Therefore a location number has not to be specified.
C
C       Exp.:
C                  Input image file   :  my_image.mrc
C
C                  The image or the 3D volume stored in my_image.mrc
C                  is used as input.
C
 
C
C-IMPINP7---------------------------------------------------------------
C
C       Please specify the input file name.
C
C       The input image file is assumed to contain the image(s) which
C       should be converted into another format.
C
C       The input image file should be in the default directory you
C       are working in. If you want a file from another directory
C       specify that by using the correct prefixes.
C
C       NOTE:
C
C       The input file contains only one image (or one 3D volume) per
C       file. Therefore a location number has not to be specified.
C
C       Exp.:
C                  Data format      :  MDPP
C                  Input image file :  my_image
C
C                  The image or the 3D volume stored in my_image.bmd
C                  is used as input.
C
 
C
C-EXPOUT1---------------------------------------------------------------
C
C       Please specify the output file name together WITH extension.
C
C       The output file will contain the converted 3D volume and can
C       have any legitimate operating system file name. It will be
C       stored in the default directory you are working in. If you
C       want another directory specify that by using the correct
C       prefixes.
C
 
C
C-EXPOUT2-------------------------------------------------------------
C
C       Please specify the output file name.
C
C       The output file will contain the converted 3D volume and can
C       have any legitimate operating system file name. It will be
C       stored in the default directory you are working in. If you
C       want another directory specify that by using the correct
C       prefixes.
C
 
C
C-EXPOUT3---------------------------------------------------------------
C
C       The input file contains a 3D volume which should be converted
C       to an image format that can only store 2D images. Therefore
C       the 2D sections of the 3D volume have ta be stored in a SET
C       (sequence) of independant output image files.
C
C       The output files will be constructed in the following way:
C
C             rootname1.xxx   rootname2.xxx   rootname3.xxx   ...
C
C       where .xxx is the extension which has to be specified later.
C
C       Please specify the name for "rootname" WITHOUT extension. The
C       "rootname" can have any legitimate operating system file name.
C       It will be stored in the default directory you are working in.
C       If you want another directory specify that by using the correct
C       prefixes.
C
C       Example:
C
C              Output rootname (NO EXT.),first#,last# : my_image_,3,5
C
C              In this case  my_image_3.xxx  my_image_4.xxx ...
C              are used as output files. "xxx" is the extension which
C              you have to specify later.
C
C      NOTE 1: If you specify
C                               none   or    NONE
C
C              as rootname such as in:
C                                        none,13,144
C
C              the output filename is assumed to consist ONLY of the
C              numbers 13 to 144 followed by the extension.
C
C      NOTE 2: Do not specify the file extension here!
C
 
C
C-EXPLOC1---------------------------------------------------------------
C
C       Please specify the length of the string used to write the
C       image numbers.
C
C       Examples:  If "out_" is the rootname then
C
C         0 means: out_1,  ... out_9,   out_10, ...  out_100, ...
C         1 means: out_1,  ... out_9
C         2 means: out_01, ... out_09,  out_10
C         3 means: out_001,... out_009, out_010, ... out_999
C                           etc.
C
 
C
C-EXPEXT3---------------------------------------------------------------
C
C       The extension of the output files is what comes after the dot
C       (possibly including the dot itself).
C
C       Please specify this extension. If the specified output data
C       format has a well defined extension then this extension is
C       proposed as default and you can simply give CR/ENTER.
C
C       NOTE: Be careful that the input image file(s) do not have
C             extensions which might also automatically be used to write
C             the images into the specified output format (".img" or
C             ".hed" for IMAGIC, ".pif" for PORTABLE IMAGE FORMAT,
C             ".shf" for GATAN's simple header format, etc.)
C
 
C
C-EXPOUT4---------------------------------------------------------------
C
C       Please specify the output file name WITHOUT extension.
C
C       The output file will contain the converted image(s) and can
C       have any legitimate operating system file name. It will be
C       stored in the default directory you are working in. If you
C       want another directory specify that by using the correct
C       prefixes.
C
C       NOTE: Your output image can store more than one image per file.
C             The converted images will therefore be stored in ONE
C             output file.
C
 
C
C-EXPEXT4---------------------------------------------------------------
C
C       The extension of the output file is what comes after the dot
C       (possibly including the dot itself).
C
C       Please specify this extension. If the specified output data
C       format has a well defined extension then this extension is
C       proposed as default and you can simply give CR/ENTER.
C
C       NOTE: Be careful that the input image file does not have an
C             extension which might also automatically be used to write
C             the images into @he specified output format (".img" or
C             ".hed" for IMAGIC, ".pif" for PORTABLE IMAGE FORMAT,
C             ".shf"for GATAN's simple header format, etc.)
C
 
C
C-EXPOUT5---------------------------------------------------------------
C
C     A sequence of 2D images has to be converted. This set of images
C     cannot be stored in ONE output file because the specified
C     image format only allows ONE image per file.
C
C     The output images must therefore be stored in a SET (sequence)
C     of independant output files. The output files will be constructed
C     in the following way:
C
C          rootname1.xxx   rootname2.xxx   rootname3.xxx   ...
C
C     where .xxx is the extension you have to specify later.
C
C     Please specify the name for rootname WITHOUT extension. If the
C     input image contains only one image "rootname" will be used as
C     output filename without any number.
C
C     "rootname" can have any legitimate operating system file name.
C     It will be stored in the default directory you are working in.
C     If you want another directory specify that by using the correct
C     prefixes.
C
C     Example:
C
C              Output rootname (NO EXT.),first#,last# : my_image_,3,5
C
C              In this case  my_image_1.xxx  my_image_2.xxx
C              my_image_3.xxx ...  my_image_141.xxx  are
C              used as output files. "xxx" is the extension which
C              you have to specify later.
C
C    NOTE 1:  If you specify
C                               none   or    NONE
C
C             as rootname such as in:
C                                        none,13,144
C
C             the output filename is assumed to consist ONLY of the
C             numbers 13 to 144 followed by the extension.
C
C    NOTE 2:  Do not specify the file extension here!
C
 
C
C-EXPLOC2---------------------------------------------------------------
C
C     Please specify the length of the string used to write the
C     image numbers.
C
C     Examples:  If "out_" is the rootname then
C
C        0 means: out_1,  ... out_9,   out_10, ...  out_100, ...
C        1 means: out_1,  ... out_9
C        2 means: out_01, ... out_09,  out_10
C        3 means: out_001,... out_009, out_010, ... out_999
C                           etc.
C
 
C
C-EXPEXT5--------------------------------------------------------------
C
C     The extension of the output files is what comes after the dot
C     (possibly including the dot itself).
C
C     Please specify this extension. If the specified output data format
C     has a well defined extension then this extension is proposed as
C     default and you can simply give CR/ENTER.
C
C     NOTE: Be careful that the input image file(s) do not have
C           extensions which might also automatically be used to write
C           the images into the specified output format (".img" or
C           ".hed" for IMAGIC, ".pif" for PORTABLE IMAGE FORMAT, ".shf"
C           for GATAN's simple header format, etc.)
C
 
C
C-EXPOUT6---------------------------------------------------------------
C
C     Please specify the output file name together WITH extension.
C
C     The output file will contain the converted image and can
C     have any legitimate operating system file name. It will be
C     stored in the default directory you are working in. If you
C     want another directory specify that by using the correct
C     prefixes.
C
 
C
C-EXPOUT7-------------------------------------------------------------
C
C       Please specify the output file name.
C
C       The output file will contain the converted image and can
C       have any legitimate operating system file name.
C       It will be stored in the default directory you are
C       working in. If you want another directory specify that
C       by using the correct prefixes.
C
 
C
C-PIXANG----------------------------------------------------------------
C
C       Please specify the size of one pixel/voxel in Angstroem.
C
C       Please note: This question is needed because many EM formats
C                    do not store this information in the header.
C
C                    If the pixel spacing is stored in the input
C                    header then this question is obsolete and
C                    IMAGIC will use this header value ignoring the
C                    answer you are giving here.
C
 
C
C-UNXFTP----------------------------------------------------------------
C
C       If the import image file was ftped from a UNIX workstation
C       to the VAX/VMS workstation then the file attributes have to
C       be changed so that the VMS operating system can read the data.
C       Unfortunately, this information is not stored in any header
C       value.
C
C       If the image was ftped from a UNIX workstation please answer
C       YES else give NO.
C
 
C
C-CPUIMPU---------------------------------------------------------------
C
C       For the various computers the floating point values (formats
C       REAL, INTEGER, COMPLEX) are stored in a different way (byte
C       shift/ byte swap).
C
C       Some file formats store the type of workstation on which
C       the image(s) were created in their header.
C
C       Unfortunately, the input image(s) did not contain this
C       CPU information. Therefore, please give the name of the
C       workstation where the input images have been created.
C
C       Computers allowed:  LINUX
C                           PC/MS-WINDOWS
C                           INTEL-MAC
C                           POWERPC-MAC
C                           HP
C                           IBM
C                           SGI/SILICON_GRAPHICS
C                           SUN/STARDENT
C                           DEC/ULTRIX (RISK)
C                           DEC/OSF
C                           VAX/VMS
C                           VAX/OPENVMS
C
 
C
C-CPUEXPU---------------------------------------------------------------
C
C       For the various computers the floating point values (formats
C       REAL, INTEGER, COMPLEX) are stored in a different way (byte
C       shift/ byte swap).
C
C       Some file formats store the type of workstation on which
C       the image(s) were created in their header.
C
C       Usualy IMAGIC stores the output images according to the
C       floating point format of the current CPU. If you want to
C       have another floating point format, please specify if here.
C
C       Default is the current CPU.
C
C       Computers allowed:  LINUX
C                           PC/MS-WINDOWS
C                           INTEL-MAC
C                           POWERPC-MAC
C                           HP
C                           IBM
C                           SILICON_GRAPHICS
C                           SUN/STARDENT
C                           DEC/ULTRIX  (RISK)
C                           DEC/OSF
C                           VAX/VMS
C                           VAX/OPENVMS
C
 
C
C-FORDIM----------------------------------------------------------------
C
C       Option FORMAT means that no header values are available.
C       You must therefore specify some parameters of the input
C       image(s).
C
C       Here you must specify the image dimensions.
C
C       The IMAGIC dimension conventions are as follows:
C
C                X : the number of lines
C                Y : the number of pixels per line
C
C                    -------------->  Y coordinate
C                   |
C                   |
C                   |
C                   |
C                   V  X coordinate
C
 
C
C-FORD3M----------------------------------------------------------------
C
C       Option FORMAT means that no header values are available.
C       You must therefore specify some parameters of the input
C       image(s).
C
C       Here you must specify the image dimensions.
C
C       The IMAGIC dimension conventions are as follows:
C
C                X : the number of lines in every section
C                Y : the number of pixels per line
C                z : number of sections
C
C                Z coordinate  ^
C                              |
C                              |
C                              |
C                              |
C                               - - - - ->  Y coordinate
C                             /
C                            /
C                           /
C                      X coordinate
C
 
C
C-FORMI-----------------------------------------------------------------
C
C       Option FORMAT means that no header values are available.
C       You must therefore specify some parameters of the input
C       image(s).
C
C       Please specify the format in which the input data is stored,
C       that is, ou have to type a valid FORTRAN-77 format string.
C       Example of valid format strings:
C
C           "30I4"  or "40I3"  or "10F8.4"  etc. .
C
C       Another option is FREE FORMAT:
C
C           The data is expected to contain one variable per line
C           and is read with the FORTRAN-77 format specification '*'.
C
C       The format will be repeated as often as needed to read in one
C       image line.
C
C       NOTE#1: Lines may not overflow into the next record.
C               That is, if the line end is reached before the
C               end of one record, the rest of that record MUST
C               NOT contain the beginning of the next line but
C               MUST rather be empty.
C
C       NOTE#2: Some computer/compilers do not like formated records
C               of longer than 128 bytes. It is YOUR, repeat YOUR,
C               responsibility to make sure the records you are
C               generating fullfil such (maybe irritating) boundary
C               conditions.
C
 
C
C-FORMO-----------------------------------------------------------------
C
C       Please specify the output format which should be used to
C       store the image data. This format is kept flexible, that is,
C       you have to type a valid FORTRAN-77 format string.
C
C       Example of valid format strings:
C
C           "30I4"  or "40I3"  or "10F8.4"  etc. .
C
C       Another option is FREE FORMAT:
C
C           The data is expected to contain one variable per line
C           and is read with the FORTRAN-77 format specification '*'.
C
C       The format will be repeated as often as needed to
C       write out one image line.
C
C       NOTE: Some computer/compilers do not like formated records
C             of longer than 128 BYTES. It is YOUR, repeat YOUR,
C             responsibility to make sure the records you are
C             generating fullfill such (maybe irritating) boundary
C             conditions.
C
 
C
C-VMSREC----------------------------------------------------------------
C
C   The record length of an VAX/VMS file is not always the same.
C   Unfortunately, this important MDPP information is no more available
C   if the file was copied with ftp.
C
C   Please specify this record length here. Usually the record length
C   is 2048 (in bytes). If you had trouble to convert PIC files then
C   you should try other values like: 512 or 256 or ... (or ask the
C   sender which record length was used.
C
 
C
C-OFFDIM----------------------------------------------------------------
C
C       Option OFFSET means that IMAGIC should not read any header
C       values. You must therefore specify some parameters of the
C       input image(s).
C
C       Please specify the image dimensions.
C
C       The IMAGIC dimension conventions are as follows:
C
C                X : the number of lines
C                Y : the number of pixels per line
C
C                    -------------->  Y coordinate
C                   |
C                   |
C                   |
C                   |
C                   V  X coordinate
C
 
C
C-OFFD3M----------------------------------------------------------------
C
C       Option OFFSET means that IMAGIC should not read any header
C       values. You must therefore specify some parameters of the
C       input image(s).
C
C       Please specify the image dimensions.
C
C       The IMAGIC dimension conventions are as follows:
C
C                X : the number of lines in every section
C                Y : the number of pixels per line
C                z : number of sections
C
C                Z coordinate  ^
C                              |
C                              |
C                              |
C                              |
C                               - - - - ->  Y coordinate
C                             /
C                            /
C                           /
C                      X coordinate
C
 
C
C-ITYP------------------------------------------------------------------
C
C    The input images can be read if they have one of the following
C    data types:
C
C    PACK  : every pixel density is represented by a byte number
C            (1 byte)
C
C    INTG  : every pixel density is represented by a INTEGER*2 / short
C            number (2 bytes)
C
C    LONG  : every pixel density is represented by a INTEGER*4 / long
C            number (4 bytes)
C
C    REAL  : every pixel density is represented by a REAL / float
C            number (4 bytes)
C
C    PCOMP : every pixel is represented by a complex number of two
C            byte numbers (2 bytes)
C
C    SCOMP : every pixel is represented by a complex number of two
C            INTEGER*2 / short numbers (4 bytes)
C
C    LCOMP : every pixel is represented by a complex number of two
C            INTEGER*4 / long numbers (8 bytes)
C
C    COMP  : every pixel is represented by a complex number of two
C            REAL / float numbers (8 bytes)
C
 
C
C-UPSD------------------------------------------------------------------
C
C       IMAGIC builds up an image with the first line to be the upper
C       left line of the image.
C
C             ........    1st line
C             ........    2nd line
C             ........    3rd line
C
C                     etc.
C
C             ........    nth line
C
C      In this case answer this question with NO.
C
C      If the input image is built up the other way round
C
C             ........    nth line
C
C                     etc.
C
C             ........    3rd line
C             ........    2nd line
C             ........    1st line
C
C       then answer this question with YES.
C
 
C
C-XYZ-------------------------------------------------------------------
C
C       Please specify the XYZ coordinate system in the input
C       data:
C
C       IMAGIC style:
C                         Z coordinate
C                              ^
C                 last section |
C                              |
C                              |
C                              |
C                 1st section  |
C                               - - - - - - - >  Y coordinate
C                             / 1st image line
C                            /
C                           / last image line
C
C                     X coordinate
C
C       MRC/CCP4/MDPP style:
C
C                      Z coordinate
C                              ^
C                 last section |    Y coordinate
C                              |
C                              |   / last image line
C                              |  /
C                              | /
C                 1st section  |/ 1st image line
C                               - - - - - - - - >  X coordinate
C
C       OTHER style:
C                               - - - - - - - >  Y coordinate
C                             /| 1st image line
C                            / |
C                           /  |
C                          / last image line
C                X coordinate  |
C                              | last section
C                              V
C                          Z coordinate
C
 
C
C-IOFF------------------------------------------------------------------
C
C       Please specify the offset in bytes where IMAGIC should start to
C       read the image densities.
C
C       Please note:  Offset 0 means: start with the first byte
C                     (the input file is a "raw " image)
C
 
C
C-OO3D------------------------------------------------------------------
C
C    The following options are available:
C
C    YES :  The 3D input volume will be stored in ONE output file
C           which means that the section are stored one by one
C           in one output file.
C    NO:    The sections of the 3D input volume will be stored in a
C           series of 2D image files.
C
 
C
C-OO2D------------------------------------------------------------------
C
C    The following options are available:
C
C    YES :  The input images will be stored in ONE output file.
C
C    NO:    The input images will be stored in a series of
C           2D image files.
C
 
C
C-OOTYP-----------------------------------------------------------------
C
C    The output images can have one of the following data types:
C
C    PACK  : every pixel density is represented by a byte number
C            (1 byte)
C
C    INTG  : every pixel density is represented by a INTEGER*2 / short
C            number (2 bytes)
C
C    LONG  : every pixel density is represented by a INTEGER*4 / long
C            number (4 bytes)
C
C    REAL  : every pixel density is represented by a REAL / float
C            number (4 bytes)
C
C    PCOMP : every pixel is represented by a complex number of two
C            byte numbers (2 bytes)
C
C    SCOMP : every pixel is represented by a complex number of two
C            INTEGER*2 / short numbers (4 bytes)
C
C    LCOMP : every pixel is represented by a complex number of two
C            INTEGER*4 / long numbers (8 bytes)
C
C    COMP  : every pixel is represented by a complex number of two
C            REAL / float numbers (8 bytes)
C
C    NOTE:   If you give AUTOMATIC then the output type will be chosen
C    =====   according to the input image type.
C
 
C
C-PGMMOD----------------------------------------------------------------
C
C     There are to modes how to store an image in .pgm format:
C
C     P2 :    Usual mode
C             Header and data are stored in decimal ASCII
C
C     P5 :    Header values are stored in ASCII decimal (like P2).
C             Data are stored as bytes (greyvalues must be 0 - 255)
C             which means that input/output is much faster compared
C             to P2.
C
 
C
C-SPIDXXX---------------------------------------------------------------
C
C       Please specify if you want to use a single SPIDER file or
C       a set of many SPIDER files.
C
 
C
C-CELLC-----------------------------------------------------------------
C
C       Please specify the cell constants in Angstroem.
C
 
C
C-CELLA-----------------------------------------------------------------
C
C       Please specify the cell angles in Angstroem.
C

