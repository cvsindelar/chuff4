Programs notes:

Current frealign is version 8.11, with a single source modification:

In frealign_v8.f, line 10 was modified to increase MAXSET to 10000 (allows the input data 
 to be split up into large numbers of stacks)
