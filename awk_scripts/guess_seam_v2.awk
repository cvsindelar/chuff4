BEGIN {
  while( (getline < box_file) == 1)  {
    if($1 !~ "^;") {
      ++n_boxes;
      box_x[n_boxes] = $3;
      box_y[n_boxes] = $4;
    }
  }
  n_boxes = 0;
  while( (getline < ref_align_file) == 1)  {
    if($1 !~ "^;") {
      ++n_boxes;
      align_x[n_boxes] = $5;
      align_y[n_boxes] = $6;
    }
  }
  if(n_boxes < 2)  {
    print "ERROR: defective box file '" box_file "'";
    exiterror = 1;
    exit(2);
  }

  delta_dist = sqrt((box_x[2]-box_x[1])**2 + (box_y[2]-box_y[1])**2);
  delta_dist = delta_dist*pixel_size;
  
  seam_theta = seam_theta + 90;
  pi = atan2(0,-1);
  d_repeat = helical_repeat_distance;
  r13pf = 112;
  true_num_starts = num_starts; # This would be different (2x) for tubulin!
  twist_per_rep = num_pfs*twist_per_subunit;
  twist_per_rep = twist_per_rep - 360*int(twist_per_rep/360);
  if(individual)
    twist = twist_per_rep;
  else
    twist = twist_per_rep * delta_dist/helical_repeat_distance;

  twist = twist - 360*int(twist/360);
  print twist, delta_dist, helical_repeat_distance,box_x[1], box_y[1] ,  box_x[2], box_y[2]
}

NF > 8 {
  if(exiterror) exit(2);
  read_doc_line();
  if(val[1] > num_boxes)
    num_boxes = val[1];
  for(i = 2; i <= num_vals; ++i) {
    box_info[val[1]" " i - 1] = val[i];
  }
}

END {

  if(num_boxes < 1) exit(0);

  if(exiterror) exit(2);

#######
# User can specify the range of "good" boxes:
#######
  if(first_good == 0)
    first_good = 1;
  if(last_good == 0)
    last_good = num_boxes;

  for(i = 1; i <= num_boxes; ++i) {
    radon_rot = box_info[i" "1];
    avg_radon_rot += radon_rot; 
    psi = box_info[i" "5];
    if(psi == 270)
      ++psi270count;
    else
      ++psi90count;
  }
  avg_radon_rot /= num_boxes;

  if(psi270count > psi90count)
    seam_psi = 270;
  else
    seam_psi = 90;

# Note: psi = 270 corresponds to filament going in the plus Z direction
  if(seam_psi == 90) {
    twist = -twist;
    twist_per_rep = -twist_per_rep;
  }

  printf("      predicted twist per repeat: %5.2f ; per box: %6.2f\n", twist_per_rep, twist);

  guess_proto_phi();
  fix_proto_phi();
  guess_seam();

  if(num_seam_boxes >= min_num_seam_boxes && \
     num_seam_boxes/num_boxes >= min_frac_seam_boxes)
    select_mt = 1;
  else
    select_mt = 0;

  output_select_file = substr(output_file, 1, length(output_file)-4) "_good.spi";

  printf("") > output_file;

  num_selected_boxes = 0;
  select_success = 0;
  for(i = 1; i <= num_boxes; ++i)  {
    radon_rot = box_info[i" "1];
    psi = box_info[i" "5];
    theta = box_info[i" "6];
    phi = box_info[i" "7];
    ccc_score = box_info[i" "8];
    phi = int(phi + 0.5) % 360;
    val[1] = i;
    val[2] = seam_psi;
    val[3] = proto_theta[i];
    val[4] = seam_phi[i];
    val[5] = phi;
    val[6] = int(proto_phi[i] + 0.5);
    val[7] = ccc_score;
    val[8] = radon_rot - avg_radon_rot;

    for(j=1;j<=8;++j)
	newval[j] = val[j];
    print_doc_line(val, 8, output_file);
    for(j = 1; j < num_pfs; ++j)  {
	newval[4] += twist_per_subunit;
	if(newval[4] > 360) newval[4] -= 360;
	if(newval[4] < -360) newval[4] += 360;
#	print i,j,newval[4];
	print_doc_line(newval, 8, output_file);
    }

    if(i > 1) {
      delta1 = abs(seam_phi[i] - seam_phi[i-1] - twist);
    } else
      delta1 = 0;
    if(i < num_boxes) {
      delta2 = abs(seam_phi[i+1] - seam_phi[i] - twist)
    }    else
      delta1 = 0;

    if(delta1 > 180)
      delta1 -= 360;
    if(delta2 > 180)
      delta2 -= 360;

    if(select_mt && seam_index[i] == best_seam_index && \
       delta1 <= phi_tolerance && delta2 <= phi_tolerance && \
       psi == seam_psi && i >= first_good && i <= last_good) {
      if(! select_success) {
        select_success = 1;
        printf("") > output_select_file;
      }
      ++num_selected_boxes;
      print_doc_line(val, 7, output_select_file);
    }
  }
  print "num_selected_boxes: ", num_selected_boxes;
}

function guess_proto_phi()  {

  found_good_psi = 0;

  for(i = 1; i <= num_boxes; ++i) {
    psi = box_info[i" "5];
    theta = box_info[i" "6];
    phi = box_info[i" "7];

    if(psi == seam_psi)
      seam_theta = theta;

    if(psi != seam_psi)
      phi = (-phi) % 360;   # this is a crude guess, if ref. alignment
                            #  made an error with the directionality
    if(phi < 0)
      phi += 360;

    proto_theta[i] = theta;

    twist_change += twist; # Accumulated twist since last "good" psi

    if(psi != seam_psi)
      proto_phi[i] = -1;
    else if(! found_good_psi)  {
      proto_phi[i] = phi;
      found_good_psi = 1;

      twist_change = 0;
      last_proto_phi = proto_phi[i];

    }  else {
      best_dphi = 361;
      for( j = 0; j < num_pfs; ++j) {
        dphi = phi + j*360/num_pfs - last_proto_phi - twist_change;
        if(dphi > 180)
          dphi -= 360;
        if(dphi < -180)
          dphi += 360;
        if(abs(dphi - twist) < best_dphi) {
          best_dphi = abs(dphi - twist);
          proto_phi[i] = (phi + j*360/num_pfs);
          proto_phi[i] = proto_phi[i] % 360;
          if(proto_phi[i] < 0) proto_phi[i] += 360;
        }
      }
      twist_change = 0;
      last_proto_phi = proto_phi[i];
    }
  }
}

function fix_proto_phi()
{
#############
# Make guesses where the reference alignment presumably failed
#  (indicated by psi not matching the majority of boxes)
#############
  for(i = 1; i <= num_boxes; ++i)  {
    psi = box_info[i" "5];

    if(psi != seam_psi)  {

      max_delta = num_boxes + 1;
#############
# Loop to find closest box with the "correct" psi
#############
      for(j = 1; j <= num_boxes; ++j)  {
        if(box_info[j" "5] == seam_psi && i != j) {
          delta = abs(i - j);
          if(delta < max_delta) {
            max_delta = delta;
            proto_phi[i] = proto_phi[j] + (i - j) * twist;
            proto_theta[i] = box_info[j" "6];
          }
        }
      }
    }
  }
}

function guess_seam()
{
  for(i = 1; i <= num_boxes; ++i)  {
    psi = box_info[i" "5];
    phi = box_info[i" "7];

#    if(psi != seam_psi)  {
#############
# Make guesses where the reference alignment presumably failed
#############
#      phi = (-phi) % 360;   # this is a crude guess, if ref. alignment
                            #  made an error with the directionality
#    }

    best_dphi = 361;
    for( j = 0; j < num_pfs; ++j) {
      dphi = proto_phi[i] + j*360/num_pfs - phi;
      if(dphi > 180)
        dphi -= 360;
      if(dphi < -180)
        dphi += 360;
      if(abs(dphi) < best_dphi) {
        best_dphi = abs(dphi - twist);
        best_j = j;
      }
    }
    if(psi == seam_psi) {
      seam_index[i] = best_j;
      seam_count[best_j] += 1;
    } else
      seam_index[i] = -1;
  }

  num_seam_boxes = -1;
  for(j = 0; j < num_pfs; ++j)  {
    if(seam_count[j] > num_seam_boxes)  {
      num_seam_boxes = seam_count[j];
      best_seam_index = j;
    }
  }
  for(i = 1; i <= num_boxes; ++i)  {
    seam_phi[i] = proto_phi[i] + best_seam_index*360/num_pfs;
    seam_phi[i] = int(seam_phi[i] + 0.5) % 360;
  }
}

function read_doc_line()
{
  line = $0;
  num_args = split(line, args);
  val[1] = args[1]; 
  num_vals = args[2] + 1;

  if(num_vals <= 8)  {
    for(i = 0; 9+13*i <= length(line); ++i) {
      val[i + 2] = substr(line,9+13*i,12) + 0;
    }
  } else  {
    for(i = 3; i <= num_args; ++i)
      val[i-1] = args[i];
  }
}

function print_doc_line(info, num_infos, file)
{
  printf("%5d%3d", info[1], num_infos - 1) >> file;
  for(pdl_j = 2; pdl_j <= num_infos; ++pdl_j)
    printf(" %11.4f ", info[pdl_j]) >> file;
  printf "\n" >> file;
}

function abs(x)
{
  if(x >= 0) {
    return x;
  } else {
    return(-x);
  }
}
