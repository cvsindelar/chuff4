BEGIN {
  min_boxes = 2;
}

$5 == -1 {
  new_mt = 1;
  if(mt_num > 0 && fuse == 1) {
    split(line_info[mt_num" "num_boxes[mt_num]], coords);
    x0 = coords[1];
    y0 = coords[2];
    x1 = $1;
    y1 = $2;
    dim = $3;
    if(abs(x1-x0) < dim/2 && abs(y1-y0) < dim/2) {
      new_mt = 0;
      num_boxes[mt_num] -= 1;
    }
  }

  if(new_mt == 1)
    mt_num = mt_num + 1;
}
1 == 1 {
  num_boxes[mt_num] += 1;
  line_info[mt_num" "num_boxes[mt_num]] = $0;
}

END {
  num_mts = mt_num;
  mt_num = 0;
  for(i = 1; i <= num_mts; ++i) {
    if(num_boxes[i] > 1)  {
      mt_num = mt_num + 1;
      box_file_name = substr(FILENAME, 1, length(FILENAME) - 4) "_MT" mt_num "_1.box";
      if(num_boxes[i] >= min_boxes)  {
	print box_file_name; #, num_boxes[mt_num];

	if(test == 0) {
	  printf "" > box_file_name;
	  for(j = 1; j <= num_boxes[mt_num]; ++j) {
	    print line_info[i" "j] >> box_file_name;
	  }
	}
      }
      close(filename);
    }
  }
}

function abs(x)
{
  if(x >= 0) {
    return x;
  } else {
    return(-x);
  }
}
  
