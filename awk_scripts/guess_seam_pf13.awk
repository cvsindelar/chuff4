BEGIN {
# compensate for the fact I subtracted 90 from theta to make it better for plotting
  seam_theta = seam_theta + 90;
}

NF > 8 {
  read_doc_line();
  num_boxes = val[1];
  for(i = 2; i <= num_vals; ++i) {
    box_info[val[1]" " i - 1] = val[i];
  }
}

END {
  for(i = 1; i <= num_boxes; ++i) {
    phi = box_info[i" "7];

# if phi is too far from the last known seam position, adjust it to the protofilament closest 
#  to the last known seam position

    best_dphi = 361;
    new_seam_phi = 1000;
    for( j = 0; j < 13; ++j) {
      dphi = phi + 360/13*j - seam_phi;
      if(dphi > 180)
	dphi -= 360;
      if(dphi < -180)
	dphi += 360;
      if(abs(dphi) < best_dphi) {
	best_dphi = abs(dphi);
	new_seam_phi = phi + 360/13*j;
	if(new_seam_phi >= 360)
	  new_seam_phi -= 360;
      }
    }
    if(best_dphi > 7) {
#      print "Failure: " seam_phi"," phi"," new_seam_phi;
    } else if(box_info[i" "5] == seam_psi) {
      seam_phi = new_seam_phi;
      seam_theta = box_info[i" " 6];
      seam_psi = box_info[i" " 5];
    }
    val[1] = i;
    val[2] = seam_psi;
    val[3] = seam_theta;
    val[4] = int(seam_phi + 0.5);

    print_doc_line(val, 4);
#    print seam_phi;
  }
}

function read_doc_line()
{
  line = $0;
  split(line, args);
  val[1] = args[1]; 
  num_vals = args[2] + 1;

  for(i = 0; 9+13*i <= length(line); ++i) {
    val[i + 2] = substr(line,9+13*i,12) + 0;
  }
}

function print_doc_line(info, num_infos)
{
  printf("%5d%3d", info[1], num_infos - 1);
  for(j = 2; j <= num_infos; ++j)
    printf(" %11.4f ", info[j]);
  printf "\n";
}

function abs(x)
{
  if(x >= 0) {
    return x;
  } else {
    return(-x);
  }
}
  
