#     +      PSI(NP),THETA(NP),PHI(NP),SHX(NP),
#     +      SHY(NP),ABSMAGP(NP),FILM(NP),
#     +      DFMID1(NP),DFMID2(NP),ANGAST(NP),
#     +      OCC(NP),LGP(NP),S(NP),PRES(NP),DPRES(NP)
# 7011      FORMAT(I7,3F8.2,2F10.2,I8,I6,2F9.1,2F8.2,I10,F11.4,2F8.2)

$1 != "C" {
  n =$1;psi=$2;theta=$3;phi=$4;shx=$5;shy=$6;mag=$7;film=$8;dfmid1=$9;dfmid2=$10;angast=$11;
  occ=1;
  lgp=0;
  s=0;
  pres=$12;
  dpres=$13;

  printf("%7d%8.2f%8.2f%8.2f%10.2f%10.2f%8d%6d%9.1f%9.1f%8.2f%8.2f%10d%11.4f%8.2f%8.2f\n", \
         n,psi,theta,phi,shx,shy,mag,film,dfmid1,dfmid2,angast,occ,lgp,s,pres,dpres);
}
