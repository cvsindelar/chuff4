NF > 2 {
  read_doc_line();
  num_boxes = val[1];
  for(i = 2; i <= num_vals; ++i) {
    box_info[val[1]" " i - 1] = val[i];
  }
}

END {
    if(window_half_width "a" == "a")
	window_half_width = 2;
    if(error_threshhold "a" == "a")
	error_threshhold = 2;

    col_num = 1;

    smooth_column(box_info, col_num, num_boxes, window_half_width, error_threshhold);

    print " ;spi doc file";
    for(i = 1; i <= num_boxes; ++i)  {
	val[1] = i;
	val[2] = box_info[i" "col_num];
	if(box_info[i" "col_num"bad"] && box_info[i" "col_num"fit"] != "")
	    val[3] = box_info[i" "col_num"fit"];
	else
	    val[3] = box_info[i" "col_num];
	val[4] = box_info[i" "col_num"bad"];
	val[5] = box_info[i" "col_num"fit"];
	print_doc_line(val, 5);
    }
}

function smooth_column(fn1_vals, fn1_col_num, fn1_num_boxes, fn1_window_half_width, fn1_error_threshhold)
{
    last_num_bad = -1;
    num_bad = 0;
    
    while(last_num_bad != num_bad)  {
	++iteration;
#	print "Iteration " iteration;
	if(iteration > fn1_num_boxes) {        # Guess that this number of iterations is a bad sign...
	    print "Programming failure..."
	    exit(2)
	}
	    
	last_num_bad = num_bad;
	num_bad = 0;

	for(fn1_i = 1; fn1_i <= fn1_num_boxes; ++fn1_i)  {
	    fn1_begin = fn1_i - fn1_window_half_width;
	    if(fn1_begin < 1) {
		fn1_end = fn1_i + fn1_window_half_width + (1 - fn1_begin);   # add some more points on the other side
		fn1_begin = 1;
	    } else
		fn1_end = fn1_i + fn1_window_half_width;

	    if(fn1_end > fn1_num_boxes) {
		fn1_begin = fn1_i - fn1_window_half_width - (fn1_end - fn1_num_boxes);
		if(fn1_begin < 1)
		    fn1_begin = 1;

		fn1_end = fn1_num_boxes
	    }
	    iterate_lsq_fit(fn1_vals, fn1_col_num, fn1_num_boxes, fn1_i, fn1_begin, fn1_end, fn1_error_threshhold, 0);

#	    printf "%4d %6.2f %4d %4d %6.2f %1d    \n", fn1_i, fn1_vals[fn1_i" "fn1_col_num], fn1_begin, fn1_end, fn1_vals[fn1_i" "fn1_col_num"fit"], fn1_vals[fn1_i" "fn1_col_num"bad"];
#	    for(i = fn1_begin; i <= fn1_end; ++i)
#		printf "%1d ", fn1_vals[i" "fn1_col_num"bad"];
#	    printf "\n";

	    if(fn1_vals[fn1_i" "fn1_col_num"bad"])
		++num_bad;
	}
	for(fn1_i = 1; fn1_i <= fn1_num_boxes; ++fn1_i)  {
	  if(abs(fn1_vals[fn1_i" "fn1_col_num] - fn1_vals[fn1_i" "fn1_col_num"fit"]) < fn1_error_threshhold) {
#	    printf "resetting";
	    fn1_vals[fn1_i" "fn1_col_num"bad"] = 0;
	  }
#	  print fn1_i, fn1_vals[fn1_i" "fn1_col_num"bad"];
	}
    }

# Do one last least squares fitting to try and take care of any "failed" boxes (bad = -1)
    for(fn1_i = 1; fn1_i <= fn1_num_boxes; ++fn1_i)  {
      iterate_lsq_fit(fn1_vals, fn1_col_num, fn1_num_boxes, fn1_i, fn1_begin, fn1_end, fn1_error_threshhold, 1);
    }
}

function iterate_lsq_fit(fn2_vals, fn2_col_num, fn2_num_boxes, fn2_origin, fn2_begin, fn2_end, bad_thresh, final_fit)
{
# from: http://en.wikipedia.org/wiki/Linear_least_squares
    sum_x = 0;
    sum_x2 = 0;
    sum_y = 0;
    sum_xy = 0;
    fn2_n = 0;

    for(fn2_i = fn2_begin; fn2_i <= fn2_end; ++fn2_i)  {
      if(! fn2_vals[fn2_i" "fn2_col_num"bad"]) {
	    ++fn2_n;
	    sum_x += fn2_i;
	    sum_x2 += fn2_i*fn2_i;
	    sum_y += fn2_vals[fn2_i" "fn2_col_num];
	    sum_xy += fn2_i*fn2_vals[fn2_i" "fn2_col_num];
      } else if(final_fit && (fn2_vals[fn2_i" "fn2_col_num"fit"] != ""))  {
#############
# If fitting for the last time, use interpolated points to guess the "unfittable" points
#############
	    ++fn2_n;
	    sum_x += fn2_i;
	    sum_x2 += fn2_i*fn2_i;
	    sum_y += fn2_vals[fn2_i" "fn2_col_num"fit"];
	    sum_xy += fn2_i*fn2_vals[fn2_i" "fn2_col_num"fit"];
      }
    }
##############
# Fit only possible with 2 or more points
##############
    if(fn2_n >= 2)  {
	fn2_slope = (fn2_n*sum_xy - sum_x*sum_y)/(fn2_n*sum_x2 - sum_x*sum_x);
	fn2_intercept = (sum_y*sum_x2 - sum_x*sum_xy)/(fn2_n*sum_x2 - sum_x*sum_x);
	fit_val = fn2_slope*fn2_origin + fn2_intercept;

	if(final_fit == 0)  {
	  fn2_vals[fn2_origin" "fn2_col_num"fit"] = fit_val;

	  if(abs(fit_val - fn2_vals[fn2_origin" "fn2_col_num]) > bad_thresh)  {
	    fn2_vals[fn2_origin" "fn2_col_num"bad"] = 1;
	  }  else  {
	    fn2_vals[fn2_origin" "fn2_col_num"bad"] = 0;
	  }
        } else if(fn2_vals[fn2_origin" "fn2_col_num"bad"] == -1) {
#############
# If fitting for the last time, use interpolated points to guess the "unfittable" points
#############
	  fn2_vals[fn2_origin" "fn2_col_num"fit"] = fit_val;
	}
    } else  {
	fn2_vals[fn2_origin" "fn2_col_num"bad"] = -1;    # "-1" means can't fit, so don't know if good or bad
	fn2_vals[fn2_origin" "fn2_col_num"fit"] = "";
#	print "doh", fn2_origin;
    }
}

function read_doc_line()
{
  line = $0;
  split(line, args);
  val[1] = args[1]; 
  num_vals = args[2] + 1;

  for(i = 0; 9+13*i <= length(line); ++i) {
    val[i + 2] = substr(line,9+13*i,12) + 0;
  }
}

function print_doc_line(info, num_infos)
{
  printf("%5d%3d", info[1], num_infos - 1);
  for(j = 2; j <= num_infos; ++j)
    printf(" %11.4f ", info[j]);
  printf "\n";
}

function abs(x)
{
  if(x >= 0) {
    return x;
  } else {
    return(-x);
  }
}
  
