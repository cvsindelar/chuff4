
{
  outfile = sprintf("%s/multiref_plot.txt",$1);
  plotfile = sprintf("%s/multiref_plot.gnu",$1);

  directory = $1;
  n_files = NF-1;
  for(i = 1; i <= n_files; ++i)
    infile[i] = $(i+1);

  for(i = 1; i <= n_files; ++i) {
    if(psi_search == 1) {
	pf[i] = substr(infile[i], length(infile[i]) - 19, 2) + 0;
	start[i] = substr(infile[i], length(infile[i]) - 11, 1) + 0;
	psi[i] = substr(infile[i], length(infile[i]) - 6, 3);
    } else {
	pf[i] = substr(infile[i], length(infile[i]) - 12, 2) + 0;
	start[i] = substr(infile[i], length(infile[i]) - 4, 1) + 0;
    }

    print infile[i], pf[i], start[i];
    n_boxes = 0;
    while( (getline < infile[i]) == 1)  {
      if($1 !~ "^[;]") {
        ++n_boxes;
        dir[i] = $7;
        ccc[n_boxes" "i] = $10;
#        printf("%5d %7.2f %9.6f\n",n_boxes,dir[i],ccc[n_boxes" "pf[i]]);
      }
    }
  }

  printf "" > outfile;
  printf("#      ",i) >> outfile;
  for(j = 1; j <= n_files; ++j) {
#      printf("%9.6f ",dir[j]" "start[j]],ccc[i" "j]]);
    printf("%2d,%1d,%3s  ",pf[j],start[j],psi[j]) >> outfile;
  }
  printf("\n") >> outfile;

  for(i = 1; i <= n_boxes; ++i)  {
    printf("%5d ",i) >> outfile;
    for(j = 1; j <= n_files; ++j) {
#      printf("%9.6f ",dir[j]" "start[j]],ccc[i" "j]]);
      printf("%9.6f ",ccc[i" "j]) >> outfile;
    }
    printf("\n") >> outfile;
  }

  printf("set term post color\n") > plotfile;
  printf("set output \'multiref_psisearch.ps\'\n") >> plotfile;
  printf("set title \'%s\'\n",title) >> plotfile;
#  printf("set key bottom center\n") >> plotfile;
  printf("set key outside\n") >> plotfile;
  printf("set xlabel \'Box number\'\n") >> plotfile;
  printf("set ylabel \'Correlation coefficient\'\n") >> plotfile;
  printf("plot ") >> plotfile;
  for(i = 1; i <= n_files; ++i) {
    printf("\'%s\' using 1:%d with lines lw 3 title \'%d pfs %d starts (psi=%d)\'",outfile,i+1,pf[i],start[i],dir[i]) >> plotfile;
    if(i < n_files)
      printf(", ") >> plotfile;
  }
  printf("\n") >> plotfile;
#  printf("pause -1\n") >> plotfile;
}