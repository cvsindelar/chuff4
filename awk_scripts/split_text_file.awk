
{
  record[NR] = $0
  ++n_records;
}

END {
  if(num_files " " == " ")
    num_files = 8;

  n_split = int(n_records/num_files) + 1;
  
  current_rec = 1;
  while(current_rec <= n_records)  {
    for(i = 1; i <= num_files; ++i)  {
      if(current_rec > n_records)
        continue
      temp = record[current_rec];
      first_micrograph_name = strip_mt_info(record[current_rec]);
      next_micrograph_name = first_micrograph_name;
      if(group_mts != 1) {
	n_for_file[i] += 1;
	rec_for_file[i" "n_for_file[i]] = record[current_rec];
	current_rec += 1;
      } else {
	while(next_micrograph_name == first_micrograph_name && current_rec <= n_records) {
	  n_for_file[i] += 1;
	  rec_for_file[i" "n_for_file[i]] = record[current_rec];
	  current_rec += 1;
	  next_micrograph_name = strip_mt_info(record[current_rec]);
	}
      }
    }
  }

  if(header " " == " ")
    header = substr(FILENAME,1,length(FILENAME)-4);

  current_rec = 1;
  for(i = 1; i <= num_files; ++i) {
    filename = header "_" i ".txt";
    if(n_for_file[i] > 0) {
      printf "" > filename;

      for(j = 1; j <= n_for_file[i]; ++j) {
	print rec_for_file[i" "j] >> filename;
      }
    }
  }
}

function strip_mt_info(name)
{
  name2 = name;
  sub("_MT[0-9]*_[0-9]*.box$", "",name2);
  sub("_MT[0-9]*_[0-9]$", "",name2);
  return(name2);
}

