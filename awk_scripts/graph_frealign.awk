BEGIN {
    last_psi = "null";
    first_psi = 0;
}

$1 != "C"{
      particle_num = $1;
      num_particles = particle_num;
      phi[particle_num] = $4;
      theta[particle_num] = $3;
      psi[particle_num] = ($2 >= 360 ? $2 - 360 : $2);
      x[particle_num] = $5;
      y[particle_num] = $6;
      pres[particle_num] = $12;

      if(last_psi != "null") {
	  if(abs(psi[particle_num] - last_psi) > 180) {
	      if(psi[particle_num] > last_psi)
		  psi[particle_num] = psi[particle_num] - 360;
	      else
		  psi[particle_num] = psi[particle_num] + 360;
	  }
      } else {
	  first_psi = psi[particle_num];
      }
      last_psi = psi[particle_num];

      delta_psi[particle_num] = psi[particle_num] - first_psi;
}
END {
  for(i = 1; i <= num_particles; ++i) {
      if(i == 1) {
	  phi_deviation[i] = 0;
      } else {

	  min_j = -4;
	  max_j = 4;
	  min_err = 10000;
	  for(k = -3*360; k <= 3*360; k += 360 ) {
	  for(j = min_j; j <= max_j; ++j) {
	      delta_phi[j] = phi[i] - phi[i-1];
	      delta_phi[j] = delta_phi[j] + k;
	      phi_dev_err[j] = abs(delta_phi[j] - j*helical_twist);
	      if(phi_dev_err[j] < min_err) {
		  min_err = phi_dev_err[j];
		  phi_deviation[i] = delta_phi[j] - j*helical_twist;
		  delta_repeat[i] = j;
	      }
	  }
	  }
      }

      printf("%8.2f %8.2f %8.2f %8.2f %8.2f   %d  %8.2f [ %2d ] %8.2f %8.2f %5.2f\n", \
	     phi[i], theta[i], psi[i], x[i], y[i], \
	     filament_id[i], phi_deviation[i], delta_repeat[i], \
             90 - theta[i],delta_psi[i],pres[i]);
  }
}

function abs(x)
{
    return(x >= 0 ? x : -x);
}
