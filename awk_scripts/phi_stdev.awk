BEGIN {
    n_outliers = 0;
}

{
    if(abs($7) < outlier_mag) {
	n += 1;
	phi_dev[n] = $7;
    } else {
	n_outliers += 1;
    }
}

END {
    for(i = 1; i <= n; ++i) {
	phi_avg += phi_dev[i];
    }
    phi_avg /= n;

    for(i = 1; i <= n; ++i) {
	phi_stdev += (phi_dev[i] - phi_avg)^2;
    }
    phi_stdev = sqrt(phi_stdev/(n - 1));
    percent_outliers = 100*n_outliers/n;
    printf("%8.2f  %8.2f  %4d  %s\n", phi_avg, phi_stdev, percent_outliers, FILENAME);
}

function abs(x) {
    return(x >= 0 ? x : -x);
}
