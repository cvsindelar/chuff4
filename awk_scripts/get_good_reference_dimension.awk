################################
# This script figures out the right size for reference projections.
# 
# To account for a bug in SPIDER, this script also 'fixes' image dimensions
#  to have prime factors of less than 29
#  ('fp' command fails if dimension has prime factor of 29 or higher)
################################

{
  orig_voxel = $1;
  helix_dist = $2;
  bin_factor = $3;
  mt_diam = $4;

  proj_x = int( (mt_diam/orig_voxel) / bin_factor);
  proj_x = 2*int(proj_x/2 + 1);
  
  proj_y = int(2 * ( (helix_dist/orig_voxel) / bin_factor));

  spider_fp_bug_limit = 29;

  while(largest_prime_factor(proj_x) >= spider_fp_bug_limit || int(proj_x/2) != proj_x/2)  {
    proj_x += 1;
  }

  while(largest_prime_factor(proj_y) >= spider_fp_bug_limit || int(proj_y/2) != proj_y/2)  {
    proj_y += 1;
  }

  print proj_x, proj_y
}

function largest_prime_factor(x)  {
  lpf = 1;
  left_over = x;
  i = 1;
  while(lpf < left_over)  {
    i += 1;
    while(left_over/i == int(left_over/i))  {
        lpf = i;
        left_over /= lpf;
    }
  }
  return(lpf);
}
