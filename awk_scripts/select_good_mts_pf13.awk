BEGIN {
# (user setable) peak window is angular half-width of the window considered to be "one" peak...
  if(peak_window "a" == "a")
    peak_window = 20/2;

# (user setable) cutoff_frac is how much more populous the most populated peak must be over the
# next most populated peak, in order to be accepted as the "right" orientation of the microtubule...
  if(cutoff_ratio "a" == "a")
    cutoff_ratio = 1;

# (user setable) cutoff_frac is the minimum fraction of boxes that must be "good"
#  to be accepted...
  if(cutoff_frac "a" == "a")
    cutoff_frac = 0.4;

# (user setable) min_good_boxes is how many it takes to convince us we have "identified" the MT's orientation... 
# 
  if(min_good_boxes "a" == "a")
    min_good_boxes = 4;
}

NF >= 8 {
  ++box_count;
  box_info[box_count] = $0;

  if(box_count == 1) {
    peak_count += 1;
    box_peak_info[box_count] = peak_count;
    peaks[peak_count] = $8;
    peak_psis[peak_count] = $6
    peak_box_count[peak_count] += 1;
    peak_boxes[peak_count" "peak_box_count[peak_count]] = $1;
    box_peak_index[box_count] = 1;
  }  else {
    for(i = box_count - 1; i >= 1; --i) {
      j = box_peak_index[i]
      if((abs($8 - peaks[j]) <= peak_window || abs($8 + 360 - peaks[j]) <= peak_window || abs($8 - (peaks[j] + 360)) <= peak_window) && $6 == peak_psis[j]) {
#	print "yay", box_count, j, $8, peaks[j];
	box_peak_info[box_count] = j;
	peaks[j] = $8;
	peak_psis[j] = $6;
	++peak_box_count[j];
	peak_boxes[j" "peak_box_count[j]] = $1;
	box_peak_index[box_count] = j;
	break;
      } else {
#	print "boo", box_count, $8, abs($8 - peaks[i]);
      }
    }
    if(i < 1) {
      peak_count += 1;
      box_peak_info[box_count] = peak_count;
      peaks[peak_count] = $8;
      peak_psis[peak_count] = $6
      peak_box_count[peak_count] += 1;
      peak_boxes[peak_count" "peak_box_count[peak_count]] = $1;
      box_peak_index[box_count] = peak_count;
    }
    
  }
}

END {
  for(i = 1; i <= peak_count; ++i) {
    if(peak_box_count[i] > peak_box_count[max_peak]) {
      max_peak = i;
    }
  }
  for(i = 1; i <= peak_count; ++i) {
    if(peak_box_count[i] > peak_box_count[next_max_peak] && i != max_peak) {
      next_max_peak = i;
    }
  }

  if(next_max_peak == 0)
    good_mt = 1;
  else if(peak_box_count[max_peak]/peak_box_count[next_max_peak] >= cutoff_ratio)
    good_mt = 1;

  if(peak_box_count[max_peak] < min_good_boxes)
    good_mt = 0;
  if(peak_box_count[max_peak]/box_count <= cutoff_frac) {
    good_mt = 0;
#    print "bye-bye", peak_box_count[max_peak], box_count, cutoff_frac;
  }
  
  if(good_mt == 1) {

    outfile=substr(FILENAME,1,length(FILENAME) - 4) "_good.txt";
    printf "" > outfile;
    for(i = 1; i <= box_count; ++i) {
      if(box_peak_info[i] == max_peak)
	print box_info[i] >> outfile;
    }
    printf "    ACCEPTED: ";
  } else {
    printf "    REJECTED: ";
  }

  printf "Most populated peak at phi: "peaks[max_peak]" ("peak_box_count[max_peak]" boxes); ";
  if(next_max_peak != 0)
    printf " followed by "peaks[next_max_peak]" ("peak_box_count[next_max_peak]" boxes)";
  printf "\n";

  if(printinfo == 1) {  
    for(i = 1; i <= box_count; ++i) {
      print box_info[i] " " box_peak_info[i];
    }
  }
}

function abs(x) {
  if(x >= 0)
    return(x);
  else
    return(-x);
}
