BEGIN {
# (user setable) peak window is how many degrees phi is allowed to vary from one box to the next, while
#   still calling the boxes "good"
  if(peak_window "a" == "a")
    peak_window = 4;
  if(minimum_run_length "a" == "a")
    minimum_run_length = 3;
  if(min_good_fraction "a" == "a")
    min_good_fraction = 0.25;
}

NF >= 8 {
  ++box_count;
  box_info[box_count] = $0;
  good_box[box_count] = 1;

  if(box_count == 1) {
    last_seam_pos = $8;
    last_box = 1;
  }  else {
    if(abs($8 - last_seam_pos) <= peak_window || abs($8 + 360 - last_seam_pos) <= peak_window || abs($8 - (last_seam_pos + 360)) <= peak_window) {
      ;
    } else {
      good_box[box_count] = 0;
      good_box[last_box] = 0;
    }
  }
  last_seam_pos = $8;
  last_box = box_count;
}

END {
  run_length = 1;
  best_run_length = 1;
  last_good_box = -1;
  for(i = 1; i <= box_count; ++i) {
    if(good_box[i])  {
      ++num_good_boxes;
      if(i == last_good_box + 1)  {
        ++run_length;
      }  else  {
        if(run_length > best_run_length)
          best_run_length = run_length;
        run_length = 1;
      }
      last_good_box = i;
    }
  }

  if(run_length > best_run_length)
    best_run_length = run_length;

#  print "hooray", best_run_length, minimum_run_length, "num_good", num_good_boxes/box_count, min_good_fraction;

  if(best_run_length >= minimum_run_length && num_good_boxes/box_count >= min_good_fraction)  {

    outfile=substr(FILENAME,1,length(FILENAME) - 4) "_good.txt";
    printf "" > outfile;

    for(i = 1; i <= box_count; ++i) {
      if(good_box[i]) 
        print box_info[i] >> outfile
    }
  }
}

function abs(x) {
  if(x >= 0)
    return(x);
  else
    return(-x);
}
