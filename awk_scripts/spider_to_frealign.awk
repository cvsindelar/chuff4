BEGIN  {

###################
# Note that the user must provide, via awk's -v option, the variables:
#  spider_file : document file containing alignment info
#  magnification : magnification of micrographs
###################

  pi = atan2(0, -1);

  shx = 0;
  shy = 0;

  presa = 50.0;
  dpres = 50.0;

  defocus1 = 0;
  defocus2 = 0;
  astig_angle = 0;

  still_reading = 1;

####################
# Assume the following format for the spider DOC file:
# 1: index
# 2: number of data fields
# 3: in-plane rotation angle applied by spider prior to reconstruction (via 'RT SQ')
# 4: scale-factor
# 5: x-shift applied by spider prior to reconstruction (via 'RT SQ')
# 6: y-shift "
# 7: psi
# 8: theta
# 9: phi
###################

  while(still_reading == 1)  {
    still_reading = getline < spider_file;
    scale = $4;
    inplane_rt_sq[$1] = $3;   # The in-plane rotation "$rot_angle"
    best_boxfitx[$1] = $5;
    best_boxfity[$1] = $6;
    psi[$1] = $7;           # "$psi", either 90 or 270: the euler angle achieved by applying inplane_rt_sq with "rt sq"
    theta[$1] = $8;
    phi[$1] = $9;
    num_particles = $1;
  }
}

{
  for(i = 1; i <= num_particles; ++i)  {
    adjusted_astig_angle = astig_angle;

#######################
# The logic below: if a particle image corresponds to a given psi angle "psi_particle", 
#  the "RT SQ" argument that brings it to psi = 0 is -psi_particle.  In SPIDER one often
#  does a pre-rotation/shift of the particle with "RT SQ", and then assumes a psi of zero.
# In FREALIGN, there is no such pre-rotation step; instead one chooses the psi that corresponds
#  to the pre-rotation.  
# Note that the code below additionally allows for the case where the pre-rotation
#  is intended to bring the particle image, not to psi = 0, but an arbitrary value psi[i].
#######################

    final_psi = psi[i] - inplane_rt_sq[i];

#######################
# Note in the following equations that -$align_y is substituted in place of $align_y, 
#  and the final result for $shift_y
#  is negated-- these two actions are needed to go from spider's left-handed screen 
#  coordinate system to a conventional right-handed coordinate system, and back again.
#######################

    shx = (best_boxfitx[i] * cos(rad(-inplane_rt_sq[i])) - (-best_boxfity[i]) * sin(rad(-inplane_rt_sq[i])));
    shy = -(best_boxfitx[i] * sin(rad(-inplane_rt_sq[i])) + (-best_boxfity[i]) * cos(rad(-inplane_rt_sq[i])));

    printf("%7d%8.2f%8.2f%8.2f%8.2f%8.2f%8.0f%6d%9.1f%9.1f%8.2f%7.2f%6.2f\n", \
           i, final_psi, theta[i], phi[i], -shx, -shy, \
           magnification, micrograph_number,                        \
           defocus1, defocus2, adjusted_astig_angle, presa, dpres);
  }
}
function rad(x) {
    return(x*pi/180)
}
