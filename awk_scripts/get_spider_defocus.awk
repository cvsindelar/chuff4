
BEGIN  {
  micrograph_number += 0;
  found_defocus = 0;
  still_reading = 1;
  while(still_reading == 1)  {
    still_reading = getline < ctf_file;
    if($1 + 0 == micrograph_index)  {
      if(micrograph_number != $13) {
#        print "# ERROR: mismatch between micrograph number and micrograph index!!!";
#        exit(2);
      }
      defocus = $3;
      astig = $4;
      astig_angle = $5;
      found_defocus = 1;
    }
  }

  if(! found_defocus)  {
    exit(2);
  } else {
    print defocus, astig, astig_angle;
  }
  exit(0);
}
