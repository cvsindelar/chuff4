
BEGIN {

  still_reading = 1;
  particle_num = 0;
  filament_num = 0;
  still_reading = getline < box_list;

  while(still_reading == 1)  {
      particle_num += 1;

      box_name[particle_num] = $6;
      gsub("^[a-zA-Z].*[/]","",box_name[particle_num]);
      gsub("[.]box$","",box_name[particle_num]);

      if(box_name[particle_num] != last_box_name) {
	  filament_num += 1;
      }
      
      filament_id[particle_num] = filament_num;
      last_box_name = box_name[particle_num];

      still_reading = getline < box_list;
  }

  num_particles = particle_num;

  still_reading = 1;
  particle_num = 0;

  still_reading = getline < sparx_params;

  while(still_reading == 1)  {
      particle_num += 1;

      phi[particle_num] = $1;
      theta[particle_num] = $2;
      psi[particle_num] = $3;
      x[particle_num] = $4;
      y[particle_num] = $5;

      still_reading = getline < sparx_params;
  }

  if(num_particles != particle_num) {
      print "ERROR: mismatch in particle number between " sparx_params" and "box_list;
      print num_particles, particle_num;
      exit(2);
  }

  for(i = 1; i <= num_particles; ++i) {
      if(filament_id[i] != filament_id[i-1]) {
	  output_name = box_name[i];
	  phi_deviation[i] = 0;
      } else {

	  min_j = -4;
	  max_j = 4;
	  min_err = 10000;
	  for(k = -3*360; k <= 3*360; k += 360 ) {
	  for(j = min_j; j <= max_j; ++j) {
	      delta_phi[j] = phi[i] - phi[i-1];
	      delta_phi[j] = delta_phi[j] + k;
	      phi_dev_err[j] = abs(delta_phi[j] - j*helical_twist);
	      if(phi_dev_err[j] < min_err) {
		  min_err = phi_dev_err[j];
		  phi_deviation[i] = delta_phi[j] - j*helical_twist;
		  delta_repeat[i] = j;
		  guessed_dphi[i] = j*helical_twist;
	      }
	  }
	  }
      }

      output_file = output_dir "/" box_name[i] ".txt";

      printf("%8.2f %8.2f %8.2f %8.2f %8.2f   %d  %8.2f [ %2d ] %8.2f %8.2f  %8.2f\n", \
	     phi[i], theta[i], psi[i], x[i], y[i], \
	     filament_id[i], phi_deviation[i], delta_repeat[i], \
             90 - theta[i],psi[i] - 90, psi[i] - 270) \
        > output_file;
  }
  print "...done.";
}

function abs(x)
{
    return(x >= 0 ? x : -x);
}
