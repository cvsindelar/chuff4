
BEGIN  {

###################
# Note that the user must provide, via awk's -v option, the variable
#  "nominal_magnification", which is what was given to ctffind.
#  The true magnification of the micrographs may be more accurately
#   determined by the actual spacing found in the microtubules, but
#   this would require re-running ctffind.  Currently, therefore,
#   the repeat spacing of the microtubules is considered in units 
#   of pixels.  (i.e. the user-specified repeat distance, in angstroms,
#   is ignored).
###################

  pi = atan2(0, -1);

  shx = 0;
  shy = 0;

  presa = 50.0;
  dpres = 50.0;

  found_defocus = 0;
  still_reading = 1;
  while(still_reading == 1)  {
    still_reading = getline < ctf_file;
    if($1 + 0 == micrograph_index)  {
      if(micrograph_number != $13) {
        print "# ERROR: mismatch between micrograph number and micrograph index!!!";
        exit(2);
      }
      defocus1 = $3;
      defocus2 = $4;
      astig_angle = $5;
      found_defocus = 1;
    }
  }

  if(! found_defocus)  {
    exit(2);
  }

  still_reading = 1;

  while(still_reading == 1)  {
    still_reading = getline < euler_file;
    scale = $4;
    inplane_rt_sq[$1] = $3;   # The in-plane rotation "$rot_angle"
    best_boxfitx[$1] = $5;
    best_boxfity[$1] = $6;
    psi[$1] = $7;           # "$psi", either 90 or 270: the euler angle achieved by applying inplane_rt_sq with "rt sq"
    theta[$1] = $8;
    phi[$1] = $9;
  }
}

{
#  true_magnification = scanner_pixel_size*10000 / (bin_factor*helical_repeat_distance/pixels_per_repeat);
  for(i = 1; i <= NF; ++i)  {
    adjusted_astig_angle = astig_angle;

# The logic below: if a particle image corresponds to a given psi angle "psi_particle", 
#  the "RT SQ" argument that brings it to psi = 0 is -psi_particle.  In SPIDER one often
#  does a pre-rotation/shift of the particle with "RT SQ", and then assumes a psi of zero.
# In FREALIGN, there is no such pre-rotation step; instead one chooses the psi that corresponds
#  to the pre-rotation.  
# Note that the code below additionally allows for the case where the pre-rotation
#  is intended to bring the particle image, not to psi = 0, but an arbitrary value psi[$i].

    final_psi = psi[$i] - inplane_rt_sq[$i];

# Note in the following equations that -$align_y is substituted in place of $align_y, and the final result for $shift_y
#  is negated-- these two actions are needed to go from spider's left-handed screen coordinate system to a
#  conventional right-handed coordinate system, and back again.

    shx = (best_boxfitx[$i] * cos(rad(-inplane_rt_sq[$i])) - (-best_boxfity[$i]) * sin(rad(-inplane_rt_sq[$i])));
    shy = -(best_boxfitx[$i] * sin(rad(-inplane_rt_sq[$i])) + (-best_boxfity[$i]) * cos(rad(-inplane_rt_sq[$i])));
    shx /= bin_factor;
    shy /= bin_factor;

    printf("%7d%8.2f%8.2f%8.2f%8.2f%8.2f%8.0f%6d%9.1f%9.1f%8.2f%7.2f%6.2f\n", \
           proj_num_start + (i - 1), final_psi, theta[$i], phi[$i], -shx, -shy, \
           nominal_magnification/bin_factor, micrograph_number,                        \
           defocus1, defocus2, adjusted_astig_angle, presa, dpres);
  }
}
function rad(x) {
    return(x*pi/180)
}
