################################
# To account for a bug in SPIDER, this script 'fixes' image dimensions
#  to have prime factors of less than 17 (?!), and also to be even
#  (new dimension guaranteed <= original dimension)
################################

{
  x = $1;

  spider_lpf_limit = 13;

  while(largest_prime_factor(x) > spider_lpf_limit || x/2 != int(x/2))  {
    x -= 1;
    if(debug) {
      print "mmkay", x, largest_prime_factor(x);
    }
  }
  
  print x;
}

function largest_prime_factor(x)  {
  lpf = 1;
  left_over = x;
  i = 1;
  while(lpf < left_over)  {
    i += 1;
    while(left_over/i == int(left_over/i))  {
        lpf = i;
        left_over /= lpf;
    }
  }
  return(lpf);
}
