# Bin data for a histogram
# Modifying Tara Murphy's code
# usage: for data values between 0 and 10, with 5 bins
# gawk -f histbin.awk 0 10 5 data.out data.in

BEGIN { 
  m = ARGV[1]      # minimum value of x axis
  gsub("[\\\\]", "", m);
  ARGV[1] = ""
  M = ARGV[2]      # maximum value of x axis
  gsub("[\\\\]", "", M);
  ARGV[2] = ""
  b = ARGV[3]      # number of bins
  ARGV[3] = ""
  col = ARGV[4]    # column with data
  ARGV[4] = ""
  file = ARGV[5]   # output file for plotting
  ARGV[5] = ""

  w = (M-m)/b      # width of each bin
  
  print "number of bins = "b
  print "width of bins  = "w 
  print

  # set up arrays
  for (i = 0 ; i <= b; ++i) {
    n[i] = m+(i*w)        # upper bound of bin
    c[i] = n[i] - (w/2)   # centre of bin
    f[i] = 0              # frequency count
  }

  n[b] = M;                # Fix rounding errors

  c[0] = "# -infinity";    # These bins count everything less than the min bin value
  n[b+1] = "# +infinity";    #  so there is no effective bin center
  c[b+1] = "# +infinity";
  f[b+1] = 0;
}

substr($1, 1, 1) != "#" { 
  # bins the data
  for (i = 0; i <= b; ++i) {
    if ($col <= n[i]) {
         ++f[i]
         break        
    }
  }
  if($col > n[b])
    ++f[b+1];
}

END {
  # print results to screen
  # and to a file for plotting
#  print "# bin(centre), freq"
  print "# bin(centre), freq" > file

  for (i = 0; i <= b+1; ++i) {
    if (f[i] > 0) {
#      print ""c[i]"", f[i]
      print c[i], f[i] >> file
    }
    else {
#      print ""c[i]"", 0
      print c[i], 0 >> file
    }
  }
}
