set refinement_resolutions = (15 15 15 12)
set frealign_dir = frealign_bin1_gold1_graphs_smooth_recon

set frealign_header = $frealign_dir/$cwd:t
set frealign_info = $frealign_dir/frealign_input_file_info.txt

set qsub = 1
set nodes = 2
set cores = 16
set queue = sindelar

####################
# Generate the frealign directory (nuke=1 should work, but use /bin/rm to be safe)
####################

/bin/rm -r $frealign_dir

chuff_export_smoothed_to_frealign frealign_bin1_gold?_graphs_smooth/*1.txt

####################
# Loop through frealign rounds
####################

    set round = 0

    @ max_round = $#refinement_resolutions - 1
    while($round <= $max_round)
	@ round = $round + 1

        set refine_high_res = $refinement_resolutions[$round]

	if($round == 2) then
	    set refine_option = 'refine_coordinate_mask=0,0,0,1,1'
	else
	    set refine_option = ''
	endif

	chuff_frealign_parallel \
            frealign_dir = $frealign_dir \
	    input_file_info = $frealign_info \
	    round=$round \
	    refine_high_res=$refine_high_res $refine_option \
	    recon_cores=16 nodes=$nodes cores=$cores qsub=$qsub queue=$queue
        if($status != 0) exit(2)

	proc3d ${frealign_header}'_'${round}.{img,spi} spidersingle

	set mask_file = $frealign_header'_1_softmask.spi'
	if($round == 1) then
	    chuff chuff_soft_mask vol = $frealign_header'_1.spi' mass = 1.2 \
                          nodes=1 cores=4 qsub=$qsub queue=$queue
            if($status != 0) exit(2)

	    ln -s $mask_file .
	endif
	set mask_file = $mask_file:t

	if(-e $mask_file) then
	    spif mu $frealign_header'_'${round}, $mask_file:r,$frealign_header'_'${round}'_masked', '*'
	    /bin/mv $frealign_header'_'${round}{,_nomask}.spi
	    /bin/mv $frealign_header'_'${round}{_masked,}.spi

	    proc3d ${frealign_header}'_'${round}.{spi,img} spidersingle
	else
	    echo Error: failed to generate soft mask file...
	    exit(2)
    	endif

####################
# End of loop through frealign rounds
####################
    end
