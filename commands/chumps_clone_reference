#! /bin/csh

# chuff_args round=1 num_pfs=13 num_starts=3 tolerance=0.05 dir
# chuff_args subunit=0

# chuff_required_variables target_magnification scanner_pixel_size

# chuff_help_info
# chumps_import_reference dir=<dir> [<job_dir1> <job_dir2> ...]
#:  <dir> is location of directory with 
#    pre-generated reference images
#   <job_dir1> etc. are processed microtubules used to estimate the magnification
#
# Available options: 
#   round [=1]                  Which round of chumps, i.e. chumps_round1
#   tolerance [=0.05]           Specify the fractional error that can be tolerated in 
#                                in the reference pixel size, compared with current estimate
#                                from the data set
#   subunit=1                   Specify that the linked directory contains 
#                                individual subunit projections(for chumps_find_kinesin, etc)
#                                rather than whole MT projections

################
# Set up chuff environment
################
source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

################
# Set up microtubule-specific environment
################
source "$chuff_dir"/csh_scripts/setup_chumps.csh \
    make_synth_mt.spi.spif \
    make_ref_proj_oversamp.spi.spif
if($status != 0) exit(2)

################
# Link to reference directory
################

if(! $?dir) then
    echo 'ERROR: please specify "dir=<dir>"'
    exit(2)
endif

if(! -d $dir) then
    echo "ERROR: directory $dir does not exist..."
    exit(2)
endif

set dir = `echo $dir | awk '{sub("[/]$", "", $1); print $1}'`

if(-e $dir/info.txt) then
    set num_pfs = `awk '$1 == "num_pfs" {print $2}' $dir/info.txt`
    set num_starts = `awk '$1 == "num_starts" {print $2}' $dir/info.txt`
endif

echo Number of protofilaments, starts: $num_pfs $num_starts
##################
# Get best estimate of pixel size, using user input helical_repeat_distance
#  and radon information
##################

if(1 == 1) then
    cd $orig_dir
    set scale_files = ()
    foreach job_dir($job_dirs)
        if(-e chumps_round1/$job_dir:t/radon_scale_doc.spi) then
            set scale_files = ($scale_files chumps_round1/$job_dir:t/radon_scale_doc.spi)
        endif
    end

    if($#scale_files == 0) then
        echo 'WARNING: did not find any scaling information from chumps_radon;'
        echo '  guessing pixel size from $target_magnification and $scanner_pixel_size'
        set est_pixel_size = $micrograph_pixel_size
    else
        set est_pixel_size = `awk -v cur_est_pixel_size=$micrograph_pixel_size '$1 == 1 && $2 == 2 {sum += $3; count += 1} END {print sum/count * cur_est_pixel_size}' $scale_files`
    endif
endif

set good_psize = `awk -v est_pixel_size=$est_pixel_size \
                  -v tolerance=$tolerance \
                  '$1 == 2 {ref_psize = $5} \
                  END {ratio = ref_psize/est_pixel_size; \
                       desired_ratio = int(ratio + 0.5); \
                       frac = (ratio - desired_ratio)/desired_ratio; \
                       if(frac < 0) frac = -frac; \
                       if(frac > tolerance) print 0,frac,ref_psize; else print 1,frac,ref_psize}' \
                  $dir/ref_params.spi`

echo Estimated pixel size: $est_pixel_size 
echo "   (estimate from chuff_parameters.m : $micrograph_pixel_size )"

echo 'Reference pixel size: ' $good_psize[3]
echo 'Fractional error: ' $good_psize[2]

if($good_psize[1] == 0) then
    echo 'ERROR: reference pixel size is not an integral multiple of est_pixel_size'
    exit(2)
endif

if($subunit == 0) then
    set ref_dir = ref_pf${num_pfs}_start${num_starts}
else
    set ref_dir = $dir:t
endif

if(-e $chumps_dir/$ref_dir) then
    echo 'NOTE: reference directory already exists. Nothing done.'
    exit(0)
endif

cd $dir
set dir = $cwd
cd $orig_dir/$chumps_dir/
ln -s $dir $ref_dir

if(! -e $orig_dir/$chumps_dir/$ref_dir) then
    echo 'ERROR: reference directory not created: ' $chumps_dir/$ref_dir
    exit(2)
endif
