#! /bin/csh
# chuff_args renumber=0

# chuff_required_executables dm2mrc

# chuff_help_info 
# chuff add_movie_links <micrograph or box file> [...]
# 
#  This command adds links to the specified files, in your "movies/" directory
#  
#  Use "renumber=1" option to rename links 
#    as "0001.mrc", "0002.mrc", etc.
#####################################

################
# Set up chuff environment
################
if(! "$?chuff_dir") then
    echo 'ERROR: please set the shell variable "$chuff_dir" to the location of your'
    echo '      "chuff3" directory.  The best way to do this is to add the line:'
    echo '           source <...>/chuff3/chuff.cshrc'
    echo '      to your ".cshrc" file in your home directory.'
    exit(2)
endif

source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

if($#script_args < 1) then
    echo 'ERROR: must provide at least one micrograph file to this script...'
    exit(2)
endif

if(! -d movies) mkdir movies

@ new_micrograph_num = 1
@ num_old_micrographs = 0
@ num_new_micrographs = 0

##############################
# First rebuild the link_info.txt file to make sure it is up to date
##############################

printf "" >! movies/link_info.txt

cd $orig_dir/movies
/bin/ls *.mrc *.tif *.tiff *.dm4 >& /dev/null
if($status == 0) then
    foreach file(`/bin/ls *.mrc |sort -t "_" -n -k2,2 -k3,3 -k4,4 -k5,5 -k6,6 -k7,7 -k8,8 -k9,9 -k10,10 -k11,11 -k12,12 -k13,13`)
	if(-l $file) then
	    (/bin/ls -l $file | awk '{print $(NF-2), $NF}' >>! link_info.txt) >& /dev/null
	endif
    end
endif

cd $orig_dir

set mrc_files = ()

set gainref_files = `/bin/ls -1 $script_args[1]:h/Count*dm4`
if($#gainref_files == 0) set gainref_files = `/bin/ls -1 $script_args[1]:h/gain*dm4`

set all_files = (`/bin/ls -1 $script_args $gainref_files | sort -u`)

set all_files_sort = (`/bin/ls -1 $all_files | sort -t "_" -n -k2,2 -k3,3 -k4,4 -k5,5 -k6,6 -k7,7 -k8,8 -k9,9 -k10,10 -k11,11 -k12,12 -k13,13`)

if($#all_files_sort == 0) then
    echo 'ERROR: no files found...'
    exit(2)
endif

set movie_files = ()
foreach file($all_files_sort)
    if($file:e == 'mrc' || $file:e == 'dm4' || $file:e == 'tif' || $file:e == 'tiff') then
        set movie_files = ($movie_files $file)
    else
        echo 'ERROR: file "'$file'" must have the proper extension: ".mrc", ".dm4", ".tif", or ".tiff" '
        exit(2)
    endif
end

foreach file(`/bin/ls $movie_files | sort -t "_" -n -k2,2 -k3,3 -k4,4 -k5,5 -k6,6 -k7,7 -k8,8 -k9,9 -k10,10 -k11,11 -k12,12 -k13,13`)
    if(! -e $file) then
        echo 'ERROR: file "'$file'" does not exist...'
        exit(2)
    endif

    set extension = $file:e
    set new_micrograph_num = `echo $new_micrograph_num | awk '{printf "%04d\n", $1}'`

    cd movies
    while(-e ${new_micrograph_num}.$extension)
        set new_micrograph_num = `echo $new_micrograph_num | awk '{printf "%04d\n", $1 + 1}'`
        @ num_old_micrographs = $num_old_micrographs + 1
    end
    cd $orig_dir

    if($file:t != $file) then
        cd $orig_dir
        cd $file:h
        set file = $cwd/$file:t
        cd $orig_dir
    else
        set file = $cwd/$file
    endif

########################
# Dereference symbolic links if possible
########################
    if($?orig_link) unset orig_link
    if(-l $file) then

        set orig_link = $file
##################################
# awk script and following if statement handles the pathological case where the symbolic link is relative, not absolute
#  i.e.: 0000.mrc -> ../../data/071019_abeta_-norm_198.mrc
##################################

        set true_source_file = `/bin/ls -l $file | awk '{if($NF ~ "^[/]") {print $NF;} else {print $(NF-2), $NF}}'`

        if($#true_source_file == 2) then
            if($true_source_file[1] != $true_source_file[1]:h) cd $true_source_file[1]:h
	    if($true_source_file[2] != $true_source_file[2]:h) cd $true_source_file[2]:h

            set file = $cwd/$true_source_file[2]:t
        endif
    else
	set true_source_file = $file
    endif

    if(! -e $true_source_file) then
        echo 'ERROR: file "'$true_source_file'" does not exist...'
        if($?orig_link) then
            echo '    (this was a symbolic link dereferenced from: "'$orig_link'")'
        endif
        exit(2)
    endif
    cd $orig_dir

##########################################
# Following awk statement tests if an entry for this file exists already in movies/link_info.txt
#  (so no redundant entries are added)
##########################################

    set add_file = `awk -v candidate=$true_source_file '{if($2 == candidate) bad = 1;} END {if(bad == 1) print 0; else print 1}' movies/link_info.txt`

    if($add_file == 1) then

        echo Adding: $true_source_file:t' '

##########################################
# Just add mrc files
##########################################

#        echo adding: $true_source_file
        if($extension == mrc || $extension == dm4 || $extension == tif || $extension == tiff) then
            cd $orig_dir/movies

	    if($renumber == 0) then
		if(-e $file:t) then
		    echo 'Note: target file "'$file:t'" already exists within "movies/"... skipping.'
		    cd $orig_dir
		    continue
		endif

		/bin/ln -s $true_source_file $file:t
		echo $file:t $true_source_file >>! link_info.txt
	    else
		/bin/ln -s $true_source_file ${new_micrograph_num}.$extension
		echo ${new_micrograph_num}.$true_source_file:e $true_source_file >>! link_info.txt
	    endif

	    @ num_new_micrographs = $num_new_micrographs + 1

            cd $orig_dir
        endif

    endif # if link didn't already exist
    cd $orig_dir
end

cd $orig_dir

echo '       '$num_new_micrographs' micrographs added.'

if($#gainref_files > 0) then
    echo Using gain reference: $gainref_files[1]
    set gainref_mrc = movies/$gainref_files[1]:r:t.mrc

    if(! -e $gainref_mrc) then
	dm2mrc_prog $gainref_files[1] $gainref_mrc
    endif
else
    echo Note: gain reference image not found
endif
