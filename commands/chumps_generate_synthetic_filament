# chuff_args round compile micrograph_header=micrograph micrograph_x=2000 micrograph_y=2000
# chuff_args decorate twist_per_subunit psi=90 theta=90
# chuff_args psi_oscillation_amplitude=0 theta_oscillation_amplitude=0
# chuff_args phi_oscillation_amplitude=0 psi_oscillation_period=50
# chuff_args theta_oscillation_period=50 phi_oscillation_period=50
# chuff_args angle_step=1 min_theta=75 max_theta=105
# chuff_args snr_solvent=1 snr_microscope=0.1
# chuff_args defocus=12000 astig=0 astig_angle=45 bin_factor=1 
# chuff_args actomyosin amyloid microtubule b_factor=0 input_pdb

# chuff_required_variables helical_repeat_distance
# chuff_required_variables invert_density
# chuff_required_variables target_magnification scanner_pixel_size
# chuff_required_variables spherical_aberration_constant accelerating_voltage amplitude_contrast_ratio

# chuff_required_executables spider

# chuff_help_info
# Generate a synthetic micrograph containing a filament
# 
# Output micrographs will be placed in the directory "synthetic_micrographs/", and will
#  be named "micrograph_0001.spi", etc. (unless otherwise specified). 
# 
# This command can be issued multiple times to make multiple micrographs.  In this case, the reference
#  projections are not re-calculated, so some parameters (i.e. angle_step, min_theta, etc.) become
#  irrelevant.
#
# NOTE: to turn off CTF application, specify defocus=0 (NB default defocus is 12000 Angstroms)
# 
#     [micrograph_x = <val>]          micrograph dimensions
#     [micrograph_y = <val>]          micrograph dimensions
#     [amyloid=1 || microtubule=1 || actomyosin=1]    use provided coordinates: 
#                                    chuff_data/amyloid_guess.pdb
#                                    chuff_data/tub_only.pdb
#                                    chuff_data/actomyosin_single.pdb
#     [input_pdb=<pdb file>]         provide coordinates for generating filament
#     [micrograph_header=<string>]  micrographs are named <micrograph_header>_0001.spi, etc.
#     [twist_per_subunit=<val>]     phi rotation per helical repeat
#     [psi=<val>]     In-plane rotation (at beginning of filament) (90 means vertical)
#     [theta=<val>]                 Euler angle for out of plane tilt (90 means in plane)
#     [psi_oscillation_amplitude=<val>]       in-plane rotation will oscillate with this amplitude (degrees)
#     [theta_oscillation_amplitude=<val>]     out-of-plane rotation will oscillate with this amplitude (degrees)
#     [phi_oscillation_amplitude=<val>]       twist of filament will oscillate with this amplitude (degrees)
#     [psi_oscillation_period=<val>]          period of the oscillation (in units of repeats) (try 50)
#     [theta_oscillation_period=<val>]        period of the oscillation (in units of repeats) (try 50)
#     [phi_oscillation_period=<val>]          period of the oscillation (in units of repeats) (try 50)
#     [bin_factor=<val>]               To speed up calculations, can bin the reference volume used for generating 
#                                         projections; this affects the quality of the synthetic micrograph,
#                                         NOT the micrograph pixel size.
#     [angle_step=<val>]        parameter for reference projection: how finely to sample the euler angles
#     [min_theta=<val>]         parameter for reference projection: how much out of plane tilt to allow (min)
#     [max_theta=<val>]         parameter for reference projection: how much out of plane tilt to allow (max)
#     [snr_solvent=<val>]       SNR of solvent noise, relative to MT density (set to 0 to disable this term)
#     [snr_microscope=<val>]    SNR of the imaging process, relative to MT density (set to 0 to disable this term)
#     [defocus=<val>]           default defocus is 12000 Angstroms = 1.2 microns
#     [astig=<val>]
#     [astig_angle=<val>]
#     [invert_density=<val>]    use -1 or or 1; -1 is the default, to make images corresponding to negatives

################
# Set up chuff environment
################
source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

if($#script_args > 0) then
    echo 'ERROR: arguments not understood: '$script_args
    exit(2)
endif

########################################################
# Input parameters
########################################################

#######################
# Default behavior: use the amyloid file
#######################

if(! $?input_pdb && ! $?amyloid && ! $?microtubule && ! $?actomyosin) then
    set amyloid
endif

set true_repeat_distance = $helical_repeat_distance

if(! $?twist_per_subunit) then
    set twist_per_subunit = $helical_twist      # as specified in chuff_parameters.txt

#################################
# FUDGE: the true symmetry of the beta amyloid filament is probably different than what we are 
#   currently telling frealix.  Here we assume the "true" symmetry.
#################################

    if($?amyloid) then
        set twist_per_subunit = `echo $twist_per_subunit | awk '{print $1/2 + 180}'`
        set true_repeat_distance = `echo $helical_repeat_distance | awk '{print $1/2}'`
    endif
endif

if(! $?input_pdb) then
    if($?amyloid) then
        set input_pdb = $chuff_dir/data/amyloid_guess.pdb
    endif
    if($?microtubule) then
        set input_pdb = $chuff_dir/data/tub_only.pdb
    endif
    if($?actomyosin) then
        set input_pdb = $chuff_dir/data/actomyosin_single.pdb
    endif
endif

if(! $?input_pdb) then
    echo 'ERROR: no pdb model defined...'
    exit(2)
endif

########################################################
# End of input parameters
########################################################

set output_directory = synthetic_micrographs

if(-d $output_directory) then
    echo 'Note: directory "'$output_directory'" exists already...'
#    exit(2)
else
    mkdir $output_directory
endif

set pdb_file = filament_model.pdb

cat $input_pdb >! $output_directory/$pdb_file

cd $output_directory

########################
# Get scripts
########################

/bin/cp $chuff_dir/spider_scripts/{generate_micrograph.spi{,.spif},make_ref_proj.spi{,.spif},downsample_centered.spi{,.spif}} ./
if($status != 0) then
    exit(2)
endif

########################
# Set up other variables
########################

set filament_diameter = `echo $filament_outer_radius | awk '{print $1*2}'`  # filament diameter in Angstroms, generous (~175 is good for kin*MT)

set box_dim = `echo $micrograph_pixel_size $true_repeat_distance $bin_factor $filament_diameter | awk -f $chuff_dir/awk_scripts/get_good_reference_dimension.awk`
set box_dim = $box_dim[1]

set synth_voxel_size = `echo $micrograph_pixel_size $bin_factor | awk '{print $1*$2}'`

#####################################
# Generate the reference images
#####################################

set min_phi = 0
set max_phi = 359.99

set reference_directory = ref_synth
set reference_base = $reference_directory/ref
set input_volume = synth_filament.spi
set secondary_bin_factor = 1

if(! -e ${reference_base}_000001.spi) then

########################
# Make reference volume
########################

     if(! -e synth_filament.spi) then

        if($?decorate) then
            cat $orig_dir/$decorate >>! $pdb_file
            if($status != 0) then
                echo 'ERROR: could not read the pdb file: "'$decorate'"...'
                exit(2)
            endif
        endif
        echo END >>! $pdb_file

        $spif_prog cp_from_pdb $pdb_file , $synth_voxel_size , N , A , $box_dim $box_dim $box_dim , $input_volume

     endif

     mkdir $reference_directory

     $spif_prog ./make_ref_proj.spi \
     $reference_base \
     $input_volume \
     $synth_voxel_size \
     $true_repeat_distance \
     $secondary_bin_factor \
     $filament_outer_radius \
     $min_phi $max_phi $min_theta $max_theta $angle_step
else
    echo 'Using previously-calculated reference image set: "'$reference_base'"...'
endif

#####################################
# Now make fake micrograph
#####################################

@ i = 1
set num = `echo $i | awk '{printf "%04d\n", $1}'`
while(-e ${micrograph_header}_${num}.spi)
    @ i = $i + 1
    set num = `echo $i | awk '{printf "%04d\n", $1}'`
end

$spif_prog generate_micrograph.spi \
          $micrograph_header \
          $reference_directory \
          $micrograph_x $micrograph_y $theta $psi \
          $micrograph_pixel_size $twist_per_subunit \
          $psi_oscillation_amplitude $theta_oscillation_amplitude $phi_oscillation_amplitude \
          $psi_oscillation_period $theta_oscillation_period $phi_oscillation_period \
	  $snr_solvent \
	  $snr_microscope \
	  $b_factor \
          $defocus $astig $astig_angle \
          $amplitude_contrast_ratio \
          $accelerating_voltage \
          $spherical_aberration_constant \
          $invert_density >& /dev/null

if(-e ${micrograph_header}_${num}.spi) then
    echo ${micrograph_header}_${num}'.spi' $chuff_args >>! micrograph_info.txt
    printf "\n"
    echo 'Micrograph "'${micrograph_header}_${num}'.spi" generated.'
    echo 'Parameters saved in "'$output_directory'/micrograph_info.txt"'
else
    echo 'ERROR: failed to generate "'${micrograph_header}_${num}'.spi"'
    echo '  use "tail -1 synthetic_micrographs/spif.log" to see the failed command...'
    exit(2)
endif

# spif 'cp to tif('$micrograph_name:r,$micrograph_name:r')' >& /dev/null
# spif 'pw('$micrograph_name:r,$micrograph_name:r'_pw)' >& /dev/null
# spif 'cp to tif('$micrograph_name:r'_pw, ' $micrograph_name:r'_pw)' >& /dev/null
# spif 'wi(' ${micrograph_name}_pw, ${micrograph_name}_layerline, 1000 100, 501 430 ')' >& /dev/null
# spif 'cp to tif('${micrograph_name}_layerline,' ' ${micrograph_name}_layerline')' >& /dev/null

