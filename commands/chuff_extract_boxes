#! /bin/csh
# chuff_args nuke=0 phase_flipped=0 pre_rotate=1
# chuff_args bin_factor=1
# chuff_args hp_filter=1000 graph_dir=null frealign_dir=null 
# chuff_args frealign_round stack_format=sparx fix_polarity=1 extract_debug=0
# chuff_args box_dim

# chuff_required_executables proc2d batchboxer octave

################
# Set up chuff environment
################
if(! $?chuff_dir) then
    echo 'ERROR: please set the shell variable "$chuff_dir" to the location of your'
    echo '      "chuff3" directory.  The best way to do this is to add the line:'
    echo '           source <...>/chuff3/chuff.cshrc'
    echo '      to your ".cshrc" file in your home directory.'
    exit(2)
endif

source $chuff_dir/csh_scripts/chuff.csh
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source $chuff_dir/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Set up the directories, etc
################

foreach box_file($script_args)
    if(! -e $box_file) then
	echo 'ERROR: box file "'$box_file'" does not exist...'
	exit(2)
    endif

    if(-e ${box_file:r}_bin${bin_factor}.spi && $nuke == 0 & $stack_format != 'null') then
#	echo 'Note: ' ${box_file:r}_bin${bin_factor}.spi processed already.
#	echo ' Skipping. Use 'nuke=1' to override...'
#	exit(2)
    endif
end

################
# Loop through each box file
################

foreach box_file($script_args)

    if(-e ${box_file:r}_bin${bin_factor}.spi && $nuke == 0) then
	echo 'Note: ' ${box_file:r}_bin${bin_factor}.spi processed already.
	echo ' Skipping. Use 'nuke=1' to override...'
	continue
    endif

    echo $box_file

    if($graph_dir != 'null') then
# Get rid of trailing "/" to avoid problems with ":h" operator
	set graph_dir = `echo $graph_dir | awk '{sub("[/]$", "", $1); print $1}'`
	set align_info = $graph_dir'/'$box_file:t:r'.txt'
	set align_bin_factor = `awk '$1 == "bin_factor" {print $2}' $graph_dir:h/info.txt`
	set stack_format = 'frealign'
#	set phase_flipped = 0
    else
	set align_info = 'null'
	set align_bin_factor = 1
	set fix_polarity = 0        # polarity fixing requires an initial alignment
    endif

    if($frealign_dir != 'null') then
	if(! $?frealign_round) then
	    echo 'ERROR: need to specify frealign_round=<n> for frealign_dir option'
	    exit(2)
	endif
	set frealign_align_info = $frealign_dir'_graphs/'$box_file:t:r'_'${frealign_round}'.txt'
	set frealign_bin_factor = `awk '$1 == "bin_factor" {print $2}' $frealign_dir/info.txt`
    else
	set frealign_align_info = 'null'
	set frealign_bin_factor = $align_bin_factor
    endif

    if(-e $box_file:r'_temp.spi') /bin/rm $box_file:r'_temp.spi'

    set micrograph = `echo $box_file | \
        awk '{mic = $1; \
              sub("_MT[0-9]+_[0-9]+.box$","",mic); \
              print mic".mrc"}'`

    if($phase_flipped != 0) then
        set micrograph = $micrograph:r'_flipped.spi'
    endif

    if($bin_factor != 1) then
	set micrograph = $micrograph:r'_bin'$bin_factor'.spi'
    endif

    if($micrograph:e != 'mrc') then
	if($stack_format != 'null') then
	    proc2d $micrograph $micrograph:r'.mrc'
	endif
	set micrograph = $micrograph:r'.mrc'
    endif

    if(! -e $micrograph) then
	echo 'ERROR: "'$micrograph'" does not exist...'
	exit(2)
    endif

    set scale = `echo $bin_factor | awk '{print 1.0/$1}'`
    set binned_pixel_size = `echo $micrograph_pixel_size $bin_factor | awk '{print $1*$2}'`

    if(! $?box_dim) then
	set final_box_dim = `awk -v bin_factor=$bin_factor 'NR == 1 {print 2*int($3/bin_factor/2)}' $box_file`
    else
	set final_box_dim = $box_dim
    endif

# Double the box size because we need padding for the rotation;
#  also note that batchboxer expects "newsize" as for the input, not the 
#  output, box
    set big_box_dim = `echo $final_box_dim | awk -v bin_factor=$bin_factor '{print 2*bin_factor*$1}'`

    if($extract_debug == 0) then
	batchboxer_prog input=$micrograph dbbox=$box_file \
	       output=$box_file:r'_temp.spi' \
	       scale=$scale newsize=$big_box_dim
iminfo $box_file:r'_temp.spi'
    endif

#    set angle = `awk '$1 == 1 {print $3}' $box_file:r'_angle_doc.spi'`

    set box_doc_file = $box_file:r'_doc.spi'

# echo     octave_prog --eval "rotate_spider_stack('${box_file:r}_temp.spi','${box_file:r}_bin${bin_factor}.spi','${box_file:r}','$align_info','$frealign_align_info','$box_doc_file',$binned_pixel_size,$hp_filter,$align_bin_factor,$frealign_bin_factor,$bin_factor,$final_box_dim,'$stack_format',$pre_rotate,$fix_polarity);"

     octave_prog --eval "rotate_spider_stack('${box_file:r}_temp.spi','${box_file:r}_bin${bin_factor}.spi','${box_file:r}','$align_info','$frealign_align_info','$box_doc_file',$binned_pixel_size,$hp_filter,$align_bin_factor,$frealign_bin_factor,$bin_factor,$final_box_dim,'$stack_format',$pre_rotate,$fix_polarity);"

    /bin/rm $box_file:r'_temp.spi'

end
