#! /bin/csh
# chuff_args box_list sparx_params yrange=30 phi_histogram_bins=10 
# chuff_args outlier_magnitude=10

################
# Set up chuff environment
################
source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

if(! $?sparx_params) then
    echo 'ERROR: must specify a SPARX parameter file'
    exit(2)
endif

set graph_dir = $sparx_params:h:r'_graphs'
set sparx_dir = $sparx_params:h
set sparx_dir = $sparx_dir:h

if(! $?box_list) then
    set box_list = $sparx_dir/sxihrsr_stack.box
endif

if(! -e $box_list) then
    echo "ERROR: box list can't be found: " $box_list
    exit(2)
endif

if(! -d $graph_dir) mkdir $graph_dir

echo null | awk -v sparx_params=$sparx_params -v box_list=$box_list \
                -v output_dir=$graph_dir \
                -v helical_twist=$helical_twist \
                -f $chuff_dir/awk_scripts/graph_sxihrsr.awk

set phi_stats_file = $graph_dir'/phi_stats.txt'
if(-e $phi_stats_file) /bin/rm $phi_stats_file

foreach blah($graph_dir/*MT*txt)

    awk -v outlier_mag=$outlier_magnitude \
        -f $chuff_dir/awk_scripts/phi_stdev.awk $blah >> $phi_stats_file

    set phi_stats = (`tail -1 $phi_stats_file | awk '{print $1,$2,$3}'`)

cat <<EOF > plot.gnu
set out '${blah:r}.png'
set y2tics
set term png
set title 'Phi deviations: avg ${phi_stats[1]} stdev ${phi_stats[2]} outliers ${phi_stats[3]} %'
set yrange [-${yrange}:${yrange}]
plot '$blah' using :7 with lines title 'phi dev', '$blah' using :11 with lines title '90-theta','$blah' using :12 with points title 'psi-90', '$blah' using :13 with points title 'psi-270', '$blah' using :4 with lines title 'x'
EOF

    gnuplot plot.gnu
    convert $blah:r.png $blah:r.jpg
    echo $blah
end

##############
# Make histograms of the phi behavior
##############
$chuff_dir/csh_scripts/histograph.csh $phi_histogram_bins 1 $phi_stats_file
/bin/mv $phi_stats_file:r'_histograph.txt' $graph_dir'/phi_stats_phi_diff.txt'
/bin/mv $phi_stats_file:r'_histograph.ps' $graph_dir'/phi_stats_phi_diff.ps'
/bin/mv $phi_stats_file:r'_histograph.jpg' $graph_dir'/phi_stats_phi_diff.jpg'

$chuff_dir/csh_scripts/histograph.csh $phi_histogram_bins 2 $phi_stats_file
/bin/mv $phi_stats_file:r'_histograph.txt' $graph_dir'/phi_stats_phi_stdev.txt'
/bin/mv $phi_stats_file:r'_histograph.ps' $graph_dir'/phi_stats_phi_stdev.ps'
/bin/mv $phi_stats_file:r'_histograph.jpg' $graph_dir'/phi_stats_phi_stdev.jpg'

$chuff_dir/csh_scripts/histograph.csh $phi_histogram_bins 3 $phi_stats_file
/bin/mv $phi_stats_file:r'_histograph.txt' $graph_dir'/phi_stats_phi_outlier.txt'
/bin/mv $phi_stats_file:r'_histograph.ps' $graph_dir'/phi_stats_phi_outlier.ps'
/bin/mv $phi_stats_file:r'_histograph.jpg' $graph_dir'/phi_stats_phi_outlier.jpg'
