#! /bin/csh

# chuff_args reference round ref_debug=0 compile box_num use_ctf import_reference input_round r13pf decorate

# chuff_required_executables spider proc2d

# chuff_required_variables helical_repeat_distance
# chuff_required_variables target_magnification scanner_pixel_size

# chuff_help_info
# [<dir1> [<dir2> ...]] [round=<which round of refinement>] [debug=1] [compile=1] [use_ctf=1]
#####################################################################
# Performs reference alignment on microtubule box segments, using the
#  pre-determined seam orientation from ref_align.spi.spif
# 
# Example:
#  chuff chumps_final_align 0001_MT1_1 find_seam=0 use_ctf=1
# 
# <box_dir1> <box_dir2> etc. are the directory names of specific
#   microtubules within "box_files/"; if none specified, all microtubules 
#   are processed.
# 
#####################################################################
# Spider script:
#   final_align_pf13.spi.spif and other ancillary scripts
# 
# Essential output files: ( in chumps_round<round#>/{<box_dir1>,<box_dir2>...} )
#   final_align_doc.spi  For each box, contains:
#                      -in-plane-rotation, scale factor (from radon_align.spi.spif)
#                      -shift_x, shift_y, psi, theta, phi from alignment
#
#   final_xxxx.spi       Averaged 80A repeat for each MT box segment.
#
#   <box_dir>_finalplot.jpg  Graph of the found euler angles, highlighting the contiguous seam
#                             (having no large interruptions) with red boxes.
#   
#   <box_dir>_stacked_final.tif   Image of stacked final_xxxx.spi results, for diagnostics
#   <box_dir> straightened_pw.tif Power spectrum of the whole, straightened MT cut from the
#                                  micrograph, for diagnostics


################
# Set up chuff environment
################
source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Set up microtubule-specific environment
################
source $chuff_dir/csh_scripts/setup_chumps.csh final_align_pf13.spi.spif align_grid_pf13.spi.spif read_ref_pf13.spi.spif downsample_centered.spi.spif avg_box_dimer.spi.spif rotate2d_90.spi.spif mirror2d.spi.spif stackfinal_pf13.spi.spif calc_straightened_pw_pf13.spi.spif

if($status != 0) then
    exit(2)
endif

##################
# Assign a binning factor:
#  by default, we choose the largest binning factor that keeps the pixel size <= 12A
##################

if(! $?bin_factor) then
    set bin_factor = `echo nothing | \
    	awk -v psize=$micrograph_pixel_size -v target_psize=12 \
    	'{print int(target_psize/psize)}'`
endif

############################################################################
# Initialize variables from arguments
############################################################################

if(! $?box_num) then
    set box_num = 0
endif

if(! $?r13pf) then
    set r13pf = 112
endif

if(! $?decorate) then
     set decorate = 1
endif

if(! $?use_ctf) then
    set use_ctf = 0
else
    if("$use_ctf" != 0) then
        set use_ctf = 1
    endif
endif

if(! $?ctf_dir) then
     set ctf_dir = $orig_dir/ctf_files
endif

set ctf_docfile = $ctf_dir/ctf_docfile_for_frealign.spi
set ctf_docfile_forspider = $ctf_dir/ctf_docfile_for_spider.spi

############################################################################
# Create reference projection images
############################################################################

############################################################################
# Loop through the job directories
############################################################################

foreach job_dir($job_dirs)

    cd $orig_dir
    if(! -d $chumps_dir/$job_dir:t) then
        mkdir $chumps_dir/$job_dir:t
    endif

    if(-e chumps_round1/$job_dir:t/guess_seam_doc.spi) then
        set do_refine = ()
    else
        if($?do_refine) unset do_refine
        echo $job_dir:t': Ignoring'
    endif

##################
# Test if spider script needs to be run
##################
    if(-e $chumps_dir/$job_dir:t/final_align_doc.spi && $ref_debug == 0) then
        set box_file = scans/$job_dir:t'.box'
        set num_boxes = `awk '{line += 1;} END {printf "%04d\n", line}' $box_file`
        set last_refined_no = `tail -1 $chumps_dir/$job_dir:t/final_align_doc.spi | awk '{printf "%04d\n", $1}'`
        if($num_boxes == $last_refined_no) then
            echo $job_dir:t': Microtubule already processed...'
            unset do_refine
        endif
    endif

    if($?do_refine) then

        echo $job_dir:t

        cd $orig_dir
        if(! -d $chumps_dir/$job_dir:t) then
            mkdir $chumps_dir/$job_dir:t
        endif

##################
# Extract micrograph number from the micrograph name:
##################
        set micrograph_name = `echo $job_dir | awk '{sub("_MT[0-9]*_[0-9]*$", ""); print};'`
        set micrograph_name = scans/${micrograph_name}.spi
        set micrograph_number = `echo $micrograph_name:r | \
                                 awk '{match($1,"[0-9]+$"); print substr($1, RSTART, RLENGTH)}'`

        if(! -e $micrograph_name) then
            cd $orig_dir
            echo 'Converting micrograph to SPIDER format...'
echo            proc2d_prog $micrograph_name:r.mrc $micrograph_name spider-single >>&! proc2d.log
            proc2d_prog $micrograph_name:r.mrc $micrograph_name spider-single >>&! proc2d.log
            echo ' ...done.'
            if(! -e $micrograph_name) then
                echo 'Failure of procd program:'
                which proc2d_prog
                exit(2)
            endif
        endif

##################
# Get defocus info for micrograph
##################
	if($use_ctf == 1) then
            set micrograph_index = `echo $micrograph_number | awk '{print $1 + 1};'`
            set defocus_info = `echo 0 | awk -v ctf_file=$ctf_docfile_for_spider -v micrograph_index=$micrograph_index -v micrograph_number=$micrograph_number -f $chuff_dir/awk_scripts/get_spider_defocus.awk`

	    if($status != 0) then
		echo 'ERROR: no defocus information found; does "'$ctf_docfile'" exist?'
		echo '   if so, is there an entry #'$micrograph_index' for micrograph number '$micrograph_number'?'
		echo 'Try re-running "chuff ctf '$job_dir:t'" ...'
		exit(2)
	    endif
	else
	    set defocus_info = (0 0 0)
	endif

##################
# Run the spider script
##################

        if($current_round > 1 && ! $?input_round) then
            @ input_round = $current_round - 1
        else if(! $?input_round) then
            @ input_round = 1
        endif

        set box_header = scans/$job_dir
        @ radon_round = 1

	if(! -e $orig_dir/chumps_round${radon_round}/$job_dir:t/radon_scale_doc.spi) then
	    echo 'ERROR: radon information not found.  Please run "chumps radon" first...'
	    exit(2)
	endif

        set find_seam = 1          # find_seam = 0 case NOT IMPLEMENTED YET!!
#        set use_ctf = 1
        cd $chumps_dir/$job_dir:t
        spif_prog ../spider/final_align_pf13.spi \
                   ../../$micrograph_name \
                   ../../$box_header \
                   ../../chumps_round${input_round}/$job_dir:t \
                   ../$job_dir:t \
                   ../ref \
                   $box_num \
                   $bin_factor \
                   $helical_repeat_distance \
                   $chumps_pixels_per_repeat \
                   $find_seam \
                   $use_ctf \
                   $defocus_info \
                   $amplitude_contrast_ratio \
                   $accelerating_voltage \
                   $spherical_aberration_constant \
                   $invert_density \
                   $ref_debug \
#                   |& awk -v job_dir=$job_dir:t '$1 == "Box" || $1 == "skipping" || $1 == "Filament:" {fflush(); print "  "$0;} tolower($0) ~ "error" || tolower($0) ~ "warn" {print; exit(2)}'

        if($status == 2) then
            exit(2)
        endif

    endif

##################
# Calculated some useful additional diagnostic images
##################
    cd $orig_dir

    echo 'Making diagnostic images...'

    if($?do_refine) then
        set last_mic_name = ()

	if(! -e $chumps_dir/$job_dir:t/$job_dir:t'_straightened_pw.tif') then
	    cd $orig_dir/$chumps_dir/$job_dir:t
	    spif_prog ../spider/calc_straightened_pw_pf13.spi \
                   ../../$micrograph_name \
                   ../../$box_header \
		    final_align_doc \
		    $job_dir:t \
		    $helical_repeat_distance \
		    $micrograph_pixel_size \
		    $filament_outer_radius \
                    |& awk 'tolower($0) ~ "error" || tolower($0) ~ "warn" {print; exit(2)}'
	endif

	if(! -e $chumps_dir/$job_dir:t/$job_dir:t'_stacked_final.tif') then
	    cd $orig_dir/$chumps_dir/$job_dir:t
	    spif_prog ../spider/stackfinal_pf13.spi \
		../../$micrograph_name \
		../../$box_header \
		final_align_doc \
		$job_dir \
                $invert_density \
                $helical_repeat_distance \
                $micrograph_pixel_size \
                $filament_outer_radius
#                    |& awk 'tolower($0) ~ "error" || tolower($0) ~ "warn" {print; exit(2)}'
	endif

###################
# Delete SPIDER micrographs to save space if doing more than one
#  micrograph at a time
###################

        if($#last_mic_name == 0) set last_mic_name = $micrograph_name

        if($last_mic_name != $micrograph_name) then
            cd $orig_dir
            if(-e $micrograph_name:r'.mrc') then
                /bin/rm $micrograph_name
            endif
        endif

        set last_mic_name = $micrograph_name
    endif

    echo '...done.'

    source $chuff_dir/csh_scripts/graph_final_alignment_pf13.csh
    if($status == 2) then
        exit(2)
    endif

end

printf "\n"
# echo 'Now run "chuff chumps_setup_recon", followed by "chuff chumps_reconstruct"...'
# printf "\n"
