#! /bin/csh

# chuff_args cores=16 nodes=1 qsub
# chuff_args input output

# chuff_required_executables spider

# chuff_help_info
#  Example: 
#   chumps_ihrsr_center_tubes_parallel input=stack.spi output=stack_cent.spi nodes=2 cores=4

################
# Set up chuff environment
################
set chuff_get_extra_variables
source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Do stuff
################

if($#script_args != 0) then
    echo 'ERROR: all arguments must be given as input = x, etc.'
    exit(2)
endif

if(! $?qsub) set qsub = 0 

set extension = $input:e

set num_boxes = `spif -dat $extension -r fi_x $input:r'@' | awk '$1 == "(S2)" {print substr($5,1,length($5)-1)}'`

#################################
# Divide particles into groups for parallel processing
#################################

set particle_spec = `echo nothing | \
                     awk -v first=1 -v last=$num_boxes \
                         -v nodes=$nodes -v cores=$cores \
  '{ n_proc = nodes*cores; \
     step = (last-first+1)/n_proc; \
     if(step != int(step)) step += 1; \
     step = int(step); \
     current_first = 1; \
     for(i = 1; i <= n_proc; ++i) { \
       current_last = current_first+step-1; \
       if(i == n_proc) current_last = last; \
       print current_first","current_last; \
	   current_first = current_first + step; \
     } \
   }'`

$chuff_dir/chuff chumps_ihrsr_center_tubes \
    $particle_spec input=$input output=$output $chuff_extra_args \
    nodes=$nodes cores=$cores qsub=$qsub

###########################
# Merge results into one stack
###########################

echo 'Merging results into one stack...'

if(-e $output) /bin/rm $output

if(! -e insert_stack.spi) /bin/cp $chuff_dir/spider_scripts/insert_stack.spi .

printf '' > $output:r'_shift_info.'$extension

foreach spec($particle_spec)
#    set size = `spif -dat $extension -r fi $input:r | awk '$1 == "(R3)" {print $4}'`
    set specs = `echo $spec | awk '{n = split($0,p,","); print p[1], p[2]}'`
    echo $output:r'_'$specs[1]'_'$specs[2]'.'$extension

    spif -dat $extension \
        insert_stack.spi $output:r'_'$specs[1]'_'$specs[2]'.'$extension \
        $output > /dev/null

    cat $output:r'_'$specs[1]'_'$specs[2]'_shift_info.'$extension >> $output:r'_shift_info.'$extension
    /bin/rm $output:r'_'$specs[1]'_'$specs[2]'.'$extension
    /bin/rm $output:r'_'$specs[1]'_'$specs[2]'_shift_info.'$extension
end

echo '...done' 
