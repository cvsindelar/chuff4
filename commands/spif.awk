##########################################
# spif.awk
#   Converts "fancy variables" spider script code into conventional spider code
# 
#   This script needs to be passed the script file name twice, so it can do the 2 scans necessary to get all the 
#     variables and registers correct with no conflicts.
# 
#   If it is passed a "results" file (as the 3rd argument), then it doesn't print out the "compiled" script,
#     but instead prints out a "decompiled" results file, with register names in this file replaced
#     by the variable assignments it made in the script file.  
#   The results file may include calls to other spider procedures; spif.awk is smart enough to replace only
#     registers within the top-level spider procedure...
##########################################

BEGIN {
  var_regexp = "[$][A-Za-z0-9_]+";                     # "regular expression" that defines an initial "$"
                                                       #   followed by any number of alphanumeric characters or 
                                                       #   the underscore "_"

  register_regexp = "[xX][0-9][0-9]";                   # spider register like "x11"
}

##########################################
# Keep track of which file we are scanning
##########################################

FNR == 1 {			# FNR is the line number, for the current file being scanned
  file_num = file_num + 1;

                                # If the target process hasn't been defined by the caller, use the script filename
                                #  (the first file processed by spif.awk), but with the trailing "spif" replaced
                                #  by "spi"
  if(file_num == 1 && target_proc "a" == "a") {
    n = split(FILENAME, script_dirs, "/");
    target_proc = script_dirs[n];
    sub("[.]spif$","",target_proc);            # NB: "$" here is regexp symbol for end-of-line
  }
}

##########################################
# The first pass through the file: record all the register names in direct use by the script...
##########################################

file_num == 1 {
  the_line = $0;
  gsub("[;].*","", the_line);                    # eliminate comments

  if(match(the_line, register_regexp)) {
    gsub(var_regexp,"",the_line);                           # Zap all the "spif" variable names...

    while(match(the_line, register_regexp)) {
      this_register = substr(the_line, RSTART + 1, RLENGTH - 1);
      if(!match(known_registers, "x" this_register)) {
	known_registers = known_registers " x" this_register;
      }
      
      sub(register_regexp,"",the_line);                      # Zap register name after recording...
    }
  }
}

##########################################
# The second pass through the file... do the variable substitution
##########################################

file_num == 2 {
  the_line = $0;
  trimmed_line = the_line;
  gsub("[;].*","", trimmed_line);                    # eliminate comments

  if(match(the_line, "[;].*"))                       # save comment part
    comment_part = substr(the_line, RSTART, RLENGTH);
  else
    comment_part = "";

  if(match(trimmed_line, var_regexp)) {
    while( match(trimmed_line, var_regexp) ) {
      cur_var = substr(trimmed_line, RSTART + 1, RLENGTH - 1);
      cur_reg = get_register(cur_var);
      sub(var_regexp, cur_reg, trimmed_line);
    }
  }
  output_buffer = output_buffer trimmed_line comment_part"\n";
}

##########################################
# The third pass: "decompile" the optional results file
#
#   This is by far the least essential part of the code (even though it is the most complicated), 
#   because it is just for debugging spider runs--
#   nothing here actually affects the way spider will run.
##########################################

#####################
# First, keep track of which subroutine is being executed, within the results file
#####################

file_num == 3 && $2 == "START" && $3 == "OF:" {
  ++nest_index;
  n = split($4, proc_dirs, "/");
  spi_proc[nest_index] = proc_dirs[n];
}

file_num == 3 && $2 == "END" && $3 == "OF:" {
  --nest_index;
}

#####################
# Queries of the form "?blah?" produce, in the results output, a line from the calling procedure-- 
#  the line which is fed into the query.  We handle this by temporarily bumping down the "nest index" for 
#  one line only...
#####################

$0 ~ "^ *[?].*[?]" {               # detect queries of the form " ?...?"
  query_line = 1;
}

query_line != 0 {
  ++query_line;
  if(query_line == 3)  {          # this is true on the line following the "?...?" query
    --nest_index;
  }  else if(query_line == 4) {
    ++nest_index;
    query_line = 0;
  }
}
    
#####################
# Do the substitutions
#####################

file_num == 3 {
  the_line = $0;

#####################
# Ensure substitution happens only within the correct subroutines...
#####################

  if(spi_proc[nest_index] == target_proc) {
    the_line_copy = $0;
    
    if(match(the_line, register_regexp)) {
      while( match(the_line_copy, register_regexp) ) {
	cur_reg = substr(the_line_copy, RSTART + 1, RLENGTH - 1);
	cur_var = var_names[index_from_reg(cur_reg)];
	i = index_from_reg(cur_reg);
	
	if(cur_var != "")
	  gsub("[xX]" cur_reg, "$" cur_var, the_line);
	gsub("[xX]" cur_reg, "", the_line_copy);
      }
    }
  }
  print the_line;
}    

##########################################
# Print out the compiled script, if we are not decompiling another file...
##########################################

END {
  if(bomb)  {
    exit(2);    
  }

  if(file_num == 2) {
    print output_buffer;

################
# Append warning message as a comment, if applicable
################

    for(i = 1; i <= num_regs; ++i) {
      if(!var_used[i] && var_names[i] != "")
	warning_string = warning_string "$"var_names[i]" ";
    }
    if(warning_string != "")
      print "; Warning: the following variables were used only once: " warning_string;
  }
}

function get_register(the_var)
{
################
# If already assigned, substitute the register
################

  the_reg_index = 0;

  if(match(known_vars, "[$]"the_var " ")) {
    for(i = 1; i <= num_regs; ++i) {
      if(var_names[i] == the_var) {
	the_reg_index = i;
	var_used[i] = 1;
	break;
      }
    }
  }  else {
################
# If not assigned, assign a new register value
################

################
# First, check the user's code to see if s/he is accidentally using a variable before
#  having assigned it...
################

    test_line = the_line
    gsub("[;].*","", test_line);                    # eliminate comments

    if(match(test_line, "[=].*[$]"the_var"([^A-Za-z0-9_]|$)")) {
      print "ERROR: Variable used before being assigned: $"the_var;
      print " --> "$0
      bomb = 1;
      exit(2);
    }

    do {                                            # Find the first free register...
      num_regs = num_regs + 1;
      if(reg_from_index(num_regs) > 99) {
	print "ERROR: More than 89 variables used: not enough registers available!";
        print "   Offending variable: $" the_var
	bomb = 1;
	exit(2);
      }
      if(reg_from_index(num_regs) > 99) {
	print "ERROR: More than 89 variables used: not enough registers available!";
        print "   Offending variable: $" the_var
	bomb = 1;
	exit(2);
      }
      register_name = sprintf("x%d", reg_from_index(num_regs));

    } while(match(known_registers, register_name))

    known_vars = known_vars " $" the_var " ";
    var_names[num_regs] = the_var;
    the_reg_index = num_regs;
  }

  if(the_reg_index == 0) {
    print "ERROR: (bad programming in spif): variable cannot be assigned to a register!";
    print "Variable name: " the_var;
    print "Known variables: " known_vars;
    bomb = 1;
    exit(2);
  }

  return("x" sprintf("%2d", reg_from_index(the_reg_index)));
}

function reg_from_index(reg_index) {
  return(reg_index + 10);
}
function index_from_reg(reg_num) {
  return(reg_num - 10);
}
