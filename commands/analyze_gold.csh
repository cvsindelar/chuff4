set max_outliers = 10
set max_phi_stdev = 3.2

set input_round = 0
set refinement_resolutions = (30 30 25 20 15 )

set bin_factor = 1

set qsub = 1
set nodes = 4
set cores = 16
set queue = general

set test = 0

set sparx_n_procs = `echo $nodes $cores | awk '{print $1*$2}'`

####################
# Prepare boxes, measure CTF
####################

# init_boxes scans/????.box overlap=387 nuke=1 box_dim=400

# chuff ctf scans/????.mrc qsub=$qsub cores=$cores nodes=$nodes
# chuff_phase_flip scans/????.mrc bin=4

# chuff chuff_extract_boxes scans/????_MT*_1.box bin = 4 phase_flip = 1 nuke=1 qsub=$qsub cores=$cores nodes=$nodes queue=$queue

if($bin_factor != 1) then
    chuff chuff_binbox scans/????.mrc bin=$bin_factor qsub=$qsub cores=$cores nodes=$nodes queue=$queue
endif

####################
# Divide into 'gold standard' data sets, as equally sized as possible
####################

if(-e boxlist_all.txt) /bin/rm boxlist_all.txt
if(-e boxlist_gold1.txt) /bin/rm boxlist_gold1.txt
if(-e boxlist_gold2.txt) /bin/rm boxlist_gold2.txt

foreach blah( scans/????_MT*box)
   awk 'END {print FILENAME, NR}' $blah >> boxlist_all.txt
end

sort -nr --key=2 boxlist_all.txt > boxlist_all_sort.txt

awk '{if(sum1 > sum2) {sum2 += $2; print $1 >> "boxlist_gold2.txt"} else {sum1 += $2; print $1 >> "boxlist_gold1.txt"; }}'  boxlist_all_sort.txt

##########################################
# Loop through both gold standard cases
##########################################

foreach igold(1 2)

    if($test == 1) then
	set boxlist_gold = (`head -1 boxlist_gold$igold.txt`)
    else
	set boxlist_gold = (`cat boxlist_gold$igold.txt`)
    endif

####################
# Run sxihrsr
####################

    set sparx_dir = sxihrsr_bin4_gold$igold

    chuff_export_to_sxihrsr $boxlist_gold bin=4 output_dir = $sparx_dir nuke=1

    chuff chuff_sxihrsr ref_vol= actin100_050A.spi sparx_dir = $sparx_dir n_procs=$sparx_n_procs nuke=1 qsub=$qsub cores=$cores nodes=$nodes queue=$queue

    chuff_graph_sxihrsr sparx = $sparx_dir/output1/parameters_0003_0003.txt

####################
# Export to frealign
####################

    set sparx_dir = sxihrsr_bin4_gold$igold
    set frealign_dir = frealign_bin$bin_factor'_gold'$igold
    set frealign_header = $frealign_dir/$cwd:t
    set frealign_info = $frealign_dir/frealign_input_file_info.txt

    chuff chuff_extract_boxes $boxlist_gold$igold bin=$bin_factor graph_dir = $sparx_dir/output1_graphs nuke=1 qsub=$qsub cores=$cores nodes=$nodes queue=$queue

    chuff_sxihrsr_select graph_dir = $sparx_dir/output1_graphs max_phi = $max_phi_stdev max_outliers = $max_outliers nuke=1

    chuff_export_sxihrsr_to_frealign `cat $sparx_dir/output1_graphs/good_boxes.txt` sparx = $sparx_dir/output1/parameters_0003_0003.txt bin=$bin_factor output_dir = $frealign_dir

####################
# Loop through frealign rounds
####################

    set round = $input_round
    foreach refine_high_res($refinement_resolutions)
	@ round = $round + 1

	if($round == 2) then
	    set refine_option = 'refine_coordinate_mask=0,0,0,1,1'
	else
	    set refine_option = ''
	endif

	chuff_frealign_parallel \
	    input_file_info = $frealign_info \
	    round=$round \
	    refine_high_res=$refine_high_res $refine_option \
	    recon_cores=$cores nodes=$nodes cores=$cores qsub=$qsub queue=$queue

	if($round == 1) then
	    chuff chuff_soft_mask vol = $frealign_header'_1.spi' mass = 1.2 nodes=1 cores=4 qsub=$qsub queue=$queue
	    set mask_file = $frealign_header'_1_softmask.spi'
	    ln -s $mask_file .
	    set mask_file = $mask_file:t
	endif

	if(-e $mask_file) then
	    spif mu $frealign_header'_'${round}, $mask_file:r,$frealign_header'_'${round}'_masked', '*'

	    /bin/mv $frealign_header'_'${round}{,_nomask}.spi
	    /bin/mv $frealign_header'_'${round}{_masked,}.spi
	else
	    echo doh
	    exit
	endif

####################
# End of loop through frealign rounds
####################
    end

##########################################
# End of loop through both gold standard cases
##########################################
end
