#! /bin/csh
# chuff_args compile smooth_window=3 error_threshhold=2 nuke=0 radon_debug=0 layer_line_order=1
# chuff_args filter_res=50 tubular=1 angle_range=20 angle_step=0.5 bin_factor=1

# chuff_required_variables helical_repeat_distance
# chuff_required_variables target_magnification scanner_pixel_size
# chuff_required_variables spherical_aberration_constant accelerating_voltage amplitude_contrast_ratio

# chuff_required_executables spider

# chuff_help_info
# Use <file1> <file2> etc. to specify directory names of specific
#   microtubules within "box_files/"; if none specified, all microtubules 
#   are processed.
#
# Available options: debug=1 ; compile=T
# 
#####################################################################
# Finds in-plane rotation of microtubule box segments, using a radon-transform
#   algorithm.
# 
# Example:
#  chuff chumps_radon 0001_MT1_1
#####################################################################
# 
# Spider script:
#   radon_align.spi.spif
# 
# Essential output files: ( in chumps_round1/{<box_dir1>,<box_dir2>...} )
#   radon_scale_doc.spi  Contains normalizing scale factor for alignment
#                          as well as estimated magnification for this
#                          microtubule.
# 
#   radon_scale_doc.spi  Contains in-plane rotation angles for each box.

################
# Set up chuff environment
################
if(! "$?chuff_dir") then
    echo 'ERROR: please set the shell variable "$chuff_dir" to the location of your'
    echo '      "chuff3" directory.  The best way to do this is to add the line:'
    echo '           source <...>/chuff3/chuff.cshrc'
    echo '      to your ".cshrc" file in your home directory.'
    exit(2)
endif

source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Set up microtubule-specific environment
################
source "$chuff_dir"/csh_scripts/setup_chumps.csh radon_align_v2.spi.spif downsample_centered.spi.spif calc_straightened_pw_general.spi.spif simple_helix_align.spi.spif
if($status != 0) exit(2)

######################################
# Loop through the job directories
######################################

set last_mic_name = ()

foreach job_dir($job_dirs)

    cd "$orig_dir"

    set box_header = scans/$job_dir

###########################
# Create SPIDER version of micrograph if necessary
###########################

    set micrograph_name = `echo $job_dir | awk '{sub("_MT[0-9]*_[0-9]*$", ""); print};'`
    set micrograph_name = scans/${micrograph_name}.spi
    set micrograph_number = `echo $micrograph_name:r | \
                                 awk '{match($1,"[0-9]+$"); print substr($1, RSTART, RLENGTH)}'`
######################################
# Run the spider script
######################################

    if(! -d $chumps_dir/$job_dir) mkdir $chumps_dir/$job_dir

    cd $chumps_dir/$job_dir

    spif_prog ../spider/simple_helix_align.spi \
                   ../../$micrograph_name:r \
                   ../../$box_header \
                   ./ \
                   $micrograph_pixel_size \
                   $filter_res \
		   $tubular $invert_density $bin_factor \
                   $angle_range $angle_step

#    spif_prog ../spider/calc_straightened_pw_general.spi \
#                   ../../$micrograph_name:r'_flipped' \
#                   ../../$box_header \
#		   simple_align_doc \
#                   ./ \
#                   $micrograph_pixel_size \
#                   $filament_outer_radius \
#                   $filter_res $self_align
end

