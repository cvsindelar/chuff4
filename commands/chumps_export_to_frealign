#! /bin/csh

# chuff_args bin_factor num_pfs=13 num_starts=3 frealign_dir round=1 nuke=0
# chuff_args  max_resolution=7 box_dim=0

# chuff_required_executables octave

# chuff_help_info
# Available options:
#   bin_factor num_pfs [=13] num_starts [=3] frealign_dir round [=1] nuke [=0]                     

################
# Set up chuff environment
################
source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Set up microtubule-specific environment
################
source "$chuff_dir"/csh_scripts/setup_chumps.csh
if($status != 0) exit(2)

##########
# Assume here that resolution of reconstruction will not exceed $max_resolution.
#  Therefore, if pixel size is <= $max_resolution/4, we can afford to bin the volume...
##########
if(! $?bin_factor) then
    set bin_factor = `echo nothing | \
    	awk -v psize=$micrograph_pixel_size -v max_resolution=$max_resolution \
    	'{bin_factor = int( (max_resolution/2)/psize); if(bin_factor == 0) bin_factor = 1; \
          print bin_factor}'`
endif

if(! $?frealign_dir) then
    set frealign_dir = \
      chumps_round${round}/frealign_pf{$num_pfs}_start{$num_starts}_bin${bin_factor}
endif

if($nuke == 1) then
  /bin/rm -r $frealign_dir
endif

if(! -e $frealign_dir) then
    mkdir $frealign_dir
endif

octave_prog    --path $chuff_dir/octave_scripts \
        $chuff_dir/octave_scripts/chumps_export_to_frealign.m \
        $frealign_dir \
        $cwd:t \
        $bin_factor \
        $num_pfs $num_starts $round $box_dim \
        $job_dirs
