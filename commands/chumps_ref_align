#! /bin/csh
# chuff_args round=1
# chuff_args final=0 nuke=0 himem=1
# chuff_args num_pfs=13 num_starts=3 
# chuff_args mt_pf_types ref_dir decorate=1
# chuff_args box_num=0 use_ctf=0 write_images=0 compile 
# chuff_args psi_search=0 invert_density

# chuff_required_variables helical_repeat_distance
# chuff_required_variables invert_density
# chuff_required_variables target_magnification scanner_pixel_size
# chuff_required_variables spherical_aberration_constant accelerating_voltage amplitude_contrast_ratio

# chuff_required_executables spider

# chuff_help_info
# Available options: 
#   final=1            Perform final alignment (after chumps_select_seam)
#   num_pfs=<num>      What type of MT to process (0 for all usual types)
#   num_starts=<num>
#   box_num=<num>      process a specific box number (0 for all)
#
#   compile=1          recompile the requisite spider scripts
#   himem=1            load all references into RAM for faster calculation
#   nuke=1             redo the calculation
#   use_ctf=1          multiply data images by CTF
#   write_images=1     create diagnostic images
# 

################
# Set up chuff environment
################
source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Set up microtubule-specific environment
################
source "$chuff_dir"/csh_scripts/setup_chumps.csh \
       ref_align_general.spi.spif \
       align_individual_repeats.spi.spif \
       ref_align_individual.spi.spif \
       align_grid_general.spi.spif \
       read_ref_general.spi.spif \
       rotate2d_90.spi.spif \
       downsample_centered.spi.spif \
       mirror2d.spi.spif
if($status != 0) exit(2)

################
# Set up a few variables
################

set radon_round = 1

if($num_pfs != 0) then
  set mt_pf_types = ($num_pfs $num_starts)
endif

if(! $?mt_pf_types) then
    set mt_pf_types = (13 3 14 3 12 3)
endif

# echo $mt_pf_types

set global_mt_pf_types = ($mt_pf_types)

################
# Loop through MT's
################

set last_mic_name = ()

foreach job_dir ($job_dirs)

    echo "$job_dir"
    if($final == 2 && -e $chumps_dir/$job_dir/selected_mt_type.txt) then
	set mt_pf_types = (`cat $chumps_dir/$job_dir/selected_mt_type.txt`)
    else
	set mt_pf_types = ($global_mt_pf_types)
    endif

    set box_header = scans/$job_dir

    if(! -e $orig_dir/chumps_round${radon_round}/$job_dir/radon_scale_doc.spi) then
        echo 'ERROR: radon information not found.  Please run "chumps_radon" first...'
        exit(2)
    endif

################
# Loop for all MT types
################

    set i = 1
    while($i <= $#mt_pf_types)

        cd $orig_dir
        set n_pfs = $mt_pf_types[$i]
        @ i = $i + 1
        set n_starts = $mt_pf_types[$i]
        @ i = $i + 1

        echo "$n_pfs protofilaments $n_starts starts"

	if(! $?ref_dir) then
	if($decorate == 1) then
	    set ref_dir = ref_pf${n_pfs}_start${n_starts}
	    else
		set ref_dir = tubref_pf${n_pfs}_start${n_starts}
	    endif
	endif

	if(! -d $chumps_dir/$ref_dir) then
	    echo 'ERROR: reference directory does not exist: ' $chumps_dir/$ref_dir
	    exit(2)
	endif

        set micrograph_name = `echo $job_dir | awk '{sub("_MT[0-9]*_[0-9]*$", ""); print};'`
        set micrograph_name = scans/${micrograph_name}.spi
        set micrograph_number = `echo $micrograph_name:r | \
                                 awk '{match($1,"[0-9]+$"); print substr($1, RSTART, RLENGTH)}'`
##################
# Check if alignment was completed already
##################

        if($final == 1) then
            set align_doc = final_align_doc_pf{$n_pfs}_start{$n_starts}.spi
        else if($final == 2) then
            set align_doc = individual_align_doc_pf{$n_pfs}_start{$n_starts}.spi
        else
            set align_doc = ref_align_doc_pf{$n_pfs}_start{$n_starts}.spi
        endif

        if(-e $chumps_dir/$job_dir/$align_doc && $final != 2) then
            set n_boxes = `/usr/bin/tail -n 1 $box_header'_doc.spi' | awk '{print $1}'`

            set completed = `awk -v n_boxes=$n_boxes '{box_done[$1] = 1} END {for(i = 1; i <= n_boxes; ++i) if(! box_done[i]) {print 0; exit} print 1}' $chumps_dir/$job_dir/$align_doc`

            if($completed == 1 && $nuke == 0 && $write_images == 0) then
                echo '   Already processed.  Skipping...'
                continue
            endif
        endif

        printf '\n'

##################
# Get defocus info for micrograph
##################

	set ctf_docfile_for_spider = ${micrograph_name:r}'_ctf_doc_spider.spi'
        set micrograph_index = `echo $micrograph_number | awk '{print $1 + 1};'`
#        set defocus_info = `echo 0 | awk -v ctf_file=$ctf_docfile_for_spider -v micrograph_index=$micrograph_index -v micrograph_number=$micrograph_number -f $chuff_dir/awk_scripts/get_spider_defocus.awk`
        set defocus_info = `echo 0 | awk -v ctf_file=$ctf_docfile_for_spider -v micrograph_index=1 -v micrograph_number=-1 -f $chuff_dir/awk_scripts/get_spider_defocus.awk`

        if($status != 0) then
            set defocus_info = ()
        endif
        if($#defocus_info < 3) then
            set defocus_info = (0 0 0)
        else if($#defocus_info != 3) then
            set defocus_info = (0 0 0)
        endif

        if($defocus_info[1] == 0) then
            if($use_ctf == 1) then
                echo 'WARNING: no defocus information found; does "'$ctf_docfile_for_spider'" exist?'
                echo '   if so, is there an entry #'$micrograph_index' for micrograph number '$micrograph_number'?'
                echo 'Try re-running "ctf '$job_dir'" ...'
                set use_ctf = 0
            endif
        endif

######################################
# Run the spider script
######################################

        cd $orig_dir
        if(! -d $chumps_dir/$job_dir) then
            mkdir $chumps_dir/$job_dir
        endif

        if(! -e $micrograph_name) then
	    echo 'Please convert micrographs to SPIDER format, i.e. with mrc_to_spi'
            exit(2)
        endif

        if(! -e $chumps_dir/$ref_dir) then
            echo 'ERROR: no reference directory: ' $chumps_dir/$ref_dir
            exit(2)
        endif

        cd $chumps_dir/$job_dir

    if($final != 2) then
###################
# Run job once for each possible polarity
###################
	if($psi_search == 0) then
	    set psi_vals = -1
        else
	    set psi_vals = (270 90)
        endif

        foreach psi($psi_vals)
          spif_prog ../spider/ref_align_general.spi \
           ../../$micrograph_name \
           ../../$box_header \
           ../../chumps_round${radon_round}/$job_dir \
           ../$job_dir \
           ../$ref_dir $himem \
           $n_pfs $n_starts \
           $box_num \
           $use_ctf \
           $micrograph_pixel_size \
           $defocus_info \
           $amplitude_contrast_ratio \
           $accelerating_voltage \
           $spherical_aberration_constant \
           $invert_density \
           $psi \
           $final $write_images $nuke
          if($status == 2) exit(2)
        end
    else
        spif_prog ../spider/ref_align_individual.spi \
           ../../$micrograph_name \
           ../../$box_header \
           ../../chumps_round${radon_round}/$job_dir \
           ../$job_dir \
           ../$ref_dir $himem \
           $n_pfs $n_starts \
           $box_num \
           $helical_repeat_distance \
           $filament_outer_radius \
           1 \
           $use_ctf \
           $micrograph_pixel_size \
           $defocus_info \
           $amplitude_contrast_ratio \
           $accelerating_voltage \
           $spherical_aberration_constant \
           $invert_density \
           $write_images $nuke
	if($status != 0) exit(2)

	touch "individual_complete_${num_pfs}_${num_starts}"
    endif

###################
# Delete SPIDER micrographs to save space if doing more than one
#  micrograph at a time
###################

        if($#last_mic_name == 0) set last_mic_name = $micrograph_name

        if($last_mic_name != $micrograph_name) then
            cd $orig_dir
            if(-e $micrograph_name:r'.mrc') then
#                /bin/rm $micrograph_name
            endif
        endif

        set last_mic_name = $micrograph_name
    end
end
