Synthetic data for testing chuff3 can be found in:
test_data/eg5_0002_bin2_emify.mrc
test_data/eg5_0002_bin2_emify.box
test_data/chuff_parameters.m

##############
INSTRUCTIONS
##############

###########	   		 
# Box the microtubules
###########	   		 
boxer xxx.mrc
      - Use "helix" option; select box size ~8-10 8nm repeats; 
         overlap ~1/3 of box size works well.
      - You can select multiple microtubule segments in a single box file
      - Note that for a bent microtubule, you can specify a continuation 
      -  of a boxed helix by clicking the first box of the next helical segment
      -  near the center of the last box in the previous helical segment.
      - Save boxes as xxx.box

init_boxes  # Note this is very fast now, it just makes text files

###########	   		 
# Compute defocuses:
# NOTE: for most of the following commands, except chumps_export_to_frealign and afterwards, 
#   you can parallize them to run many times faster with the following type of command:
#       -  chuff ctf scans/????.mrc cores=4
#       -  this will run 4 jobs in parallel, in the background
###########	   		 

ctf <xxx.mrc> [...]
      - example: ctf scans/????.mrc

###########	   		 
# Convert micrographs for the following steps:
###########	   		 
mrc_to_spi <xxx.mrc>
      - example: mrc_to_spi scans/????.mrc

###########	   		 
# Generate radon alignment files
###########	   		 
chumps_radon <box file> [...]
      - example: chumps_radon scans/????_MT*.box

###########	   		 
# Generate references:
###########	   		 
chumps_synth_mt_vol num_pfs=13
chumps_prepare_ref_fromvol synthetic = 1 num_pfs=13
      - Note- voxel size is stored in: chumps_round1/ref_pf14_start3/info.txt 

      - Note- chuff does not work for this, but you can parallelize by using
 chumps_prepare_ref_parallel vol = chumps_round1/ref_pf14_start3/mt_eulers_pf14_start3_doc_vol.spi \
     num_pfs=14 voxel_size = <xxxx> cores=4

###########	   		 
# Reference alignment step 1:
###########	   		 
chumps_ref_align <box file> [...] num_pfs=<n>
chumps_select_seam <box file> [...] [num_pfs=<n>]

###########	   		 
# Reference alignment step 2:
###########	   		 
chumps_ref_align <box file> [...] final=1 num_pfs=<n>
chumps_select_seam <box file> [...] [num_pfs=<n>]

##########
# Diagnostic images:
##########
chumps_final_diagnostic <box file> [...] num_pfs=<n>
	
###########	   		 
# Reference alignment step 3:
###########	   		 
chumps_ref_align <box file> [...] final=2 num_pfs=<n>
chumps_select_seam <box file> [...] [num_pfs=<n>]
	
###########
# Prepare for and run frealign:
###########
chumps_export_to_frealign 0002_MT1_1 frealign_dir = chumps_round1/frealign_test num_pfs=14

chumps_frealign frealign_dir = chumps_round1/frealign_test round=1 refine_high_res=20 \
         recon_cores = 4

chumps_frealign frealign_dir = chumps_round1/frealign_test round=2 refine_high_res=16 \
         recon_cores = 4

chumps_frealign frealign_dir = chumps_round1/frealign_test round=3 refine_high_res=14 \
         recon_cores = 4

         - Note that you should be sure the refinement resolution cutoff is not too high

         - You can generate an asymmetric reconstruction by adding the option
           "helical_subunits=1" to the chumps_frealign command

         - rounds 2 and higher are much slower because the refinements won't be parallelized; 
         - you can parallelize these by doing i.e.:

  chumps_frealign_parallel frealign_dir = chumps_round1/frealign_test \
           round=3 refine_high_res=14 recon_cores=4 cores=4
