setenv CC /usr/bin/gcc_disable
setenv CXX /usr/bin/g++

if(! -d ../../octave_mex_OSX) mkdir ../../octave_mex_OSX
mkoctfile --mex get_full_score_matrix_accel.c -o ../../octave_mex_OSX/get_full_score_matrix_accel.mex
