#include "mex.h"
#include <math.h>
#include <stdio.h>

#define MATRIX_ELT(m, i,j,idim,jdim) m[idim * j + i];

void get_states_from_num_c(int *states, int n, int n_states, int len);
int get_num_from_states_c(int *states, int n_states, int len);
int isequal_c(int *vec1, int *vec2, int len);

/**********************************
 * function score_matrix = ...
 *  get_full_score_matrix_accel(complete_mini_score_list, complete_overlap_map_list, ...
 *                              n_classes, n_subunits,max_i, max_j);
 **********************************/

void
mexFunction (int nlhs, mxArray *plhs[], int nrhs, 
             const mxArray *prhs[])
{
  mxArray *score_matrix;
  double *complete_mini_score_list, *complete_overlap_map_list;
  int max_i, max_j, n_classes, n_subunits;
  double *score_matrix_data;
  int *completed_vec, *state_vec, *indices, *mini_state_vec, *mini_completed_vec;
  int i, j, k,indi,indj, score_idim, score_jdim, omap_idim, omap_jdim, sm_dim;
  int pos, mini_len, mini_state_index, mini_completed_index;
  double score;
  
  complete_mini_score_list = mxGetPr(prhs[0]);
  score_idim = mxGetM(prhs[0]);
  score_jdim = mxGetN(prhs[0]);
  complete_overlap_map_list = mxGetPr(prhs[1]);
  omap_idim = mxGetM(prhs[1]);
  omap_jdim = mxGetN(prhs[1]);
  n_classes = mxGetScalar(prhs[2]);
  n_subunits = mxGetScalar(prhs[3]);
  max_i = mxGetScalar(prhs[4]);
  max_j = mxGetScalar(prhs[5]);

  printf("dims: %d %.0f\n", score_jdim, pow(n_classes, omap_jdim));

  if(score_jdim != pow(n_classes, omap_jdim)) {
    printf("ERROR: inconsistency between the overlap map and the compressed score list!!!\n");
    exit(2);
  }

  state_vec = (int *) malloc(2*n_subunits*sizeof(int));
  completed_vec = (int *) malloc(2*n_subunits*sizeof(int));

  indices = (int *) malloc(omap_jdim*sizeof(int));
  mini_state_vec = (int *) malloc(omap_jdim*sizeof(int));
  mini_completed_vec = (int *) malloc(omap_jdim*sizeof(int));

  sm_dim = pow(n_classes, n_subunits);
  score_matrix = \
     mxCreateDoubleMatrix (sm_dim, sm_dim, mxREAL);
  score_matrix_data = mxGetPr (score_matrix);

  for(i = 1; i <= max_i; ++i) {
    for(j = 1; j <= max_j; ++j) {
      get_states_from_num_c(state_vec, i + (j-1)*sm_dim, n_classes, 2*n_subunits);

      for(k = 0; k < 2*n_subunits; ++k)
        completed_vec[k] = 1;

      score = 0;
      pos = 1;

      while(!isequal_c(completed_vec, state_vec, 2*n_subunits) && pos < 2*n_subunits) {
        mini_len = 0;
        for(indj = 0; indj < omap_jdim; ++indj) {
          indices[indj] = complete_overlap_map_list[(pos-1) + indj*omap_idim];
          mini_state_vec[indj] = state_vec[indices[indj]-1];
          mini_completed_vec[indj] = completed_vec[indices[indj]-1];

          if(!indices[indj])
            break;
          mini_len = mini_len + 1;
        }

        mini_state_index = get_num_from_states_c(mini_state_vec,n_classes,mini_len);
        mini_completed_index = get_num_from_states_c(mini_completed_vec,n_classes,mini_len);
        if(mini_state_index != mini_completed_index) {
          score = score +                                               \
            complete_mini_score_list[ \
              pos-1 +               \
              score_idim * (mini_state_index-1)] - \
            complete_mini_score_list[ \
              pos-1 +               \
              score_idim * (mini_completed_index-1)];

          for(k = 0; k < mini_len; ++k)
            completed_vec[indices[k]-1] = state_vec[indices[k]-1];
        }
        pos = pos + 1;
      }
      score_matrix_data[(j-1)*sm_dim + (i-1)] = score;
    }
  }

  plhs[0] = score_matrix;
  free(state_vec);
  free(completed_vec);
  free(indices);
  free(mini_state_vec);
  free(mini_completed_vec);
}

int get_num_from_states_c(int *states, int n_states, int len)
{
  int index, n_states_pow;
  int n;

  n_states_pow = 1;
  n = 1;
  for(index = 1; index <= len; ++index) {
    /*  n = n + n_states.^(index-1) * (states(index)-1);*/

    n = n + n_states_pow * (states[index-1] - 1);
    n_states_pow = n_states_pow*n_states;
  }

  return(n);
}

void get_states_from_num_c(int *states, int n, int n_states, int len)
{
  int index, num, remainder;;

  num = n - 1;
  for(index = 1; index <= len; ++index) {
    remainder = num % n_states;
    num = num / n_states;
    states[index-1] = remainder + 1;
  }
}

int isequal_c(int *vec1, int *vec2, int len)
{
  int i, ret_val;

  for(i = 0; i < len; ++i) {
    if(vec1[i] != vec2[i]) {
      return(0);
    }
  }

  return(1);
}

