/* 
   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/
void isdft(double *result, double *cosspec, double *sinspec, 
	   unsigned long length);

void dctarb(double *signal, double *spectrum, unsigned int N);

double kthdctarb(double *spectrum, unsigned int N, int kth);

double kthdstarb(double *spectrum, unsigned int N, int kth);

void invdctarb(double *signal, double *spectrum, unsigned int N);

void recdct(double *sig, double *speccopy, double *spec1, double *spec2, 
	    long point, unsigned long L, unsigned int N, int mid, int *what);

int threshold(double *spectrum,unsigned int N,double thresh,int method);

void eta_shift(double *eta, unsigned long length, int M);

void make_eta(double *data, unsigned long length, double p, int M);

void make_coef(double *coscoef, double *sincoef, double *spec, 
	       double *eta, unsigned long length);

long ind(long point, unsigned long length);





