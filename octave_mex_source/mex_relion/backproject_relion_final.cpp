#include <mex.h>
#include <src/backprojector.h>
#include <src/ctf.h>
#include "backproject_relion.h"
/**********************************
 * function 
 * [vol] = backproject_relion_final(numerator, denomirator, params)
 *
 * numerator and denomirator are returned from sum of original backproject_relion_accum. 
 * params are binary serialization of backproject project 
 *
 **********************************/


void
mexFunction (int nlhs, mxArray *plhs[], int nrhs, 
             const mxArray *prhs[])
{

  int required_args = 3;
  if (nrhs < required_args) {
      printf("Too less argument. At least provoide: vol, projs, angle_shfts\n");
      return;
  }
  // construct the backproject project

  BackProjector bp(100, 100,"c1");
  char * buf = mxGetChars(prhs[2]);
  int buf_len = sizeof(bp);
  memcpy((char*)&bp, buf, buf_len);
  
  int mysize = bp.ori_size;
  int data_size 
  // get data point to bp
  bp.data.data = new double[
}
