#include <mex.h>
#include <src/backprojector.h>
#include <src/ctf.h>
#include "backproject_relion.h"
/**********************************
 * function 
 * [denumerator, numerator, params] = backproject_relion_accum(projs, angle_shfts, ctf, interpolator = TRILINEAR, padding_factor_3d = 2, r_min_nn = 10, blob_order = 0, blob_radius = 1.9, blob_alpha = 15, fn_sym = 'c1')
 *
 *
 *  % projs are the 2D projections (particles)
 *  % angles are the (phi, theta, psi, sx, sy) angles for each particle
 *  % padding_factor is the padding factor
 *  % interpolator: in projector.h
 *  #define NEAREST_NEIGHBOUR 0
 *  #define TRILINEAR 1     // default
 *  #define CONVOLUTE_BLOB 2
 *
 *  ctf has values for each particles. [defocus1 defocus2 astig_angle voltage cs amplitude_contrast bfactor scale]; can be provided partially from beginning, stores in ptcl / row, at least 6 values to amplitude_contrast
 *
 *  % other parameters have default value as in back_projector constructor.
 *
 **********************************/


void
mexFunction (int nlhs, mxArray *plhs[], int nrhs, 
             const mxArray *prhs[])
{

  int required_args = 3;
  if (nrhs < required_args) {
      printf("Too less argument. At least provoide: vol, projs, angle_shfts\n");
      return;
  }
  // define the backproject insertion variable

  const mxArray * ptcl_array = prhs[0];
  const mxArray * angle_array = prhs[1];
  const mxArray * ctf_array = 0;
  if (nrhs > 3) {
      ctf_array = prhs[1];
  }

  int mysize = -1; // no default
  int ref_dim = 3; // no default
  int padding_factor_3d = 2; 
  int r_min_nn = 10;
  int  blob_order = 0;
  int blob_radius = 1.9;
  int blob_alpha = 15; 
  int interpolator = TRILINEAR;
  float angpix = 1.; // default to 1 A/pix. Used for CTF calculation
  int maxres = -1;
  int r_max = -1;
  int iter = 10; // number of gridding correction iterations
  bool do_ctf = false;
  bool do_map = false; // do fsc calculation
  bool ctf_phase_flipped = false;
  bool only_flip_phases = false;
  bool intact_ctf_first_peak = false;

  if (nrhs > required_args + 1) interpolator = mxGetScalar(prhs[required_args]);
  if (nrhs > required_args + 2) padding_factor_3d = mxGetScalar(prhs[required_args + 1]);
  if (nrhs > required_args + 3) r_min_nn = mxGetScalar(prhs[required_args + 2]);
  if (nrhs > required_args + 4) blob_order= mxGetScalar(prhs[required_args + 3]);
  if (nrhs > required_args + 5) blob_radius = mxGetScalar(prhs[required_args + 4]);
  if (nrhs > required_args + 6) blob_alpha = mxGetScalar(prhs[required_args + 5]);
  char fn_sym[8];
  fn_sym[7] = 0;
  // default to 'c1'
  fn_sym[0] = 'c';
  fn_sym[1] = '1';
  fn_sym[2] = 0;
  if (nrhs > required_args + 7) {
      int rs = mxGetString(prhs[required_args + 6], fn_sym, 7);

      if (rs <=0) {
          // ERROR OF WRONG SYM parameter
      }
  }
  
  int *dims = mxGetDimensions(prhs[1]);
  int ndims = mxGetNumberOfDimensions(prhs[1]);

  mysize = dims[0];
  BackProjector bp(mysize, ref_dim, fn_sym, interpolator, padding_factor_3d, r_min_nn, blob_order, blob_radius, blob_alpha);
  if (maxres < 0.)
      r_max = -1;
  else
      r_max = CEIL(mysize * angpix / maxres);
  bp.initZeros(2 * r_max); 
  printf("TEST\n");
  // do backprojection
  insert_into_backprojector(bp, ptcl_array, angle_array, ctf_array);
  
  printf("nlhs=%d\n", nlhs);
  // get weight and data to result
  if (nlhs > 0) {
      dims[0] = mysize;
      dims[1] = mysize;
      dims[2] = mysize;
      plhs[0] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
      double *d = mxGetPr(plhs[0]);
      for (int i = 0; i < mysize * mysize * mysize; i++) {
            d[i] = bp.weight.data[i]; 
      }
  }
  if (nlhs > 1) {
      dims[0] = mysize;
      dims[1] = mysize;
      dims[2] = mysize;
      plhs[1] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxCOMPLEX);
      double *r = mxGetPr(plhs[1]);
      double *img = mxGetPi(plhs[1]);
      for (int i = 0; i < mysize * mysize * mysize; i++) {
          r[i] = bp.data.data[i].real;
          img[i] = bp.data.data[i].imag;
      }
  }
  if (nlhs > 2) {
      int buf_len = sizeof(bp);
      char * buf = new char[buf_len];
      dims[0] = buf_len;
      memcpy(buf, (char *)&bp, buf_len);
      plhs[2] = mxCreateCharArray(1, dims);
      char *d = (char*) mxGetData(plhs[2]);
      for (int i = 0; i < buf_len; i++) {
          d[i] = buf[i];
      }
      delete [] buf;

  }
}
