#include "mex.h"
#include "my_projector_func.h"

/**********************************
 * function 
 *  project_relion(proj, euler_matrix, volfft)
 **********************************/

void
mexFunction (int nlhs, mxArray *plhs[], int nrhs, 
             const mxArray *prhs[])
{
  double *vol, *pw;
  int vol_dim, pad_factor;

  if(nrhs > 2) {
    vol = mxGetPr(prhs[2]);
    vol_dim = mxGetM(prhs[2]);
  }
  if(nrhs > 3) {
    pad_factor = mxGetScalar(prhs[3]);
  }

  my_projector_func(vol, pw, vol_dim);  
}
