#ifndef BACKPROJECT_RELION_H
#define BACKPROJECT_RELION_H

#include <src/backprojector.h>
#include <mex.h>
/*
 * Insert multiple images into 3D backprojector. 
 */
void insert_into_backprojector(BackProjector & bp, const mxArray *ptcl_array, const mxArray *angle_array, const mxArray *ctf_array);
void reconstruct_from_backprojector(mxArray *vol, BackProjector &bp, bool do_map, bool do_use_fsc, MultidimArray<double> &fsc);
void save_bp_to_file(BackProjector & bp, char * denom_file, char * nomi_real_file, char * nomi_image_file);
#endif

