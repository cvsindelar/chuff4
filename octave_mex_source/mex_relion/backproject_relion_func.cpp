#include "backproject_relion.h"
#include <src/ctf.h>
/*
 * Insert multiple images into 3D backprojector. 
 */
void insert_into_backprojector(BackProjector & bp, const mxArray *ptcl_array, const mxArray *angle_array, const mxArray *ctf_array) {

    /* might be args
     */
    bool ctf_phase_flipped = false;
    bool only_flip_phases = false;
    bool intact_ctf_first_peak = false;
    double angpix = 1.;

    // variables for back projection
    MultidimArray<double> img, Fctf;
    MultidimArray<Complex> F2D;
    FourierTransformer transformer;
    Matrix2D<double> A3D;     // Rotation Matrix
    Matrix1D<double> trans(2);

    int *dims = mxGetDimensions(ptcl_array);
    int mysize = dims[0];
    img.resize(mysize, mysize);
    img.initZeros();
    double * oldimage_data = img.data;
    int nimgs = dims[2];
    double *all_data = mxGetPr(ptcl_array);
    double *all_angles = mxGetPr(angle_array);
    double * all_ctf = 0;
    bool do_ctf = false;
    int n_ctf_par_dims = 0;
    double * ctf_data = 0;
    if (ctf_array != 0 && mxGetNumberOfElements(ctf_array) > 0) {
        all_ctf = mxGetPr(ctf_array);
        do_ctf = true;

        n_ctf_par_dims = mxGetNumberOfDimensions(ctf_array);
        ctf_data = mxGetPr(ctf_array);
    }
    int nxy = mysize * mysize;
    for (int i = 0; i < nimgs; i++) {
        img.data = all_data + nxy * i;
        double rot = all_angles[i];
        double tilt = all_angles[i + nimgs];
        double psi = all_angles[i + 2 * nimgs];
        double sx = all_angles[i + 3 * nimgs];
        double sy = all_angles[i + 4 * nimgs];
        Euler_angles2matrix(rot,tilt,psi,A3D);
        trans.initZeros();
        trans(0) = sx;
        trans(1) = sy;
        /* TODO
         *       if (do_fom_weight) 
         *                 fom = ...;
         *                       */

        CenterFFT(img, true);
        transformer.FourierTransform(img, F2D);
        if (ABS(XX(trans)) > 0. || ABS(YY(trans)) > 0.)
            shiftImageInFourierTransform(F2D, F2D, XSIZE(img), trans );
        // CTF informations
        Fctf.resize(F2D);
        Fctf.initConstant(1.);
        if (do_ctf) {
          CTF ctf;
          ctf.scale = 1.;
          ctf.DeltafU           = ctf_data[i];
          ctf.DeltafV           = ctf_data[i + nimgs];
          ctf.azimuthal_angle   = ctf_data[i + 2 * nimgs];
          ctf.kV                = ctf_data[i + 3 * nimgs];
          ctf.Cs                = ctf_data[i + 4 * nimgs];
          ctf.Q0                = ctf_data[i + 5 * nimgs];
          if (n_ctf_par_dims > 6)
            ctf.Bfac            = ctf_data[i + 6 * nimgs];
          if (n_ctf_par_dims > 7)
            ctf.scale           = ctf_data[i + 7 * nimgs];
          ctf.initialise();
          ctf.getFftwImage(Fctf, mysize, mysize, angpix, ctf_phase_flipped, only_flip_phases, intact_ctf_first_peak,true);
          FOR_ALL_DIRECT_ELEMENTS_IN_MULTIDIMARRAY(F2D) {
              DIRECT_MULTIDIM_ELEM(F2D, n)  *= DIRECT_MULTIDIM_ELEM(Fctf, n);
              DIRECT_MULTIDIM_ELEM(Fctf, n) *= DIRECT_MULTIDIM_ELEM(Fctf, n);
          }
        }
      /* TODO
      if (do_fom_weighting) {
          FOR_ALL_DIRECT_ELEMENTS_IN_MULTIDIMARRAY(F2D) {
              DIRECT_MULTIDIM_ELEM(F2D, n)  *= fom;
              DIRECT_MULTIDIM_ELEM(Fctf, n)  *= fom;

          }
      }
      */
      bp.set2DFourierTransform(F2D, A3D, IS_NOT_INV, &Fctf);

    }
    img.data = oldimage_data;
}

void reconstruct_from_backprojector(mxArray *vol, BackProjector &bp, bool do_map, bool do_use_fsc, MultidimArray<double> &fsc) {

    int nr_threads = 1;
    Image<double> vol_relion;
    MultidimArray<double> dummy;
    int iter = 10;
    bp.reconstruct(vol_relion(), iter, do_map, 1, dummy, dummy, dummy, fsc, 1., do_use_fsc, true, nr_threads, -1);
    
    int *dims = mxGetDimensions(vol);
    if (dims[2] ==  XSIZE(vol_relion()) && dims[1] == YSIZE(vol_relion()) && dims[0] == ZSIZE(vol_relion())) {
        int mysize = dims[0];
        int nxy = mysize * mysize;
        double *vol_data = mxGetPr(vol);
        for (int i = 0; i < mysize; i++) {
            for (int j = 0; j < mysize; j++) {
              for (int k = 0; k < mysize; k++) {
                  vol_data[i * nxy + j * mysize + k] = DIRECT_A3D_ELEM(vol_relion(), i, j, k);
              } 
            }
        }
    }
}

