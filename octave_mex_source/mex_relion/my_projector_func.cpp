#include "src/projector.h"
#include "my_projector_func.h"

extern "C" void my_projector_func(double *vol, int current_size)
{
  static Projector projector;
  MultidimArray<double> vol_in, pw;

  projector.computeFourierTransformMap(vol_in, pw, current_size, 1);
}
