#include <mex.h>
#include <src/projector.h>

/**********************************
 * function 
 *  project_relion(proj, euler_matrix, vol, project_radius, padding_factor)
 *  % proj should be initialized with the same dimension in 2D as vol(real space). we do not check here.
 *  % euler_matrix: Transformation Matrix, 4 x 4
 *  % vol: the 3D volume for projection
 *  % project_radius: The radius of projection
 **********************************/

void
mexFunction (int nlhs, mxArray *plhs[], int nrhs, 
             const mxArray *prhs[])
{
  if (nrhs < 2) {
      printf("Too less argument\n");
      return;
  }
  static Projector projector;
  static MultidimArray<double> img;
  static MultidimArray<Complex> F2D;
  static FourierTransformer transformer;
  if (nrhs > 2) {
      // for the first run, we need to setup the projector
      MultidimArray<double> pw_relion;
      MultidimArray<double> vol_relion;
      double * vol = mxGetPr(prhs[2]);
      int ndims = mxGetNumberOfDimensions(prhs[2]);
      if (ndims != 3) {
          // err the vol is not a volume
      }
      int *dims = mxGetDimensions(prhs[2]);
      if (dims[0] != dims[1] || dims[0] != dims[2]) {
          // we only support cubic volume now
      } 
      int padding_factor = 2;
      if (nrhs > 3) padding_factor = mxGetScalar(prhs[3]);
      int r_min_nn = 10;
      int r_max = dims[0];
      if (nrhs > 4) r_max = mxGetScalar(prhs[4]);
      int interpolator = TRILINEAR;
      projector = Projector(dims[0], interpolator, padding_factor, r_min_nn);
      vol_relion.resize(dims[0], dims[1], dims[2]);
      // we copy the vol to relion to reserve the original volume. Because this is only one time thing, and the volume is not so big,  should be fast
      int nxy = dims[0] * dims[1];
      for (int i = 0; i < dims[0]; i++) {
          for (int j = 0; j < dims[1]; j++) {
              for (int k = 0; k < dims[2]; k++) {
      //            vol_relion(i, j, k) = vol[k * nxy + j * dims[0] + i];
                  DIRECT_A3D_ELEM(vol_relion, i, j, k) = vol[i * nxy + j * dims[0] + k];
              }
          }
      }
      projector.computeFourierTransformMap(vol_relion, pw_relion, 2 * r_max);


      // we init a 2D image instance and re-point to new data each time. THis is easy than what we need to set all the parameters using default constructor
      img.resize(dims[0], dims[1]);
      img.initZeros();
      transformer.setReal(img);
      transformer.getFourierAlias(F2D);
  }
  // we save the original image data pointer and load back to make sure the Image destructor called properly
  double * oldimage_data = img.data;
  // get the pointer to the projection matrix 
  img.data = mxGetPr(prhs[0]);
  double * e = mxGetPr(prhs[1]);
  Matrix2D<double> A3D; // get A3D from second argument, rotation matrix
  A3D.resize(4,4);
  for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++)
          A3D(i, j) = e[j * 4 + i];
  }
  F2D.initZeros(); // this is essential if we do change the radius of projection. If we only do on radius projection, this initialization might be omited.
  projector.get2DFourierTransform(F2D, A3D, IS_NOT_INV);
  transformer.inverseFourierTransform(F2D, img);
  CenterFFT(img, false);  
  img.data = oldimage_data;
}
