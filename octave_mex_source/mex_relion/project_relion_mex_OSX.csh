setenv CC /usr/bin/gcc_disable
setenv CXX /usr/bin/g++

###########################
# NOTE: for my installation of octave on OS X, mkoctfile seems not to be able to 
#  properly compile c plus plus object files: when such object files are linked 
#  in the final mkoctfile step, the standard object libraries (i.e. strings) can't 
#  be found.  The fix seems to be to compile all c plus plus source files using 
#  the g++ compiler, only using mkoctfile for the straight c files and for the
#  final linking step.
#
# Note also that the top-level mex function can be written as a c plus plus file
#  if it is likewise compiled with g++, as confirmed by the mexcpp.cpp, make_mexcpp_OSX.csh
#  files here
###########################

if(! -d OSX) mkdir OSX

foreach blah ( src/*.cpp )
    echo $blah
    if(! -e OSX/${blah:r:t}.o) then
#        mkoctfile $blah -I. -c -o OSX/${blah:r:t}.o
        /usr/bin/g++ $blah -I. -c -o OSX/${blah:r:t}.o -fPIC -arch i386 \
            -I/Users/cvsindelar/macdevel/relion-1.2/include

    endif
end

if(! -e OSX/my_projector_func.o) then
#    mkoctfile my_projector_func.cpp -I. -c -o OSX/my_projector_func.o
    /usr/bin/g++ my_projector_func.cpp \
        -I/Users/cvsindelar/macdevel/relion-1.2/include \
        -I. \
        -c -o OSX/my_projector_func.o -fPIC -arch i386
endif

if(! -e OSX/project_relion.o) then
    mkoctfile project_relion.c -I. -c -o OSX/project_relion.o
endif

if(! -e ../../octave_mex_OSX/project_relion.mex) then
    mkoctfile --mex OSX/*.o \
        -v \
        -L/Users/cvsindelar/macdevel/fftw-3.3.4/lib \
        -o ../../octave_mex_OSX/project_relion \
        -lfftw3_threads -lfftw3
endif
