#include <mex.h>
#include <src/backprojector.h>
#include <src/ctf.h>
#include "backproject_relion.h"
/**********************************
 * function 
 *  [vol] = backproject_relion(projs, angle_shfts, interpolator = TRILINEAR, padding_factor_3d = 2, r_min_nn = 10, blob_order = 0, blob_radius = 1.9, blob_alpha = 15, fn_sym = 'c1', ctf=[])
 *
 *  % vol is the output volume
 *
 *  % projs are the 2D projections (particles)
 *  % angles are the (phi, theta, psi) angles for each particle
 *  % padding_factor is the padding factor
 *  % interpolator: in projector.h
 *  #define NEAREST_NEIGHBOUR 0
 *  #define TRILINEAR 1     // default
 *  #define CONVOLUTE_BLOB 2
 *
 *  ctf has values for each particles. [defocus1 defocus2 astig_angle voltage cs amplitude_contrast bfactor scale]; can be provided partially from beginning, stores in ptcl / row, at least 6 values to amplitude_contrast
 *
 *  % other parameters have default value as in back_projector constructor.
 *
 **********************************/


void
mexFunction (int nlhs, mxArray *plhs[], int nrhs, 
             const mxArray *prhs[])
{

  int required_args = 3;
  if (nrhs < required_args) {
      printf("Too less argument. At least provoide: vol, projs, angle_shfts\n");
      return;
  }
  // define the backproject insertion variable
  MultidimArray<double> img;
  MultidimArray<Complex> F2D;
  FourierTransformer transformer;
  Matrix2D<double> A3D;     // Rotation Matrix
  MultidimArray<double> Fweight, Fctf, dummy;
  Matrix1D<double> trans(2);
  int mysize = -1; // no default
  int ref_dim = 3; // no default
  int padding_factor_3d = 2; 
  int r_min_nn = 10;
  int  blob_order = 0;
  int blob_radius = 1.9;
  int blob_alpha = 15; 
  int interpolator = TRILINEAR;
  float angpix = 1.; // default to 1 A/pix. Used for CTF calculation
  int maxres = -1;
  int r_max = -1;
  int iter = 10; // number of gridding correction iterations
  bool do_ctf = false;
  bool do_map = false; // do fsc calculation
  bool ctf_phase_flipped = false;
  bool only_flip_phases = false;
  bool intact_ctf_first_peak = false;

  if (nrhs > required_args + 1) interpolator = mxGetScalar(prhs[required_args]);
  if (nrhs > required_args + 2) padding_factor_3d = mxGetScalar(prhs[required_args + 1]);
  if (nrhs > required_args + 3) r_min_nn = mxGetScalar(prhs[required_args + 2]);
  if (nrhs > required_args + 4) blob_order= mxGetScalar(prhs[required_args + 3]);
  if (nrhs > required_args + 5) blob_radius = mxGetScalar(prhs[required_args + 4]);
  if (nrhs > required_args + 6) blob_alpha = mxGetScalar(prhs[required_args + 5]);
  char fn_sym[8];
  fn_sym[7] = 0;
  // default to 'c1'
  fn_sym[0] = 'c';
  fn_sym[1] = '1';
  fn_sym[2] = 0;
  if (nrhs > required_args + 7) {
      int rs = mxGetString(prhs[required_args + 6], fn_sym, 7);

      if (rs <=0) {
          // ERROR OF WRONG SYM parameter
      }
  }
  // ctf_par will contain: DF1, DF2, AstigAngle, KV, Cs, AmpCont(Q0), Bfactor
  double *ctf_data = 0;
  int n_ctf_par_dims = 0;
  if (nrhs > required_args + 8) {
      // CTF PARAMETER WILL BE STORED IN SINGLE VECTOR;
      do_ctf = true;
      n_ctf_par_dims = mxGetNumberOfDimensions(prhs[required_args + 8]);
      ctf_data = mxGetPr(prhs[required_args + 8]);
  }
  // get pointers to data
  double * vol_data = mxGetPr(prhs[0]);
  
  int *dims = mxGetDimensions(prhs[1]);
  int ndims = mxGetNumberOfDimensions(prhs[1]);
  // should be 3 dimensional matrix
  double *all_data = mxGetPr(prhs[1]);
  // should be 2 dimensional array, with each row is the phi, theta, psi
  double *all_angles = mxGetPr(prhs[2]); 

  mysize = dims[0];
  BackProjector bp(mysize, ref_dim, fn_sym, interpolator, padding_factor_3d, r_min_nn, blob_order, blob_radius, blob_alpha);
  if (maxres < 0.)
      r_max = -1;
  else
      r_max = CEIL(mysize * angpix / maxres);
  bp.initZeros(2 * r_max); 
  img.resize(mysize, mysize);
  img.initZeros();
  // keep img original pointer to restore at the end
  double * oldimage_data = img.data;
  int nimgs = dims[2];
  // main loop, add images

  // nxy is the size of one image (with 'double' size, hopefully 8 bytes);
  int nxy = mysize * mysize;
  for (int i = 0; i < nimgs; i++) {
      // all_data is the double type, so pointer moves nxy * i * size(double) amount;
      img.data = all_data + nxy * i;
      // need rot, tilt. psi, sx, sy
      // need phi, theta, psi, sx, sy

      // octave matrix is column major. so to get a row, we need to get 
      int offset = i;
      double rot = all_angles[i];
      double tilt = all_angles[i + nimgs];
      double psi = all_angles[i + 2 * nimgs];
      double sx = all_angles[i + 3 * nimgs];
      double sy = all_angles[i + 4 * nimgs];
      // get rotation matrix
      Euler_angles2matrix(rot,tilt,psi,A3D);
      trans.initZeros();
      trans(0) = sx;
      trans(1) = sy;
      /* TODO
      if (do_fom_weight) 
          fom = ...;
      */
      CenterFFT(img, true);
      transformer.FourierTransform(img, F2D);
      if (ABS(XX(trans)) > 0. || ABS(YY(trans)) > 0.)
          shiftImageInFourierTransform(F2D, F2D, XSIZE(img), trans );
      // CTF informations
      Fctf.resize(F2D);
      Fctf.initConstant(1.);
      if (do_ctf) {
          CTF ctf;
          ctf.scale = 1.;
          ctf.DeltafU           = ctf_data[i];
          ctf.DeltafV           = ctf_data[i + nimgs];
          ctf.azimuthal_angle   = ctf_data[i + 2 * nimgs];
          ctf.kV                = ctf_data[i + 3 * nimgs];
          ctf.Cs                = ctf_data[i + 4 * nimgs];
          ctf.Q0                = ctf_data[i + 5 * nimgs];
          if (n_ctf_par_dims > 6) 
            ctf.Bfac            = ctf_data[i + 6 * nimgs];
          if (n_ctf_par_dims > 7)
            ctf.scale           = ctf_data[i + 7 * nimgs];
          ctf.initialise();
          ctf.getFftwImage(Fctf, mysize, mysize, angpix, ctf_phase_flipped, only_flip_phases, intact_ctf_first_peak,true);
          FOR_ALL_DIRECT_ELEMENTS_IN_MULTIDIMARRAY(F2D) {
              DIRECT_MULTIDIM_ELEM(F2D, n)  *= DIRECT_MULTIDIM_ELEM(Fctf, n);
              DIRECT_MULTIDIM_ELEM(Fctf, n) *= DIRECT_MULTIDIM_ELEM(Fctf, n);
          }
      }
      /* TODO
      if (do_fom_weighting) {
          FOR_ALL_DIRECT_ELEMENTS_IN_MULTIDIMARRAY(F2D) {
              DIRECT_MULTIDIM_ELEM(F2D, n)  *= fom;
              DIRECT_MULTIDIM_ELEM(Fctf, n)  *= fom;

          }
      }
      */
      bp.set2DFourierTransform(F2D, A3D, IS_NOT_INV, &Fctf);
  }
  img.data = oldimage_data;
  int nr_threads = 1;
  // fsc section
  MultidimArray<double> fsc;
  fsc.resize(mysize / 2 + 1);
  bool do_use_fsc = false;
  Image<double> vol;
  //vol.data = vol_data;
  iter = 1;
  bp.reconstruct(vol(), iter, do_map, 1., dummy, dummy, dummy, fsc, 1., do_use_fsc, true, nr_threads, -1);
  // copy vol data to output data
  if (nlhs > 0) {
      cout << "COPING DATA OUT" << endl;
      int dims[3] = {mysize, mysize, mysize};
      prhs[0] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
      double * vol_data = mxGetPr(plhs[0]);
      for (int i = 0; i < mysize; i++) {
        for (int j = 0; j < mysize; j++) {
          for (int k = 0; k < mysize; k++) {
              vol_data[i * nxy + j * mysize + k] = DIRECT_A3D_ELEM(vol(), i, j, k);
          }
        }
      }
      cout << "DON CPY DAT"<<endl;
  }
}
