#include "my_projector_func.h"

/**********************************
 * function 
 *  project_relion(proj, euler_matrix, volfft)
 **********************************/


int main (int argc, char **argv)
{
  double *vol, *pw;
  int vol_dim, pad_factor;

  if(argc > 2) {
    vol = (double *) argv[1];
    vol_dim = argc;
  }
  if(argc > 3) {
    pad_factor = argc;
  }

  my_projector_func(vol, pw, vol_dim);  
}
