if(! -d ../../octave_mex_linux) mkdir octave_mex_linux
if(! -d linux) mkdir linux

foreach blah ( src/*.cpp )
    echo $blah
    if(! -e linux/${blah:r:t}.o) then
        mkoctfile $blah -I. -c -o linux/${blah:r:t}.o
    endif
end

if(! -e linux/my_projector_func.o) then
    mkoctfile my_projector_func.cpp -I. -c -o linux/my_projector_func.o
endif

if(! -e linux/project_relion.o) then
    mkoctfile project_relion.c -I. -c -o linux/project_relion.o
endif

if(! -e ../../octave_mex_linux/project_relion.mex) then
    mkoctfile --mex linux/*.o \
              -v \
              -L/home/cvs2/programs/relion-1.2/lib \
              -o ../../octave_mex_linux/project_relion
endif
