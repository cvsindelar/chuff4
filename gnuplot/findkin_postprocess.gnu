set boxwidth 0.1

set yrange [0:1]
set term post color enhanced

plot_file=system 'echo $plot_file'

set title 'Even sites'
set out output_file_even
plot plot_file using ($1+0.11):2 with boxes fs solid lt 1 title 'low DF even', \
     plot_file using ($1+0.33):3 with boxes fs solid lt 2 title 'hi DF even', \
     plot_file using ($1+0.55):4 with boxes fs solid lt 3 title 'composite even'

# pause -1

set title 'Odd sites'
set out output_file_odd
plot plot_file using ($1+0.11):2 with boxes fs solid lt 1 title 'low DF even', \
     plot_file using ($1+0.33):3 with boxes fs solid lt 2 title 'hi DF even', \
     plot_file using ($1+0.55):4 with boxes fs solid lt 3 title 'composite even'

# pause -1
