#! /home/cvs2/programs/EMAN2/Python-2.7-ucs4/bin/python

import os
from global_def import *
from optparse import OptionParser
import sys

from EMAN2  import *

def main():
	arglist = []
        for arg in sys.argv:
		arglist.append( arg )
	        progname = os.path.basename(arglist[0])
	        usage = progname + " raw_images apix <CTFparfile>"
	        parser = OptionParser(usage,version="1.0")
	        (options, args) = parser.parse_args(arglist[1:])
        if len(args) < 2:
                print "usage: " + usage
                print "Please run '" + progname + " -h' for detailed options"
	else:
		a = EMData()
		fname = args[0]
		apix = float(args[1])
		ctfpar = None
		helical = False

		imn = EMUtil.get_image_count(fname) 
		print "Updating SPARX header info (%i particles)"%imn
		for i in xrange(imn):
                        a.read_image(fname,i,True)
			a.set_attr_dict({'active':1})
			if ctfpar is not None:
				a.set_attr("ctf",ctfinfo[i])
			if helical is True:
				a.set_attr("h_angle",hinfo[i])
			t2 = Transform({"type":"spider","phi":0,"theta":0,"psi":0})
		        a.set_attr("xform.projection", t2)
			a.set_attr("apix_x",apix )
			a.write_image(fname,i,EMUtil.ImageType.IMAGE_HDF, True)
			print "%3i%% complete\t\r"%(int(float(i)/imn*100)),
		print "100% complete\t"

if __name__ == "__main__":
        main()

