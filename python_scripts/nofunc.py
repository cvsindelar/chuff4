# Apply precalculated mask
from EMAN2_cppwrap import *
from global_def import *
def apply_mask( ref_data ):
        from utilities      import print_msg
        from filter         import fit_tanh, filt_tanl
        from morphology     import threshold
        from utilities import sym_vol
        #  Input: list ref_data
        #   0 - raw volume
        #  Output: filtered, and masked reference image
#        stat = Util.infomask(ref_data[0], None, True)
#        volf = ref_data[0] - stat[0]

	a = EMData()
	print "reading mask..."
	a.read_image("vol0008_20A_cosmask2.spi")

        volf = ref_data[0] * a
        return  volf 
