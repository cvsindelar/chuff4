 
C
C-IMPDAT----------------------------------------------------------------
C
C     Please specify the data type of the input file to be converted.
C     Data type currently supported are:
C
C     BROOKHAVEN STEM:
C                A format used to store STEM images. One data file
C                actually contains two images: the large angle elastic
C                scatter normalised signal, and the small angle elastic
C                scatter signal (or bright field, if selected).
C
C     CCP4:
C                A format in use in X_ray crystallography to which the
C                MRC format is largely but not completely compatible.
C
C     DATA_ONLY /
C       RAW_IMAGE:
C                The input image file does not contain any header.
C                The user has to specify image size and image format.
C                (corresponds to option OFFSET with offset = 0).
C
C     DIGITAL
C       MICROGRAPH:
C                GATAN's Digital Micrograph image format.
C
C     EM:
C                Image data format used by the EM image processing
C                system at the Max Planck Institute of Biochemistry
C                (Martinsried, Germany) and by the TVIPS company
C                (Tietz Image and Video Processing Systems, Gauting,
C                Germany).
C
C     FABOSA:
C                Special image format used in the FABOSA project
C                Requires two files: a TIFF image and a text file
C                with additional information (SDI).
C
C     FORMATTED:
C                Import ASCII formatted input files. Formatted ASCII
C                files are rather inefficient because of storage
C                needs and long conversion times required. YET, any
C                computer and mailing system can handle ASCII so the
C                format is universal. The user is requested to give
C                the format of the formatted input image in F77
C                style like: "20(I4)" etc.
C
C     GIF:
C                G(raphics) I(nterface) F(ormat) (tm).  GIF87a
C                A standard defining a mechanism for the storage and
C                transmission of raster-based graphics information
C                (CompuServe Inc., Columbus, Ohio USA, 1987).
C
C     IMAGIC:
C                Advanced image processing in 2-D and 3-D. Developed
C                by Marin van Heel (now: Imperial College, London, UK)
C                and commercially distributed by: Image Science,
C                Berlin, Germany.
C
C     KONTRON:
C                Image format of the KONTRON ELEKTRONIK GmbH (Eching
C                near Muenchen, Germany). This format is used in the
C                IMCO/S system, for example.
C
C     MDPP:
C                Ross Smith's Micrograh Data Processing Program
C                (N.Y.U. School of Medicine, NYU-MC).
C
C     MRC:
C                This is one of the oldest image formats in use in
C                electron microscopy. It is a relatively rich file
C                format which allows for various data formats such as
C                8 bits integers, (raw byte data), 16 bit integers,
C                32 bit REALs and complex formats. One of the
C                philosophies behind this data format is that it is
C                compatible to the CCP4 format in use in X-ray
C                crystallography (not completely true).
C
C     OFFSET:
C                Do not read any input header values, i.e. skip a
C                specified number of bytes and read image densities
C                only. Of course, the user has to know the image size
C                and format.
C
C     PIC:
C                VAX/VMS image format used at the "Computational
C                Bioscience and Engineering Laboratory", NIH, Bethesda
C                (Alasdair Steven, Benes Trus)
C
C     PIF:
C                Portable image format for EM data
C                Purdue University (Tim Baker).
C
C     PGM:
C                Portable greymap format.
C
C     RAW_IMAGE /
C       DATA_ONLY:
C                The input image file does not contain any header.
C                The user has to specify image size and image format.
C                (corresponds to option OFFSET with offset = 0).
C
C     RAWIV / VOLUMETRIC:
C                The rawiv data format is used to represent 3D
C                volumetric data of scalar fields defined on a
C                regular grid.
C
C     SEMPER:
C                Semper 6 Read/Write file format
C                SEMPER is image processing system of SYNOPTICS
C                originally designed by O. Saxton (Cambridge, UK).
C
C     SHF:
C                GATAN's simple header file format.
C
C     SITUS:
C                The map format of Willy Wriggers docking program Situs
C
C     SPIDER:
C                The format(s) used by the SPIDER image processing
C                system of J. Frank (Albany NY, USA).
C
C     SUPRIM:
C                Image format of the image processing system developed
C                by J.P. Schroeter and P. Bretaudiere.
C                The SUPRIM image format is also used in the PHOELIX
C                image processing system written by
C                B. Carragher, M. Whittaker and R. A. Milligan.
C
C     TIFF:
C                Tagged Image Format: this has become one of the
C                standard formats in desk-top plublishing oriented image
C                processing. Desk-top flat-bed scanners, for example,
C                can all generate this image format.
C
C     TVIPS:
C                Image formats used by the TVIPS (Tietz Video and Image
C                Processing Systems) company, Germany. (The two formats
C                used are EM and TIFF with a specific tag.)
C
C     VOLUMETRIC / RAWIW:
C                The rawiv data format is used to represent 3D
C                volumetric data of scalar fields defined on a
C                regular grid.
C
 
C
C-VMRC------------------------------------------------------------------
C
C       MRC     : The new "image2000" MRC (MRC2000) format
C                 which was adapted to the CCP4 image format
C
C       OLD_MRC : Old MRC format with differences to CCP4
C
 
C
C-ITVIPS----------------------------------------------------------------
C
C       The two formats used by TVIPS are:
C
C       EM:   Image data format (new version) originally created
C             by the EM image processing group at the Max Planck
C             Institute of Biochemistry in Martinsried, Germany.
C
C       TIFF: Tagged Image Format: this has become one of the
C             standard formats in desk-top plublishing oriented
C             image processing. Desk-top flat-bed scanners, for
C             example, can all generate this image format.
C
 
C
C-EXPDAT--------------------------------------------------------------
C
C     Please specify the data type of the output file to be converted.
C     Data type currently supported are:
C
C     BROOKHAVEN STEM:
C                A format used to store STEM images. One data file
C                actually contains two images:  the large angle elastic
C                scatter normalised signal, and the small angle elastic
C                scatter signal (or bright field, if selected).
C
C     DATA_ONLY /
C       RAW_IMAGE:
C                The output image file will only contain the image data
C                without any header.
C
C     DIGITAL
C      MICROGRAPH:
C                GATAN's Digital Micrograph image format.
C
C     CCP4:
C                A format in use in X_ray crystallography to which the
C                MRC format is largely but not completely compatible.
C
C     EM:
C                Image data format used by the EM image processing
C                system at the Max Planck Institute of Biochemistry
C                (Martinsried, Germany) and by the TVIPS company
C                (Tietz Image and Video Processing Systems, Gauting,
C                Germany)
C
C     FORMATTED:
C                Export to ASCII formatted output file(s).
C                Formatted ASCII files are rather inefficient because
C                of storage needs and long conversion times required.
C                YET, any computer and mailing system can handle ASCII
C                files, so the format is universal. The user is
C                requested to give the format of the formatted input
C                image in F77 style like: "20(I4)" etc.
C
C     IMAGIC:
C                Advanced image processing in 2-D and 3-D. Developed
C                by Marin van Heel (now: Imperial College, London, UK)
C                and commercially distributed by: Image Science,
C                Berlin, Germany
C
C     KONTRON:
C                Image format of the KONTRON ELEKTRONIK GmbH (Eching
C                near Muenchen, Germany). This format is used in the
C                IMCO/S system, for example.
C
C     MDPP:
C                Ross Smith's Micrograh Data Processing Program
C                (N.Y.U. School of Medicine, NYU-MC)
C
C     MRC:
C                This is one of the oldest image formats in use in
C                electron microscopy. It is a relatively rich file
C                format which allows for various data formats such as
C                8 bits integers, (raw byte data), 16 bit integers,
C                32 bit REALs and complex formats. One of the
C                philosophies behind this data format is that it be
C                compatible to the CCP4 format in use in X-ray
 
C
C-ITVIPS----------------------------------------------------------------
C
C       The two formats used by TVIPS are:
C
C       EM:   Image data format (new version) originally created
C             by the EM image processing group at the Max Planck
C             Institute of Biochemistry in Martinsried, Germany.
C
C       TIFF: Tagged Image Format: this has become one of the standard
C             formats in desk-top plublishing oriented image processing.
C             Desk-top flat-bed scanners, for example, can all generate
C             this image format.
C
 
C
C-REVERSE---------------------------------------------------------------
C
C       The various image formats unfortunately have different
C       coordinate systems. Simply converting the image data one by
C       one can lead to mirrored 2D images and/or to 3D volumes with
C       another handedness.
C
C       The options available are:
C
C       YES :  Consider the different co-ordinate systems in the
C              input and output format.
C
C       NO  :  Do not change the order of the image data according
C              to the different co-ordinate systems. Please be
C              careful and check for mirrored images and/or different
C              handedness.
C
 
C
C-MNUMBER---------------------------------------------------------------
C
C       You can specify an identification number of the micrograph
C       (the input image itself or the micrograph the image was cut
C       or copied).
C
C       This header value is useful to keep track of the origin of
C       images. The value is also needed if the image should be
C       exported to / imported by FREEALIGN, for example.
C
C       NOTE: Only an INTEGER value is allowed here
C
 
C
C-debug-------------------The following is wrong if the SPIDER images --
C                         have different sizes (don't know if this
C                         is allowed - IMAGIC-5 could not store it
C                         anyway!)
C

