set psnr = -1
set mrc_index_offset = 0

if(! -e chumps_round1/ref_tub_subunit_bin1/tub_centered_vol.spi) then
    chumps_prepare_findkin bin=1 dimer=0 pdb=$chuff_dir/data/actin_fit.pdb
endif

if(! -d synth_mt) mkdir synth_mt

set psnr_label = `echo $psnr | awk '{printf("%d_%02d", int($1), ($1*100+0.5) % 100)}'`
set psnr_sign = `echo $psnr | awk '{if($1 > 0) print 1; else print -1;}'`

octave make_synth_data.m

set synth_eulers = `cat synth_eulers.txt`
set synth_defoci = `cat synth_defoci.txt`

@ i = 0
@ name_index = 1
while($i < $#synth_eulers)
    #while($i < 1)

  set defocus = $synth_defoci[$name_index]

  @ i = $i + 1
  set psi = $synth_eulers[$i]
  @ i = $i + 1
  set theta = $synth_eulers[$i]
  @ i = $i + 1
  set phi = $synth_eulers[$i]

  chuff_synth_filament_image synth_mt/synth_data$name_index.spi \
             box_dim = 1000,1000 num_starts=1 \
             decorate = 1 \
             psi_v = $psi phi_v = $phi theta_v = $theta

  set job_dir = `echo $name_index | awk -v offset=$mrc_index_offset '{printf("%04d_MT1_1",$1+offset)}'`

  if($psnr_sign >= 0) then
    chumps_emify_image synth_mt/synth_data$name_index.spi  \
                     synth_mt/synth_data${name_index}_psnr${psnr_label}.spi image_psnr = ${psnr} \
                     defocus = $defocus

    if(! -e synth_mt/synth_data${name_index}_psnr${psnr_label}.mrc) bimg synth_mt/synth_data{$name_index}_psnr${psnr_label}.{spi,mrc}

    add_micrograph_links synth_mt/synth_data${name_index}_psnr${psnr_label}.mrc
    touch scans/$job_dir.box

    chumps_make_synthetic_alignment emify_header = synth_mt/synth_data${name_index}_psnr${psnr_label}  \
                                  synth_header = synth_mt/synth_data${name_index} \
                                  job_dir = $job_dir
  else
    if(! -e synth_mt/synth_data${name_index}.mrc) bimg synth_mt/synth_data{$name_index}.{spi,mrc}

    add_micrograph_links synth_mt/synth_data${name_index}.mrc
    touch scans/$job_dir.box

    chuff_make_synthetic_alignment synth_header = synth_mt/synth_data${name_index} \
                                      job_dir = $job_dir
  endif
  mkdir graphs
  octave --eval "make_syn_graph('$job_dir', 'synth_mt/synth_data${name_index}');"
  cd graphs
  ln -s $job_dir.txt ${job_dir}_5.txt
  cd ..

  @ name_index = $name_index + 1
end
