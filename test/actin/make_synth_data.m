n_images = 16;

rand_eulers = [360*rand([1 n_images]);(5*randn([1 n_images])+90); 360*rand([1 n_images])];
rand_defoci = [8000 + 7000*rand([1 n_images]);];

if(~exist('synth_eulers.txt'))
  dlmwrite('synth_eulers.txt',rand_eulers', ' ');
end

if(~exist('synth_defoci.txt'))
  dlmwrite('synth_defoci.txt',rand_defoci', ' ');
end

